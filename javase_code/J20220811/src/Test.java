import java.util.Arrays;
import java.util.Locale;

class Student{
    public String name;
    public int age;
    public Student(String name,int age){
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
public class Test {

    public static void main(String[] args) {
        String str = "abcabcdabcdef";
        System.out.println(str.endsWith("cdefd"));
        System.out.println(str.startsWith("abcaa"));
    }

    public static void main12(String[] args) {
        String str = " hello hello              ";
        boolean flg = str.contains("hello");
        System.out.println(flg);
        String ret = str.trim();
        System.out.println(ret);

    }

    public static void main11(String[] args) {
        String str = "helloWorld";
        String ret = str.substring(1,4);
        System.out.println(ret);
    }

    public static void main10(String[] args) {
        String str = "zhangsan&wangwu&zhaoliu&lisi";
        String[] ret = str.split("&");
        for (String x:ret) {
            String[] ss = x.split("=");
            for (String s:ss) {
                System.out.println(s);
            }
        }

    }

    public static void main9(String[] args) {
        String str = "zhangsan&wangwu&zhaoliu&lisi";
        String[] ret = str.split("&");
        System.out.println(Arrays.toString(ret));
        System.out.println("========================");
        String str2  = "127.0.0.1";
        String[] ret1 = str2.split("\\.");
        System.out.println(Arrays.toString(ret1));
        System.out.println("===========================");
        String  str3 = "127\\0\\0\\1";
        String[] ret3 = str3.split("\\\\");
        for (String x:ret3) {
            System.out.println(x);
        }
        System.out.println("=======================");
        String str4 = "zhangsan&wangwu&zhaoliu&lisi";
        String[] ret5 = str4.split("=|&");
        for (String x:ret5) {
            System.out.println(x);
        }

    }
    public static void main8(String[] args) {
        String str1 = "ababccabcdabcdef";
       /* String ret = str1.replace('a','p');
        System.out.println(ret);

        String ret2 = str1.replace("ab","pk");
        System.out.println(ret2);

        String ret3 = str1.replaceAll("ab","sfb");
        System.out.println(ret3);*/

        String ret3 = str1.replaceFirst("ab","sfb");
        System.out.println(ret3);

    }

    public static void main7(String[] args) {
        String str = "HellO";
        String ret = str.toUpperCase();
        System.out.println(ret);

        String str2 = "ABCDEF";
        String ret2 = str2.toLowerCase();
        System.out.println(ret2);

        System.out.println("=========================");
        char[] chars = str2.toCharArray();
        System.out.println(Arrays.toString(chars));
        String ret3 = String.format("%d,%d,%d",2022,8,11);
        System.out.println(ret3);

    }

    public static void main6(String[] args) {
        String str = String.valueOf(123);
        System.out.println(str);
        String str2 = String.valueOf(true);
        System.out.println(str2);
        String s4 = String.valueOf(new Student("韩梅梅",18));
        System.out.println(s4);

        System.out.println("===================================");
        int val1 = Integer.parseInt("123");
        System.out.println(val1);
        double val2 = Double.parseDouble("12.25");
        System.out.println(val2);
    }

    public static void main5(String[] args) {
        String str = "abcdef";
        /*for (int i = 0; i < str.length(); i++) {
            System.out.print(str.charAt(i) + " ");
        }*/
        System.out.println(str.indexOf('c'));//从头开始找遇到第一个就结束
        System.out.println(str.indexOf('c',3));//从第三个开始找，遇到第一个就结束
        System.out.println(str.indexOf("abc"));
        System.out.println("==============================");
        System.out.println(str.lastIndexOf('c'));//从后往前找
        System.out.println(str.lastIndexOf('c',3));
        System.out.println(str.lastIndexOf("abc"));
        System.out.println(str.lastIndexOf("abc",3));


    }

    public static void main4(String[] args) {
        String s1 = new String("hello");
        String s2 = new String("hello");
        int ret = s1.compareTo(s2);
        System.out.println(ret);
        ret = s1.compareToIgnoreCase(s2);
        if(ret > 0){
            System.out.println("s1>s2");
        }else if(ret < 0){
            System.out.println("s1 < s2");
        }else{
            System.out.println("==");
        }

    }

    public static void main3(String[] args) {
        String s1 = new String("hello");
        String s2 = new String("hello");
        System.out.println(s1==s2);
        System.out.println(s1.equals(s2));
        System.out.println(s1.equalsIgnoreCase(s2));
    }

    public static void main2(String[] args) {
        String s1 = new String("hello");
        String s2 = new String("world");
        String s3 = s1;
        System.out.println(s3);
        System.out.println(s3.length());
    }

    public static void main1(String[] args) {
        String str1 = "hello";
        System.out.println(str1);

        String str2 = new String("abc");
        System.out.println(str2);

        char[] chars = {'w','a','n','g'};
        String str3 = new String(chars);
        System.out.println(str3);
    }
}
