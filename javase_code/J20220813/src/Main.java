class A{
    public  boolean isNoLetter(char ch){
        if((ch >= 'a' && ch <= 'z')||(ch >= 'A' && ch <= 'Z')){
            return false;
        }
        return true;

    }
    public boolean isPalindrome(String s) {
        s = s.toLowerCase();
        int left = 0;
        int right = s.length() - 1;
        while(left < right){
            while(left < right && isNoLetter(s.charAt(left)) ){
                left++;
            }
            while(left < right && isNoLetter(s.charAt(right)) ){
                right--;
            }
            if(s.charAt(right) != s.charAt(left)){
                return false;
            }
            left++;
            right--;
        }
        return true;
    }

}
public class Main {
    public String tolowerCase(String s){
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if(ch >= 'A' && ch <= 'Z'){
                sb.append((char)(ch+32));
            }else{
                sb.append(ch);
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {

    }
    public static void main1(String[] args) {
        String str = "0P";
        A a = new A();
        boolean flg = a.isPalindrome(str);
        System.out.println(flg);


    }
    /*class Solution {
        public int countSegments(String s) {
            int count = 0;
            int i = 0;
            for(i = 0;i < s.length();i++){
                if(s.charAt(i) != ' '){
                    count++;
                    break;
                }
            }
            for(int j = i;j < s.length();j++){
                if(j + 1 == s.length()){
                    break;
                }
                if((s.charAt(j)==' '&&s.charAt(j+1)!=' ')){
                    count++;
                }
            }
            return count;
        }
    }*/
}
