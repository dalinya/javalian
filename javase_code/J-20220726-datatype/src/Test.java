import java.sql.SQLOutput;

public class Test {
    public static void main9(String[] args) {
        int a = 10;
        String str = a+"";
        System.out.println(str);
        String ret = String.valueOf(10);
        System.out.println(ret);
        String str1 = "1234";
        int b = Integer.parseInt(str1);
        System.out.println(b);
        /*b  = Integer.valueOf(str1);
        System.out.println(b+1);*/
    }

    public static void main8(String[] args){
        String s = "hello ";
        System.out.println(s);
        int a = 10;
        int b = 20;
        System.out.println(a+b);
        System.out.println("a = "+a+" b = "+b);
        System.out.println("a = "+b+a);
        System.out.println(a+b+"hello");
        String s1 = " world";
        System.out.println(s+s1);
    }

    public static void main7(String[] args){
        boolean b = false;
        System.out.println(b);
    }

    public static void main(String[] args){
        char ch = '哈';
        System.out.println(ch);
       /* System.out.println(Character.MAX_VALUE);
        System.out.println(Character.MIN_VALUE);*/

    }

    public static void main5(String[] args){
        double d  = 3.0/2.0;
        System.out.println(d);

        float f = 12.5f;
        System.out.println(f);

        System.out.println(Double.MAX_VALUE);
        System.out.println(Double.MIN_VALUE);
    }

    public static void main4(String[] args){
        byte b = 10;
        System.out.println(b);
        System.out.println(Byte.MAX_VALUE);
        System.out.println(Byte.MIN_VALUE);
    }

    public static void main3(String[] args){
        short sh = 10;
        System.out.println(sh);
        System.out.println(Short.MAX_VALUE);
        System.out.println(Short.MIN_VALUE);
    }


    public static void main2(String[] args){
        short sh = 0;
        System.out.println(Short.MAX_VALUE);
        System.out.println(Short.MIN_VALUE);

        byte b = 0;
        System.out.println(Byte.MAX_VALUE);
        System.out.println(Byte.MIN_VALUE);

        long a = 0L;//L代表该数字是长整型，写不写都可以但是建议写上
        System.out.println(Long.MAX_VALUE);
        System.out.println(Long.MIN_VALUE);
    }

    public static void main1(String[] args) {
        int a = 10;
        System.out.println(a);
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);
        //int c = 2147483648;
//        System.out.println(c);
    }

    //psvm
    //main
   /* public static void main(String[] args) {
        //sout
        System.out.println();
    }*/
//注释快捷键:Ctrl+shift+/
//行注释Ctrl+/
}
