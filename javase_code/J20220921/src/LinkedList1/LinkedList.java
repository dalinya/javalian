package LinkedList1;

import SingleLinkedList1.IndexWrongFulException;

public class LinkedList {
    static class ListNode{
        int val;
        ListNode next;
        ListNode prev;

        public ListNode(int val) {
            this.val = val;
        }
    }
    ListNode head;
    ListNode tail;


    //查找是否包含关键字key是否在单链表当中
    public boolean contains(int key){
        ListNode cur = this.head;
        while (cur != null){
            if (cur.val == key){
                return true;
            }
            cur = cur.next;
        }
        return false;
    }
    //得到单链表的长度
    public int size(){
        ListNode cur = this.head;
        int count = 0;
        while (cur != null){
            cur = cur.next;
            count++;
        }
        return count;
    }
    public void display(){
        ListNode cur = this.head;
        while (cur!= null){
            System.out.print(cur.val + " ");
            cur = cur.next;
        }
        System.out.println();
    }
    //头插法
    public void addFirst(int data){
        ListNode node = new ListNode(data);
        if (this.head == null){
            this.head = node;
            this.tail = node;
            return;
        }
        node.next = this.head;
        this.head.prev = node;
        this.head = node;
    }

    //尾插法
    public void addLast(int data){
        ListNode node = new ListNode(data);
        if (this.head == null){
            this.head = node;
            this.tail = node;
            return;
        }
        node.prev = this.tail;
        this.tail.next = node;
        this.tail = node;
    }

    //任意位置插入,第一个数据节点为0号下标
    public void addIndex(int index,int data){
        if (index<0||index > this.size()){
            throw new  IndexWrongFulException("插入位置错误");
        }
        if (index == 0){
            this.addFirst(data);
            return;
        }
        if (index == this.size()){
            this.addLast(data);
            return;
        }
        ListNode cur = this.head;
        ListNode node = new ListNode(data);
        for (int i = 0; i < index; i++) {
            cur = cur.next;
        }
        node.next = cur;
        node.prev = cur.prev;
        cur.prev.next = node;
        cur.prev = node;

    }
    //删除第一次出现关键字为key的节点
    public void remove(int key){
        ListNode cur = this.head;
        while (cur != null){
            if (cur.val == key){
                if(cur == this.head){//头结点的情况
                    this.head = head.next;
                    if(this.head != null){
                        this.head.prev = null;
                    }else {//只有一个结点的情况
                        this.tail = null;
                    }
                }else {//非头结点的情况
                    cur.prev.next = cur.next;
                    if (cur.next != this.tail){
                        cur.next.prev = cur.prev;
                    }else {//尾结点的情况
                        this.tail = null;
                    }
                }
                return;
            }
            cur = cur.next;
        }


    }
    //删除所有值为key的节点
    public void removeAllKey(int key){
        ListNode cur = this.head;
        while (cur != null){
            if (cur.val == key){
                if(cur == this.head){//头结点的情况
                    this.head = head.next;
                    if(this.head != null){
                        this.head.prev = null;
                    }else {//只有一个结点的情况
                        this.tail = null;
                    }
                }else {//非头结点的情况
                    cur.prev.next = cur.next;
                    if (cur.next != null){
                        cur.next.prev = cur.prev;
                    }else {//尾结点的情况
                        this.tail = cur.prev;
                    }
                }
            }
            cur = cur.next;
        }

    }

    public void clear(){
        ListNode cur = this.head;
        while(cur != null){
            ListNode curNext = cur.next;
            cur.prev = null;
            cur.next = null;
            cur = curNext;
        }
        this.head = null;
        this.tail = null;
    }

   /*








*/
}
