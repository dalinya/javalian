package demo;

import java.util.Arrays;
import java.util.Comparator;

class Student implements Comparable<Student>{
    public String name;
    public int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Student o) {
        if(this.age - o.age > 0){
            return 1;
        }else if(this.age - o.age < 0){
            return -1;
        }else {
            return 0;
        }
    }
}

class NameComparator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        return o1.name.compareTo(o2.name);
    }
}

public class Test {
    public static void bubbleSort(Comparable[] array){
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j].compareTo(array[j + 1]) > 0){
                    Comparable tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
    }
    public static void main(String[] args) {
        Student[] students = new Student[3];
        students[0] = new Student("zhang san",18);
        students[1] = new Student("lisi",21);
        students[2] = new Student("wang wu",17);

        bubbleSort(students);
        System.out.println(Arrays.toString(students));
    }

    public static void main5(String[] args) {
        Student student1 = new Student("zhang san",18);
        Student student2 = new Student("lisi",21);
        NameComparator nameComparator = new NameComparator();
        if (nameComparator.compare(student1,student2) > 0){
            System.out.println("student1 > student2");
        }else {
            System.out.println("student1 < student2");

        }

    }

    public static void main4(String[] args) {
        Student[] students = new Student[3];
        students[0] = new Student("zhang san",18);
        students[1] = new Student("lisi",21);
        students[2] = new Student("wang wu",17);
        NameComparator nameComparator = new NameComparator();
        Arrays.sort(students,nameComparator);
        System.out.println(Arrays.toString(students));
    }

    public static void main3(String[] args) {
        Student student1 = new Student("zhang san",18);
        Student student2 = new Student("lisi",21);
        if(student1.compareTo(student2) > 0){
            System.out.println("student1 > student2");
        }else {
            System.out.println("student1 < student2");

        }
    }

    public static void main2(String[] args) {
        Student[] students = new Student[3];
        students[0] = new Student("zhang san",18);
        students[1] = new Student("lisi",21);
        students[2] = new Student("wang wu",17);

        Arrays.sort(students);
        System.out.println(Arrays.toString(students));
    }

    public static void main1(String[] args) {
        String[] strings = {"zhang san","lisi","wang wu"};

        Arrays.sort(strings);
        System.out.println(Arrays.toString(strings));
    }

}
