package chap06io;

import java.io.*;

public class CharStreamCopy1 {
    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        FileReader fr = new FileReader("src\\chap06io\\DirTest.java");
        FileWriter fw = new FileWriter("src\\chap05\\DirTest.java");
        BufferedReader br = new BufferedReader(fr);
        BufferedWriter bw = new BufferedWriter(fw);
        String str = null;
        //按字符数组枚举
        while ((str = br.readLine() )!= null){
            System.out.println(str);
            bw.write(str);
            bw.newLine();//写入一个换行符
        }

        fr.close();
        fw.close();
        long end = System.currentTimeMillis();
        System.out.println(end - start + "毫秒");
    }
}
