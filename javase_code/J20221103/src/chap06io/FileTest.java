package chap06io;

import java.io.File;
import java.io.IOException;

public class FileTest {
    public static void main(String[] args) throws IOException {
        File f = new File("src\\chap06io\\FileTest.java");
        System.out.println("f.exists() = " + f.exists());
        System.out.println("f.exists() = " + f.isDirectory());
        System.out.println("f.exists() = " + f.length());
        System.out.println("f.exists() = " + f.getName());
        System.out.println("f.exists() = " + f.getParent());
        System.out.println("f.exists() = " + f.getPath());
        File f1 = new File("d:\\hb\\hd\\hdc");
        f1.mkdirs();//创建多个文件夹
        File f2 = new File("d:\\hb\\hd\\hebeu");
        f2.mkdir();
        File f3 = new File("d:\\a.dat");
        File f4 = new File("d:\\hb\\hd\\b.dat");
        f3.createNewFile();
        f3.renameTo(f4);
    }

}
