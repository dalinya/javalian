package chap06io;

import java.io.File;

public class Tree {

    public static void main(String[] args) {
        File f=new File("D:\\D");
        int level=1;//文件夹的深度
        System.out.printf("%-"+(level*4)+"s├-- %s\n","",f.getName());
        Tree t=new Tree();
        t.listSubDir(f,level);
    }
    void listSubDir(File dir,int level){
        level++;
        File[] fa=dir.listFiles();//获取当前目录下所有的"一级文件对象"到一个文件对象数组中去返回
        //
        for(int i=0;i<fa.length;i++){
            for(int j = 1;j < level;j++){
                System.out.printf("%5s","│");//打印五个空格 + ‘|’
            }
            if(i==fa.length-1){//最后一个文件
                System.out.printf("%-4s└-- %s\n","",fa[i].getName());
            }else{//不是最后也各文件
                System.out.printf("%-4s├--%s\n","",fa[i].getName());
            }

            if(fa[i].isDirectory()&&fa[i].listFiles().length!=0){//是文件夹 && 文件夹中的内容不为空
                listSubDir(fa[i],level);
            }
        }
    }
}