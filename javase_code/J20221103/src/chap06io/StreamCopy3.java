package chap06io;

import java.io.*;

public class StreamCopy3 {
    public static void main(String[] args) throws IOException {
        //System.out.println("");
        FileInputStream fis = new FileInputStream("C:\\Users\\王艳斌的红米笔记本\\期中题目.mp4");
        //读文件的内容写到另外一个文件中
        //文件不存在时，自动生成，存在覆盖
        FileOutputStream fos = new FileOutputStream("D:\\Test\\期中题目.mp4");
        BufferedInputStream bis = new BufferedInputStream(fis);
        BufferedOutputStream bos = new BufferedOutputStream(fos);

         long len1 = System.currentTimeMillis();
        int len = 0;
        byte[] buff = new byte[1024];
        //每次读一个字节
        while ((len = bis.read())!=-1){//当读出内容不为-1时，表示文件没有结尾
            //System.out.print((char)len);
            bos.write(len);
        }

        bos.close();
        bis.close();
        fos.close();
        fis.close();
        long len2 = System.currentTimeMillis();
        System.out.println("按缓冲字节流数组逐个复制时间" + (len2 - len1) + "毫秒");
    }
}