package chap06io;

import java.io.File;

import java.util.Scanner;

public class Tree2 {

	public static int depth = 0;
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		String path = cin.nextLine();
		File dir = new File(path);
		dfs(dir);
	}

	public static void dfs(File parentDir) {
		depth ++;
		if(parentDir.isDirectory()) {
			print(parentDir);
			File[] fileArr = parentDir.listFiles();
			for(File f : fileArr) {
				dfs(f);
			}
		}
		depth--;
	}

	public static void print(File f){
		for(int i=1; i<depth; i++) {
			System.out.print("   ");
		}
		System.out.println("|__" + f.getName());
	}
}
