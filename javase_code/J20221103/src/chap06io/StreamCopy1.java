package chap06io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class StreamCopy1 {
    public static void main(String[] args) throws IOException {
        //System.out.println("");
        FileInputStream fis = new FileInputStream("C:\\Users\\王艳斌的红米笔记本\\期中题目.mp4");
        //读文件的内容写到另外一个文件中
        //文件不存在时，自动生成，存在覆盖
        FileOutputStream fos = new FileOutputStream("D:\\Test\\期中题目.mp4");
         long len1 = System.currentTimeMillis();
        int len = 0;
        //每次读一个字节
        while ((len = fis.read())!=-1){//当读出内容不为-1时，表示文件没有结尾
            //System.out.print((char)len);
            fos.write(len);
        }

        fos.close();
        fis.close();
        long len2 = System.currentTimeMillis();
        System.out.println("按字节逐个复制时间" + (len2 - len1) + "毫秒");
    }
}
