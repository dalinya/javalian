package chap06io;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DirTest {
    public static void main(String[] args) {
        File f = new File("src\\chap06io");//哈哈
        File[] fa = f.listFiles((dir,name)->name.endsWith(".java"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = null;//拉拉
        String isDir = null;
        for (File ff: fa) {
            date = sdf.format(new Date(ff.lastModified()));
            isDir = ff.isDirectory()?"<Dir>":"";
            System.out.printf("%-22s%-8s%-10s%-20s\n",date,isDir,ff.length(),ff.getName());
        }
    }
}
