import java.util.Stack;

class MyQueue {
    Stack<Integer> stack1;
    Stack<Integer> stack2;


    public MyQueue() {
        stack1 = new Stack<>();
        stack2 = new Stack<>();
    }
    
    public void push(int x) {
        stack1.add(x);


    }
    
   public int pop() {
        if (this.empty()){
            return -1;
        }
        if (stack2.empty()){
            while (!stack1.empty()){
                stack2.add(stack1.pop());
            }
        }
        return stack2.pop();
    }
    
    public int peek() {
        if (this.empty()){
            return -1;
        }
        if (stack2.empty()){
            while (!stack1.empty()){
                stack2.add(stack1.pop());
            }
        }
        return stack2.pop();
    }
    
    public boolean empty() {
        return (this.stack1.empty() && this.stack2.empty());
    }
}