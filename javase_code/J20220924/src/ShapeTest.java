import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

abstract  class Shape{
    public final double PI = 3.1415926;
    abstract double getArea();
    abstract double getPerimeter();
}
class Circle extends Shape{

    private double radius;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    double getArea() {
        return super.PI * this.radius * this.radius;
    }

    @Override
    double getPerimeter() {
        return 2 * super.PI * this.PI;
    }

    @Override
    public String toString() {
        return "Circle[" +
                this.radius + "]";
    }
}


class Triangle extends Shape{
    private double a;
    private double b;
    private double c;

    public double[] getTriangle(){
        return new double[]{this.a,this.b,this.c};
    }

    public void setTriangle(double a,double b,double c){
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    double getArea() {
        double p = (this.a + this.b + this.c)/2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    @Override
    double getPerimeter() {
        return (this.a + this.b + this.c);
    }

    @Override
    public String toString() {
        return "Triangle[" +
                this.a + "," +
                this.b + "," +
                this.c + "]";
    }
}

class Rectangle extends Shape{
    private double length;
    private double width;
    public double[] getRectangle(){
        return new double[]{this.length,this.width};
    }

    public void setRectangle(double length,double width){
        this.length = length;
        this.width = width;
    }


    @Override
    double getArea() {
        return (this.length * this.width);
    }

    @Override
    double getPerimeter() {
        return 2 * (this.width + this.length);
    }

    @Override
    public String toString() {
        return "Triangle[" +
                this.length + "," +
                this.width + "]";
    }
}

public class ShapeTest {


    // 获取叶子节点的个数
    public static void addOne(Shape shape){
        if (shape instanceof Circle){
            Circle circle = (Circle)shape;
            double d = circle.getRadius();
            circle.setRadius(d + 1);
        }else if(shape instanceof Rectangle){
            Rectangle rectangle = (Rectangle) shape;
            double[] array = rectangle.getRectangle();
            rectangle.setRectangle(array[0] + 1,array[1] + 1);
        }else {
            Triangle triangle = (Triangle) shape;
            double[] array = triangle.getTriangle();
            triangle.setTriangle(array[0] + 1,array[1] + 1,array[2] + 1);
        }
    }
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
        Date date = sdf.parse("2022-09-25");
        System.out.println(sdf.format(date));
        Circle circle = new Circle();
        circle.setRadius(10);
        System.out.println(circle);
        addOne(circle);
        System.out.println(circle);
        Rectangle rectangle = new Rectangle();
        rectangle.setRectangle(4,5);
        System.out.println(rectangle);
        addOne(rectangle);
        System.out.println(rectangle);
        Triangle triangle = new Triangle();
        triangle.setTriangle(11,12,13);
        System.out.println(triangle);
    }
}
