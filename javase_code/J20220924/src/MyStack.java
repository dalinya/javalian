import java.util.LinkedList;
import java.util.Queue;

class MyStack {
    Queue<Integer> qu1;
    Queue<Integer> qu2;
//<<creat>>
    public MyStack() {
        qu1 = new LinkedList<>();
        qu2 = new LinkedList<>();
    }
    
    public void push(int x) {
        if (!qu1.isEmpty()){
            qu1.offer(x);
        }else if (!qu2.isEmpty()){
            qu2.offer(x);
        }else {
            qu2.offer(x);
        }
    }
    
    public int pop() {
        if (this.empty()){
            return -1;
        }
        if( !this.qu1.isEmpty()){
            int num = qu1.size();
            for (int i = 0; i < num - 1; i++) {
                qu2.offer(qu1.poll());
            }
            return qu1.poll();
        }else{
            int num = qu2.size();
            for (int i = 0; i < num - 1; i++) {
                qu1.offer(qu2.poll());
            }
            return qu2.poll();
        }
    }
    
    public int top() {
        if (this.empty()){
            return -1;
        }
        if( !this.qu1.isEmpty()){
            int num = qu1.size();
            int tmp = 0;
            for (int i = 0; i < num; i++) {
                tmp = qu1.poll();
                qu2.offer(tmp);
            }
            return tmp;
        }else{
            int num = qu2.size();
            int tmp = 0;
            for (int i = 0; i < num; i++) {
                tmp = qu2.poll();
                qu1.offer(tmp);
            }
            return tmp;
        }
    }
    
    public boolean empty() {
        return (qu1.isEmpty() && qu2.isEmpty());
    }
}