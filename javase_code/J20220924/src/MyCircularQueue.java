class MyCircularQueue {
    private int[] elem;
    public int front;
    public int rear;

    public MyCircularQueue(int k) {
        elem = new int[k];

    }
    
    public boolean enQueue(int value) {
        if(isFull()){
            return false;
        }

        this.elem[rear] = value;
        rear = (rear + 1) % elem.length;

        return true;
    }
    
    public boolean deQueue() {
        if (isEmpty()){
            return false;
        }

        front = (front + 1) % elem.length;

        return true;
    }
    
    public int Front() {
        if (isEmpty()){
            return -1;
        }
        return elem[front - 1];
    }
    
    public int Rear() {
        if (isEmpty()){
            return -1;
        }
        return rear == 0 ? elem[elem.length - 1] : elem[rear - 1];
    }
    
    public boolean isEmpty() {
        return front == rear;
    }
    
    public boolean isFull() {
        return ((rear + 1)%elem.length ==  front);
    }
}