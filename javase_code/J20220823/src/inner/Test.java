package inner;
//static 类加载的时候
//final 程序编辑时
class OuterClass{
    private int data1 = 1;
    int data2 = 2;
    public static int data3 = 3;
    class InnerClass{
        public int data1 = 111;
        public int data4 = 4;
        int data5 = 5;
        public static final int data6  = 6;
        public void fuc(){
            System.out.println("InnerClass::fuc()");
            OuterClass outerClass = new OuterClass();
            System.out.println(outerClass.data1);
            System.out.println(this.data1);
            System.out.println(data2);
            System.out.println(data3);
            System.out.println(data4);
            System.out.println(data5);
            System.out.println(data6);
        }
    }

    public void test(){
        // static final int c =0;
        System.out.println("OuterClass::test()");
        InnerClass innerClass = new InnerClass();
        innerClass.fuc();
    }

}

public class Test {
    public static void main(String[] args) {
        OuterClass outerClass = new OuterClass();
        OuterClass.InnerClass innerClass = outerClass.new InnerClass();
        innerClass.fuc();

    }

    public static void main1(String[] args) {
        OuterClass outerClass = new OuterClass();
        //System.out.println(outerClass.data2);
        OuterClass.InnerClass innerClass = outerClass.new InnerClass();
        OuterClass.InnerClass innerClass1 = new OuterClass().new InnerClass();
    }


}
