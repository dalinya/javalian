import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int[] arr = new int[num];
        for (int i = 0; i < num; i++) {
            arr[i] = scanner.nextInt();
        }
        Arrays.sort(arr);
        System.out.println(arr[arr.length/2]);

    }

    public static void main31(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int[] arr = new int[num];
        for (int i = 0; i < num; i++) {
            arr[i] = scanner.nextInt();
        }
        int key = arr[0];
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] == key){
                count++;
            }else {
                count--;
            }
            if (count == 0){
                key = arr[i + 1];
            }
        }
        System.out.println(key);

    }

    public static void main22(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] arr = new int[10];
        for (int i = 0; i < 9; i++) {
            arr[i] = scanner.nextInt();
        }
        int num = scanner.nextInt();
        int length = arr.length - 2;
        boolean flg = true;
        while(length >= 0){
            if(arr[length] > num){
                arr[length + 1] = arr[length];
            }else {
                flg = false;
                arr[length + 1] = num;
                break;
            }
            length--;
        }
        if (flg){
            arr[0] = num;
        }
        for (int x: arr) {
            System.out.print(x + " ");
        }
    }

    public static void main21(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] arr = new int[10];
        for (int i = 0; i < 10; i++) {
            arr[i] = scanner.nextInt();
        }

        Arrays.sort(arr);
        for (int x: arr) {
            System.out.print(x + " ");
        }
    }

    public static void main12(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int[] arr = new int[num];
        for (int i = 0; i < num; i++) {
            arr[i] = scanner.nextInt();
        }
        int[] arr2 = new int[num];
        Arrays.fill(arr2,0);
        int flg = 0;
        for (int i = 1; i < num; i++) {
            if(Math.abs(arr[i] - arr[i-1]) < num){
                arr2[Math.abs(arr[i] - arr[i-1])] = Math.abs(arr[i] - arr[i-1]);
            }else {
                flg = 1;
                System.out.println("no");
            }
        }
        for (int i = 1; i < num; i++) {
            if (flg == 1){
                break;
            }
            if(arr2[i] == 0) {
                System.out.println("no");
                flg = 1;
            }
        }
        if (flg == 0){
            System.out.println("happy");
        }
    }

    public static void main11(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int[] arr = new int[num];
        for (int i = 0; i < num; i++) {
            arr[i] = scanner.nextInt();
        }
        int val = 0;
        for (int i = 1; i < num; i++) {
            val ^= Math.abs(arr[i] - arr[i-1]);
            val ^= i;
        }
        if (val == 0){
            System.out.println("happy");
        }else {
            System.out.println("no");
        }
    }
}
