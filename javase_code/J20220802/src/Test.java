import java.util.Arrays;

public class Test {



    public static void main2(String[] args) {
        int[] array = {1,5,3,4,8,6};
        System.out.println("调整前：");
        System.out.println(Arrays.toString(array));
        int right = array.length-1;
        int left = 0;

        while(left < right){
            while(array[left]%2 != 0 && left < right){
               left++;
            }
            while(array[right]%2 == 0 && left < right){
                right--;
            }
            int tmp = array[left];
            array[left] = array[right];
            array[right] = tmp;
        }
        System.out.println("调整后：");
        System.out.println(Arrays.toString(array));
    }

    public static void main1(String[] args) {
        int[] array = {1,2,2,1,6,5,5};
        int ret = search(array);
        System.out.println(ret);
    }
    public static int search(int[] array){
        int num = 0;
        for (int x:
             array) {
            num ^= x;
        }
        return num;
    }


}
