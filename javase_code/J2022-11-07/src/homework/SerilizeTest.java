package homework;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

class Student implements Comparable<Student>, Serializable {
     int Sno; //学号
    String Sname;//姓名
    transient String passworld;//密码
    Date birthSDate;//生日
    String sex;//性别
    double salary;//薪资

    public Student(int sno, String sname, String passworld, Date birthSDate, String sex, double salary) {
        Sno = sno;
        Sname = sname;
        this.passworld = passworld;
        this.birthSDate = birthSDate;
        this.sex = sex;
        this.salary = salary;
    }

    public Student(String sname, Date birthSDate, String sex) {
        Sname = sname;
        this.birthSDate = birthSDate;
        this.sex = sex;
    }

    @Override
    public int compareTo(Student o) {
        if (this.sex.compareTo(o.sex) == 0){
            return  this.birthSDate.compareTo(o.birthSDate);
        }else {
            return this.sex.compareTo(o.sex);
        }
       /* return this.sex.compareTo(o.sex) == 0
                ? this.birthSDate.compareTo(o.birthSDate) : this.sex.compareTo(o.sex);*/
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        return "Student{" +
                "Sno=" + Sno +
                ", Sname=" + Sname +
                ", passworld=" + passworld +
                ", birthSDate=" + sdf.format(birthSDate) +
                ", sex='" + sex +
                ", salary=" + salary +
                '}';
    }
}
public class SerilizeTest {
    public static void main(String[] args) throws ParseException, IOException, ClassNotFoundException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        /*Map<String,Student> map = new TreeMap<>();
        map.put("stu1",new Student(1,"张三","12345",sdf.parse("2021/02/04"),"男",1000));
        map.put("stu2",new Student(2,"李四","14567",sdf.parse("2021/05/07"),"女",2000));
        map.put("stu3",new Student(3,"王五 ","13456",sdf.parse("2021/01/03"),"男",1500));
        map.put("stu4",new Student(4,"赵六","12375",sdf.parse("2021/01/02"),"男",1400));
        map.put("stu5",new Student(5,"梗慧博","17653",sdf.parse("2021/06/09"),"女",1700));
        */
        Map<Student,String> map = new TreeMap<>();
        map.put(new Student(1,"张三","12345",sdf.parse("2021/02/04"),"男",1000),"stu1");
        map.put(new Student(2,"李四","14567",sdf.parse("2021/05/07"),"女",2000),"stu2");
        map.put(new Student(3,"王五 ","13456",sdf.parse("2021/01/03"),"男",1500),"stu3");
        map.put(new Student(4,"赵六","12375",sdf.parse("2021/01/02"),"男",1400),"stu4");
        map.put(new Student(5,"梗慧博","17653",sdf.parse("2021/06/09"),"女",1700),"stu5");
        //序列化
        FileOutputStream fos = new FileOutputStream("Student.dat");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(map);
        oos.close();
        fos.close();
        //反序列化
        FileInputStream fis = new FileInputStream("Student.dat");
        ObjectInputStream ois = new ObjectInputStream(fis);
        Map<Student,String> map2 = (TreeMap<Student,String>)ois.readObject();
        map2.forEach((v,k)-> System.out.println(v + " ---> " + k));
        System.out.println("方法二");

        for(Map.Entry<Student, String> entry : map2.entrySet()){
                System.out.println(entry.getKey() + " ---> " + entry.getValue());
        }

        System.out.println("方法三");
        for (Student stu: map2.keySet()) {
            System.out.println(stu + " ==> " + map2.get(stu));
        }
        System.out.println("方法4");
        Set set = map2.keySet();
        Iterator<Student> it =  set.iterator();
        while (it.hasNext()){
            Student stu = it.next();
            System.out.println(stu + " ==> " + map2.get(stu));
        }
    }
}
