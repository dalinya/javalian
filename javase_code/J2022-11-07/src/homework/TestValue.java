package homework;

import java.io.*;

public class TestValue {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("src\\data.dat");
        DataOutputStream dos = new DataOutputStream(fos);
        for (int i = 1; i <= 100; i++) {
            double num = 50 + 50*Math.random();
            dos.writeDouble(num);
            dos.writeChar('#');
        }
        dos.close();
        fos.close();

        FileInputStream fis = new FileInputStream("src\\data.dat");
        DataInputStream dis = new DataInputStream(fis);
        double max = 0;
        double min = 101;
        double sum = 0;
        for (int i = 1; i <= 100; i++) {
            double num = dis.readDouble();
            System.out.println("i="+i+" " + num);
            dis.readChar();
            if (num > max){
                max = num;
            }
            if(num < min){
                min=num;
            }
            sum += num;
        }
        dis.close();
        fis.close();
        System.out.println("最大值:"+max);
        System.out.println("最小值:"+min);
        System.out.println("平均值:"+sum/100.0);

    }

}
