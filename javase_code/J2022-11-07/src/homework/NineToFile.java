package homework;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class NineToFile {
    public static void main(String[] args) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter("src\\nine.txt");
        for (int i = 1; i <= 9 ; i++) {
            for (int j = 1; j <= i; j++) {
               //System.out.printf("%2d * %2d = %2d  ",j,i,i*j);
                pw.printf("%2d * %2d = %2d  ",j,i,i*j);
            }
            //System.out.println();
            pw.println();
        }
        pw.close();
    }
}
