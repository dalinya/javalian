import java.io.IOException;
import java.io.RandomAccessFile;

public class Test6_6 {
    public static void main(String[] args) throws IOException {
        RandomAccessFile raf = new RandomAccessFile("e:\\test.dat","rw");
        raf.writeDouble(3.14);
        raf.writeBoolean(false);
        raf.writeChar('A');
        System.out.println(raf.getFilePointer());//返回指针的当前位置
        raf.seek(8);//指针调回到所需位置,必须是非负数
        System.out.println(raf.readBoolean());
    }
}
