import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Test6_12 {
    public static void main(String[] args) throws FileNotFoundException {
        TreeMap<String,Integer> tm = new TreeMap<>();
        Scanner sc  = new Scanner(new File("src\\Test6_8.java"));
        sc.useDelimiter("\\s|,|\\.|\"|\"|\"|\\)|\\(");
        String s = null;
        int count = 0;
        while (sc.hasNext()){
            s = sc.next();
            if(!s.trim().equals("")){//去点字符串中的空格
                count++;
                if(tm.containsKey(s)){
                    tm.put(s,tm.get(s) + 1);
                }else {
                    tm.put(s,1);
                }
            }
        }
        System.out.println("共有" + count + "个单词");
        //System.out.println(tm);
        tm.forEach((k,v)-> System.out.println(k + " => " + v));
    }
}
