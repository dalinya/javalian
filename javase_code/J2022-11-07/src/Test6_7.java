import java.io.*;

public class Test6_7 {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("e:\\test.dat");
        DataOutputStream dos = new DataOutputStream(fos);
        dos.writeByte((byte)123);
        dos.writeShort((short)11223);
        dos.writeInt((int)1234567890);
        dos.writeLong(998877665544332211L);
        dos.writeFloat(2.7182f);
        dos.writeDouble(3.1415936);
        dos.writeChar('J');
        dos.writeBoolean(true);
        dos.writeUTF("邯郸");
        FileInputStream fis = new FileInputStream("e:\\test.dat");
        DataInputStream dis = new DataInputStream(fis);
        System.out.println("read from file test.date");
        System.out.println(dis.readByte());
        System.out.println(dis.readShort());
        System.out.println(dis.readInt());
        System.out.println(dis.readLong());
        System.out.println(dis.readFloat());
        System.out.println(dis.readDouble());
        System.out.println(dis.readChar());
        System.out.println(dis.readBoolean());
        System.out.println(dis.readUTF());
    }

}
