package dem0;


import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

class Student implements  Comparable<Student>, Serializable {
    private long Sno; //学号
    private String Sname;//姓名
    transient String passworld;//密码
    private Date birthSDate;//生日
    private String sex;//性别
    private double score;//成绩

    public Student(long sno, String sname, String passworld, Date birthSDate, String sex, double score) {
        Sno = sno;
        Sname = sname;
        this.passworld = passworld;
        this.birthSDate = birthSDate;
        this.sex = sex;
        this.score = score;
    }

    public Student(String sname, Date birthSDate, String sex) {
        Sname = sname;
        this.birthSDate = birthSDate;
        this.sex = sex;
    }

    public long getSno() {
        return Sno;
    }

    public void setSno(long sno) {
        Sno = sno;
    }

    public String getSname() {
        return Sname;
    }

    public void setSname(String sname) {
        Sname = sname;
    }

    public String getPassworld() {
        return passworld;
    }

    public void setPassworld(String passworld) {
        this.passworld = passworld;
    }

    public Date getBirthSDate() {
        return birthSDate;
    }

    public void setBirthSDate(Date birthSDate) {
        this.birthSDate = birthSDate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        return "Student{" +
                "Sno=" + Sno +
                ", Sname=" + Sname +
                ", passworld=" + passworld +
                ", birthSDate=" + sdf.format(birthSDate) +
                ", sex='" + sex +
                ", score=" + score +
                '}';
    }


    @Override
    public int compareTo(Student o) {
        if (this.sex.compareTo(o.sex) == 0){
            return  this.birthSDate.compareTo(o.birthSDate);
        }else {
            return this.sex.compareTo(o.sex);
        }
       /* return this.sex.compareTo(o.sex) == 0
                ? this.birthSDate.compareTo(o.birthSDate) : this.sex.compareTo(o.sex);*/
    }
}

class StuComparator implements Comparator<Student> {

    @Override
    public int compare(Student o1, Student o2) {
        return o1.getSex().compareTo(o2.getSex()) == 0
                ? o1.getBirthSDate().compareTo(o2.getBirthSDate()) : o1.getSex().compareTo(o2.getSex());
    }
}
public class SerilizeTest {
    public static void main(String[] args) throws ParseException, IOException, ClassNotFoundException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        TreeMap<String,Student> map = new TreeMap<>();
        map.put("1",new Student(20181819401001l,"张三","000",sdf.parse("2000/09/28"),"男",90.5));
        map.put("2",new Student(20181819401002l,"张4","111",sdf.parse("2000/09/28"),"女",79.5));
        map.put("3",new Student(20181819401003l,"张5","222",sdf.parse("2000/04/18"),"男",90.5));
        map.put("4",new Student(20181819401004l,"张6","333",sdf.parse("2000/10/01"),"男",67.5));
        map.put("5",new Student(20181819401005l,"张7","444",sdf.parse("2000/09/28"),"女",90.5));
        map.put("6",new Student(20181819401006l,"张8","555",sdf.parse("2001/03/16"),"男",65.5));
        map.put("7",new Student(20181819401007l,"张9","666",sdf.parse("2000/09/28"),"男",91.5));
        map.put("8",new Student(20181819401008l,"张10","777",sdf.parse("2002/01/02"),"女",86.5));
        map.put("9",new Student(20181819401009l,"张11","888",sdf.parse("2000/09/28"),"男",78.5));
        //序列化
        FileOutputStream fos = new FileOutputStream("student.dat");
        ObjectOutputStream oos  = new ObjectOutputStream(fos);
        oos.writeObject(map);
        oos.close();
        fos.close();
        //反序列化
        FileInputStream fis = new FileInputStream("student.dat");
        ObjectInputStream ois = new ObjectInputStream(fis);
        map = (TreeMap<String, Student>)ois.readObject();
        ois.close();
        fis.close();

        for (String key:map.keySet()) {
            System.out.println(key + "--->" + map.get(key));
        }
        System.out.println("=============");

        Set<Map.Entry<String,Student>> entrySet =  map.entrySet();
        List<Map.Entry<String,Student>> list = new ArrayList<>(entrySet);
        Collections.sort(list,(o1, o2) -> o1.getValue().compareTo(o2.getValue()));
        Iterator<Map.Entry<String,Student>> it = list.iterator();
        while (it.hasNext()){
            Map.Entry<String,Student> entry = it.next();
            String key = entry.getKey();
            Student stu = entry.getValue();
            System.out.println(key+"--->"+ stu);
        }

        System.out.println("=============");
        map.forEach((k,v)-> System.out.println(k+"--->"+ v));

    }
}



























