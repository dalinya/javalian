import java.io.*;

class Student implements Serializable{
    private static final long serialVersionUID = 1l;
    transient int id;//不会被序列化的变量
    static int age;
    private String name;
    String dept;

    public Student(int id, String name, int age,String dept) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.dept = dept;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name +
                ", dept='" + dept +
                ", age=" + age +
                '}';
    }
}
public class Test6_8 {
    public void saveObj(){
        Student stu = new Student(981036,"李明",16,"CSD");
        try {
            FileOutputStream fo = new FileOutputStream("o.user");
            ObjectOutputStream so = new ObjectOutputStream(fo);
            so.writeObject(stu);
            so.close();
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    public void readObj() throws Exception {
        Student stu;
        FileInputStream fi = new FileInputStream("o.user");
        ObjectInputStream si = new ObjectInputStream(fi);
        stu = (Student) si.readObject();
        si.close();
        System.out.println(stu);
    }
    public static void main(String[] args) throws Exception {
        Test6_8 os = new Test6_8();
        os.saveObj();
        os.readObj();
    }
}
