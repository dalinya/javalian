
class  PetDog{
    //属性 -> 成员变量
    public String name;
    public int age;
    public String colour;
    //行为 -> 成员方法
    public void barks(){
        System.out.println(name + "汪汪叫 " + age + "岁 " + colour);
    }
    public void wag(){
        System.out.println(name + "摇尾巴 " + age + "岁 " + colour);
    }
}

public class Test {
    public static void main(String[] args) {
        PetDog dog1 = new PetDog();
        dog1.name = "来福";//使用 . 来访问对象中的属性和方法.
        dog1.colour = "黄色";
        dog1.age = 5;
        dog1.barks();
        dog1.wag();
        PetDog dog2 = new PetDog();
        PetDog dog3 = new PetDog();
        PetDog dog4 = new PetDog();
        PetDog dog5 = new PetDog();//一个类可以实例化无数个对象
    }
}
