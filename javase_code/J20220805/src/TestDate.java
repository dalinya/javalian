public class TestDate {
    public int year;
    public int month;
    public int day;
    public TestDate(){
        this(2022,8,6);
        System.out.println("无参数的构造函数");
    }
    public TestDate(int year,int month,int day){
        this();
        System.out.println("有三个参数的构造函数");
        this.setDate(year,month,day);
    }
    public void setDate(TestDate this ,int year,int month,int day){
        this.year = year;
        this.month = month;
        this.day = day;
    }
    public void printDate(TestDate this){
        System.out.println(this.year + "-" + this.month + "-" + this.day);
    }
    public static void main(String[] args) {
        TestDate testdate = new TestDate();
        testdate.printDate();
    }
}


