public class Triangle {
    private double a;
    private double b;
    private double c;

    public double getA() {
        return a;
    }

    public Triangle(){}
    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }



    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    public double getArea(){
        double p = (a + b + c)/2;
        return Math.sqrt(p*(p-a)*(p-b)*(p-c));
    }

    public double getPrimeter(){
        return (this.a + this.b + this.c);
    }

    public static boolean isTri(int a,int b,int c){
        if(a >= b+ c || b >= a + c || c >= a + b){
            return false;
        }
        return true;
    }
    @Override
    public String toString() {
        return "三角形[" +
                String.format("%.2f", a) +
                "," +String.format("%.2f", b) +
                "," + String.format("%.2f", c) +
                "]\n" + "getArea = " +  String.format("%.2f", this.getArea()) + "\n" + String.format("%.2f",this.getPrimeter());
    }
}
class TestTriangle{
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle();
        triangle1.setA(3);
        triangle1.setB(3);
        triangle1.setC(6);
        if(Triangle.isTri(3,3,6)){
            System.out.println(triangle1);
        }else {
            System.out.println("不能构成三角形");
        }
        Triangle triangle2 = new Triangle(5,3,5);
        if(Triangle.isTri(5,3,5)){
            System.out.println(triangle2);
        }else {
            System.out.println("不能构成三角形");
        }
    }

}