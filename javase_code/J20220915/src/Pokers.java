import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Pokers {

    public  static List<poker> buyPokers(){
        List<poker> pokerList = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j <= 13; j++) {

                String suit = SUITS[i];
                poker poker1 = new poker(suit,j);
                pokerList.add(poker1);
            };
        }
        return pokerList;
    }
    public static void swap(List<poker> pokerList,int i,int index){
        poker poker = pokerList.get(i);
        pokerList.set(i,pokerList.get(index));
        pokerList.add(index,poker);
    }
    public static final String[] SUITS = {"♥","♠","♣","♦"};
    public static void shuffle(List<poker> pokerList){
        Random random = new Random();
        for (int i = pokerList.size() - 1; i > 0; i--) {
            int index = random.nextInt(i);
            swap(pokerList ,i,index);
        }
    }


    public static void main(String[] args) {

        List<poker> pokerList = buyPokers();
        System.out.println("买牌：" + pokerList);
        shuffle(pokerList);
        System.out.println("洗牌" + pokerList);

        List<poker> hand1 = new ArrayList<>();
        List<poker> hand2 = new ArrayList<>();
        List<poker> hand3 = new ArrayList<>();

        List<List<poker>> hand = new ArrayList<>();
        hand.add(hand1);
        hand.add(hand2);
        hand.add(hand3);

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 3; j++) {
                List<poker> handTmp = hand.get(j);//确定是谁的手
                handTmp.add(pokerList.remove(0));
            }

        }
        for (int i = 0; i < hand.size(); i++) {
            System.out.println("第" + (i + 1) + "个人的牌是：" + hand.get(i));
        }
        System.out.println("剩余的牌" + pokerList);

    }

}
