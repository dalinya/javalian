import java.util.*;

///////////////////////////////////////////////////////////////////////////

class Student implements Comparable<Student>{
    int age;
    String name;
    double score;
    public Student(int age,String name,double score){
        this.age = age;
        this.name = name;
        this.score = score;
    }

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", score=" + score +
                '}';
    }


    @Override
    public int compareTo(Student o) {
        return (int) (this.score - o.score);
    }
}

public class Test {




    public static ArrayList<Character> fuc(String str1,String str2){
        ArrayList<Character> arrayList = new ArrayList<>();
        for (int i = 0; i < str1.length(); i++) {
            char ch = str1.charAt(i);
            if(!str2.contains(ch + "")){
                arrayList.add(ch);
            }
        }
        return arrayList;
    }

    public static List<Character> fuc2(String str1,String str2){
        List<Character> arrayList = new ArrayList<>();
        for (int i = 0; i < str1.length(); i++) {
            char ch = str1.charAt(i);
            if(!str2.contains(ch + "")){
                arrayList.add(ch);
            }
        }
        return arrayList;
    }

    public static void main(String[] args) {
        List<Character> list = fuc2("welcome to bit","come");
        System.out.println(list);

    }

    public static void main4(String[] args) {
        ArrayList<Student> arrayList = new ArrayList<>();

        arrayList.add(new Student(21,"张三",90.2));
        arrayList.add(new Student(21,"李思",99));
        arrayList.add(new Student(21,"王五",86));
        for (Student s: arrayList) {
            System.out.println(s);
        }
        Collections.sort(arrayList);
        for (Student s: arrayList) {
            System.out.println(s);
        }
    }

    public static void main3(String[] args) {
        ArrayList<Integer> arrayList1 = new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        arrayList1.add(3);
        arrayList1.add(4);
        arrayList1.add(5);
        int size = arrayList1.size();
        for (int i = 0; i < size; i++) {
            System.out.print(arrayList1.get(i) + " ");
        }
        System.out.println();
        for (int x: arrayList1) {
            System.out.print(x + " ");
        }
        System.out.println();

        Iterator<Integer> it = arrayList1.iterator();
        while(it.hasNext()){
            System.out.print(it.next() + " ");
        }
        System.out.println();
        ListIterator<Integer> it2 = arrayList1.listIterator();
        while (it2.hasNext()){
            System.out.print(it2.next() + " ");
        }
        System.out.println();

    }


    public static void main2(String[] args) {
        ArrayList<Integer> arrayList1 = new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        arrayList1.add(3);
        arrayList1.add(4);
        arrayList1.add(5);

        List<Integer> list = arrayList1.subList(1,3);
        System.out.println(list);
        System.out.println(arrayList1);

        System.out.println("==================");
        /*list.set(0,99);
        System.out.println(arrayList1);
        System.out.println(list);*/

        arrayList1.add(0,7);
        System.out.println(arrayList1);


        arrayList1.remove(new Integer(7));
        System.out.println(arrayList1);

    }

    public static void main1(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        System.out.println(arrayList);

        ArrayList<Integer> arrayList1 = new ArrayList<>();
        arrayList1.add(2);
        arrayList1.add(3);
        arrayList1.add(4);
        System.out.println(arrayList1);

    }


}
