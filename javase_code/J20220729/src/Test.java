import java.util.Scanner;

public class Test {
    public static void main13(String[] args) {
        int a  = 10;
        int b  = 20;
        System.out.println(sum(a,b));
        int c =40;
        System.out.println(sum(a,b,c));
    }
    public static int sum(int x,int y){
        return (x + y);
    }

    public static int sum(int x,int y,int z){
        return (x + y + z);
    }

    public static void main12(String[] args) {
        int a = 10;
        int b = 20;
        System.out.println(max(a,b));
        int c = 50;
        System.out.println(max(a,b,c));
    }
    public static int max(int x,int y){
        return x > y ? x: y;
    }

    public static int max(int x,int y,int z){
        if(x<y){
            x = y;
        }
        if(x<z){
            x = z;
        }
        return x;
    }

    public static void main11(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        System.out.println(fib(num));

    }
    public static int fib(int n){
        int a = 0;
        int b = 1;
        while(n>1){
            int c = a + b;
            a = b;
            b = c;
            n--;
        }
        return b;
    }

    public static void main10(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        System.out.println(facSum(num));
    }
    public static int facSum(int n){
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            sum += fac(i);
        }
        return sum;
    }

    public static void main9(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        System.out.println(fac(num));
    }
    public static int fac(int n){
        int ret  = 1;
        for (int i = 1; i <= n; i++) {
            ret *= i;
        }
        return ret;
    }

    public static void main8(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        System.out.println("两数之和："+ add(a,b));
    }

    public static int add(int x,int y){
        return x + y;
    }
    public static void main7(String[] args) {
        String str = "521";
        int num = Integer.parseInt(str);
        System.out.println(num);
        str = "1314.521";
        double  d= Double.parseDouble(str);
        System.out.println(d);
    }


    public static void main6(String[] args) {
        double d = 10.5;
        String str = d + "";
        System.out.println(str);
        long l = 1314;
        str = l+"";
        System.out.println(str);
        int i = 521;
        str = String.valueOf(i);
        System.out.println(str);
    }
    public static void main5(String[] args) {
        String s1 = "zhang ";
        String s2 = "san";
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s1+s2);
    }
    public static void main4(String[] args) {
        byte a = 1;
        byte b = 2;
        byte c = (byte)(a+b);
        int d = a+b;
    }

    public static void main3(String[] args) {
//        boolean b = false;
//        int i = (int)b;

    }

    public static void main2(String[] args) {
        float f = 2.5f;
        int a = (int)f;
        System.out.println(a);
    }

    public static void main1(String[] args) {
        byte b = 20;
        short s = b;
        //b = s//err
        int a = 10;
        long l = a;
        //a = l;//err
        float f  = l;
        double d = f;
        //f = d;//err
    }

}
