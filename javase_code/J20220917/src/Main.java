class Creature extends java.lang.Object {

    public Creature() {

        System.out.println("-Creature-");

    }


}

class Animal extends Creature implements Comparable<Animal> {

    public Animal() {

        System.out.println("-Animal-");

    }

    @Override
    public int compareTo(Animal o) {
        return 0;
    }
}

class Wolf extends Animal {

    public Wolf() {

        System.out.println("-Wolf-");

    }

}

class ConStructorCallTest {

    public static void main(String[] args) {

        new Wolf();

    }

}
public class Main {
}
