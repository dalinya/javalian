package MyArrayList1;

public class Test {
    public static void main(String[] args) {
        MyArraylist myArraylist = new MyArraylist();

        myArraylist.add(1);
        myArraylist.add(2);
        myArraylist.add(3);
        myArraylist.add(4);
        myArraylist.display();
        try {
            System.out.println(myArraylist.indexOf(3));
        }catch (PosWrongfulException e){
            e.printStackTrace();
        }catch (EmptyException e){
            e.printStackTrace();
        }
        myArraylist.remove(3);
        myArraylist.display();

    }

}
