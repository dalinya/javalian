package MyArrayList1;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.logging.StreamHandler;

public class MyArraylist {

    public int[] elem;
    public int usedSize;//0
    //默认容量
    private static final int DEFAULT_SIZE = 3;

    public MyArraylist() {
        this.elem = new int[DEFAULT_SIZE];
    }

    /**
     * 打印顺序表:
     * 根据usedSize判断即可
     */
    public void display() {
        for (int i = 0; i < this.usedSize; i++) {
            System.out.print(this.elem[i] + " ");
        }
        System.out.println();
    }

    // 新增元素,默认在数组最后新增
    public void add(int data) {
        if(isFull()){
            this.elem = Arrays.copyOf(this.elem,2*this.elem.length);
        }
        this.elem[this.usedSize] = data;
        this.usedSize++;

    }

    /**
     * 判断当前的顺序表是不是满的！
     *
     * @return true:满   false代表空
     */

    public boolean isFull() {
        return this.usedSize == this.elem.length;
    }


    private boolean checkPosInAdd(int pos) {

        if(pos < 0 || pos > this.usedSize){
            return false;
        }
        return true;//合法


    }

    // 在 pos 位置新增元素
    public void add(int pos, int data) {
        if (checkPosInAdd(pos) == false){
            throw new PosWrongfulException("插入位置不合法");
        }
        if(isFull()){
            this.elem = Arrays.copyOf(this.elem,2*this.elem.length);
        }
        int end = this.usedSize;
        while(end > pos){
            this.elem[end] = this.elem[end - 1];
            end--;
        }
        this.elem[pos] = data;
        this.usedSize++;
    }


    // 判定是否包含某个元素
    public boolean contains(int toFind) {
        if(isEmpty()){
            throw new EmptyException("该顺序表为空。");
        }
        for (int i = 0; i < this.usedSize; i++) {
            if(this.elem[i] == toFind){
                return true;
            }
        }
        return false;
    }

    // 查找某个元素对应的位置
    public int indexOf(int toFind) {
        if(isEmpty()){
            throw new EmptyException("该顺序表为空。");
        }
        for (int i = 0; i < this.usedSize; i++) {
            if(this.elem[i] == toFind){
                return i;
            }
        }
        return -1;
    }
    private boolean isEmpty() {
        return this.usedSize == 0;
    }


    // 获取 pos 位置的元素
    public int get(int pos) {
        if(isEmpty()){
            throw new EmptyException("该顺序表为空。");
        }
        if (pos >= this.usedSize || pos < 0){
            throw new PosWrongfulException("返回位置不合法");
        }
        return this.elem[pos];
    }



    // 给 pos 位置的元素设为【更新为】 value
    public void set(int pos, int value) {
        if(isEmpty()){
            throw new EmptyException("该顺序表为空。");
        }
        if (pos >= this.usedSize || pos < 0){
            throw new PosWrongfulException("返回位置不合法");
        }
        this.elem[pos] = value;
    }


    /*
     * 删除第一次出现的关键字key
     *
     * @param key
     */
    public void remove(int key) {
        if(isEmpty()){
            throw new EmptyException("该顺序表为空。");
        }
        for (int i = 0; i < this.usedSize; i++) {
            if(this.elem[i] == key){
                for (int j = i; j < this.usedSize; j++) {
                    this.elem[j] = this.elem[j + 1];
                }
                this.usedSize--;
                return;
            }
        }
    }

    // 获取顺序表长度
    public int size() {
        return this.usedSize;
    }

    // 清空顺序表
    public void clear() {
        this.usedSize = 0;
    }
}