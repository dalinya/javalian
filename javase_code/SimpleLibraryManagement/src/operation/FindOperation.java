package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

public class FindOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("查找图书");
        Scanner sc= new Scanner(System.in);
        System.out.println("请输入你要查找的图书的名字");
        String name = sc.nextLine();
        int size = bookList.getUsedSize();
        for (int i = 0; i < size; i++) {
            Book book = bookList.getBook(i);
            if (book.getName().equals(name)){
                System.out.println("找到这本书了");
                System.out.println(book);
                return;
            }
        }
        System.out.println("没有找到你要查找的这本书");
    }
}
