import book.BookList;
import user.AdminUser;
import user.NormalUser;
import user.User;

import java.util.Scanner;

public class Main {
    public static User login(){
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入你的名字：");
        String name = sc.nextLine();
        System.out.println("请输入你的身份：1 --> 管理员。0 --> 普通用户");
        int choice = sc.nextInt();
        if (choice == 1){
            return new AdminUser(name);
        }else {
            return new NormalUser(name);
        }
    }
    public static void main(String[] args) {
        BookList bookList = new BookList();//创建书架
        User user = login();//登录功能
        while (true) {
            int choice = user.menu();//调用菜单
            //根据choice来指定是哪个操作
            user.doOperation(choice,bookList);
        }
    }
}
