package user;

import book.BookList;
import operation.IOperation;

abstract public class User {
    protected String name;
    protected IOperation[] iOperations;//未进行分配空间，空间分配要在其子类当中去进行
    public User(String name) {
        this.name = name;
    }
    public abstract int menu();
    //根据/根据choice来指定是哪个操作
    public void doOperation(int choice, BookList bookList){
        this.iOperations[choice].work(bookList);
    }
}
