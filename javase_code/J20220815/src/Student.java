import java.sql.SQLOutput;

public class Student {
    public String name;
    public int age;

    public static String classes = "软件工程一班";

    {
        name = "李四";
        System.out.println("实例代码块");
    }
    static {
        classes = "软件工程2班";
        System.out.println("静态代码块");
    }
    public Student(){
        System.out.println("不带参数的构造方法");
    }

    public Student(String name,int age){
        System.out.println("带两个参数的构造方法");
        this.name = name;
        this.age = age;
    }
    public  void print() {
        System.out.println(this.name + " => " + this.age + " -> " + Student.classes);
    }

    public static void main(String[] args) {
        Student student1 = new Student();
        System.out.println("=========");
        Student student2 = new Student();
        System.out.println(Student.classes);

    }

    public static void main2(String[] args) {
      Student.classes = "106JAVA";
      Student student = new Student("张三",12);
      student.print();

    }

    public static void main1(String[] args) {
        Student student1 = new Student("张三",12);
        Student student2 = new Student("李四",13);
        Student student3 = new Student("王五",15);
        Student.classes = "软件工程";
        student1.print();
    }

}
