import java.util.List;

/**
 * @Author 12629
 * @Description：
 */
public class MySingleList {

    /**
     * 节点内部类
     */
    static class ListNode {
        public int val;
        public ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    public ListNode head;//不初始化了 默认就是null

    public void createList() {
        ListNode listNode1 = new ListNode(12);
        ListNode listNode2 = new ListNode(23);
        ListNode listNode3 = new ListNode(34);
        ListNode listNode4 = new ListNode(45);
        ListNode listNode5 = new ListNode(56);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode5;
        this.head = listNode1;
    }

    /**
     * 打印链表里面的数据
     */
    public void display() {
        ListNode cur = head;
        while(cur != null){
            System.out.print(cur.val + " ");
            cur = cur.next;
        }
        System.out.println();
    }

    public void display(ListNode listNode) {
        ListNode cur = listNode;
        while(cur != null){
            System.out.print(cur.val + " ");
            cur = cur.next;
        }
        System.out.println();
    }

    //得到单链表的长度
    public int size(){
        ListNode cur = head;
        int count = 0;
        while(cur != null){
            cur = cur.next;
            count++;
        }
        return count;
    }

    //头插法
    public void addFirst(int data){
        ListNode listNode = new ListNode(data);
        listNode.next =  this.head;
        this.head = listNode;
    }
    //尾插法
    public void addLast(int data){
        ListNode listNode = new ListNode(data);
        ListNode cur = this.head;
        if(cur == null){
            this.head = listNode;
        }else {
            while(cur.next!=null){
                cur = cur.next;
            }
            cur.next = listNode;
        }
    }
    //任意位置插入,第一个数据节点为0号下标
    public void addIndex(int index,int data){
        ListNode listNode = new ListNode(data);
        ListNode cur = this.head;
        if (index < 0 || index > this.size()){
            throw new IndexWrongFulException("index位置不合法");
        }

        if (index == 0){
            this.addFirst(data);
            return;
        }
/*        if (index == this.size()){
            this.addLast(data);
            return;
        }*/
        for (int i = 0; i < index - 1; i++) {
            cur = cur.next;
        }
        listNode.next = cur.next;
        cur.next = listNode;
    }
    //查找是否包含关键字key是否在单链表当中
    public boolean contains(int key){
        ListNode cur = head;
        while(cur != null){
            if (cur.val == key){
                return true;
            }
            cur = cur.next;
        }
        return false;
    }
    //删除第一次出现关键字为key的节点
    public void remove(int key){
        if (this.head == null){
            return;
        }

        if(this.head.val == key){
            this.head = this.head.next;
            return;
        }
        ListNode cur = head;
        while (cur.next != null){
            if(cur.next.val == key){
                ListNode listNode = cur.next;
                cur.next = listNode.next;
                return;
            }
            cur = cur.next;
        }
        System.out.println("没有你要删除的数字");
    }
    //删除所有值为key的节点
    public void removeAllKey(int key){
        if (this.head == null){
            return;
        }


        ListNode prev = this.head;
        ListNode cur = prev.next;
        while(cur != null){
            if(cur.val == key){
                prev.next = cur.next;
                cur = cur.next;
            }else {
                prev = cur;
            }
            cur = cur.next;
        }
        if (head.val == key){
            this.head = this.head.next;
        }
    }


    public void clear() {

    }


}
