public class Test {
    public static MySingleList.ListNode mergeTwoLists(MySingleList.ListNode head1,
                                                      MySingleList.ListNode head2) {

        if (head1 == null || head2 == null){
            return null;
        }
        MySingleList.ListNode newHead = new MySingleList.ListNode(0);
        MySingleList.ListNode tmp = newHead;
        while(head1 != null && head2 != null){
            if(head1.val < head2.val){
                tmp.next = head1;
                tmp = tmp.next;
                head1 = head1.next;
            }else {
                tmp.next = head2;
                tmp = tmp.next;
                head2 =head2.next;
            }
        }
        if (head1 != null){
            tmp.next = head1;
        }
        if (head2 != null){
            tmp.next = head2;
        }
        return newHead.next;

    }

    public static void main(String[] args) {
        MySingleList mySingleList1 = new MySingleList();
        mySingleList1.addLast(12);
        mySingleList1.addLast(22);
        mySingleList1.addLast(32);
        mySingleList1.addLast(42);
        mySingleList1.addLast(52);
        mySingleList1.display();

        MySingleList mySingleList2 = new MySingleList();
        mySingleList2.addLast(13);
        mySingleList2.addLast(45);
        mySingleList2.addLast(67);
        mySingleList2.addLast(89);
        mySingleList2.addLast(90);
        mySingleList2.display();


        MySingleList.ListNode newHead =
                mergeTwoLists(mySingleList1.head,mySingleList2.head);

        mySingleList1.display(newHead);
    }


}
