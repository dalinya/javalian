import java.util.HashMap;
import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        HashMap<String,Integer> map = new HashMap<>();
        while (sc.hasNextLine()){
            String str = sc.nextLine();
            String[] strS = str.split(" ");
            for (int i = 0; i < strS.length; i++) {
                String tmp = strS[i];
                if (!map.containsKey(tmp)){
                    map.put(tmp,1);
                }else {
                    int value = map.get(tmp);
                    map.put(tmp,value + 1);
                }
            }
        }
        System.out.println(map.size());
    }
}
