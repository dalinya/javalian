import java.util.Scanner;

public class Main1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        sc.close();
        int a = 0;
        int b = 1;
        while(b < num){
            int c = a + b;
            a = b;
            b = c;
        }
        System.out.println(Math.min(num - a,b - num));
    }
}
