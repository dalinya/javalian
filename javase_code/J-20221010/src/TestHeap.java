import java.util.Arrays;

public class TestHeap {
    public int[] elem;

    public int usedSize;//有效的数据个数

    public static final int DEFAULT_SIZE = 10;

    public TestHeap() {
        elem = new int[DEFAULT_SIZE];
    }
    public void initElem(int[] array){
        for (int i = 0; i < array.length; i++) {
            elem[i] = array[i];
            usedSize++;
        }
    }
    public void createHeap(){
        for (int parent = (this.usedSize - 1 - 1) / 2; parent >= 0; parent--) {
            shiftDown(parent,usedSize);
        }
    }
    public void shiftDown(int parent,int len){
        int child = 2 * parent + 1;
        while(child < len) {
            if (child + 1 < len && this.elem[child] < this.elem[child + 1]) {
                child++;
            }
            if (this.elem[child] > this.elem[parent]) {
                int tmp = this.elem[child];
                this.elem[child] = this.elem[parent];
                this.elem[parent] = tmp;
                parent = child;
                child = 2 * parent + 1;
            }else {
                break;
            }

        }
    }

    //插入数据
    public void offer(int val){
        if(isFull()){
            this.elem = Arrays.copyOf(this.elem,2*this.elem.length);
        }
        this.elem[this.usedSize] = val;
        this.usedSize++;
        shiftUp(this.usedSize - 1);
    }
    private  boolean isFull(){
        return this.usedSize == this.elem.length;
    }
    public void shiftUp(int child){
        int parent = (child - 1)/2;

        while (child > 0) {
            if (this.elem[child] > this.elem[parent]) {
                int tmp = this.elem[child];
                this.elem[child] = this.elem[parent];
                this.elem[parent] = tmp;
                child = parent;
                parent = (child - 1) / 2;
            }else {
                break;
            }
        }

    }

    //删除
    public int pop() {
        if (isEmpty()){
            return -1;
        }
        int tmp = this.elem[0];
        this.elem[0] = this.elem[this.usedSize - 1];
        this.elem[this.usedSize - 1] = tmp;
        this.usedSize--;
        shiftDown(0,this.usedSize - 1);
        return tmp;
    }
    private boolean isEmpty(){
        return this.usedSize == 0;
    }
    public int peek(){
        if (isEmpty()){
            return -1;
        }
        return this.elem[0];
    }
}
