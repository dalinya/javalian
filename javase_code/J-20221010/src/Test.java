import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

class Student implements Comparable<Student>{

    int age;

    public Student(int age) {
        this.age = age;
    }

    @Override
    public int compareTo(Student o) {
        return o.age - this.age;
    }
}

class InCmp implements Comparator<Integer>{

    @Override
    public int compare(Integer o1, Integer o2) {
        return o2.compareTo(o1);
    }
}
public class Test {

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(11);
        arrayList.add(5);
        arrayList.add(10);
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(arrayList);
        System.out.println(priorityQueue);
    }
    public static void main4(String[] args) {
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2.compareTo(o1);
            }
        });
        PriorityQueue<Integer> priorityQueue2 = new PriorityQueue<>((o1,o2)->{return o2.compareTo(o1);});
        PriorityQueue<Integer> priorityQueue3 = new PriorityQueue<>((o1,o2)->o2.compareTo(o1));

    }
    public static void main3(String[] args) {
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(new InCmp());
        priorityQueue.offer(10);
        priorityQueue.offer(20);
        priorityQueue.offer(5);
        System.out.println(priorityQueue.poll());
        System.out.println(priorityQueue.poll());
        System.out.println(priorityQueue.poll());



    }
    public static void main2(String[] args) {
        PriorityQueue<Student> priorityQueue = new PriorityQueue<>(10);
        priorityQueue.offer(new Student(10));
        Student student = new Student(5);
        priorityQueue.offer(student);
        System.out.println();
    }

    public static void main1(String[] args) {
        TestHeap testHeap = new TestHeap();
        int[] array = { 27,15,19,18,28,34,65,49,25,37 };
        testHeap.initElem(array);
        testHeap.createHeap();
        testHeap.pop();
        System.out.println("faasafsasafa");
        System.out.println(-1/2);
    }
}