import java.util.Stack;

public class TreeNode {
 int val;
 TreeNode left;
 TreeNode right;

 TreeNode(int x) {
  val = x;
 }
  public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
    if (root == null){
     return null;
    }

   Stack<TreeNode> stack1 = new Stack<>();
    lowestCommonAncestorchild(root,p,stack1);

   Stack<TreeNode> stack2 = new Stack<>();
   lowestCommonAncestorchild(root,q,stack2);
   int len = stack1.size() - stack2.size();
   if (len > 0){
    while (len > 0){
      stack1.pop();
      len--;
    }
   }else {
    len = -len;
    while (len > 0){
     stack2.pop();
     len--;
    }
   }

   while (!stack1.peek().equals(stack2.peek())){
     stack1.pop();
     stack2.pop();
   }

   return stack1.peek();
  }
  public void lowestCommonAncestorchild(TreeNode root, TreeNode key, Stack<TreeNode> stack){
       if (root == null || key == null){
        return;
       }
       stack.add(root);
       if (root.equals(key)){
        return;
       }
       lowestCommonAncestorchild(root.left,key,stack);
       if (stack.peek().equals(key)){
        return;
       }
       lowestCommonAncestorchild(root.right,key,stack);
       if (stack.peek().equals(key)){
        return;
       }
       stack.pop();
  }



}