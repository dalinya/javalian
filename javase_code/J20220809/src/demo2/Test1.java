package demo2;

import java.util.Arrays;
import java.util.Comparator;

class Student implements Comparable<Student>{
    public String name;
    public int age;
    public Student(String name,int age){
        this.age = age;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Student o) {
       if(this.age - o.age>0){
           return 1;
       }else if(this.age - o.age<0){
           return -1;
       }else{
           return 0;
       }
    }
}
class AgeComparator implements Comparator<Student>{
    @Override
    public int compare(Student o1, Student o2) {
        return o1.age - o2.age;
    }
}

class NameComperator implements  Comparator<Student>{

    @Override
    public int compare(Student o1, Student o2) {
        return o1.name.compareTo(o2.name);
    }
}

public class Test1 {

    public static void bubbleSort(Comparable[] array){
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if(array[j].compareTo(array[j+1])>0){
                    Comparable tmp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = tmp;
                }
            }
        }

    }

    public static void main(String[] args) {
        Student[] students = new Student[]{new Student("z张三", 10), new Student("l李四", 30), new Student("w王五", 7)};
        bubbleSort(students);
        System.out.println(Arrays.toString(students));
    }

    public static void main4(String[] args) {
        Student student1 = new Student("张三",10);
        Student student2 = new Student("李四",30);
        AgeComparator ageComparator = new AgeComparator();
        if(ageComparator.compare(student1,student2)>0){
            System.out.println("student1 > student2");
        }else{
            System.out.println("student1 < student2");
        }

    }
    public static void main3(String[] args) {
        Student[] students = new Student[]{new Student("z张三", 10), new Student("l李四", 30), new Student("w王五", 7)};
        // AgeComparator ageComparator = new AgeComparator();
        NameComperator nameComperator = new NameComperator();
        Arrays.sort(students,nameComperator);
        System.out.println(Arrays.toString(students));

    }


    public static void main2(String[] args) {
        Student student1 = new Student("张三",40);
        Student student2 = new Student("李四",30);
        if(student1.compareTo(student2)>0){
            System.out.println("student1 > student2");
        }else{
            System.out.println("student1 < student2");
        }

    }

    public static void main1(String[] args) {
     Student[] students = new Student[]{new Student("张三",10),new Student("李四",30),new Student("王五",7)};
        Arrays.sort(students);
        System.out.println(Arrays.toString(students));
    }
}
