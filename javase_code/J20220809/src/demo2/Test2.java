package demo2;
class Money implements Cloneable{
    public double m = 12.5;
    protected Object clone() throws CloneNotSupportedException{
        return super.clone();
    }

}
class Person implements Cloneable{
    public int id;
    public Money money = new Money();

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Person tmp = (Person) super.clone();
        tmp.money = (Money) this.money.clone();
        return tmp;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", money=" + money +
                '}';
    }
}

public class Test2 {
    public static void main(String[] args) throws CloneNotSupportedException{
        Person person = new Person();

        Person person1 = (Person) person.clone();
        person1.money.m = 1999;
        System.out.println("person: " + person.money.m);
        System.out.println("person1: "+ person1.money.m);
    }
}
