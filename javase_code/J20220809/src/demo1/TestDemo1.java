package demo1;
class Shape{
    public void draw(){
        System.out.println("话图形");
    }
}
class Rect extends Shape{
    @Override
    public void draw() {
        System.out.println("画矩形");
    }
}
class Cycle extends Shape{
    @Override
    public void draw() {
        System.out.println("⚪");
    }
}
class Triangle extends Shape{
    @Override
    public void draw() {
        System.out.println("▲");
    }
}
class Flower extends Shape{
    @Override
    public void draw() {
        System.out.println("❀");
    }
}
public class TestDemo1 {
    public static void drawShapes1(){
        String[] strings = {"cycle","rect","cycle","rect","flower"};
        for (String x:strings) {
            if(x.equals("cycle")){
                Cycle cycle = new Cycle();
                cycle.draw();
            }else if(x.equals("rect")){
                Rect rect = new Rect();
                rect.draw();
            }else{
                Flower flower = new Flower();
                flower.draw();
            }
        }
    }
    public static void drawShapes(){
        Shape[] shapes = {new Cycle(),new Rect(),new Cycle(),new Rect(),new Flower()};
        for (Shape s: shapes) {
            s.draw();
        }
    }

    public static void main(String[] args) {
        drawShapes();
    }

    //=============================================
    public static void drawMap(Shape  shape){
        shape.draw();
    }
    public static void main2(String[] args) {
        drawMap(new Rect());
        drawMap(new Cycle());
        drawMap(new Triangle());
    }
    public static void main1(String[] args) {
        Shape shape1 = new Rect();
        shape1.draw();
        Shape shape2 = new Cycle();
        shape2.draw();
        Shape shape3 = new Triangle();
        shape3.draw();
    }
}
