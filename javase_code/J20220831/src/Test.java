import java.util.concurrent.Callable;

class Animal{
    public String name;
    public int age;

    public void eat(){
        System.out.println(name + "正在吃饭！");
    }

    public Animal(String name){
        this.name = name;
    }
}

class Dog extends Animal{

    /*@Override
    public void eat() {//重写--> 动态绑定
        System.out.println(name + "正在吃狗粮");
    }
*/
    public void houseGuard(){
        System.out.println(super.name + "正在看家护院！！");
    }

    public Dog(String name){
        super(name);
    }
}
class Cat extends Animal{
    @Override
    public void eat() {//重写--> 动态绑定
        System.out.println(name + "正在吃猫粮");
    }

    public void catchMouse(){
        System.out.println(super.name + "正在抓老鼠！！");
    }

    public Cat(String name){
        super(name);
    }
}

public class Test {

    public static void main(String[] args) {
        Animal animal = new Dog("来福");
        //animal.catchMouse();
        //animal 是不是引用了 Cat这个对象
        if(animal instanceof Cat){
            Cat cat = (Cat) animal;
            cat.catchMouse();
        }
    }

    public static void main3(String[] args) {
        Animal animal = new Cat("二喵");
        //animal.catchMouse();
        Cat cat = (Cat)animal;
        cat.catchMouse();

    }

    public static Animal func2(){
        return new Cat("二喵");
    }

    public static void func(Animal animal){
        animal.eat();
    }

    public static void main2(String[] args) {
        Dog dog = new Dog("来福");
        func(dog);

        Cat cat = new Cat("二喵");
        func(cat);
    }
    public static void main1(String[] args) {
        Animal animal1 = new Dog("来福");
        animal1.eat();
        Animal animal2 = new Cat("二喵");
        animal2.eat();
        //animal2.houseGuard(); ERROR Animal类中没有houseGuard方法
    }

}
