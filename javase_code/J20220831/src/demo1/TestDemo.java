package demo1;

import java.util.Random;

class Shape{
    public void draw(){
        System.out.println("画图形");
    }
}
class Rect extends Shape{
    @Override
    public void draw() {
        System.out.println("画矩形");
    }
}
class Cycle extends Shape{
    @Override
    public void draw() {
        System.out.println("画圆形");
    }
}
class Square extends Shape{
    @Override
    public void draw() {
        System.out.println("画正方形");
    }
}
class Flower extends Shape{
    @Override
    public void draw() {
        System.out.println("❀❀");
    }
}

class Triangle extends Shape {
    @Override
    public void draw() {
        System.out.println("△");
    }
}
public class TestDemo {


    public static void drawShapes(){
        Shape[] shapes = {new Cycle(),new Rect(),new Cycle(),new Rect(),new Flower()};
        for (Shape s: shapes) {
            s.draw();
        }
    }
    public static void main2(String[] args) {
        drawShapes();
    }


    public static void drawMap(Shape shape){
        shape.draw();
    }

    public static void main(String[] args) {
        /*Rect rect = new Rect();
        drawMap(rect);
        drawMap(new Cycle());
        drawMap(new Triangle());
        drawMap(new Flower());
        int a = (int)(Math.random()*10) + 90;
        String Str = "abc";*/
        //判断
        char ch = 'a';
        System.out.println(Character.isLetter(ch));//判断是否为字母
        Character.isLetterOrDigit(ch);//是否为数字和字母
        Character.isLowerCase(ch);//是否是小写字母
        Character.isUpperCase(ch);//是否是大写字母

        //随机数
        //0<= Math.random() <=1
        int ran = (int)Math.random()*90 + 10;
        Random random = new Random();
        System.out.println(random.nextInt(90) + 10);

    }
}
