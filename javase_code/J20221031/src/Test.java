import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Test {
    public static void main(String[] args) {
        File f = new File("D:\\Program Files\\javacode\\javalian\\J20221031\\src");
        System.out.println("f.exists() = " + f.exists());
        System.out.println("f.isFile() = " + f.isFile());
        System.out.println("f.isDirectory() = " + f.isDirectory());
        System.out.println("f.length() = " + f.length());
        System.out.println("f.getAbsolutePath() = "+f.getAbsolutePath());
        System.out.println("f.getName() = " + f.getName());
        System.out.println("f.getParent() = " + f.getParent());
        System.out.println("f.getPath() = " + f.getPath());
        long milliSecond = f.lastModified();
        Date date = new Date();
        date.setTime(milliSecond);
        System.out.println("f.lastModified() = " +  new SimpleDateFormat("yyyy-MM-dd HH:ss:mm").format(date));

        /*  File[] files = f.listFiles();
        for (File file:
             files) {
            System.out.println(f.getName());
        }*/

        File f1 = new File("D:\\Program Files\\javacode\\javalian\\J20221031\\hdc");
        f1.mkdirs();
        File f2 = new File("D:\\Program Files\\javacode\\javalian\\J20221031\\hebeu");
        f2.mkdir();
    }

}
