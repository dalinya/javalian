import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class Test2 {
    public static void main(String[] args) {
        //指定目录
        File dir = new File("D:\\Program Files\\javacode\\javalian\\J20221031\\src");
        //获取dir所有的子文件夹和文件的name
        if (dir.isDirectory()){
            String[] fileName = dir.list();
            System.out.println(Arrays.toString(fileName));
            Arrays.stream(fileName).forEach(f -> System.out.println(f));
        }
        System.out.println("=======================");
        //获取dir所有的子文件夹和文件
        File[] files = dir.listFiles();
        extracted(files);
        files = dir.listFiles((dir1, name) -> {
           return name.endsWith(".java");
        });
        System.out.println("======================");
        extracted(files);
        files = dir.listFiles((dir1, name) -> {//获取以a或者w或者T开头的
            return name.startsWith("a") ||name.startsWith("w") || name.startsWith("T");
        });
        System.out.println("======================");
        extracted(files);
    }

    private static void extracted(File[] files) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (File file: files) {
            //修改时间
            Date date = new Date(file.lastModified());
            String modify = sdf.format(date);
            //【DIR】
            String isDir = file.isDirectory() ? "【DIR】" : "";
            //文件大小
            long len = file.length();
            String lens = "";
            if(file.isFile()){
                lens = String.valueOf(len);//len->String
            }
            //文件夹或文件名称
            String name = file.getName();
            System.out.printf("%-22s%-8s%-10s%-20s\n",modify,isDir,lens,name);
        }
    }
}
