
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;






public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        int[] array = new int[str.length()*str.length()];
        int count = 0;
        for (int j = 1; j <= str.length(); j++) {
            for (int z = 0; z < j; z++) {
                HashMap<Character,Integer> map = new HashMap<>();
                for (int i = z; i < j; i++) {
                    if (!map.containsKey(str.charAt(i))){
                        map.put(str.charAt(i),1);
                    }else {
                        int num = map.get(str.charAt(i));
                        map.put(str.charAt(i),num + 1);
                    }
                }
                int max = map.get(str.charAt(z));
                int min = map.get(str.charAt(z));
                for (Map.Entry<Character,Integer> entry: map.entrySet()) {
                    if(entry.getValue() > max){
                        max = entry.getValue();
                    }
                    if(entry.getValue() < min){
                        min = entry.getValue();
                    }
                }
                array[count++] = max - min;
            }
        }


        Arrays.sort(array);
        System.out.println(array[array.length - 1]);


    }
//aabbaaabab
}
