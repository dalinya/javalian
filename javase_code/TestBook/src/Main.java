import book.Book;
import book.BookList;
import user.AdminUser;
import user.NomalUser;
import user.User;

import java.util.Scanner;

public class Main {
    public static User login(){
        System.out.println("请输入你的姓名:");
        Scanner scan = new Scanner(System.in);
        String userName = scan.nextLine();
        System.out.println("请输入你的身份：1-> 管理员，0-> 普通用户");
        int choice = scan.nextInt();
        if(choice == 1){
            return new AdminUser(userName);
        }else{
            return new NomalUser(userName);
        }
    }

    public static void main(String[] args) {
        BookList bookList = new BookList();
        User user = login();

        while(true){
            int choice = user.menu();
            user.doOperation(choice,bookList);
        }
    }
}
