package demo;
import java.text.SimpleDateFormat;
import java.util.Scanner;

class StudentTest {
    public static void main(String[] args) {
        Student s = new Student();
        Scanner sc = new Scanner(System.in);

        s.setSno(sc.nextDouble());
        s.setSname(sc.next());
        s.setSex(sc.next().charAt(0));
        s.setHight(sc.nextDouble());
        s.setBirthDate(sc.next());

        System.out.print(s);

    }
}
public class Student {
    private double sno;
    private String sname;
    private char sex;
    private double hight;
    private String birthDate;

    public Student(){}
    public Student(double sno, String sname, char sex, double hight, String birthDate) {
        this.sno = sno;
        this.sname = sname;
        this.sex = sex;
        this.hight = hight;
        this.birthDate = birthDate;
    }

    public double getSno() {
        return sno;
    }

    public void setSno(double sno) {
        this.sno = sno;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public double getHight() {
        return hight;
    }

    public void setHight(double hight) {
        this.hight = hight;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String toString(){
        SimpleDateFormat myFmt=new SimpleDateFormat("yyyy-MM-dd日");
        if(sex == 'm'){
            return "Student" + "[" + sno + "," + sname + ","+ "男" + ","
                    + hight + "," + myFmt.format(birthDate) + "," +" ]";
        }
        return "Student" + "[" + sno + "," + sname + ","+ "女" + ","
                + hight + "," + myFmt.format(birthDate) + "," +" ]";
    }

}
