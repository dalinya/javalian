package demo;
abstract class Shape{
    //抽象方法
    public  abstract void draw();
    //普通类的成员
    int a;
    public void func(){
        System.out.println("hello world");
    }

}

class Rect extends Shape{
    @Override
    public void draw() {
        System.out.println("画矩形");
    }
}

class Cycle extends Shape {
    @Override
    public void draw() {
        System.out.println("画圆形");
    }
}

class Triangle extends Shape{
    @Override
    public void draw() {
        System.out.println("画三角形");
    }
}

class Flower extends Shape{

    @Override
    public void draw() {
        System.out.println();
    }
}

public class Test {
    public static void main(String[] args) {

    }
}
