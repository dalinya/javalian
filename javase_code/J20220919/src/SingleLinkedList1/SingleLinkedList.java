package SingleLinkedList1;

import java.util.List;

public class SingleLinkedList {

    class ListNode{
        int val;
        ListNode next;
        public ListNode(int val){
            this.val = val;
        }
    }
     ListNode head;
    //头插法
    public void addFirst(int data){
        ListNode newNode = new ListNode(data);
        newNode.next = this.head;
        this.head = newNode;
    }
    //尾插法
    public void addLast(int data){
        ListNode newNode = new ListNode(data);
        if (this.head == null){
            this.head = newNode;
            return;
        }
        ListNode cur = this.head;
        while (cur.next != null){
            cur = cur.next;
        }
        cur.next = newNode;
    }
    public void display(){
        ListNode cur = this.head;
        while(cur!= null){
            System.out.print(cur.val + " ");
            cur = cur.next;
        }
        System.out.println();
    }
    //得到单链表的长度
    public int size() {
        ListNode cur = this.head;
        int count  = 0;
        while(cur!= null){
            count++;
            cur = cur.next;
        }
        return count;
    }
    //任意位置插入,第一个数据节点为0号下标
    public void addIndex(int index,int data){
        if(index <  0 || index > this.size()){
            throw new IndexWrongFulException("插入位置错误");
        }
        ListNode newNode = new ListNode(data);
        if(index == 0){
            newNode.next = this.head;
            this.head = newNode;
            return;
        }
        ListNode cur = this.head;
        for (int i = 0; i < index -1; i++) {
            cur = cur.next;
        }
        newNode.next = cur.next;
        cur.next = newNode;
    }

    //查找是否包含关键字key是否在单链表当中
    public boolean contains(int key){
        if(this.head == null){
            return false;
        }
        ListNode cur = this.head;
        while(cur != null){
            if(cur.val == key){
                return true;
            }
            cur = cur.next;
        }
        return false;
    }

    public void clear(){
        this.head = null;
    }
    //删除第一次出现关键字为key的节点
    public void remove(int key){
        if(this.head == null){
            return;
        }
        if(this.head.val == key){
            this.head = this.head.next;
            return;
        }
        ListNode cur = this.head;
        while (cur.next != null){
            if (cur.next.val == key){
                cur.next = cur.next.next;
                return;
            }
            cur = cur.next;

        }
    }
    //删除所有值为key的节点
    public void removeAllKey(int key){
        if(this.head == null){
            return;
        }
        ListNode cur = this.head;
        while (cur.next != null){
            if(cur.next.val == key){
                cur.next = cur.next.next;
            }else {
                cur = cur.next;
            }
        }
        if (this.head.val == key){
            this.head = this.head.next;
        }
    }
  /*






*/
}
