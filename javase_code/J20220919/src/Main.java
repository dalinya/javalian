import java.util.Arrays;
import java.util.Scanner;
public class Main {
    public static void main4(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Cylindrical cylindrical = new Cylindrical(new Circle(scanner.nextDouble()),scanner.nextDouble());
        if(cylindrical.circle.r <= 0 || cylindrical.height <= 0){
            System.out.println("input error");
        }else {
            System.out.printf("%.2f", cylindrical.volume());
        }
    }

    public static void main3(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Student student = new Student(scanner.nextLong(),scanner.next(),scanner.next(),scanner.nextInt(),scanner.next());
        System.out.println(student);

    }
    public static void main2(String[] args) {
        Dog dog1 = new Dog("Tom","哈士奇",2,"拆家");
        System.out.println(dog1);
        Dog dog2 = new Dog("jerry","中华田园犬",3,"护家");
        System.out.println(dog2);
        Dog dog3 = new Dog("旺财","柯基",2,"吃喝玩");
        System.out.println(dog3);
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int array[] = new int[1];
        int count = 0;
        while (scanner.hasNextInt()){
            array[count] = scanner.nextInt();
            count++;
            array = Arrays.copyOf(array,count + 1);
        }
        System.out.println(Addition.add(array));
    }
}
class Circle{
    double r;


    public Circle(double r) {
        this.r = r;
    }

    public double area(){
        return 3.1415926*this.r*this.r;
    }
}
class Cylindrical{
    double height;
    Circle circle;

    public Cylindrical(Circle circle, double height) {
        this.height = height;
        this.circle = circle;
    }
    public double volume(){
        return this.height*this.circle.area();
    }
}
class Student{

    private long sno;//（学号）
    private String sname;//（姓名）
    private String sex;//（性别）(f代表女，m代表男)
    private int hight;//（身高）
    private String brithDate;//（出生日期）

    public Student() {
    }

    public Student(long sno, String sname, String sex, int hight, String brithDate) {
        this.sno = sno;
        this.sname = sname;
        this.sex = sex;
        this.hight = hight;

        this.brithDate = brithDate;
    }

    public long getSno() {
        return sno;
    }

    public void setSno(long sno) {
        this.sno = sno;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getHight() {
        return hight;
    }

    public void setHight(int hight) {
        this.hight = hight;
    }

    public String getBrithDate() {
        return brithDate;
    }

    public void setBrithDate(String brithDate) {
        this.brithDate = brithDate;
    }

    @Override
    public String toString() {
        if(this.sex.equals("m")){
            return "Student[" + sno +
                    "," + sname  +
                    ",男" +
                    "," + hight +
                    "," + brithDate +
                    ']';
        }
        return "Student[" + sno +
                "," + sname  +
                ",女"   +
                "," + hight +
                "," + brithDate +
                ']';
    }


}
class Dog{
    private String name;
    private String variety;//品种
    private int age;
    private String hobby;//爱好

    public Dog() {

    }
    public Dog(String name, String variety, int age, String hobby) {
        this.name = name;
        this.variety = variety;
        this.age = age;
        this.hobby = hobby;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    @Override
    public String toString() {
        return "小狗名称：" + this.name+
        "\n品种：" + this.variety +
        "\n小狗年龄：" + this.age +
        "\n小狗爱好：" + this.hobby;
    }

}



class Addition{
    public static int add(int... array){
        int sum = 0;
        for (int num:array) {
            sum += num;
        }
        return sum;
    }

}


