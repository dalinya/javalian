import java.util.Scanner;

public class Main{

    String b;

    String t;

    double p;

    String o;

    int m;

    Main(){}

    Main(String b,String t,String o,double p,int m){

        this.b=b;

        this.t=t;

        this.o=o;

        this.p=p;

        this.m=m;

    }

    void about(){

        System.out.printf("品牌：%s\n型号:%s\n操作系统：%s\n价格：%。1f\n内存：%d\n",b,t,o,p,m);

    }

    void call(int n){

        if(n==1){

            System.out.println("正在给爸爸打电话");

        }else if(n==2){

            System.out.println("正在给妈妈打电话");

        }else if(n==3){

            System.out.println("正在给姐姐打电话");

        }else{

            System.out.println("您所拨打的电话为空号");

        }

    }

    void play(String name){

        System.out.println("正在播放"+name);

    }

    public static void main(String[] args){

        Scanner s=new Scanner(System.in);

        String str=s.nextLine();

        String[]ss=str.split(" ");

        Main m=new Main(ss[0],ss[1],ss[2],Double.parseDouble(ss[3]),Integer.parseInt(ss[4]));

        m.about();

        m.call(Integer.parseInt(ss[5]));

        m.play(ss[6]);

    }

}
