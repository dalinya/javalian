package ArrayList1;

import java.util.Arrays;

public class MyArrayList {
    private int[] elem;
    private int usedSize;
    private static final int DEFAULT_SIZE = 2;

    public MyArrayList(){
       this.elem = new int[DEFAULT_SIZE];
    }

    //打印顺序表
    public void display(){
        for (int i = 0; i < this.usedSize; i++) {
            System.out.print(this.elem[i] + " ");
        }
        System.out.println();
    }
    public boolean isFull(){

        return  this.usedSize >= this.elem.length;
    }
    public void add(int data){
        if(isFull()){
            this.elem = Arrays.copyOf(this.elem,2 * this.elem.length);
        }
        elem[this.usedSize] = data;
        this.usedSize++;
    }


    public void add(int pos,int data){
        if(isFull()){
            this.elem = Arrays.copyOf(this.elem,2 * this.elem.length);
        }
        if(pos > this.usedSize || pos < 0){
            throw new  PosWrongfulException("插入位置错误不合法");
        }

        int end = this.usedSize;
        while (end > pos){
            this.elem[end]= this.elem[end - 1];
            end--;
        }
        this.elem[pos] = data;
        this.usedSize++;

    }



    public boolean contains(int toFind){
        for (int i = 0; i < this.usedSize; i++) {
            if(this.elem[i] == toFind){
                return true;
            }
        }
        return false;
    }
    public int indexOf(int toFind){
        for (int i = 0; i < this.usedSize; i++) {
            if(this.elem[i] == toFind){
                return i;
            }
        }
        return -1;
    }
    public boolean isEmpty(){
        return this.usedSize == 0;
    }
    public int get(int pos){
        if(isEmpty()){
            throw new EmptyException("当前顺序表为空");
        }
        if(pos >= this.usedSize || pos < 0){
            throw new  PosWrongfulException("返回位置错误不合法");
        }
        return this.elem[pos];
    }

    public void set(int pos,int val){
        if(isEmpty()){
            throw new EmptyException("当前顺序表为空");
        }
        if(pos >= this.usedSize || pos < 0){
            throw new  PosWrongfulException("set获取元素的时候，pos不合法异常");
        }
        this.elem[pos] = val;
    }
    public void remove(int toRemove){
        if(isEmpty()){
            throw new EmptyException("当前顺序表为空");
        }
        int index = this.indexOf(toRemove);
        if(index == -1){
            System.out.println("找不到数字");
            return;
        }
        while (index < this.usedSize - 1){
            this.elem[index] = this.elem[index + 1];
            index++;
        }
        this.usedSize--;

    }
    public int size(){
        return this.usedSize;
    }
    public void clear(){
        this.usedSize = 0;

    }



}
