import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        int[] arr = {10,11,12,13,14,15,16,17};
        printArray(arr);
    }
    public static void printArray(int []arr){
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

    public static void main8(String[] args) {
        int[] arr = new int[100];
        for (int i = 0; i < 100; i++) {
            arr[i] = i+1;
            System.out.println(arr[i]);
        }
    }

    public static void main7(String[] args) {
        hanio(3,'A','B','C');
    }
    public static void hanio(int n,char pos1,
                             char pos2,char pos3) {
        if(n == 1) {
            move(pos1,pos3);
            return;
        }
        hanio(n-1,pos1,pos3,pos2);
        move(pos1,pos3);
        hanio(n-1,pos2,pos1,pos3);
    }

    public static void move(char pos1,char pos2) {
        System.out.print (pos1+" -> " + pos2+" ");
    }

    public static void main6(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        System.out.println(fib(num));
    }
    public static int fib(int num){
        if(num<=2){
            return 1;
        }
        return fib(num-1)+fib(num-2);
    }

    public static void main5(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        System.out.println(sum(num));
    }
    public static int sum(int num){
        if(num < 10){
            return num;
        }
        return num%10 + sum(num/10);
    }

    public static void main4(String[] args) {
        int num = 1234;
        print(num);
    }
    public static void print(int num){
        if(num < 10){
            System.out.print(num + " ");
            return;
        }
        print(num/10);
        System.out.print(num % 10 + " ");
    }

    public static void main3(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        System.out.println(sum1(num));
    }
    public static int sum1(int num){
        if(num<1){
            return num;
        }
        return num+ sum1(num-1);
    }

    public static void main2(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        System.out.println(fac(num));
    }
    public static int fac(int num){
        if(num == 1){
            return 1;
        }
        return num*fac(num-1);
    }

    public static void main1(String[] args) {
        int num = 1234;
        System.out.println(printl(num));
    }
    public static int printl(int num){
        if(num < 9){
            return num;
        }
        return printl(num/10) + num%10;
    }
}
