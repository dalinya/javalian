import java.util.Arrays;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        int[] array = new int[]{1,2,3,4,5,6,7,8,9,10};
        Scanner scan = new Scanner(System.in);
        int key = scan.nextInt();
        int ret = binarySearch(array,key);
        if(ret < 0){
            System.out.println("没有找到");
        }else{
            System.out.println("找到了，下标是" + ret);
        }
    }
    public static int binarySearch(int[] array,int key){
        int left = 0;
        int right = array.length - 1;
        while(left <= right){
            int mid = left + (right - left) / 2;
            if(array[mid] < key){
                left = mid + 1;
            }else if(array[mid] > key){
                right = mid - 1;
            }else{
                return mid;
            }
        }
        return -1;
    }

    public static void main6(String[] args) {
        int[] array = {1,2,3,4,5,6};
        System.out.println("调整前：");
        System.out.println(Arrays.toString(array));
        int right = array.length-1;
        int left = 1;
        while((right+1)%2 == 0){
            right--;
        }
        while(left < right){

        int tmp = array[left];
        array[left] = array[right];
        array[right] = tmp;
        left += 2;
        right -= 2;

        }
        System.out.println("调整后：");
        System.out.println(Arrays.toString(array));
    }

    public static void main5(String[] args) {
        int[] array = {1,2,3,4,5};
        System.out.println(avg(array));
    }
    public static double avg(int[] array){
        int sum = 0;
        for (int x:array) {
            sum+=x;
        }
        return sum*1.0/array.length;
    }

    public static void main4(String[] args) {
        int[] array = new int[]{1,2,3};
        for (int x:
             array) {
            x*=2;
        }
        System.out.println(Arrays.toString(array));
    }

    public static void main3(String[] args) {
        int[] array = new int[]{1,2,3};
        transform(array);
        System.out.println(Arrays.toString(array));
    }
    public static void transform(int[] array){
        for (int i = 0; i < array.length; i++) {
            array[i] *= 2;
        }
    }

    public static void main2(String[] args) {
        int[] array = {1,2,3,4,5,6,7,8,9,10};
        String str = Arrays.toString(array);
        System.out.println(str);
    }
    public static void main1(String[] args) {
        int[] array = {99,100};
        swap(array);
        System.out.println(Arrays.toString(array));
    }
    public static void swap(int[] array){
        int tmp = array[0];
        array[0] = array[1];
        array[1] = tmp;
    }
}
