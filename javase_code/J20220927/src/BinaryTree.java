import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BinaryTree {
 
    static class TreeNode {
        public char val;
        public TreeNode left;//左孩子的引用
        public TreeNode right;//右孩子的引用
 
        public TreeNode(char val) {
            this.val = val;
        }
    }

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if(root == null){
            return null;
        }
        Stack<TreeNode> qu1= new Stack<>();
        lowestCommonAncestorChild(root,p,qu1);
        Stack<TreeNode> qu2= new Stack<>();
        lowestCommonAncestorChild(root,q,qu2);
        if (qu1.size() - qu2.size() > 0){
            int size = qu1.size() - qu2.size();
            while (size > 0){
                qu1.pop();
                size--;
            }
            while(qu1.peek() != qu2.peek()){
                qu1.pop();
                qu2.pop();
            }
        }else {
            int size = qu2.size() - qu1.size();
            while (size > 0){
                qu2.pop();
                size--;
            }
            while(qu1.peek() != qu2.peek()){
                qu1.pop();
                qu2.pop();
            }
        }

        return qu1.peek();
    }

    public void lowestCommonAncestorChild(TreeNode root, TreeNode p,Stack<TreeNode> queue){
        if(root == null || p == null){
            return;
        }

        queue.add(root);
        if(queue.peek().equals(p)){
            return;
        }
        lowestCommonAncestorChild(root.left,p,queue);
        if(queue.peek().equals(p)){
            return;
        }
        lowestCommonAncestorChild(root.right,p,queue);
        if(queue.peek().equals(p)){
            return;
        }

        queue.pop();
    }
    /**
     * 创建一棵二叉树 返回这棵树的根节点
     *
     * @return
     */
    /*public TreeNode createTree() {
 
    }
 
    // 前序遍历
    public void preOrder(TreeNode root) {
    }
 
    // 中序遍历
    void inOrder(TreeNode root) {
    }
 
    // 后序遍历
    void postOrder(TreeNode root) {
    }
 
    public static int nodeSize;
 
    *//**
     * 获取树中节点的个数：遍历思路
     *//*
    void size(TreeNode root) {
    }
 
    *//**
     * 获取节点的个数：子问题的思路
     *
     * @param root
     * @return
     *//*
    int size2(TreeNode root) {
    }
 
 
    *//*
     获取叶子节点的个数：遍历思路
     *//*
    public static int leafSize = 0;
 
    void getLeafNodeCount1(TreeNode root) {
    }
 
    *//*
     获取叶子节点的个数：子问题
     *//*
    int getLeafNodeCount2(TreeNode root) {
    }
 
    *//*
    获取第K层节点的个数
     *//*
    int getKLevelNodeCount(TreeNode root, int k) {
    }
 
    *//*
     获取二叉树的高度
     时间复杂度：O(N)
     *//*
    int getHeight(TreeNode root) {
       
    }
 
 
    // 检测值为value的元素是否存在
    TreeNode find(TreeNode root, char val) {
        
        return null;
    }
 
    //层序遍历
    void levelOrder(TreeNode root) {
        
    }
 
 
    // 判断一棵树是不是完全二叉树
    boolean isCompleteTree(TreeNode root) {
        return true;
    }*/
}