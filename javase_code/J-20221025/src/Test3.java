import java.util.Scanner;

public class Test3 {
    public static void subOne(Shape shape){
        if (shape instanceof Circle){
            Circle circle = (Circle) shape;
            circle.setRadius(circle.getRadius() - 1);
        } else if (shape instanceof  Triangle) {
            Triangle triangle = (Triangle) shape;
            triangle.setA(triangle.getA() - 1);
            triangle.setB(triangle.getB() - 1);
            triangle.setC(triangle.getC() - 1);
        }else {
            Rectangle rectangle = (Rectangle)shape;
            rectangle.setLength(rectangle.getLength() - 1);
            rectangle.setWidth(rectangle.getWidth() - 1);
        }

    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String[] strings = str.split(" ");
        if (strings.length == 2){
            Circle circle = new Circle(Integer.parseInt(strings[1]));
            subOne(circle);
            Cylinder cylinder = new Cylinder(Double.parseDouble(strings[0]),circle);
            System.out.println(cylinder);
        } else if (strings.length == 4) {
            Triangle triangle = new Triangle(Integer.parseInt(strings[1]),Integer.parseInt(strings[2]),Integer.parseInt(strings[3]));
            subOne(triangle);
            Cylinder cylinder = new Cylinder(Double.parseDouble(strings[0]),triangle);
            System.out.println(cylinder);
        }else {
            Rectangle rectangle = new Rectangle(Integer.parseInt(strings[1]),Integer.parseInt(strings[2]));
            subOne(rectangle);
            Cylinder cylinder = new Cylinder(Double.parseDouble(strings[0]),rectangle);
            System.out.println(cylinder);
        }

    }

}

interface Shape{

    double getArea();
    double getPerimeter();
}

 class Circle implements Shape{
    private int radius;
    @Override
    public double getArea() {
        return Math.PI * this.radius * this.radius;
    }

    public Circle(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * this.radius;
    }

    @Override
    public String toString() {
        return "Circle[" +
                radius + ","
                +  String.format("%.2f",this.getArea())
                + "," + String.format("%.2f",this.getPerimeter());
    }
}
class Triangle implements Shape{
    private int a;
    private int b;
    private int c;
    @Override
    public double getArea() {
        double p = (this.a +  this.b + this.c)/2.0;
        return Math.sqrt(p*(p-a)*(p-b)*(p-c));
    }

    public Triangle(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    @Override
    public double getPerimeter() {
        return this.a +  this.b + this.c;
    }

    @Override
    public String toString() {
        return "Triangle[" + this.a + ',' + this.b + ',' + this.c
                + "," + String.format("%.2f",this.getArea())
                + "," + String.format("%.2f",this.getPerimeter());
    }
}

class Rectangle implements Shape{
    private int length;
    private int width;

    public Rectangle(int length, int width) {
        this.length = length;
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public double getArea() {
        return this.length*this.width;
    }

    @Override
    public double getPerimeter() {
        return 2 * (this.width + this.length);
    }

    @Override
    public String toString() {
        return "Rectangle[" + this.length + ',' + this.width + ","
                + String.format("%.2f",this.getArea())
                + "," + String.format("%.2f",this.getPerimeter());
    }
}

class Cylinder{

    Shape shape;
    private double heigh;

    public Cylinder(double heigh,Shape shape) {
        this.heigh = heigh;
        this.shape =  shape;
    }

    @Override
    public String toString() {
        return shape.toString()
                + "," + String.format("%.2f",this.heigh * shape.getArea()) + "]\n";
    }
}