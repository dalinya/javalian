import java.util.Queue;

class A{
    int i = 9,j;
    public A(){
        prt("i="+i+",j="+j);
        j=10;
    }
    static {
        int x1 = prt("A is super class.");
    }
    static int prt(String s) {
        System.out.println(s);
        return 11;

    }
}

public class B extends A{
    int k = prt("B is key.");
    public  B(){
        prt("k=" + k + ",j="+j);
    }
    static int x2 = prt("B is child class");

    public static void main(String[] args) {
        prt("A is key");
        B b = new B();
    }
}
