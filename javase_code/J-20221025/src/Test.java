import javax.swing.*;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        String pwd = sc.nextLine();
        if(checkName(name)&&checkPwd(pwd)){
            System.out.println("用户名和密码正确");
        }else {
            System.out.println("用户名或密码错误");
        }
    }
    public  static boolean checkName(String name){
        return name.matches("\\D\\w{5,9}");

    }
    public  static boolean checkPwd(String pwd){
        return pwd.matches("\\w{6,20}");

    }
}
