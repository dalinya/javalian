import java.util.Scanner;

public class Test2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()){
            int num = sc.nextInt();
            int count = 0;
            for (int i = 0; i < num; i++) {
                int money = sc.nextInt();
                if (money >= 100) {
                    count += money / 100;
                    money %= 100;
                }
                if (money >= 50) {
                    count += money / 50;
                    money %= 50;
                }
                if (money >= 10) {
                    count += money / 10;
                    money %= 10;
                }
                if (money >= 5) {
                    count += money / 5;
                    money %= 5;
                }
                if (money >= 2) {
                    count += money / 2;
                    money %= 2;
                }
                if (money >= 1) {
                    count += money;
                }
            }
            System.out.println(count);
        }
    }
}
