import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

class Person{
    private String name;
    private int age;
    String sex;//默认是default权限

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void show(){
        System.out.println("姓名：" + name + " 年龄：" + age);

    }

}
public class Test {
    public static void main(String[] args) {
        Person person = new Person();
        //person.name = "张三";
        person.setName("张三");
        person.setAge(21);
        person.show();
    }


}
