public class Student {
    public String name;
    public int age;
    public static String classes = "2021级软件工程";

    {
        System.out.println("实例化代码块");
    }

    static{
        System.out.println("静态代码块");
    }
    public Student(String name,int age){
        System.out.println("带两个参数的构造方法");
        this.age = age;
        this.name = name;
    }

    public void print(){
        System.out.println(this.name + " => "+ this.age + "->" + Student.classes);
    }

    public static void main(String[] args) {
        Student student1 = new Student("张三",18);
        /*Student student2 = new Student("李四",20);
        Student student3 = new Student("王五",21);
        Student.classes = "2022级软件工程二班";*/
        student1.print();
//        student2.print();
//        student3.print();
    }
}
