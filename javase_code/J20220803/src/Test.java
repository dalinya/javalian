import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        int[] arr = new int[]{2,7,11,15};
        int target = 9;
        System.out.println(Arrays.toString(twoSum(arr, target)));

    }
    public static int[] twoSum(int[] nums, int target) {
        int[] ret = new int[2];
        for(int i = 0;i<nums.length;i++){
            for (int j = i + 1; j < nums.length; j++) {
                if(nums[i] + nums[j] == target){
                    ret[0] = nums[i];
                    ret[1] = nums[j];
                    return ret;
                }
            }
        }
        return ret;
    }
    public int majorityElement(int[] nums) {
        int ret = nums[0];
        int count = 0;
        for(int i = 0;i<nums.length;i++){
            if(nums[i] == ret){
                count++;
            }else{
                count--;
            }
            if(count == 0){
                ret = nums[i + 1];
            }
        }
        return ret;
    }
    public boolean threeConsecutiveOdds(int[] arr) {
        int count = 0;
        for(int i = 0;i < arr.length;i++){
            if(arr[i] % 2 == 1){
                count++;
            }else{
                count = 0;
            }
            if(count == 3){
                return true;
            }
        }
        return false;
    }
}
