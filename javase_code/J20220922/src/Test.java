import java.util.Date;
import java.util.Scanner;

abstract class Employee{
    protected String name;
    protected Date birthDate;
    abstract public double getSalary(Employee e);
}

class SalariedEmployee extends Employee{

    private double salary;
    @Override
    public double getSalary(Employee e) {
        return this.salary;
    }
    public SalariedEmployee(double salary) {
        this.salary = salary;
    }

    public SalariedEmployee(double salary, String name, Date birthDate) {
        this.salary = salary;
        super.name = name;
        super.birthDate =  birthDate;
    }
}

class HourlyEmployee extends Employee{
    private double hourSalary;
    private double hours;
    @Override
    public double getSalary(Employee e) {
        double sum = 0;
        if (hours <= 160){
            sum = hourSalary*hours;
        }else{
            sum = hourSalary*160 + 1.5 * (hours-160)*hourSalary;
        }
        return sum;
    }

    public HourlyEmployee(double hourSalary, double hours) {
        this.hourSalary = hourSalary;
        this.hours = hours;
    }

    public HourlyEmployee(double hourSalary, double hours, String name, Date birthDate) {
        this.hourSalary = hourSalary;
        this.hours = hours;
        super.name = name;
        super.birthDate =  birthDate;
    }
}

class SalesEmployee extends Employee{

    protected double sale;
    protected double commissionRate;
    @Override
    public double getSalary(Employee e) {
        return sale*commissionRate;
    }

    public SalesEmployee(double sale, double commissionRate) {
        this.sale = sale;
        this.commissionRate = commissionRate;
    }

    public SalesEmployee(double sale, double commissionRate, String name, Date birthDate) {
        this.sale = sale;
        this.commissionRate = commissionRate;
        super.name = name;
        super.birthDate =  birthDate;
    }
}
class BasePlusSalesEmployee extends SalesEmployee{
    private double basicSalary;

    public BasePlusSalesEmployee(double basicSalary,double sale, double commissionRate) {
        super(sale, commissionRate);
        this.basicSalary = basicSalary;
    }

    public BasePlusSalesEmployee(double sale, double commissionRate, String name, Date birthDate, double basicSalary) {
        super(sale, commissionRate, name, birthDate);
        this.basicSalary = basicSalary;
    }
    @Override
    public double getSalary(Employee e) {
        return (this.basicSalary + super.getSalary(null));
    }
}
public class Test {
    public static void func(Employee e){
        System.out.println(e.getSalary(null));
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        SalariedEmployee salariedEmployee = new SalariedEmployee(scanner.nextDouble());
        func(salariedEmployee);
        HourlyEmployee hourlyEmployee = new HourlyEmployee(scanner.nextDouble(),scanner.nextDouble());
        func(hourlyEmployee);
        SalesEmployee salesEmployee = new SalesEmployee(scanner.nextDouble(),scanner.nextDouble());
        func(salesEmployee);
        BasePlusSalesEmployee basePlusSalesEmployee = new BasePlusSalesEmployee(scanner.nextDouble(),scanner.nextDouble(),scanner.nextDouble());
        func(basePlusSalesEmployee);


    }

}
