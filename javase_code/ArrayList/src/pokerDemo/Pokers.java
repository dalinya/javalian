package pokerDemo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Pokers {
    public  static final String[] SUITS = {"♥","♠","♣","♦"};
    /**
     * 买一副扑克牌
     */
    public static List<Poker> buyPokers(){
        List<Poker> pokerList = new LinkedList<>();
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j <= 13; j++) {
                String suit = SUITS[i];
                Poker poker = new Poker(suit,j);
                pokerList.add(poker);
            }
        }
        return pokerList;
    }


    public static void swap(List<Poker> pokerList,int i,int j){
        Poker tmp = pokerList.get(i);
        pokerList.set(i,pokerList.get(j));
        pokerList.set(j,tmp);
    }

    public static void shuffle(List<Poker> pokerList){
        Random random = new Random();
        for (int i = pokerList.size() - 1; i > 0; i--) {
            int index = random.nextInt(i);
            swap(pokerList,i,index);
        }
    }
    public static void main(String[] args) {
        List<Poker> pokerList = buyPokers();
        System.out.println("买牌：" + pokerList);
        shuffle(pokerList);
        System.out.println("洗牌：" + pokerList);
        //接拍：3个人 每个人轮流抓5张牌
        //1.如何描述3个人
        List<Poker> hand1 = new ArrayList<>();
        List<Poker> hand2 = new ArrayList<>();
        List<Poker> hand3 = new ArrayList<>();
        //2.如何区分 往哪个人手里放牌？
        List<List<Poker>> hand = new ArrayList<>();
        hand.add(hand1);
        hand.add(hand2);
        hand.add(hand3);
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 3; j++) {
                List<Poker> handTmp = hand.get(j);//来确定是谁的手
                handTmp.add(pokerList.remove(0));
            }
        }
        for (int i = 0; i < hand.size(); i++) {
            System.out.println("第" + (i + 1) + "个人的牌是：" + hand.get(i));
        }
        System.out.println("剩余的牌：" + pokerList);
    }
}
