package implementation;

public class TestMyArrayList {
    public static void main(String[] args) {
        MyArraylist myArraylist = new MyArraylist();

        myArraylist.add(1);
        myArraylist.add(2);
        myArraylist.add(3);
        myArraylist.add(4);
        myArraylist.display();
        try {
            myArraylist.remove(-1);
        }catch (PosWrongfulException e){
            e.printStackTrace();
        }catch (EmptyException e){
            e.printStackTrace();
        }
        myArraylist.display();
    }
}
