package implementation;

import java.util.Arrays;

public class MyArraylist {
    public int[] elem;
    public int usedSize;//0
    //默认容量
    private static final int DEFAULT_SIZE = 10;
    public MyArraylist() {
        this.elem = new int[DEFAULT_SIZE];
    }
    /**
     * 打印顺序表:
     * 根据usedSize判断即可
     */
    public void display() {
        for (int i = 0; i < this.usedSize; i++) {
            System.out.print(this.elem[i] + " ");
        }
        System.out.println();
    }

    // 新增元素,默认在数组最后新增
    public void add(int data) {
        if (isFull()){
            this.elem = Arrays.copyOf(this.elem,this.usedSize * 2);
        }
        this.elem[this.usedSize] = data;
        this.usedSize++;
    }

    /**
     * 判断当前的顺序表是不是满的！
     *
     * @return true:满   false代表空
     */
    public boolean isFull() {
        return this.elem.length == this.usedSize;
    }
    private boolean checkPosInAdd(int pos) {
        if (pos < 0 || pos > this.usedSize){
            return false;
        }
        return true;//合法
    }

    // 在 pos 位置新增元素
    public void add(int pos, int data) {
        if(!checkPosInAdd(pos)){
            throw new PosWrongfulException("插入位置错误!!!");
        }
        if (isFull()){
            this.elem = Arrays.copyOf(this.elem,this.usedSize * 2);
        }
        int end = this.usedSize;
        while(end > pos){
            this.elem[end] = this.elem[end - 1];
            end--;
        }
        this.elem[pos] = data;
        this.usedSize++;
    }

    // 判定是否包含某个元素
    public boolean contains(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if (this.elem[i] == toFind){
                return true;
            }
        }
        return false;
    }
    // 查找某个元素对应的位置
    public int indexOf(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if (this.elem[i] == toFind){
                return i;
            }
        }
        return -1;
    }

    // 获取 pos 位置的元素
    public int get(int pos) {
        if (isEmpty()){
            throw new EmptyException("该顺序表为空");
        }
        if (pos >= this.usedSize || pos < 0){
            throw new PosWrongfulException("获取位置不合法");
        }
        return this.elem[pos];
    }

    private boolean isEmpty() {
        return this.usedSize == 0;
    }

    // 给 pos 位置的元素设为【更新为】 value
    public void set(int pos, int value) {
        if (isEmpty()){
            throw new EmptyException("该顺序表为空");
        }
        if (pos >= this.usedSize || pos < 0){
            throw new PosWrongfulException("设置位置不合法");
        }
        this.elem[pos] = value;

    }

    /**
     * 删除第一次出现的关键字key
     *
     * @param key
     */
    public void remove(int key) {
        if (isEmpty()){
            throw new EmptyException("该顺序表为空");
        }
        int index = this.indexOf(key);
        if(index == -1){
            System.out.println("找不到数字");
            return;
        }
        while (index < this.usedSize - 1){
            this.elem[index] = this.elem[index + 1];
            index++;
        }
        this.usedSize--;
    }

    // 获取顺序表长度
    public int size() {
        return this.usedSize;
    }

    // 清空顺序表
    public void clear() {
        this.usedSize = 0;
    }
}