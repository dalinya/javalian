import java.util.*;
class Student implements Comparable<Student>{
    private int age;
    private String name;
    private double score;

    public Student(int age, String name, double score) {
        this.age = age;
        this.name = name;
        this.score = score;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", score=" + score +
                '}';
    }

    @Override
    public int compareTo(Student o) {
        return (int) (this.score - o.score);
    }
}


public class Test {

    public static ArrayList<Character>  func(String str1,String str2) {
        ArrayList<Character> arrayList = new ArrayList<>();
        for (int i = 0; i < str1.length(); i++) {
            char ch = str1.charAt(i);
            if(!str2.contains(ch+"")) {
                arrayList.add(ch);
            }
        }
        return arrayList;
    }

    public static List<Character>  func2(String str1,String str2) {
        List<Character> arrayList = new ArrayList<>();
        for (int i = 0; i < str1.length(); i++) {
            char ch = str1.charAt(i);
            if(!str2.contains(ch+"")) {
                arrayList.add(ch);
            }
        }
        return arrayList;
    }

    public static void main(String[] args) {
        List<Character> ret = func("welcome to bit","come");
        //System.out.println(ret);
        for (int i = 0; i < ret.size(); i++) {
            System.out.print(ret.get(i));
        }
    }

    public static void main4(String[] args) {
        ArrayList<Student> arrayList = new ArrayList<>();
        arrayList.add(new Student(10,"小王",49.9));
        arrayList.add(new Student(50,"小红",19.9));
        arrayList.add(new Student(10,"大胖",89.9));
        for (Student s: arrayList) {
            System.out.println(s);
        }
        //Arrays.sort(arrayList);//ERR 只能对数组进行操作
    }

    public static void main3(String[] args) {//遍历顺序表
        ArrayList<Integer> arrayList1 = new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        arrayList1.add(3);
        arrayList1.add(4);
        arrayList1.add(5);
        int size = arrayList1.size();
        for (int i = 0; i < size; i++) {//for循环+下标
            System.out.print(arrayList1.get(i) + " ");
        }
        System.out.println();
        for (int x: arrayList1) {//借助foreach循环遍历
            System.out.print(x + " ");
        }
        System.out.println();
        Iterator<Integer> it = arrayList1.iterator();
        while (it.hasNext()){//使用迭代器遍历
            System.out.print(it.next() + " ");
        }
        System.out.println();
        ListIterator<Integer> it2 = arrayList1.listIterator();
        while (it2.hasNext()){
            System.out.print(it2.next() + " ");
        }
    }

    public static void main2(String[] args) {
        ArrayList<Integer> arrayList1 = new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        arrayList1.add(3);
        arrayList1.add(4);
        arrayList1.add(5);
        List<Integer> list = arrayList1.subList(1,3);//包左不包右
        System.out.println(list);
        System.out.println("----------------------------");
        list.set(0,99);
        System.out.println(arrayList1);//变
        System.out.println(list);//变

       /* arrayList1.add(0,7);
        System.out.println(arrayList1);
        arrayList1.remove(new Integer(7));
        System.out.println(arrayList1);
        arrayList1.clear();
        System.out.println(arrayList1);*/
    }
    public static void main1(String[] args) {
        ArrayList<Integer> arrayList1 = new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        System.out.println(arrayList1);
        //创建一个容量为12的顺序表
        ArrayList<Integer> arrayList2 = new ArrayList<>(12);
        arrayList2.add(2);
        arrayList2.add(3);
        arrayList2.add(4);
        System.out.println(arrayList2);
        //利用其他 Collection 构建 ArrayList
        LinkedList<Integer> linkedList = new LinkedList<>();
        ArrayList<Integer> arrayList3 = new ArrayList<>(linkedList);
    }
}
