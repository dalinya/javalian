import java.util.Scanner;

class  Complex{
    int real1;
    int image1;
    int real2;
    int image2;
    public String add(){
        int image = this.image1 + this.image2;


        int real = this.real1 + this.real2;

        if (image == 0 && real == 0){
            return "0";
        }
        if (image == 0){
            return real + "i";
        }
        if(real == 0){
            return Integer.toString(image);
        }

        return (image + "+" + real + "i");
    }
    public static void main(String[] args) {
        Complex complex = new Complex();
        Scanner scanner = new Scanner(System.in);
        complex.image1 = scanner.nextInt();
        complex.real1 = scanner.nextInt();
        complex.image2 = scanner.nextInt();
        complex.real2 = scanner.nextInt();
        System.out.println(complex.add());
    }
}


class EllipseArea {
    double a;
    double b;

    public EllipseArea(double a, double b) {
        this.a = a;
        this.b = b;
    }

    double s() {
        return 3.1415926 * a * b;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        EllipseArea e = new EllipseArea(s.nextDouble(), s.nextDouble());
        if (e.s() == 0) {
            System.out.println("ERROR");
        } else {
            System.out.printf("%.2f", e.s());
        }
    }
}

public class Main {
    String b;
    String t;
    double p;
    String o;
    int m;
    Main(){}

    Main(String b,String t,String o,double p,int m){
        this.b = b;
        this.t = t;
        this.o = o;
        this.p = p;
        this.m = m;
    }

    void about(){
        System.out.printf("品牌：%s\n型号：%s\n操作系统：%s\n价格：%.1f\n内存：%d\n",b,t,o,p,m);
    }

    void call(int n){
        if(n == 1){
            System.out.println("正在给爸爸打电话");
        } else if (n == 2) {
            System.out.println("正在给妈妈打电话");
        } else if (n == 3) {
            System.out.println("正在给姐姐打电话");
        }else{
            System.out.println("您所拨打的电话为空号");
        }
    }

    void play(String name){
        System.out.println("正在播放" + name);
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
       String str = s.nextLine();
        String[] ss = str.split(" ");
        Main m = new Main(ss[0],ss[1],ss[2],Double.parseDouble(ss[3]),Integer.parseInt(ss[4]));
        m.about();
        m.call(Integer.parseInt(ss[5]));
        m.play(ss[6]);
    }
}

class Tri{

    static void isTri(double a,double b,double c){
        if(a >= b + c || c >= a + b || b >= a + c){
            System.out.println("这三条边不能构成三角形");
        }else {
            System.out.println("这三条边可以构成三角形");
        }

    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Tri.isTri(s.nextDouble(),s.nextDouble(),s.nextDouble());
    }

}



