import java.util.Arrays;
import java.util.EmptyStackException;
import java.util.Scanner;

public class MyArrayList {

    private int[] elem;//数组
    private int useSize;//记录有效数据的个数
    public static final int DEFAULT_SIZE = 5;
    public MyArrayList(){
        this.elem = new int[DEFAULT_SIZE];
    }

    public  void display(){
        for (int i = 0; i < this.useSize; i++) {
            System.out.print(this.elem[i] + " ");
        }
        System.out.println();
    }
    //新增元素
    public void add(int data){
        //检验顺序表是不是满了
        if(isFull()){
            //如果满了就要增容
            this.elem = Arrays.copyOf(this.elem,2*elem.length);
        }
        this.elem[this.useSize++] = data;

    }
    public boolean isFull(){
        return this.useSize >= this.elem.length;
    }
    public void add(int pos,int data)throws PosWrongfulException{
        if(isFull()){
            //如果满了就要增容
            this.elem = Arrays.copyOf(this.elem,2*elem.length);
        }
        if(pos < 0 || pos > this.useSize){
            throw new PosWrongfulException("pos位置不合法！");
        }
        int end = this.useSize;
        while(end > pos){
            this.elem[end] = this.elem[end - 1];
            end--;
        }
        this.elem[pos] = data;
        this.useSize++;
    }
    public boolean contains(int toFind){
        for (int i = 0; i < this.useSize; i++) {
            if (toFind == this.elem[i]){
                return true;
            }
        }
        return false;
    }
    public int indexOf(int toFind){
        for (int i = 0; i < this.useSize; i++) {
            if (toFind == this.elem[i]){
                return i;
            }
        }
        return -1;
    }

    public boolean isEmpty(){
        return this.useSize == 0;
    }
    public int get(int pos){
        if (isEmpty()){
            throw new EmptyException("当前顺序表为空！");
        }

        if(pos < 0 || pos > this.useSize){
            throw new PosWrongfulException("pos位置不合法！");
        }
        return elem[pos];
    }
    public void remove(int key){
        //1.顺序表是否为空
        if (isEmpty()){
            throw new EmptyException("当前顺序表为空！");
        }

        //2.是否找的到key
        int index = this.indexOf(key);
        if (index == -1){
            System.out.println("找不到这个数字！");
            return;
        }
        //3.找到删除
        while (index < this.useSize - 1){
            this.elem[index] = this.elem[index + 1];
            index++;
        }
        this.useSize--;
    }

    public int size(){
        return this.useSize;
    }
    public void clear(){
            this.useSize = 0;
    }


}

