
class Food{

}

class Fruit extends Food{


}

class Apple extends Fruit{

}

class Banana extends Fruit{

}

class Plate<T>{//设置泛型
    private T plate;
    public T getPlate(){
        return plate;

    }
    public  void  setPlate(T plate){
        this.plate = plate;

    }
}

public class Test {
    public static void main(String[] args) {
        Plate<Fruit> plate1 = new Plate<>();
        plate1.setPlate(new Fruit());
        fuc(plate1);

    }
    public static void fuc(Plate<? super Fruit> temp){
        temp.setPlate(new Apple());
        temp.setPlate(new Banana());
        temp.setPlate(new Fruit());
        //Fruit fruit = temp.getPlate();

    }

    public static void main1(String[] args) {
        Plate<Apple> plate1 = new Plate<>();
        plate1.setPlate(new Apple());
        fun1(plate1);
        Plate<Banana> plate2 = new Plate<>();
        plate2.setPlate(new Banana());
        fun1(plate2);

    }
    public  static void fun1(Plate<? extends Fruit> temp){
        //temp.setPlate(new Apple());//不能放东西
        Fruit fruit = temp.getPlate();


    }

}
