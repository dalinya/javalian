

public class TestDate {
    public int year = 1999;
    public int month = 12;
    public int day = 10;
    public TestDate(){

    }
    public TestDate(int year,int month,int day){
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public void setDate(int year,int month,int day){
        this.year = year;
        this.month = month;
        this.day = day;
    }
public void printDate(){
    System.out.println(this.year + "-"+this.month+"-"+this.day);
    }
    public static void main(String[] args) {
        TestDate testDate1 = new TestDate();
        TestDate testDate2 = new TestDate();
        TestDate testDate3 = new TestDate();

        testDate1.setDate(2022,8,4);
        testDate2.setDate(2023,8,4);
        testDate3.setDate(2024,8,4);

        testDate1.printDate();
        testDate2.printDate();
        testDate3.printDate();

    }
}