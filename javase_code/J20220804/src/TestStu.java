public class TestStu {
    public String name;
    public int age;

    public TestStu(){
        this("李四",31);
        System.out.println("不带参数的狗藏方法");
    }
    public TestStu(String name,int age){
        System.out.println("带两个参数的构造方法");
        this.name = name;
        this.age = age;
    }

    public void stuSet(String name,int age){
        this.name = name;
        this.age = age;
    }
    public void print(){
        System.out.println(this.name+ "==>" + this.age);
    }
    public void fuc(){
            this.print();
    }
    public static void main(String[] args) {
        TestStu stu = new TestStu();
        stu.print();

        stu.fuc();
    }
}
