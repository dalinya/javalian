import jdk.internal.org.objectweb.asm.tree.MultiANewArrayInsnNode;

import java.util.Arrays;

class Dog{
    //属性 -> 成员变量
    public String name;
    public int age;
    //行为 -> 成员方法
    public void eat(){
        int count = 0;
        System.out.println(name + "正在吃饭");
    }
    public void barks(){
        System.out.println(name + "正在汪汪");
    }
}


public class Class {
    public static void main(String[] args) {


    }

    public static void main9(String[] args) {
        Dog dog1 = new Dog();
        dog1.name = "张三";
        dog1.age = 12;
        System.out.println(dog1.name);
        System.out.println(dog1.age);
    }


    public static void main8(String[] args) {
        int[][] array1 = new int[2][3];
        array1[0] = new int[3];
        array1[1] = new int[4];

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < array1[i].length; j++) {
                System.out.print(array1[i][j] + " ");
            }
            System.out.println();
        }
        for (int[] arr:array1) {
            for (int x:arr) {
                System.out.print(x+" ");
            }
            System.out.println();
        }
        System.out.println(Arrays.deepToString(array1));
    }

    public static void main7(String[] args) {
        int[] array1 = new int[10];

        Arrays.fill(array1,2,5,-1);

        System.out.println(Arrays.toString(array1));

        int[] array2 = array1;
        boolean flg = Arrays.equals(array1,array2);
        System.out.println(flg);
    }


    //模拟实现字符串转化函数
    public static void main6(String[] args) {
        int[] array = new int[]{1,2,3,4,5,6,7,8,9,10};
        System.out.println(myToString(array));
    }
    public static String myToString(int[] array){
        if(array == null){
            return "null";
        }
        String str = "[";
        for (int i = 0; i < array.length; i++) {
            str += array[i];
            if(i<array.length - 1){
                str += ",";
            }
        }
        str+="]";
        return str;
    }

    public static void main5(String[] args) {
        int[] array = {1,2,3,4,5,6,7,8,9,10};
        System.out.println(Arrays.toString(array));
    }

    public static void main4(String[] args) {
        int[] array = {1,2,3,4,5,6,7,8,9,10};
        for (int x:array) {
            System.out.print(x + " ");
        }
    }


    public static void main3(String[] args) {
        int[] array = {1,2,3,4,5,6,7,8,9,10};
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

    public static void main2(String[] args) {
        int[] array = new int[10];//创建一个可以容纳10个int类型元素的数组，数组中的元素默认为0
        int[] array1 = {1,2,3,4,5,6,7,8,9,10};//直接赋值，静态初始化
        int[] array2 = new int[]{1,2,3,4};//在创建数组时，直接指定数组中元素的个数，动态初始化
        int[] array3 = new int[4];
        //array3 = {1,2,3,4,5};//err,不能对数组创建完成之后再进行静态初始化
        array3 = new int[]{1,2,3,4};

        //数组可以是各种类型
        String[] str = new String[10];
        boolean[] flg = new boolean[10];
        char[] chars = new char[10];

        System.out.println(Arrays.toString(array));


    }


}
