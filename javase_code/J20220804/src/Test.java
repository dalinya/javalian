class Person{
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

public class Test {
    public static void main(String[] args) {
        Person per = new Person();
        per.setAge(32);
        per.setName("李四");
        System.out.println(per.getName()+per.getAge());
    }

}
