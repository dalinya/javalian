import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n + 1];
        for (int i = 0; i < n + 1; i++) {
            arr[i] = scanner.nextInt();
        }
        Arrays.sort(arr);
        System.out.println(n + 1);
        for (int x:arr) {
            System.out.print(x + " ");
        }

    }
    public static void main77(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int[] arr = new int[num];
        for (int i = 0; i < num; i++) {
            arr[i] = scanner.nextInt();
        }
        Arrays.sort(arr);
        int x = arr[arr.length/2];
        int count = 0;
        for (int y: arr) {
            if(y==x){
                count++;
            }
        }
        if(count > (arr.length/2)){
            System.out.println(count);
        }else{
            System.out.println("no");
        }

    }


    public static void mainysf(String[] args) {
        char[] chars = {1,2,3,4,5,6};
        int length = chars.length;

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int n = scanner.nextInt();//总人数
            int m = scanner.nextInt();//开始报数的人
            int[] arr = new int[n];
            for (int i = 0; i < n; i++) {
                arr[i] = i + 1;//初始默认未出列，
            }
            int k = scanner.nextInt();//要出列的编号
            int count = 0;//退出人数
            int key = 0;//循环报的数字
            while(count < n){
                if(arr[m] != 0){//报数的人
                    key++;
                }
                if(key == 3){//出队的人
                    System.out.print(arr[m] + " ");
                    arr[m] = 0;
                    count++;
                    key = 0;
                }
                m = (m + 1) % n;
            }

        }

    }


    public static void main112(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int num = scanner.nextInt();
            for (int i = -(num / 2); i <= num / 2; i++) {
                System.out.printf("%" + (num - Math.abs(i)) + "c",' ' );
                for (int j = 0; j < 2 * Math.abs(i) + 1; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        }
    }

    public static void main111(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int num = scanner.nextInt();
            for (int i = -(7 / 2); i <= num / 2; i++) {
                System.out.printf("%" + (36 + Math.abs(i)) + "c",' ' );
                for (int j = 0; j < num - 2 * Math.abs(i); j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        }
    }

    public static void main17(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()){
            int num = scanner.nextInt();
            System.out.print(num + "= 1");
            int i = 2;
            while(i <= num){
                boolean flg = isPriNum(i) && num % i == 0;
                if(flg){
                    System.out.print("x" + i);
                    num /= i;
                    continue;
                }
                    i++;
            }

        }

    }


    public static void main7(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //int num = scanner.nextInt();
        int count = 0;
        for (int i = 1; i <= 1000; i++) {
            if(isPriNum(i)){
                System.out.print(i + " ");
                count++;
                if(count % 10 == 0){
                    System.out.println();
                }
            }
        }
    }

    public static boolean isPriNum(int num){
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if(num %  i == 0){
                return false;
            }
        }
        return true;
    }


    public static void main6(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()){
            int year = scanner.nextInt();
            int month = scanner.nextInt();
            int day = 0;
            switch (month){
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    day = 31;
                    break;
                case 2:
                    if(isLeapYear(year)){
                        day = 29;
                    }else{
                        day = 28;
                    }
                    break;
                default:
                    day = 30;
            }
            System.out.println(year + "年" + month + "月有" + day + "天");
        }

    }
    public static boolean isLeapYear(int year){
        return (((year % 100 != 0)&&(year % 4 == 0))||(year % 400 == 0));
    }


    public static void main4(String[] args) {
        for (int i = 1; i <= 100 ; i++) {
            System.out.print(fibNum(i) + " ");
            if(i % 10 == 0){
                System.out.println();
            }

        }
    }
    public static long fibNum(int num){
        long a = 1;
        long b = 1;
        while(num > 2){
            long c = a + b;
            a = b;
            b = c;
            num--;
        }
        return b;
    }

    public static void main3(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        System.out.println(isPalNum(num));
    }
    public static boolean isPalNum2(int num){
        String str = String.valueOf(num);
        int left = 0;
        int right = str.length();
        while(left < right){
            if(str.charAt(left) != str.charAt(right)){
                return false;
            }
        }
        return true;
    }

    public static boolean isPalNum(int num){
        int tmp = num;
        int sum = 0;
        while(tmp != 0){
            sum = sum * 10 +  tmp % 10;
            tmp/=10;
        }
        return sum == num;
    }


    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        double fac = getFactorial2(num);
        System.out.println(fac);
    }

    public static double getFactorial2( int n){
        if(n > 1){
            return n*getFactorial2(n-1);
        }
        return n;
    }
    public static double getFactorial1 ( int n){//静态方法
        double fac = 1;
        for (int i = 1; i <= n; i++) {
            fac *= i;
        }
        return fac;

    }

    public static void main1(String[] args) {
        for (int i = 0; i < 128; i++) {
            System.out.print((char)i + " ");
            if(i%10==0){
                System.out.println();
            }
        }

    }

}
