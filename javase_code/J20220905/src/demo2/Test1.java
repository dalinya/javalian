package demo2;


import java.lang.reflect.Array;

class MyArray<T>{
    public T[] obj;
    public MyArray(){

    }
    public MyArray(Class<T> clazz, int capacity) {
        obj = (T[]) Array.newInstance(clazz,capacity);//反射相关，了解
    }
    public void setVal(int pos,T val){
        obj[pos] = val;
    }
    public T getPos(int pos){
        return obj[pos];
    }
    public T[] getObj2() {
        return obj;
    }



}


public class Test1 {
    public static void main(String[] args) {
    MyArray<Integer> myArray = new MyArray<>(Integer.class,10);
    Integer[] tmp = myArray.getObj2();

    }

    public static void main3(String[] args) {
        MyArray myArray = new MyArray();
        myArray.setVal(0,10);
        myArray.setVal(1,2);
        myArray.setVal(2,6);
        myArray.setVal(3,"hello");
        int a = (int)myArray.getPos(1);
        System.out.println(a);
    }


    public static void main2(String[] args) {
        MyArray<Integer> myArray = new MyArray<>();
        myArray.setVal(0,10);
        myArray.setVal(1,2);
        myArray.setVal(2,6);

        int a = myArray.getPos(1);
        System.out.println(a);
        MyArray<String> myArray1 = new MyArray<>();
        myArray1.setVal(0,"asdf");
        myArray1.setVal(1,"afsr");
        String str = myArray1.getPos(1);
        System.out.println(str);
    }

    public static void main1(String[] args) {
        Object[] array = {1,2,3,4,"array","ok"};
        //String[] array2 = (String[]) new Object[10];//Object类型数组数组不能转化


    }


}
