package demo2;

import demo.Test;

class Person implements Comparable<Person>{
    public int age;
    public Person(int age){
        this.age = age;
    }

    @Override
    public int compareTo(Person o) {
        return this.age - o.age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                '}';
    }
}



class Alg<T extends Comparable<T>>{
    public T findMax(T[] array){
        T max = array[0];
        for (int i = 1; i < array.length; i++) {
            if (max.compareTo(array[i])<0){
                max = array[i];
            }
        }
        return max;
    }
}
class TestDemo<E extends Number>{


}

class Alg2{
    public static <T extends Comparable<T>> T findMax(T[] array){
        T max = array[0];
        for (int i = 1; i < array.length; i++) {
            if (max.compareTo(array[i])<0){
                max = array[i];
            }
        }
        return max;
    }
}

class Student<T>{


}

class Alg3{
    public <T extends Comparable<T>> T findMax(T[] array){
        T max = array[0];
        for (int i = 1; i < array.length; i++) {
            if (max.compareTo(array[i])<0){
                max = array[i];
            }
        }
        return max;
    }

}

public class Test2 {
    public static void main(String[] args) {
        Student<Integer> student1 = new Student<>();
        Student<String> student2 = new Student<>();
        System.out.println(student1);
        System.out.println(student2);
    }

    public static void main4(String[] args) {
        Alg3 alg3 = new Alg3();
        Integer[] array = {1,2,3,4,5};
        Integer ret = alg3.<Integer>findMax(array);
        System.out.println(ret);
    }

    public static void main3(String[] args) {
        Integer[] array = {1,2,3,4};
        Integer ret = Alg2.<Integer>findMax(array);
        System.out.println(ret);
    }

    public static void main2(String[] args) {
        TestDemo<Integer> testDemo2 = new TestDemo<>();
        TestDemo<Number> testDemo21 = new TestDemo<>();
        TestDemo<Float> testDemo22= new TestDemo<>();
        TestDemo<Double> testDemo3 = new TestDemo<>();

    }

    public static void main1(String[] args) {
        Alg<Person> alg = new Alg<>();
        Person[] people = {new Person(10),new Person(23)};
        Person person = alg.findMax(people);
        System.out.println(person);

    }


}
