package demo1;
//抽象类

abstract class Shape{
    int a;
    public Shape(){

    }
    public void func(){
        System.out.println("哈哈哈");
    }
    public abstract  void draw();
}
class Rect extends Shape{

    @Override
    public void draw() {
        System.out.println("画矩形");
    }
}

class Cycle extends Shape{
    @Override
    public void draw() {
        System.out.println("画圆形");
    }
}

class Triangle extends Shape{

    @Override
    public void draw() {
        System.out.println("画一个三角形");
    }
}
class Flower extends Shape{
    @Override
    public void draw() {
        System.out.println("❀❀❀");
    }
}


public class Test {
    public static void drawMap(Shape shape){
        shape.draw();
    }
    public static void main(String[] args) {
        drawMap(new Rect());
        drawMap(new Triangle());
        drawMap(new Cycle());
        drawMap(new Flower());
    }
}
