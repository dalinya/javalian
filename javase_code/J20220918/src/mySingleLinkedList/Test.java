package mySingleLinkedList;

public class Test {
    public static void main(String[] args) {
        SingleLinkedList singleLinkedList = new SingleLinkedList();
        System.out.println(singleLinkedList.contains(4));

        singleLinkedList.addLast(2);
        singleLinkedList.addLast(2);
        singleLinkedList.addLast(3);
        singleLinkedList.addLast(2);
        singleLinkedList.display();

        singleLinkedList.removeAllKey(2);
        singleLinkedList.display();

    }
}
