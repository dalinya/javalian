package demo1;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;

class Student {
    public String name;
    public int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    /*@Override
    public int compareTo(Student o) {
        if (this.age - o.age > 0){
            return 1;
        } else if (this.age - o.age < 0) {
            return -1;
        }else {
            return 0;
        }
    }*/
}

class NameComparator implements Comparator<Student>{

    @Override
    public int compare(Student o1, Student o2) {
        return o1.name.compareTo(o2.name);
    }
}

public class Test {
    public static void main(String[] args) {
        Student[] students = new Student[3];
        students[0] = new Student("zhang san",18);
        students[1] = new Student("lisi",21);
        students[2] = new Student("wang wu",17);
        NameComparator nameComparator = new NameComparator();
        Arrays.sort(students,nameComparator);
        System.out.println(Arrays.toString(students));
    }
}
