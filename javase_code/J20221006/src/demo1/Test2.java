package demo1;

class Money implements Cloneable{
    public double m = 12.5;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

class Person implements Cloneable{
    public int id;
    Money money = new Money();

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Person tmp = (Person) super.clone();
        this.money = (Money) this.money.clone();

        return tmp;
    }
}

public class Test2 {
    public static void main(String[] args) throws CloneNotSupportedException {
        Person person1 = new Person();
        person1.money.m = 10;
        Person person2 = (Person) person1.clone();
        person1.money.m = 20;
        System.out.println(person2.money.m);
    }
}
