import java.util.Random;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        for (int i = 31; i > 0; i-=2) {
            System.out.print((num>>i)&1);
        }
        System.out.println("");
        for (int i = 30; i >= 0; i-=2) {
            System.out.print((num>>i)&1);
        }
    }

    public static void main12(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("请输入密码:");
        for (int i = 0; i < 3; i++) {
            String str = scan.nextLine();
            if(str.equals("12345")){
                System.out.println("登录成功");
                break;
            }else{
                System.out.println("密码错误，请重新选择：");
            }
        }
    }
    public static void main11(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        while(num!=0){
            System.out.print(num % 10 + " ");
            num /= 10;
        }
    }

    public static void main10(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        for (int i = 1; i <= num; i++) {
            for (int j = 1; j <=  i; j++) {
                System.out.printf("%d * %d = %d ", i, j,i * j);
            }
            System.out.println("");
        }
    }

    public static void main9(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int num = in.nextInt();
            for(int i = 0;i < num; i++){
                for(int j = 0; j < num; j++){
                    if(i==j||i+j==num-1){
                        System.out.print("*");
                    }else{
                        System.out.print(" ");
                    }
                }
                System.out.println("");
            }
        }
    }
    public static void main8(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        for (int i = 100; i <= num; i++) {
            int sum = 0;
            int tmp = i;
            while(tmp != 0){
                sum += (tmp % 10)*(tmp % 10)*(tmp % 10);
                tmp /= 10;
            }
            if(sum==i){
                System.out.println(i);
            }
        }
    }

    public static void main7(String[] args) {
        double sum = 0.0;
        int ret = 1;
        for (int i = 1; i <= 100 ; i++) {
            sum += (1.0*ret)/i;
            ret = -ret;
        }
        System.out.println(sum);
    }

    public static void main6(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        int count = 0;
        while(num!=0) {
            num = num&(num-1);
            count++;
        }
        System.out.println(count);
    }

    public static void main5(String[] args) {

        Random random = new Random();
        int num = random.nextInt(100);
        Scanner scan = new Scanner(System.in);
        while(true){
            System.out.println("请输入要猜的数字");
            int key = scan.nextInt();
            if(key<num){
                System.out.println("猜小了");
            }else if(key>num){
                System.out.println("猜大了");
            }else{
                System.out.println("恭喜你，猜对了！！");
                break;
            }
        }
    }

    public static void main4(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("请输入你的年龄：");
        int age = sc.nextInt();
        System.out.println("请输入你的姓名：");
        String name = sc.nextLine();

        System.out.println(age);
        System.out.println(name);

    }
    public static void main3(String[] args) {
        int num = 3;
        while(true) {
            if(num%5==0){
                System.out.println(num);
            }
            num+=3;
            if(num>100){
                break;
            }

        }

    }
    public static void main2(String[] args) {
        int i = 1;
        int sum = 0;
        while(i<=5){
            int ret  = 1;
            int j = 1;
            while(j<=i){
                ret*=j;
                j++;
            }
            sum+=ret;
            i++;
        }
        System.out.println(sum);

    }
    public static void main1(String[] args) {
        int i = 1;
        int sum = 0;
        int sumj = 0;
        int sumo = 0;
        while(i <= 100){
            sum += i;
            if(i % 2 == 0){
                sumo += i;
            }else {
                sumj += i;
            }
            i++;
        }
        System.out.println("和" + sum);
        System.out.println("奇数和" + sumj);
        System.out.println("偶数和" + sumo);
    }


}
