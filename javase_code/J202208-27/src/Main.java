import javax.swing.*;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        for (int i = 1; i <= 1000; i++) {
            if(isPerNum(i)){
                System.out.println(i);
            }
        }
    }
    public static boolean isPerNum(int num){
        int sum = 0;
        for (int i = 1; i < num; i++) {
            if(num % i ==0){
                sum += i;
            }
        }
        return sum == num;
    }

    public static void main5(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextInt()){
            int num = sc.nextInt();
            int count = 0;
            for (int i = 0; i < num; i++) {
                int money = sc.nextInt();
                if(money >= 100){
                    count += (money/100);
                    money %= 100;
                }
                if(money >= 50){
                    count += (money/50);
                    money %= 50;
                }
                if(money >= 10){
                    count += (money/10);
                    money %= 10;
                }
                if(money >= 5){
                    count += (money/5);
                    money %= 5;
                }
                if(money >= 2){
                    count += (money/2);
                    money %= 2;
                }
                if(money >= 1){
                    count += (money/1);
                    money %= 100;
                }
            }
            System.out.println(count);
        }
    }

    public static void main4(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextInt()){
            int num = sc.nextInt();
            int sum = 1;
            for (int i = 1; i < num; i++) {
                sum = 2 * (sum + 1);
            }

            System.out.println(sum);
        }
    }

    public static void main3(String[] args) {
        Scanner sc = new Scanner(System.in);
        int year = sc.nextInt();
        int month = sc.nextInt();
        int[] leapyear = {0,31,29,31,30,31,30,31,31,30,31,30,31};
        int[] noLeapyear = {0,31,28,31,30,31,30,31,31,30,31,30,31};
        if(isLeapYear(year)){
            System.out.println(leapyear[month]);
        }else {
            System.out.println(noLeapyear[month]);
        }
    }
    public static boolean isLeapYear(int year){
        if (((year % 4 == 0)&&(year % 100 != 0))||(year % 400 == 0)){
            return true;
        }
        return false;
    }

    public static void main2(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        for (int i = 2; i <= num; i++) {
            boolean flg = true;
            for (int j = 2; j <= Math.sqrt(i); j++) {
                if(i % j == 0){
                    flg = false;
                }
            }
            if(flg){
                System.out.print(i + " ");
            }
        }
    }
    public static void main1(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        if(num >= 1 && num <= 20){
            double e = 1.0;
            int ret = 1;
            for (int i = 1; i <= num; i++) {
                ret *= i;
                e += 1.0/ret;
            }
            System.out.printf("e=%.6f",e);
        }else{
            System.out.println("Invalid data!");
        }
    }
}
