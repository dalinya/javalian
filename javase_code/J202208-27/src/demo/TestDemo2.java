package demo;

class A {

}

class B extends A {

}

final class C extends B {

}

/*class D extends C{//ERROR

}*/
public class TestDemo2 extends TestDemo{

    public void fun(){
        /*TestDemo testDemo = new TestDemo2();
        testDemo.a;*/
        //ERROR对于protected权限的成员只能用super调用


        System.out.println(super.a);
        System.out.println(super.b);
        System.out.println(super.c);
        //System.out.println(super.d);//ERROR


    }
    public static void main(String[] args) {
        TestDemo2 test = new TestDemo2();
        test.fun();
    }
}
