package inherit;
class Base{
    int a;
    int b;
    public void methodA(){
        System.out.println("Base::methodA");
    }
    public void methodB(){
        System.out.println("Base::methodB");
    }
}
public class Derived extends Base {
    int a;
    int c;
    public void methodA(){
        System.out.println("Derived::methodA");
    }
    public void methodC(){
        System.out.println("Derived::methodC");
    }
    public void method(){
        methodA();
        super.methodA();
        methodB();

        a = 10;
        b = 20;
        c = 30;
        System.out.println(super.a);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }

    public static void main(String[] args) {
        Derived derived = new Derived();
        derived.method();
    }
}
