package inherit;
class Animal{//将Dog和Cat中的共性进行抽取

    {//实例代码块
        System.out.println("实例代码块Animal");
    }
    static {
        System.out.println("静态代码块Animal");
    }
    public Animal(){
        System.out.println("构造方法Animal");
    }
}

class Dog extends Animal{
    {//实例代码块
        System.out.println("实例代码块Dog");
    }
    static {
        System.out.println("静态代码块Dog");
    }
    public Dog(){
        System.out.println("构造方法Dog");
    }
}

/*class cat extends Animal{
    boolean Lovely;

    public void catchMouse(){
        System.out.println(name + "正在抓老鼠");
    }
}*/

public class Test {
    public static void main(String[] args) {
        Dog dog1 = new Dog();
        System.out.println("==============================");
        Dog dog2 = new Dog();
    }
}
