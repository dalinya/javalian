import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Test {
    public class TreeNode {
          int val;
          TreeNode left;
          TreeNode right;
          TreeNode() {}
          TreeNode(int val) { this.val = val; }
          TreeNode(int val, TreeNode left, TreeNode right) {
              this.val = val;
             this.left = left;
             this.right = right;
          }
     }
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        if(root == null){
            return list;
        }

        list.add(root.val);
        List<Integer> listLeft = preorderTraversal(root.left);
        list.addAll(listLeft);
        List<Integer> listRight = preorderTraversal(root.right);
        list.addAll(listRight);
        return list;
    }
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        if(root == null){
            return list;
        }
        List<Integer> listLeft = inorderTraversal(root.left);
        list.addAll(listLeft);
        list.add(root.val);
        List<Integer> listRight = inorderTraversal(root.right);
        list.addAll(listRight);
        return list;
    }

    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        if(root == null){
            return list;
        }
        List<Integer> listLeft = postorderTraversal(root.left);
        list.addAll(listLeft);
        List<Integer> listRight = postorderTraversal(root.right);
        list.addAll(listRight);
        list.add(root.val);
        return list;
    }
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> list = new ArrayList<>();
        if(root == null) {
            return list;
        }
        Queue<TreeNode> qu = new LinkedList<>();
        qu.offer(root);
        while(!qu.isEmpty()){
            List<Integer> tmp = new ArrayList<>();
            int size = qu.size();
            while(size > 0){
                TreeNode cur = qu.poll();
                tmp.add(cur.val);
                if(cur.left != null){
                    qu.offer(cur.left);
                }
                if (cur.right != null){
                    qu.offer(cur.right);
                }
                size--;
            }
            list.add(tmp);
        }
        return list;
    }
    public boolean isSubtree(TreeNode root, TreeNode subRoot) {
        if (root == null){
            return false;
        }
        if (isSameTree(root,subRoot)){
            return true;
        }
        return isSubtree(root.right,subRoot)
                || isSubtree(root.left ,subRoot);
    }
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if(p == null && q == null){
            return true;
        }
        if (p == null && q != null || p != null && q == null){
            return false;
        }
        if (p.val != q.val){
            return false;
        }
        return isSameTree(p.left,q.left) && isSameTree(p.right,q.right);
    }
    public boolean isBalanced(TreeNode root) {
        if (root == null){
            return true;
        }
        if (Math.abs(maxDepth(root.left) - maxDepth(root.right)) > 1){
            return false;
        }
        return isBalanced(root.right) && isBalanced(root.left);

    }
    public boolean isBalanced2(TreeNode root) {

        return isBalancedChild(root) != -1;
    }
    public int isBalancedChild(TreeNode root) {
        if(root == null){
            return 0;
        }
        int left = isBalancedChild(root.left);
        int right = isBalancedChild(root.right);
        if(Math.abs(left - right) > 1 || left == -1 || right == -1){
            return -1;
        }
        return Math.max(left,right) + 1;

    }
    public int maxDepth(TreeNode root) {
        if(root == null){
            return 0;
        }
        return Math.max(maxDepth(root.left),maxDepth(root.right)) + 1;

    }
    public boolean isSymmetric(TreeNode root) {
        if(root == null){
            return true;
        }
        return isSymmetricChild(root.left,root.right);
    }
    public boolean isSymmetricChild(TreeNode leftRoot,TreeNode rightRoot){
        if(leftRoot == null && rightRoot == null){
            return true;
        }
        if (leftRoot != null && rightRoot == null
                || leftRoot == null && rightRoot != null){
            return false;
        }
        if (leftRoot.val != rightRoot.val){
            return false;
        }
        return isSymmetricChild(leftRoot.right,rightRoot.left) && isSymmetricChild(leftRoot.left,rightRoot.right);
    }

    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        }
        TreeNode tmp = root.left;
        root.left = root.right;
        root.right = tmp;
        invertTree(root.left);
        invertTree(root.right);
        return root;
    }
}
