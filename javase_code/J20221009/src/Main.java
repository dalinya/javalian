import java.util.Scanner;
class TreeNode {
    char val;
    TreeNode left;
    TreeNode right;
    TreeNode(char val) {
        this.val = val;
    }
}
public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        TreeNode root =  createTree(str);
        inOrder(root);
    }
    private static int i = 0;
    private static TreeNode createTree(String str){
        if (str == null){
            return null;
        }
        TreeNode root = null;
        if (str.charAt(i) != '#'){
            root = new TreeNode(str.charAt(i));
            i++;
            root.left = createTree(str);
            root.right = createTree(str);
        }else {
            i++;
        }
        return root;

    }
    static void inOrder(TreeNode root){
        if (root == null){
            return;
        }
        inOrder(root.left);
        System.out.print(root.val + " ");
        inOrder(root.right);
    }
}