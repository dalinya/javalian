class Animal{
    public String name;
    public int age;
    public Animal(String name,int age){
        this.age = age;
        this.name = name;
        System.out.println("Animal(String , int )");

    }
    public void eat(){
        System.out.println(name+"正在吃饭");
    }
    public void fuction(){
        System.out.println("Animal::function()!");
    }
}
class Dog extends Animal{
    public boolean silly;
    public Dog(String name,int age,boolean silly){
        super(name, age);
        this.silly = silly;
        System.out.println("Dog(String, int ,boolean)");
    }

    public void houseGuard() {
        System.out.println(name + "正在看家护院");
    }

    @Override
    public void eat() {
        System.out.println(name + "正在吃狗粮");
    }
}

class Cat extends Animal{
    public Cat(String name,int age){
        super(name,age);
    }
    public void catchMouse(){
        System.out.println(name + "抓老鼠");
    }

    @Override
    public void eat() {
        System.out.println(name + "吃猫粮");
    }
}

public class Test {
    public static void main(String[] args) {
        Animal animal  = new Dog("旺财",10,false);
        if(animal instanceof Cat){
            Cat cat = (Cat)animal;
            cat.catchMouse();
        }
    }

    public static void main4(String[] args) {
        Animal animal2 = new Cat("咪咪",2);
        Cat cat = (Cat)animal2;
        cat.catchMouse();
    }

    public static void function2(Animal animal){
        animal.eat();
    }
    public static void main3(String[] args) {
        Dog dog  = new Dog("旺财",10,false);
        function2(dog);

        Cat cat = new Cat("咪咪",2);
        function2(cat);
    }

    public static void main2(String[] args) {
        Dog dog  = new Dog("旺财",10,false);
        Animal animal = dog;
        animal.eat();
        Animal animal2 = new Dog("来福", 12,false);
        Animal animal3 = new Cat("咪咪",2);
        animal3.eat();

    }
    public static void main1(String[] args) {
        Dog dog  = new Dog("旺财",10,false);
        System.out.println("=============");
        Dog dog2 = new Dog("来福",12,false);
    }

}
