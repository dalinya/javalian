public class Test {
    public static void main(String[] args) {
        BinaryTree binaryTree = new BinaryTree();
        BinaryTree.TreeNode treeNode =  binaryTree.createTree("ABC##DE#G##F###");
        binaryTree.preOrder(treeNode);
        System.out.println();
        binaryTree.inOrder(treeNode);
        System.out.println();
        binaryTree.postOrder(treeNode);
        binaryTree.size(treeNode);
        System.out.println(BinaryTree.nodeSize);
        System.out.println(binaryTree.size2(treeNode));
        binaryTree.getLeafNodeCount1(treeNode);
        System.out.println(binaryTree.leafSize);
        System.out.println(binaryTree.getLeafNodeCount2(treeNode));
        System.out.println(binaryTree.getKLevelNodeCount(treeNode,3));
        System.out.println("高度：" + binaryTree.getHeight(treeNode));
        System.out.println("找结点：" + binaryTree.find(treeNode,'F').val);
        System.out.println("是否为完全二叉树：" + binaryTree.isCompleteTree(treeNode));
        binaryTree.levelOrder(treeNode);
    }

}
