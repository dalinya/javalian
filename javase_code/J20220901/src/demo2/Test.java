package demo2;
//接口中可以包含static方法和default方法
interface ITest{
    int size = 10;//默认public static final修饰
    void draw();//默认public abstract修饰
    default public void func(){
        System.out.println("默认方法");
    }
    public static void func2(){
        System.out.println("hello");
    }
}

class  A implements ITest{
    @Override
    public void draw() {
        System.out.println("必须重写！");
    }
     /*@Override
    public void func() {
        System.out.println("这个方法的重写 是可以选择的! ");
    }*/
}


public class Test {
    public static void main(String[] args) {
        A a = new A();
    }
}
