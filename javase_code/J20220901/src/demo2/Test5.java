package demo2;

import sun.java2d.pipe.AAShapePipe;

import java.util.Scanner;

interface IFLying{
    void flying();
}
interface ISwimming{
    void swimming();
}
interface IRunning{
    void running();
}
//这些接口抽取了对象的一些性质，可以弥补内有多继承的缺陷
class Animal{
    public String name;
    public int age;
    public Animal(String name,int age){
        this.name = name;
        this.age = age;
    }
    public void eat(){
        System.out.println("吃饭");
    }

}
class Dog extends Animal implements ISwimming,IRunning{

    public Dog(String name, int age) {
        super(name, age);
    }

    @Override
    public void swimming() {
        System.out.println(name + "正在游泳");
    }

    @Override
    public void running() {
        System.out.println(name + "正在跑");
    }

    @Override
    public void eat() {
        System.out.println(name + "正在吃狗粮");
    }
}

class  Bird extends Animal implements IFLying{

    public Bird(String name, int age) {
        super(name, age);
    }

    @Override
    public void flying() {
        System.out.println(name + "正在飞！");
    }

    @Override
    public void eat() {
        System.out.println(name + "吃鸟粮食");
    }
}

class Duck extends Animal implements IFLying,IRunning,ISwimming{


    public Duck(String name, int age) {
        super(name, age);
    }

    @Override
    public void flying() {
        System.out.println(name + "正在飞");
    }

    @Override
    public void swimming() {
        System.out.println(name + "正在游泳");
    }

    @Override
    public void running() {
        System.out.println(name + "正在跑");
    }

    @Override
    public void eat() {
        System.out.println(name + "正在吃鸭粮");
    }
}
class Roobot implements IRunning{

    @Override
    public void running() {
        System.out.println("机器人正在跑！");
    }
}

public class Test5 {
    public static void walk(IRunning iRunning){
        iRunning.running();
    }

    public static void func(Animal animal){
        animal.eat();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        walk(new Dog("来福",10));
        walk(new Duck("唐老鸭",20));
        walk(new Roobot());
        System.out.println("=============");
        func(new Dog("来福",10));
        func(new Duck("唐老鸭",20));
    }

}
