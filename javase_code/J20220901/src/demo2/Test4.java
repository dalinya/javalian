package demo2;
interface  A1{
    void func();
}
interface B1{
    void func2();
}
interface D extends A1,B1{//接口间的继承，相当于多个接口的功能累加
    void func3();
}
class E implements D{

    @Override
    public void func() {

    }

    @Override
    public void func2() {

    }

    @Override
    public void func3() {

    }
}
class C1 implements A1,B1{//接口可以处理java中没有多继承的缺点

    @Override
    public void func() {

    }

    @Override
    public void func2() {

    }
}

public class Test4 {
}
