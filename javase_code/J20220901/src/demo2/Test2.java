package demo2;

interface IShape{
    void draw();
}
//ALT + Enter
class Rect implements IShape{

    @Override
    public void draw() {
        System.out.println("画矩形");
    }
}
class Flower implements IShape{
    @Override
    public void draw() {
        System.out.println("❀❀");
    }
}
class Cycle implements IShape{

    @Override
    public void draw() {
        System.out.println("画圆！");
    }
}

public class Test2 {
    public static void drawMap(IShape iShape){
        iShape.draw();
    }
    public static void main(String[] args) {
        Rect rect = new Rect();
        drawMap(rect);
        drawMap(new Cycle());
        drawMap(new Flower());
        //IShape iShape = new Cycle();

    }


}
