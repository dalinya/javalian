package demo;

abstract class Shape{
    public abstract void draw();//抽象方法
    //不能是私有的，满足重写的条件，final和abstract矛盾
}

abstract class A extends Shape{
    //要想继承时不写父类的抽象方法，子类必须也是抽象类

}

class Rect extends Shape{
    @Override
    public void draw() {
        System.out.println("画矩形");
    }
}
class Triangle extends Shape{
    @Override
    public void draw() {
        System.out.println("画出三角形");
    }
}

class Flower extends Shape{
    @Override
    public void draw() {
        System.out.println("❀❀");
    }
}
public class Test {
    public static void drawMap(Shape shape){
        shape.draw();
    }

    public static void main(String[] args) {
        Rect rect = new Rect();
        drawMap(rect);
        drawMap(new Triangle());
        drawMap(new Flower());
    }

}
