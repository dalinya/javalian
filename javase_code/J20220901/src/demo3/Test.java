package demo3;

import java.util.Arrays;
import java.util.Comparator;

class Student implements Comparable<Student>{
    public String name;
    public int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }


    @Override
    public int compareTo(Student o) {
        if(this.age - o.age > 0){
            return 1;
        }else if(this.age - o.age < 0){
            return -1;
        }else{
            return 0;
        }
    }
}
class AgeComparator implements Comparator<Student> {


    @Override
    public int compare(Student o1, Student o2) {
        return o1.age - o2.age;
    }
}
class NameComparator implements Comparator<Student>{


    @Override
    public int compare(Student o1, Student o2) {
        return o1.name.compareTo(o2.name);

    }
}

public class Test {
    public static void bubbleSort(Comparable[] array){
        for (int i = 0; i < array.length -1; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if(array[j].compareTo(array[j + 1]) > 0){
                    Comparable tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }

            }
        }
    }

    public static void main(String[] args) {
        Student[] students = {new Student("zhang",20),new Student("li",22),new Student("wang",18)};
        bubbleSort(students);
        System.out.println(Arrays.toString(students));

    }

    public static void main6(String[] args) {
        Student student1 = new Student("zhang",20);
        Student student2 = new Student("wang",28);
        AgeComparator ageComparator = new AgeComparator();
        if(ageComparator.compare(student1,student2) > 0){
            System.out.println("student1 > student2");
        }else{
            System.out.println("student1 < student2");
        }

    }

    public static void main5(String[] args) {
        Student[] students = {new Student("zhang",20),new Student("li",22),new Student("wang",18)};
        AgeComparator ageComparator = new AgeComparator();
//        Arrays.sort(students,ageComparator);
        NameComparator nameComparator = new NameComparator();
        Arrays.sort(students,nameComparator);
        System.out.println(Arrays.toString(students));

    }

    public static void main4(String[] args) {
        Student student1 = new Student("zhang",20);
        Student student2 = new Student("wang",28);
        if(student1.compareTo(student2) > 0){
            System.out.println("student1 > student2");
        }else {
            System.out.println("student1 < student2");
        }
    }

    public static void main3(String[] args) {
        Student[] students = {new Student("zhang",20),new Student("li",22),new Student("wang",18)};
        Arrays.sort(students);
        System.out.println(Arrays.toString(students));

    }

    public static void main2(String[] args) {
        String[] strings = {"hahah","lili","aa"};
        Arrays.sort(strings);
        System.out.println(Arrays.toString(strings));
    }

    public static void main1(String[] args) {
        int[] array = {1,4,2,7,3,8,5};
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));

    }
}
