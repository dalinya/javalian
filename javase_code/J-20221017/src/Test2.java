import java.util.*;

class Student1 {

}
public class Test2 {
    public static void func(int[] array){
        HashMap<Integer,Integer> map = new HashMap<>();
        for (int i = 0; i < array.length; i++) {
            int key = array[i];
            if (map.get(key) == null){
                map.put(key,1);
            }else {
                int val = map.get(key);
                map.put(key, val + 1);
            }

        }
        for (Map.Entry<Integer,Integer> entry:map.entrySet()) {
            System.out.println("key:" + entry.getKey() + "  value:" + entry.getValue());
        }

    }

    public static void main(String[] args) {

    }
    public static void main2(String[] args) {
        HashMap<String,Integer> map = new HashMap<>();
        map.put("漳卅",1);
        map.put("李四",2);
        map.put(null,null);
        System.out.println(map.get("漳卅"));
        Set<Map.Entry<String,Integer>> entries= map.entrySet();
        for (Map.Entry<String,Integer> entry: entries) {
            System.out.println(entry.getKey()+" -> " +entry.getValue());
        }
        HashSet<Integer> set = new HashSet<>();
        set.add(1);
        set.add(1);
        set.add(1);
        set.add(1);
        System.out.println(set.size());

    }
    public static void main1(String[] args) {
        TreeSet<Student1> set = new TreeSet<>();
        //set.add(new Student1());


    }

    public int singleNumber(int[] nums) {
        TreeSet<Integer> set = new TreeSet<>();
        for (int i = 0; i < nums.length; i++) {
            if (!set.contains(nums[i])){
                set.add(nums[i]);
            }else {
                set.remove(nums[i]);
            }
        }
        for (int i = 0; i < nums.length; i++) {
            if (set.contains(nums[i])){
                return nums[i];
            }
        }
        return -1;
    }

}
