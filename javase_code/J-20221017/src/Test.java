import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

class BinarySearchTree {
    class TreeNode{
        TreeNode left;
        TreeNode right;
        int val;
        public TreeNode(int val) {
            this.val = val;
        }
    }
    public TreeNode root = null;
    public TreeNode search(int val){
        TreeNode cur = root;
        while (cur != null){
            if (cur.val < val){
                cur = cur.right;
            }else if (cur.val > val){
                cur = cur.left;
            }else {
                return cur;
            }
        }
        return null;
    }
    public boolean insert(int key){
        if (root == null){
            root = new TreeNode(key);
            return true;
        }
        TreeNode cur = root;
        TreeNode parent = null;
        while (cur != null){
            if (cur.val < key){
                parent = cur;
                cur = cur.right;
            }else if (cur.val > key){
                parent = cur;
                cur = cur.left;
            }else {
                return false;
            }
        }
        TreeNode node = new TreeNode(key);
        if (parent.val < key){
            parent.right = node;
        }else {
            parent.left = node;
        }
        return true;
    }
    public void remove(int key){
        TreeNode parent = null;
        TreeNode cur = root;
        while (cur != null){
            if(cur.val == key){
                removeNode(parent,cur);
                return;
            }else if(cur.val < key){
                parent = cur;
                cur = cur.right;
            }else {
                parent = cur;
                cur = cur.left;
            }

        }

    }
    public void removeNode(TreeNode parent,TreeNode cur){
        if (cur.left == null){
            if (cur == root){
                root = cur.right;
            }else if (parent.left == cur){
                parent.left = cur.right;
            }else {
                parent.right = cur.right;
            }

        }else if(cur.right == null){
            if (cur == root){
                root = cur.left;
            }else if (parent.left == cur){
                parent.left = cur.left;
            }else{
                parent.right = cur.left;
            }
        }else {
            TreeNode targetParent = cur;
            TreeNode target = cur.right;
            while (target.left != null){
                targetParent = target;
                target = target.left;
            }
            cur.val = target.val;
            if (targetParent.left == target){
                targetParent.left = target.right;
            }else {
                targetParent.right = target.right;
            }
        }
    }
    public void inorder(TreeNode root){
        if (root == null){
            return;
        }
        inorder(root.left);
        System.out.print(root.val + " ");
        inorder(root.right);
    }
}
class Student {

}
public class Test {
    public static void main(String[] args) {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        int[] array = {5,3,4,1,7,8,2,6,0,9};
        for (int i = 0; i < array.length; i++) {
            binarySearchTree.insert(array[i]);
        }
        binarySearchTree.inorder(binarySearchTree.root);

    }
    public static void main2(String[] args) {
        TreeMap<String,Integer> map = new TreeMap<>();
        map.put("hello",2);
        map.put("abc",4);
        map.put("abc",14);
        Set<Map.Entry<String,Integer>> entrySet = map.entrySet();
        for (Map.Entry<String,Integer> entry: entrySet) {
            System.out.println("key:" + entry.getKey() + " val:" + entry.getValue());
        }
    }
    public static void main1(String[] args) {
        TreeMap<String,Integer> map = new TreeMap<>();
        map.put("hello",2);
        map.put("abc",4);
        System.out.println(map);
        Integer v = map.getOrDefault("abc2",100);
        System.out.println(v);
        System.out.println("取出k的值，进行组织，通过Set");
        Set<String> set = map.keySet();
        System.out.println(set);
        Collection<Integer> collection = map.values();
        System.out.println(collection);
        /*TreeMap<Student,Integer> map2 = new TreeMap<>();
        map2.put(new Student(),1);*/
    }

}
