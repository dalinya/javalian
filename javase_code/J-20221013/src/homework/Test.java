package homework;

import java.util.Scanner;
import java.util.TreeMap;

public class Test {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String strName = sc.nextLine();
        String strPwd = sc.nextLine();
        if (checkName(strName)&&checkPwd(strPwd)){
            System.out.println("用户名和密码正确");
        }else {
            System.out.println("用户名或密码错误");
        }
    }
    public static boolean checkName(String name){
        return name.matches("\\D\\w{5,9}");
    }
    public static boolean checkPwd(String pwd){
        return pwd.matches("\\w{6,20}");
    }
/*
a1354ds
123456


123aaa
123456
* */

    public static void main1(String[] args) {
        TreeMap<Character,Integer> treeMap = new TreeMap<>();
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        for (int i = 0; i < str.length(); i++) {
            if(!treeMap.containsKey(str.charAt(i))){
                treeMap.put(str.charAt(i),1);
            }else {
                int k = treeMap.get(str.charAt(i));
                treeMap.remove(str.charAt(i));
                treeMap.put(str.charAt(i),k + 1);
            }
        }
        System.out.println(treeMap);
    }


}
