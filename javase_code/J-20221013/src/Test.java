import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Random;

public class Test {
    public static void testInsert(int[] array){
        array = Arrays.copyOf(array,array.length);
        long start = System.currentTimeMillis();
        TestSort.insertSort(array);
        long end = System.currentTimeMillis();
        System.out.println("插入排序耗时：" + (end - start));
    }

    public static void testShell(int[] array){
        array = Arrays.copyOf(array,array.length);
        long start = System.currentTimeMillis();
        TestSort.shellSort(array);
        long end = System.currentTimeMillis();
        System.out.println("希尔排序耗时：" + (end - start));
    }
    public static void testSelect(int[] array){
        array = Arrays.copyOf(array,array.length);
        long start = System.currentTimeMillis();
        TestSort.selectSort(array);
        long end = System.currentTimeMillis();
        System.out.println("插入排序耗时：" + (end - start));
    }
    public static void main(String[] args) {
        int[] array = new int[10_0000];
        //initOrder(array);
        initNorOrder(array);
        testInsert(array);
        testShell(array);
        testSelect(array);

    }
    public static void initOrder(int[] array){
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
            //array[i] = array.length - i;
        }

    }
    public static void initNorOrder(int[] array){
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10_0000);
        }
    }

    public int[] smallestK(int[] arr, int k) {
        Arrays.sort(arr);
        int[] array = new int[k];
        for (int i = 0; i < k; i++) {
            array[i] = arr[i];
        }
        return array;
    }

    public int[] smallestK2(int[] arr, int k) {
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
        for (int i = 0; i < arr.length; i++) {
            priorityQueue.offer(arr[i]);
        }
        int[] array = new int[k];
        for (int i = 0; i < k; i++) {
            array[i] = priorityQueue.poll();
        }
        return array;
    }
    public int[] smallestK3(int[] arr, int k) {
        if (k==0){//防止k==0 peek异常
            return new int[0];
        }
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>((o1, o2) -> o2.compareTo(o1));//建立大根堆
        for (int i = 0; i < k; i++) {
            priorityQueue.offer(arr[i]);
        }
        for (int i = k; i < arr.length; i++) {
            if (priorityQueue.peek() > arr[i]){
                priorityQueue.poll();//弹出大的
                priorityQueue.offer(arr[i]);//存放小的
            }
        }
        int[] array = new int[k];
        for (int i = 0; i < k; i++) {
            array[i] = priorityQueue.poll();
        }
        return array;
    }
    public int[] smallestK4(int[] arr, int k) {
        if (k==0){//防止k==0 peek异常
            return new int[0];
        }
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>((o1, o2) -> o2.compareTo(o1));//建立大根堆
        for (int i = 0; i < arr.length; i++) {
            if (i < k){
                priorityQueue.offer(arr[i]);
            }else {
                if (priorityQueue.peek() > arr[i]){
                    priorityQueue.poll();//弹出大的
                    priorityQueue.offer(arr[i]);//存放小的
                }
            }
        }
        int[] array = new int[k];
        for (int i = 0; i < k; i++) {
            array[i] = priorityQueue.poll();
        }
        return array;
    }
}
