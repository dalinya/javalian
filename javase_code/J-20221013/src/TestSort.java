import java.util.Arrays;
import java.util.Queue;
import java.util.Stack;

public class TestSort {

    //直接插入排序
    public static void insertSort(int array[]){
        for (int i = 1; i < array.length ; i++) {
            int tmp = array[i];
            int j = i - 1;
            while (j >= 0){
                if (tmp < array[j]){
                    array[j + 1] = array[j];
                }else {
                    break;
                }
                j--;
            }
            array[j + 1] = tmp;
        }
    }

    //希尔排序
    public static void shellSort(int array[]){
        int gap = array.length;
        while (gap > 0){
            gap /= 2;
            shell(array,gap);
        }
    }
    private static void shell(int array[] , int gap){

        for (int i = gap; i < array.length ; i++) {
            int tmp = array[i];
            int j = i - gap;
            while (j >= 0){
                if (tmp < array[j]){
                    array[j + gap] = array[j];
                }else {
                    break;
                }
                j -= gap;
            }
            array[j + gap] = tmp;
        }
    }

    //选择排序
    public static void selectSort(int[] array){
        for (int i = 0; i < array.length; i++) {
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[minIndex]){
                    minIndex = j;
                }
            }
            if (i != minIndex){
                swap(array,minIndex,i);
            }
        }
    }
    public static void selectSort2(int[] array){
        int start = 0;
        int end = array.length - 1;

        while (start < end){
            int max = start;
            int min = start;
            for (int i = start + 1; i <= end; i++) {
                if (array[i] > array[max]){
                    max = i;
                }
                if (array[i] < array[min]){
                    min = i;
                }
            }
            swap(array,start,min);
            if (max == start){
                max = min;
            }
            swap(array,end,max);
            start++;
            end--;
        }
    }

    public static void heapSort(int[] array){
        createBigHeap(array);
        for (int i = array.length -1; i > 0; i--) {
            swap(array,i,0);
            shiftDown(array,0,i);
        }
    }
    private static void createBigHeap(int[] array){

        for (int parent = (array.length - 1)/2; parent >= 0; parent--) {
            shiftDown(array,parent,array.length);
        }
        
    }
    private static void shiftDown(int[] array,int parent,int len){
        int child = 2 * parent + 1;
        while(child < len){
            if(child + 1 < len && array[child] < array[child + 1]){
                child++;
            }
            if (array[child] > array[parent]){
                swap(array,child,parent);
                parent = child;
                child = parent * 2 + 1;
            }else {
                break;
            }
        }

    }

    public static void bubbleSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int flg = 0;
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]){
                    swap(array,j,j + 1);
                    flg = 1;
                }
            }
            if (flg == 0){
                break;
            }
        }

    }
    private static void swap(int[] array,int i,int j){
        int tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }


    private static int partitionHoare(int[] array,int left,int right){// Hoare版找基准
        int pivot = left;
        while (left < right){
            while (left < right && array[right] >= array[pivot]){//因为当左右两侧数值相等时会成为死循环
                right--;
            }
            while (left < right && array[left] <= array[pivot]){
                left++;
            }
            swap(array,left,right);
        }
        swap(array,pivot,left);
        return left;
    }
    private static int partition(int[] array,int left,int right){//挖坑法
        int key = array[left];
        while (left < right){
            while (left < right && array[right] >= key){//因为当左右两侧数值相等时会成为死循环，并且基准值第一次交换就会改变
                right--;
            }
            array[left] = array[right];
            while (left < right && array[left] <= key){
                left++;
            }
            array[right] = array[left];
        }
        array[left] = key;
        return left;
    }
    private static int partition2(int[] array,int left,int right){//前后指针法
        int prev = left;
        int cur = left + 1;
        while (cur <= right){
            if (array[left] > array[cur] && array[cur] != array[++prev]){
                swap(array,prev,cur);
            }
            cur++;
        }
        swap(array,prev,left);

        return prev;
    }
    private static int findMidValOfIndex(int[] array,int start,int end) {
        int mid = (start + end)/2;
        if (array[start] < array[end]){
            if (array[mid] < array[start]){
                return start;
            }else if (array[mid] > array[end]){
                return end;
            }else {
                return mid;
            }
        }else {
            if (array[mid] > array[start]){
                return start;
            }else if (array[mid] < array[end]){
                return end;
            }else {
                return mid;
            }
        }


    }
    private static void quick(int[] array,int start,int end){
        if(end <= start){//写<=是避免出现有序数组排序之后prov-1出现的越界
            return;
        }
        if (end - start < 10){
            insertSort(array);
        }

        int index = findMidValOfIndex(array,start,end);//三数取中，使快速排序在数据有序的情况下同样高效
        swap(array,index,start);
        int pivot = partition2(array,start,end);
        quick(array,start,pivot - 1);
        quick(array,pivot + 1,end);
    }

    private static void quickSort1(int[] array){
        quick(array,0,array.length - 1);
    }
    private static void quickSort(int[] array){
        Stack<Integer> stack = new Stack<>();
        int left = 0;
        int right = array.length - 1;
        int pivot = partition(array,left,right);
        if (pivot - left> 1){//判断左边是不是有俩元素
            stack.push(left);
            stack.push(pivot - 1);
        }

        if (right - pivot > 1){//判断右边是不是有俩元素
            stack.push(pivot + 1);
            stack.push(right);
        }
        while (!stack.isEmpty()){
            right = stack.pop();
            left = stack.pop();

            pivot = partition(array,left,right);
            if (pivot - left> 1){//判断左边是不是有俩元素
                stack.push(left);
                stack.push(pivot - 1);
            }

            if (right - pivot > 1){//判断右边是不是有俩元素
                stack.push(pivot + 1);
                stack.push(right);
            }

        }
    }

    public static void mergeSort1(int[] array){
        mergeSortChild(array,0,array.length - 1);
    }
    private static void mergeSortChild(int[] array,int left,int right){
        if(left == right){
            return;
        }
        int mid = (left + right)/2;
        mergeSortChild(array,left,mid);
        mergeSortChild(array,mid + 1,right);
        merge(array,left,mid,right);
    }
    private static void merge(int[] array,int left,int mid,int right){
        int s1 = left;
        int e1 = mid;
        int s2 = mid + 1;
        int e2 = right;
        int[] tmp = new int[right - left + 1];
        int i = 0;
        while (s1 <= e1 && s2 <= e2){
            if (array[s1] <= array[s2]){
                tmp[i++] = array[s1++];
            }else {
                tmp[i++] = array[s2++];
            }
        }
        while (s1 <= e1){
            tmp[i++] = array[s1++];
        }
        while (s2 <= e2){
            tmp[i++] = array[s2++];
        }
        for (int j = 0; j < i; j++) {
            array[left + j] = tmp[j];

        }

    }
    public static void mergeSort(int[] array){
        int gap = 1;
        while (gap < array.length){
            for (int i = 0; i < array.length; i+= (gap*2)) {
                int left = i;
                int mid = i + gap - 1;
                int right = i + 2*gap - 1;
                if (mid >= array.length){
                    mid = array.length - 1;
                }
                if (right >= array.length){
                    right = array.length - 1;
                }
                merge(array,left,mid,right);
            }

            gap *= 2;
        }


    }
    public static void countSort(int[] array) {
        int minVal = array[0];
        int maxVal = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] > maxVal){
                maxVal = array[i];
            }
            if (array[i] < minVal){
                minVal = array[i];
            }
        }
        int len = maxVal - minVal + 1;
        int[] countArr = new int[len];
        for (int i = 0; i < array.length; i++) {
            countArr[array[i] - minVal]++;
        }

        int k = 0;
        for (int i = 0; i < countArr.length; i++) {
            while (countArr[i] > 0){
                array[k++] = i + minVal;
                countArr[i]--;
            }
        }
    }
    public static void main(String[] args) {
        int[] array = {999,4,2,7,6,3,5};
        countSort(array);
        System.out.println(Arrays.toString(array));
    }
    //一小时47分
    //快速排序三数取中为什么能块
    //归并排序非递归的调整
    //基数排序的缺点
}
