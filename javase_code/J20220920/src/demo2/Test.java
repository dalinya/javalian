package demo2;
abstract class Shape{
    //抽象方法
    public abstract void draw();
    /*int a;
    public void func(){

    }

    public Shape(int a) {
        this.a = a;
    }*/
}

class Rect extends Shape{
    @Override
    public void draw() {
        System.out.println("画矩形");
    }
}

class Cycle extends Shape{
    @Override
    public void draw() {
        System.out.println("画⚪⚪⚪");
    }
}
class Triangle extends Shape{
    @Override
    public void draw() {
        System.out.println("画🔺🔺🔺");
    }
}

class Flower extends Shape{
    @Override
    public void draw() {
        System.out.println("画❀❀❀");
    }
}
public class Test {
    public static void drawMap(Shape shape){
        shape.draw();
    }


    public static void main(String[] args) {
        drawMap(new Rect());
        drawMap(new Cycle());
        drawMap(new Triangle());
        drawMap(new Flower());
    }
}
