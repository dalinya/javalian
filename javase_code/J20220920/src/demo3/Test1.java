package demo3;
interface ITeat{
    int size = 10;//默认public static final
    void draw();//默认public abstract
    default public void fuc(){
        System.out.println("默认方法！");
    }
    public static void func2(){
        System.out.println("静态方法");
    }
}
class A implements ITeat{

    @Override
    public void draw() {
        System.out.println("必须重写");
    }

    @Override
    public void fuc() {
        System.out.println("可以重写，也可以不写");
    }
}
public class Test1 {
    public static void main(String[] args) {
        //ITeat iTeat = new ITeat(); -> 接口不能够被实例化
    }
}
