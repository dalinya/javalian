package demo3;


interface IFlying{
    void flying();
}
interface ISwimming{
    void swimming();
}

interface IRunning{
    void running();
}

class Animal{
    public String name;
    public int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public void eat(){
        System.out.println(name + "正在吃");
    }

}
class Dog extends Animal implements IRunning,ISwimming{

    public Dog(String name, int age) {
        super(name, age);
    }

    @Override
    public void swimming() {
        System.out.println(name + "正在游泳");
    }

    @Override
    public void running() {
        System.out.println(name + "正在跑");
    }

    @Override
    public void eat() {
        System.out.println(name + "正在吃狗粮");
    }
}
class Bird extends Animal implements IFlying{

    public Bird(String name, int age) {
        super(name, age);
    }

    @Override
    public void flying() {
        System.out.println(name + "正在飞");
    }

    @Override
    public void eat() {
        System.out.println(name + "正在吃鸟粮");
    }
}
class Duck extends Animal implements IFlying,IRunning,ISwimming{

    public Duck(String name, int age) {
        super(name, age);
    }

    @Override
    public void flying() {
        System.out.println(name + "正在飞");
    }

    @Override
    public void swimming() {
        System.out.println(name + "正在游泳");
    }

    @Override
    public void running() {
        System.out.println(name + "正在跑");
    }

    @Override
    public void eat() {
        System.out.println(name + "正在吃鸭粮");
    }
}
class Roobot implements IRunning{

    @Override
    public void running() {
        System.out.println("机械人正在跑");
    }
}

public class Test5 {
     public static void walk(IRunning iRunning){
         iRunning.running();
     }
    public static void func(Animal animal){
         animal.eat();
    }

    public static void main(String[] args) {
         walk(new Dog("来福",10));
        walk(new Duck("唐老鸭",20));
        walk(new Roobot());

        System.out.println("======================");
        func(new Dog("来福",10));
        func(new Duck("唐老鸭",20));
    }
}
