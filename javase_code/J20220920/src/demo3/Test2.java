package demo3;



interface  IShape{
    public abstract void draw();
}

class Rect implements   IShape{
    @Override
    public void draw() {
        System.out.println("画矩形");
    }
}

class Cycle implements IShape {
    @Override
    public void draw() {
        System.out.println("画⚪⚪⚪");
    }
}
class Triangle implements IShape {
    @Override
    public void draw() {
        System.out.println("画🔺🔺🔺");
    }
}

class Flower implements IShape {
    @Override
    public void draw() {
        System.out.println("画❀❀❀");
    }
}
public class Test2 {
    public static void drawMap(IShape shape){
        shape.draw();
    }


    public static void main(String[] args) {
        drawMap(new Rect());
        drawMap(new Cycle());
        drawMap(new Triangle());
        drawMap(new Flower());
    }

}
