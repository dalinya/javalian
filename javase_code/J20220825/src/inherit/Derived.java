package inherit;
class Base{
    int a;
    int c;
    public void methodA(){
        System.out.println("Base::methodA()");
    }
}
public class Derived extends Base{
    int a;
    int b;
    public void methodA(){
        System.out.println("Derived::methodA()");
    }
    public void methodB(){
        System.out.println("Derived::methodB()");
    }

    public void method(){
        a = 10;////此时当 父类和子类 都拥有同名的变量的时候，优先访问子类自己
        b = 20;
        c = 30;
        System.out.println(super.a);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);


    }
    public static void main(String[] args) {
        Derived derived = new Derived();
        derived.method();
    }
}


