package inherit;

import com.sun.javafx.iio.common.ImageLoaderImpl;

class Animal{
public String name;
    public int age;
    public void eat(){
        System.out.println(name + "正在吃饭");
    }

}
class Dog extends Animal{

    public void barks(){
        System.out.println(name + "汪汪叫!" + " 年龄" + age);
    }
}
class Cat extends Animal{

    public void catchMouse(){
        System.out.println(name + "正在抓老鼠");
    }
}

public class Test {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.name = "旺财";
        dog.age = 6;
        dog.barks();
        dog.eat();
    }

}
