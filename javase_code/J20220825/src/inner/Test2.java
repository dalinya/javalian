package inner;

class OuterClass2{
    public int data1 = 1;
    int data2 = 2;
    public static int data3 = 3;
    public void test(){
        System.out.println("out::test");
    }
    static class InnerClass2{
        public int data4  = 4;
        int data5 = 5;
        public static int data6 = 6;
        public void fuc(){


            System.out.println("InnerClass::fuc()");
            OuterClass2 outerClass2 = new OuterClass2();
            System.out.println(outerClass2.data1);

//            System.out.println(data1);
//            System.out.println(data2);//静态内部类种不能有外部类中的非静态成员的调用
            System.out.println(data3);
            System.out.println(data4);
            System.out.println(data5);
            System.out.println(data6);

        }

    }

}
class Person{
    public int age = 18;
    public String name = "bit";
    public void show(){
        System.out.println("姓名 " + name + " 年龄 " + age);
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}

public class Test2 {
    public static void main(String[] args) {
        System.out.printf("%2$d, %1$d",100,200);
    }

    public static void main3(String[] args) {
        Person person = new Person();
        System.out.println(person);
    }

    public static void main2(String[] args) {
        Person person = new Person();//创建对象访问类中成员
        System.out.println(person.age);
        System.out.println(person.name);

        System.out.println(new Person().age);//直接实例化.成员变量
        System.out.println(new Person().name);

        new Person(){//匿名内部类

        };
    }

    public void func2(){
        //局部内布类
        class Inner{
            public void test(){
                System.out.println("faffafafafaff");
            }
        }
        Inner inner = new Inner();
        inner.test();
    }

    public static void main1(String[] args) {
        OuterClass2.InnerClass2 innerClass2 = new OuterClass2.InnerClass2();
        innerClass2.fuc();

    }
}
