package inner;

import com.sun.org.apache.bcel.internal.classfile.InnerClass;
import jdk.internal.org.objectweb.asm.tree.InnerClassNode;

class OuterClass{
    private int data1 = 1;
    int data2 = 2;
    public static int data3 = 3;
    class InnerClass{
        public int data1 = 111;
        public int data4 = 4;
        int data5 = 5;
        public static final int data6 = 6;
        public void fuc(){
            OuterClass outerClass = new OuterClass();
            System.out.println(outerClass.data1);
            System.out.println(OuterClass.this.data1);

            System.out.println("InnerClass::fuc()");
            System.out.println(data1);
            System.out.println(data2);
            System.out.println(data3);
            System.out.println(data4);
            System.out.println(data5);
            System.out.println(data6);

        }

    }

    public void test(){
        System.out.println("OuterClass::test()");
        //public static final int data6 = 6; //ERROR

    }

}
public class Test{
    public static void main(String[] args) {
        OuterClass.InnerClass innerClass = new OuterClass().new InnerClass();
        innerClass.fuc();
    }

    public static void main1(String[] args) {
        OuterClass outerClass = new OuterClass();
        OuterClass.InnerClass innerClass = outerClass.new InnerClass();
        innerClass.fuc();
    }
}
