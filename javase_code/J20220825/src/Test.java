import java.util.Date;
import java.util.concurrent.Callable;

class Animal{
    public  String name;
    public int age;
    public void eat(){
        System.out.println(name + "正在吃饭！");
    }

    /*static {
        System.out.println("Animal::static{}");
    }
    {
        System.out.println("Animal::{}");
    }*/


    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println("Animal(String, int)");
    }
}
class Dog extends Animal{
    //傻狗
    public boolean silly;
    public void houseGuard(){
        System.out.println(name + "正在看家护院");
    }
    /*static {
        System.out.println("Dog::static{}");
    }
    {
        System.out.println("Dog::{}");
    }*/

    @Override
    public void eat() {
        System.out.println(name + "正在吃狗粮");
    }

    public Dog(String name, int age, boolean silly) {
        super(name, age);
        this.silly = silly;
        System.out.println("Dog(String , int , boolean )");
    }
    public static void staticFunc(){
        //System.out.println(super.name);
    }
}
class Cat extends Animal{
    public void catchMouse(){
        System.out.println(name + "抓老鼠");
    }
    public Cat(){
        super("大喵",6);
    }

}

public class Test {
    public static void main(String[] args) {
        Animal animal = new Dog("旺财",10,false);
        if(animal instanceof Cat){
            Cat cat = (Cat)animal;
            cat.catchMouse();
        }

    }
    public static void function(Animal animal){
        animal.eat();

    }
    public static Animal function2(Animal animal){
        return new Cat();

    }

    public static void main4(String[] args) {
        Animal animal = new Cat();
        Cat cat = (Cat) animal;
        cat.catchMouse();
    }

    public static void main3(String[] args) {
        Dog dog = new Dog("旺财",10,false);
        function(dog);
        Cat cat = new Cat();
        function(cat);
    }

    public static void main2(String[] args) {
        Dog dog = new Dog("旺财",10,false);
        Animal animal = dog;
        //animal.silly = false;//ERR
        animal.eat();

        Animal animal1 = new Dog("旺财",10,false);
        Animal animal2 = new Cat();

    }
    public static void main1(String[] args) {
        Dog dog = new Dog("旺财",21,false);
        System.out.println("=========================");
        Dog dog1 = new Dog("旺财",22,false);
    }
}
