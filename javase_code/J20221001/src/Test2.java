import java.util.Scanner;
public class Test2 {
    public static void monthSalary(Employee employee){
        System.out.printf("%s%02d月份工资：%.0f",employee.name,employee.month,employee.getSalary());
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String[] strings = str.split(" ");
        if (strings.length == 3){//：：
            SalariedEmployee salariedEmployee = new SalariedEmployee(strings[0],Integer.parseInt(strings[1]),Double.parseDouble(strings[2]));
            monthSalary(salariedEmployee);
        } else if (strings.length == 4) {
            if(str.indexOf('.') == -1){
                HourlyEmployee hourlyEmployee = new HourlyEmployee(strings[0],Integer.parseInt(strings[1]),Double.parseDouble(strings[2]),Double.parseDouble(strings[3]));
                monthSalary(hourlyEmployee);
            }else {
                SalesEmployee salesEmployee = new SalesEmployee(strings[0],Integer.parseInt(strings[1]),Double.parseDouble(strings[2]),Double.parseDouble(strings[3]));
                monthSalary(salesEmployee);
            }
        }else {
            BasePlusSalesEmployee basePlusSalesEmployee = new BasePlusSalesEmployee(strings[0],Integer.parseInt(strings[1]),Double.parseDouble(strings[2]),Double.parseDouble(strings[3]),Double.parseDouble(strings[4]));
            monthSalary(basePlusSalesEmployee);
        }
    }
}

abstract class Employee{
    protected String name;
    protected int month;
    abstract public double getSalary();
}
class SalariedEmployee extends Employee{
    private double salary;
    @Override
    public double getSalary() {
        if (super.month == 12){
            this.salary += 100;
        }
        return this.salary;
    }

    public SalariedEmployee(String name,int month,double salary) {
        super.name = name;
        super.month = month;
        this.salary = salary;
    }
}

class HourlyEmployee extends Employee{
    private double hourSalary;
    private double hours;
    @Override
    public double getSalary() {
        double sum = 0;
        if (this.hours < 160){
            sum = this.hourSalary * this.hours;
        }else {
            sum = this.hourSalary * 160 + 1.5 * this.hourSalary * (this.hours - 160);
        }
        if (super.month == 12){
            sum += 100;
        }
        return sum;
    }

    public HourlyEmployee(String name,int month,double hourSalary,double hours ) {
        super.name = name;
        super.month = month;
        this.hourSalary = hourSalary;
        this.hours = hours;
    }
}
class SalesEmployee extends Employee{
    protected double sale;
    protected double commissionRate;

    @Override
    public double getSalary() {
        if (super.month == 12){
            return this.sale * this.commissionRate + 100;
        }
        return this.sale * this.commissionRate;
    }

    public SalesEmployee(String name,int month,double sale, double commissionRate) {
        super.name = name;
        super.month = month;
        this.sale = sale;
        this.commissionRate = commissionRate;
    }
}

class BasePlusSalesEmployee extends SalesEmployee{
    private double basicSalary;
    @Override
    public double getSalary() {
        return this.basicSalary +  super.getSalary();
    }

    public BasePlusSalesEmployee(String name, int month, double basicSalary, double commissionRate, double sale) {
        super(name, month, sale, commissionRate);
        this.basicSalary = basicSalary;
    }
}

