import java.util.Scanner;

public class Test1 {
    public static void main(String[] args) {
        Team team = new Team();
        Scanner scanner = new Scanner(System.in);
        team.manger.grade = scanner.nextInt();
        int num = scanner.nextInt();
        for (int i = 0; i < num; i++) {
            team.addMember();
        }
        System.out.println("该团队的总攻击力=" + team.attackSum());
    }
}

abstract class Role{
    protected String name;
    abstract public int attack();
}
class Manger extends Role{
    int grade;
    @Override
    public int attack() {
        return (this.grade * 5);
    }
}

class Soldier extends Role{
    private int stk = 3;
    @Override
    public int attack() {
        return this.stk;
    }

    public int getStk() {
        return stk;
    }

    public void setStk(int stk) {
        this.stk = stk;
    }
    public void setName(String name){
        super.name = name;
    }
}

class Team{
    Manger manger = new Manger();
    Soldier[] soldiers;
    int count = 0;
    public Team(){
        soldiers = new Soldier[6];
    }
    public boolean addMember(){
        if (count == 6){
            return false;
        }
        Soldier soldier = new Soldier();
        soldiers[count++] = soldier;
        return true;
    }
    public int attackSum(){
        int sum = manger.attack();
        for (int i = 0; i < this.count; i++) {
            sum += soldiers[i].attack();
        }
        return sum;
    }
}
