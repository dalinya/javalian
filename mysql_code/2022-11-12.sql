drop table if exists classes;
drop table if exists student;
drop table if exists course;
drop table if exists score;

create table classes (id int primary key auto_increment, name varchar(20), `desc` varchar(100));

create table student (id int primary key auto_increment, sn varchar(20),  name varchar(20), qq_mail varchar(20) ,
        classes_id int);

create table course(id int primary key auto_increment, name varchar(20));

create table score(score decimal(3, 1), student_id int, course_id int);

insert into classes(name, `desc`) values 
('计算机系2019级1班', '学习了计算机原理、C和Java语言、数据结构和算法'),
('中文系2019级3班','学习了中国传统文学'),
('自动化2019级5班','学习了机械自动化');

insert into student(sn, name, qq_mail, classes_id) values
('09982','黑旋风李逵','xuanfeng@qq.com',1),
('00835','菩提老祖',null,1),
('00391','白素贞',null,1),
('00031','许仙','xuxian@qq.com',1),
('00054','不想毕业',null,1),
('51234','好好说话','say@qq.com',2),
('83223','tellme',null,2),
('09527','老外学中文','foreigner@qq.com',2);

insert into course(name) values
('Java'),('中国传统文化'),('计算机原理'),('语文'),('高阶数学'),('英文');

insert into score(score, student_id, course_id) values
-- 黑旋风李逵
(70.5, 1, 1),(98.5, 1, 3),(33, 1, 5),(98, 1, 6),
-- 菩提老祖
(60, 2, 1),(59.5, 2, 5),
-- 白素贞
(33, 3, 1),(68, 3, 3),(99, 3, 5),
-- 许仙
(67, 4, 1),(23, 4, 3),(56, 4, 5),(72, 4, 6),
-- 不想毕业
(81, 5, 1),(37, 5, 5),
-- 好好说话
(56, 6, 2),(43, 6, 4),(79, 6, 6),
-- tellme
(80, 7, 2),(92, 7, 6);

-- 计算许仙同学的成绩
-- 得出笛卡尔积
select * from student,score;
-- 加上连接条件
select * from student,score where student.id = score.student_id;
-- 根据需求加上其他条件
select * from student,score where student.id = score.student_id and student.name = '许仙';
-- 对列进行修改
select student.name,score.score  from student,score where student.id = score.student_id and student.name = '许仙';

-- 用join  on关键字来完成
select student.name,score.score  from student join score on  student.id = score.student_id and student.name = '许仙';

select student.name,score.score  from student inner join score on  student.id = score.student_id and student.name = '许仙';

-- 查询所有同学的总成绩
-- 求出笛卡尔积
select * from student,score;
-- 加上连接条件
select * from student,score where student.id = score.student_id;
-- 根据需求加上其他条件
-- 用聚合函数，把同一个同学的行，合并到同一个组里，同时计算总分
select name, sum(score.score) from student,score where student.id = score.student_id  group by name;
 
-- 查询所有同学的成绩，：个人信息 课程名称 分数
-- 求出笛卡尔积
select * from student,course,score;
-- 加上连接条件
select * from student,course,score where score.student_id = student.id and score.course_id = course.id;
-- 列进行精简 
select student.name,course.name,score.score from student,course,score where score.student_id = student.id and score.course_id = course.id;

-- 还可以写作
select student.name,course.name,score.score from score join  student on  score.student_id = student.id join  course  on  score.course_id = course.id;

-- 外连接
drop table if exists student;
drop table if exists score;
create table student(id int primary key auto_increment,name varchar(20));
create table score(student_id int primary key auto_increment,name varchar(20));
insert into student values(null,'张三'),(null,'李四'),(null,'王五');
insert into score values(null,90),(null,80),(null,70);
-- 两个表中数据能够一一对应的时候内连接和外连接是一样的
-- 两个表中数据不一一对应的时候，结果就不同
select * from student,score where student.id = score.student_id;
select * from student join score on student.id = score.student_id;
-- 左外连接
update score set student_id = 4 where student_id = 3;
select * from student join score on student.id = score.student_id;
select * from student left join score on student.id = score.student_id;
-- 右外连接
select * from student join score on student.id = score.student_id;
select * from student right join score on student.id = score.student_id;

-- 自连接
-- 显示所有计算机原理成绩比java成绩高的学生信息 course_id = 1 --> java course_id = 3 --> 计算机原理
select * from score as s1,score as s2;
select * from score as s1,score as s2 where s1.student_id = s2.student_id;
select * from score as s1,score as s2 where s1.student_id = s2.student_id and s1.course_id = 3 and s2.course_id =1 ;
select * from score as s1,score as s2 where s1.student_id = s2.student_id and s1.course_id = 3 and s2.course_id =1 and s1.score > s2.score;

-- 子查询
-- 单行子查询查询与“不想毕业”同学的同班同学
select classes_id from student where name = '不想毕业';
select name from student where classes_id = 1 and name != '不想毕业';
-- 子查询就是把两个语句结合成一句
select name from student where classes_id = (select classes_id from student where name = '不想毕业') and name != '不想毕业';
-- 多行子查询
-- 查询语文或英文课程的成绩信息
-- 1.先根据名字查询出课程id
-- 2.根据课程id查询出课程分数
select id from course where name = '语文' or name = '英文';
select * from score where course_id = 4 or course_id = 6;
select * from score where course_id in (select id from course where name = '语文' or name = '英文');
-- 查询结果在内存中太大，内存中放不下，in就用不了了，就可以使用exists代替

-- 合并查询
-- 本质上就是把两个查询结果集合并成一个
-- union      union all
-- 查询id小于三，或者名字为‘英文的课程’
select * from course where id < 3 union select * from course where name = '英文';
-- 合并查询此处看 用or可以来代替 
-- 但是or只能查询来自于同一个表 
-- 用union查询来自于不同表 只要查询的结果匹配即可
-- union all 和 union 差不多，union会去重，union all不会进行去重

-- 题目
-- 768
create table A( name varchar(32),grade  int);
insert into A values('zhangsan',80),('lisi',60),('wangwu',84);
create table B(name varchar(32),age int);
insert into B values('zhangsan',26),('lisi',24),('wangwu',26),('wutian',26);
select B.name as NAME,A.grade as GRADE,b.age as AGE from A right join B on A.name = B.name;
-- 770
create table depart(depart_id int,name varchar(20));
create table staff(staff_id int,name varchar(20),age int,depart_id int);
create table salary(salary_id int,staff_id int,salary int,month datetime);
insert into salary values(1,1,5000,'2016_09');
-- 问题a
select depart.name, sum(salary) from salary,staff,depart 
        where salary.staff_id = staff.staff_id and staff.depart_id = depart.depart_id
        and year(salary.month) = 2016 and month(salary.month) = 9 group by depart.depart_id;
-- 问题b
select depart.name,count(staff.staff_id) from staff
        join depart on staff.depart_id = depart.depart_id group by depart.depart_id;
-- 问题c
select depart.name, month(salary.month),sum(salary.salary) from salary 
        join staff on staff.staff_id = salary.staff_id
        join depart on staff.depart_id = depart.depart_id  group by depart.depart_id,salary.month;

-- 771
create table Employee(id int,salary int);
insert into Employee values(1,100),(2,200),(3,300);
select salary as SecondHighestSalary from  Employee order by salary limit 1 offset 1;
select ifnull ((select salary from  Employee order by salary limit 1 offset 1),null)
        as SecondHighestSalary;
        -- ifnull 函数 如果第一个参数为空返回第二个参数



