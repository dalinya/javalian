-- primary key 主键约束
-- not null 非空约束
-- unique 唯一约束
-- 主键约束  = 非空约束 + 主键约束

-- default 约束
create table student(id int,name varchar(20) default '无名式');
desc student;
-- 插入数据
insert into student values(1,'张三');
insert into student(id) values(2);
-- 显示约束效果
select * from student;

-- 外键约束 foreign key 
create table class(id int primary key,name varchar(20));
create table student(id int primary key,name varchar(20),classId int,foreign key(classId) references class(id));
insert into class values(1,'java104'),(2,'java105'),(3,'java106'); -- 父表插入数据
-- 子表插入数据
insert into student values(2,'李四',100);
insert into student values(1,'张三',1); -- 插入失败，父表主键没有 1
update student set classId = 100 where name = '张三'; -- 修改 失败 因为外键约束，父类 主键没有
-- 删除父表数据
delete from class where id = 1; -- 删除失败
delete from class where id = 2; -- 删除成功

-- 正确删除表的顺序
drop table student;
drop table class;

-- 无主键的创建
create table class(id int,name varchar(20));
create table student(id int,name varchar(20),classId int,foreign key(classId) references class(id));
-- 外键要求必须不重复，所以必须被约束为unique或者primary key
 
-- 插入查询结果  -> 不常用
DROP TABLE IF EXISTS exam_result;
CREATE TABLE exam_result (
 id INT,
 name VARCHAR(20),
 chinese DECIMAL(3,1),
 math DECIMAL(3,1),
 english DECIMAL(3,1)
);
-- 插入测试数据
INSERT INTO exam_result (id,name, chinese, math, english) VALUES
 (1,'唐三藏', 67, 98, 56),
 (2,'孙悟空', 87.5, 78, 77),
 (3,'猪悟能', 88, 98.5, 90),
 (4,'曹孟德', 82, 84, 67),
 (5,'刘玄德', 55.5, 85, 45),
 (6,'孙权', 70, 73, 78.5),
 (7,'宋公明', 75, 65, 30);

create table student(name varchar(20),chinese decimal(3,1));
insert into student select name,chinese from exam_result;
-- 插入查询结果 不经常用


-- 聚合函数 行与行之间的查询 

-- 插入一行null数据
 insert into exam_result values(null,null,null,null,null);
-- count() 求合
select count(*) from exam_result; -- 先进性 select* 操作针对返回结果 进行count()运算
select count(name) from exam_result;
-- 对语文成绩求和
select sum(chinese) from exam_result;

-- 考勤系统，包含员工表，考勤记录表
create table employee(
    id int primary key, -- 编号
    name varchar(20), -- 姓名 
    idcard varchar(18), -- 身份证号
);
create table attendance_records(
    id int primary key,
    days int, -- 出勤天数
    time int  -- 出勤时间
    foreign key (id) references employee(id)
)


-- 学校宿舍管理系统，要求包含宿舍信息，学生信息，每日的宿舍查房记录。
create table dormitory(
    drom_id int primary key, -- 宿舍号 
    address varchar(20) -- 宿舍地址
);
create table student(
    id int primary key, -- 学号
    name varchar(20), -- 姓名
    drom_id int, -- 宿舍号
    foreign key (drom_id) references dormitory(drom_id)
);
create table info(
    id int primary key, -- 查寝编号
    dormitory_id int, -- 宿舍号
    status bit, -- 查寝地点 
    info_date timestamp, -- 查寝时间
    foreign key (dormitory_id) references dormitory(drom_id)
);

-- 车辆违章系统，包含用户表，车辆表，违章信息表。违章信息表中包含用户和车辆的违章信息

create table user(
    username varchar(20),-- 姓名
    idcard varchar(18) primary key-- 身份证号
);
create table cur(
    name varchar(20), -- 车辆名称
    curid varchar(20) primary key, -- 车牌号
    userid varchar(18), -- 使用者id
    foreign key (userid) references user(idcard)
);
create table violation_information(
    id int primary key, -- 信息编号
    idcard varchar(18),-- 身份证号
    curid varchar(20) unique,-- 车牌号
    foreign key (idcard) references user(idcard),
    foreign key (curid) references cur(curid)
);

-- 学校食堂管理系统，包含食堂表，食堂仓口表，仓口收费记录表
create table canteen(
    id int primary key,
    address varchar(20) -- 地址
);
create table canteen_barn(
    id int primary key, -- 窗口编号
    barn_name varchar(20) , -- 窗口名称
    canteen_id int, -- 属于哪个食堂
    foreign key(canteen_id) references canteen(id)
);
create table records_charges(
    records_id int primary key, -- 收费编号
    money int, -- 收费金额
    barn_id int, -- 属于哪个窗口
    info_time datetime, -- 收费时间
    foreign key(barn_id) references canteen_barn(id) 
);




