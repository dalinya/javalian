-- 创建考试成绩表
DROP TABLE IF EXISTS exam_result;
CREATE TABLE exam_result (
 id INT,
 name VARCHAR(20),
 chinese DECIMAL(3,1),
 math DECIMAL(3,1),
 english DECIMAL(3,1)
);
-- 插入测试数据
INSERT INTO exam_result (id,name, chinese, math, english) VALUES

 (1,'唐三藏', 67, 98, 56),
 (2,'孙悟空', 87.5, 78, 77),
 (3,'猪悟能', 88, 98.5, 90),
 (4,'曹孟德', 82, 84, 67),
 (5,'刘玄德', 55.5, 85, 45),
 (6,'孙权', 70, 73, 78.5),
 (7,'宋公明', 75, 65, 30);
insert into exam_result values(NULL,null,null,null,null);
-- 聚合函数
-- 求行数
select count(*) from exam_result;
select count(name) from exam_result;
-- 求和/平均值/最大值/最小值 只能针对数字类型操作
select sum(name) from exam_result; -- 会有警告
select sum(math)总数学成绩 from exam_result;
select avg(chinese) from exam_result;
select max(chinese) from exam_result;
select min(chinese) from exam_result;
select avg(chinese + math + english) from exam_result; -- 针对表达式进行聚合计算
-- group by分组查询
create table emp(id int primary key auto_increment,name varchar(20),role varchar(20),salary int);
insert into emp values(null,'马云爸爸','老板',10000000);
insert into emp values(null,'马化腾老公','老板',12000000);
insert into emp values(null,'汤老师','程序员',10000);
insert into emp values(null,'大博哥','程序员',11000);
insert into emp values(null,'小博哥','程序员',12000);
insert into emp values(null,'赵姐接','班主任',8000);
insert into emp values(null,'张姐姐','班主任',9000);

select role ,avg(salary) from emp group by role;
-- having 分组后过滤
select role,avg(salary) from emp where name != '汤老师' group by role;
select role,avg(salary) from emp group by role having avg(salary) < 100000;
select role,avg(salary) from emp group by role having avg(salary) < 100000 and avg(salary) > 10000;
select role,avg(salary) from emp group by role having role != '老板';

-- 多表查询
drop table if exists student;
drop table if exists class;
create table student(id int primary key auto_increment,name varchar(20),classId varchar(20));
insert into student values(null,'张三',1),(null,'李四',1),(null,'王五',2),(null,'赵六',2);
create table class(classId int primary key auto_increment,name varchar(20));
insert into class  values(1,'Java106'),(2,'java106');
select * from student,class;
select * from student,class where student.classId = class.classId


drop table if exists classes;
drop table if exists student;
drop table if exists course;
drop table if exists score;

create table classes (id int primary key auto_increment, name varchar(20), `desc` varchar(100));

create table student (id int primary key auto_increment, sn varchar(20),  name varchar(20), qq_mail varchar(20) ,
        classes_id int);

create table course(id int primary key auto_increment, name varchar(20));

create table score(score decimal(3, 1), student_id int, course_id int);

insert into classes(name, `desc`) values 
('计算机系2019级1班', '学习了计算机原理、C和Java语言、数据结构和算法'),
('中文系2019级3班','学习了中国传统文学'),
('自动化2019级5班','学习了机械自动化');

insert into student(sn, name, qq_mail, classes_id) values
('09982','黑旋风李逵','xuanfeng@qq.com',1),
('00835','菩提老祖',null,1),
('00391','白素贞',null,1),
('00031','许仙','xuxian@qq.com',1),
('00054','不想毕业',null,1),
('51234','好好说话','say@qq.com',2),
('83223','tellme',null,2),
('09527','老外学中文','foreigner@qq.com',2);

insert into course(name) values
('Java'),('中国传统文化'),('计算机原理'),('语文'),('高阶数学'),('英文');

insert into score(score, student_id, course_id) values
-- 黑旋风李逵
(70.5, 1, 1),(98.5, 1, 3),(33, 1, 5),(98, 1, 6),
-- 菩提老祖
(60, 2, 1),(59.5, 2, 5),
-- 白素贞
(33, 3, 1),(68, 3, 3),(99, 3, 5),
-- 许仙
(67, 4, 1),(23, 4, 3),(56, 4, 5),(72, 4, 6),
-- 不想毕业
(81, 5, 1),(37, 5, 5),
-- 好好说话
(56, 6, 2),(43, 6, 4),(79, 6, 6),
-- tellme
(80, 7, 2),(92, 7, 6);

-- 计算笛卡尔集
select *from student , score;
-- 引入连接条件
select *from student , score where student.id = score.student_id;
-- 加入必要条件
select *from student , score where student.id = score.student_id and student.name = '许仙';
-- 去掉不必要的列
select student.name,score.score  from student , score where student.id = score.student_id and student.name = '许仙';

create table emp(name varchar(20),sex varchar(20),depart varchar(20),salary double(7,2));
insert into emp values ('张三','男','组织部',5000), ('李四','女','组织部',6000),
        ('王五','男','生活部',12000), ('赵六','女','生活部',9000), ('申公豹','男','研发部',15000);
insert into emp values ('张三','男','办公部',9000);
select sex as 性别,avg(salary) as 平均工资 from emp group by sex;
select depart ,sum(salary) from emp group by depart;
select depart ,sum(salary) from emp group by depart order by sum(salary) desc  limit 1 offset 1;
select name from emp group by name having count(name) > 1;
select avg(salary) from emp where salary > 10000 and sex = '男';