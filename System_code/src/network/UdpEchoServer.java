package network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

//UDP版本的回显服务器
public class UdpEchoServer {
    private DatagramSocket socket = null;

    public UdpEchoServer(int port) throws SocketException {
        this.socket = new DatagramSocket(port);
    }
    public void start() throws IOException {
        System.out.println("服务器启动！");
        while (true){
            //只要有客户端送过来，就可以提供服务
            //1.读取客户端发来的请求是啥
            //   receive 方法的参数是一个输出型参数，需要先构造好个空白的 DatagramPacket 对象，交给 receive 来进行填充。
            DatagramPacket requestPacket = new DatagramPacket(new byte[4096],4096);
            socket.receive(requestPacket);
            // 此时这个DatagramPacket是一个特殊的对象，并不方便处理，可以把这里包含的数据拿出来，构造成一个字符串。
            String request = new String(requestPacket.getData(),0,requestPacket.getLength());
            //2. 根据请求计算响应，由于此处显示服务器，响应和请求相同。
            String response = process(request);
            //3. 把响应写道客户端，send 的参数也是DatagramPacket，需要把这个 Packet 对象构造好,
            //   此处构造的响应对象，不能用空的字节数组构造了，而是要使用响应数组来构造。
            DatagramPacket responsePacket = new DatagramPacket(response.getBytes(),response.getBytes().length,
                            requestPacket.getSocketAddress());
            socket.send(responsePacket);
            //4. 打印一下，当前这次请求响应的中间处理结果
            System.out.printf("[%s:%d] req : %s;resp: %s\n",requestPacket.getAddress().toString(),
                    requestPacket.getPort(),request,response);
        }
    }

    // 这个方法就表示"根据请求计算相应"
    public String process(String request){
        return request;
    }
    public static void main(String[] args) throws IOException {
        //端口号的指定，大家可以随便指定。
        //1024 -> 65535 这个范围里随便挑个数字就行了。
        UdpEchoServer server = new UdpEchoServer(9090);
        server.start();
    }
}

