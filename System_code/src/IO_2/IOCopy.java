package IO_2;

import java.io.*;
import java.util.Scanner;

public class IOCopy {
    public static void main(String[] args) {
        //输入两个路径
        //源路径 和 目标路径
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入要拷贝哪个文件");
        String srcPath = scanner.next();
        System.out.println("请输入要被拷贝的地方是那里");
        String destPath = scanner.next();
        File srcFile = new File(srcPath);
        if (!srcFile.isFile()){
            // 如果源不是一个文件(是个目录或者不存在)
            //此时不做任何操作
            System.out.println("您当前输入的源路径有误！");
            return;
        }
        File destFile = new File(destPath);
        if (destFile.isFile()){
            //如果已经存在，认为也不能拷贝
            System.out.println("您当前输入的路径有误！");
            return;
        }
        try (InputStream inputStream = new FileInputStream(srcFile);
            OutputStream outputStream = new FileOutputStream(destFile)){
            //如果已经存在，不能让拷贝的覆盖该文件 认为也不能拷贝
            //拷贝操作
            while (true){
                int b = inputStream.read();
                if (b == -1){
                    return;
                }
                outputStream.write(b);

            }
        }catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
