package IO_2;

import java.io.*;

public class IODemo4 {
    public static void main(String[] args) throws IOException {
        //创建 InputStream 对象的时候，使用绝对路径或者相对路径，都是可以的，也可以使用File对象
        InputStream inputStream = new FileInputStream("d:/Bicycle.jpg");
        //进行读操作
        int count = 0;
        while (true){
            byte[] buffer = new byte[1024];
            int len = inputStream.read(buffer);
            count++;
            if (len == -1){
                //读取完毕
                break;
            }
            System.out.printf("%d\n",len);
        }
        System.out.println("读取次数：" + count);
        inputStream.close();
    }

    //使用一下字节流来读取文件
    public static void main2(String[] args) throws IOException {
        //创建 InputStream 对象的时候，使用绝对路径或者相对路径，都是可以的，也可以使用File对象
        InputStream inputStream = new FileInputStream("d:/test.txt");
        //进行读操作
        while (true){
            byte[] buffer = new byte[1024];
            int len = inputStream.read(buffer);
            if (len == -1){
                //读取完毕
                break;
            }
            //此时读取的结果就放到了 byte 数组中
            for (int i = 0; i < len; i++) {
                System.out.printf("%x\n",buffer[i]);
            }
        }
        inputStream.close();
    }

    public static void main1(String[] args) throws IOException {
        //创建 InputStream 对象的时候，使用绝对路径或者相对路径，都是可以的，也可以使用File对象
        InputStream inputStream = new FileInputStream("d:/test.txt");
        //进行读操作
        /*while (true){
            int b = inputStream.read();
            if (b == -1){
                //读取完毕
                break;
            }
            System.out.printf("%x\n",(byte)b);
        }*/


        inputStream.close();
    }
}
