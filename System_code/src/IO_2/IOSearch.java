package IO_2;

import com.sun.xml.internal.stream.StaxXMLInputSource;

import java.io.File;
import java.util.Scanner;

public class IOSearch {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("请输入指定目录:");
        String srcPath = scanner.next();
        File file = new File(srcPath);
        if (!file.isDirectory()){
            System.out.println("输入目录有误");
            return;
        }
        System.out.println("请输入你要找的文件的名称");
        String nameToSearch = scanner.next();
        ScanDir(file,nameToSearch);

    }

    private static void ScanDir(File file, String nameToSearch) {
        File[] files = file.listFiles();
        if (files == null){
            return;
        }
        for (File f: files) {
            if (f.isDirectory()){//是目录
                ScanDir(f,nameToSearch);
            }else {//是文件
                if (f.getName().contains(nameToSearch)){
                    System.out.println("【Dir】:" + f.getAbsolutePath());
                }
            }
        }
    }
}
