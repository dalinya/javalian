package IO_2;

import java.io.File;
import java.io.IOException;

public class IODemo2 {
    public static void main(String[] args) throws IOException {
        File file = new File("./test.txt");
        file.createNewFile();
        System.out.println(file.exists());//判断是否存在
        System.out.println(file.isFile());//判断是否是文件
        System.out.println(file.isDirectory());//判断是否是目录
        System.out.println(file.canRead());//判断能否读
        System.out.println(file.canWrite());//判断能否写
    }
}
