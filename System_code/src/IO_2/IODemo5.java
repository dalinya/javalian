package IO_2;

import java.io.*;
import java.util.Scanner;

public class IODemo5 {
    public static void main(String[] args) {


    }
    public static void main4(String[] args) {
        try (InputStream inputStream = new FileInputStream("d:/test.txt")){
            Scanner scanner = new Scanner(inputStream);
            while (scanner.hasNext()){
                System.out.println(scanner.next());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public static void main3(String[] args) {
        try (Writer writer = new FileWriter("d:/test.txt")){
            writer.write("hello world");
            //手动刷新缓冲区
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public static void main2(String[] args) {
        try (Reader reader = new FileReader("d:/test.txt")){
            while (true){
                int ch = reader.read();
                if (ch == -1){
                    break;
                }
                System.out.println("" + (char)ch);
            }
        }catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public static void main1(String[] args) throws IOException {
        try (OutputStream outputStream = new FileOutputStream("D:/test.txt");){
            outputStream.write(97);
            outputStream.write(98);
            outputStream.write(99);
            outputStream.write(100);
        }
    }
}
