package IO_2;

import java.io.File;
import java.io.IOException;

public class IODemo1 {
    public static void main(String[] args) throws IOException {
        File file = new File("./test.txt");
        //不要求在d:/这里真的存在test.txt文件，不存在也能创建一个对象
        System.out.println(file.getName());//文件名
        System.out.println(file.getParent());//父目录文件路径
        System.out.println(file.getPath());//返回定义时的路径
        System.out.println(file.getAbsolutePath());//相对路径
        System.out.println(file.getCanonicalPath());//修饰过的相对路径
    }
}
