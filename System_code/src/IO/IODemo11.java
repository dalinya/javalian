package IO;

import java.io.File;
import java.util.Scanner;

public class IODemo11 {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        //让用户输入一个指定的搜索目录
        System.out.println("请输入要搜索的路径");
        String basePath = scanner.next();

        //针对用过户输入进行一个简单的判断
        File root = new File(basePath);
        if (!root.isDirectory()){
            //路径不存在，或者只是一个普通文件。此时无法进行搜索
            System.out.println("输入的目录有误");
            return;
        }
        //再让用户输入一个要删除的文件名
        System.out.println("请输入要删除的文件名");
        //此处要使用 next,而不是要使用nextLine
        String nameToDelete = scanner.next();
        //针对指定的路径进行扫描，递归操作。
        //先从根目录出发。(root)
        //先判定一下，当前这个目录是否包含咱要删除的文件
        //如果当前这里包含了一些目录，再针对子目录进行递归。
        scanDir(root,nameToDelete);
    }

    private static void scanDir(File root, String nameToDelete) {
        //1.先列出root 下的文件和目录
        File[] files = root.listFiles();
        if (files == null){
            //结束 root 目录下没东西，是一个空目录
            //结束当前递归
            return;
        }
        //2.遍历当前列出的结果
        for (File f: files) {
            if (f.isDirectory()){
                //如果是目录，就进一步递归
                scanDir(f,nameToDelete);
            }else {
                //如果是普通文件判断是否要删除
                if (f.getName().contains(nameToDelete)){
                    System.out.println("确认是否要删除" + f.getAbsolutePath() + "吗？");
                    String choice = scanner.next();
                    if(choice.equals("y")||choice.equals("Y")){
                        f.delete();
                        System.out.println("删除成功");
                    }else {
                        System.out.println("删除失败");
                    }
                }
            }
        }

    }
}
