package IO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class IODemo10 {
    public static void main(String[] args) {
        try (InputStream inputStream = new FileInputStream("d:/text.txt");
             Scanner scanner = new Scanner(inputStream)){
            //此时读取的内容就是从 文件 中进行读取了
            System.out.println(scanner.next());

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
