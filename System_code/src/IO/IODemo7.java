package IO;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class IODemo7 {
    public static void main(String[] args) throws IOException {
        //进行写文件
        /*OutputStream outputStream = new FileOutputStream("d:/text.txt");
        outputStream.write(97);
        outputStream.write(98);
        outputStream.write(99);
        outputStream.write(100);
        outputStream.close();*/

        //try with resources
        try (OutputStream outputStream = new FileOutputStream("d:/text.txt");){
            outputStream.write(97);
            outputStream.write(98);
            outputStream.write(99);
            outputStream.write(100);
        }

    }
}
