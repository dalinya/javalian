package IO;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


public class IODemo6 {
    public static void main(String[] args) throws IOException {
//        InputStream inputStream = new FileInputStream("d:/text.txt");
        InputStream inputStream = new FileInputStream("d:/Bicycle.jpg");
        //读取操作
        /*while (true){
            int b = inputStream.read();
            if (b == -1){
                //读取完毕
                break;
            }
            System.out.printf("%x\n",(byte)b);
        }*/
        while (true){
            byte[] buffer = new byte[1024];
            int len = inputStream.read(buffer);
            System.out.println("len: " + len);
            if (len == -1){
                break;
            }
//            for (int i = 0; i < len; i++) {
//                System.out.printf("%x\n",buffer[i]);
//            }
        }
        inputStream.close();
    }

}
