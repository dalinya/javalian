package Thread;
class MyRunnable implements Runnable{

    @Override
    public void run() {
        System.out.println("hello thread");
    }
}

public class TreadDemo2 {
    public static void main(String[] args) {
        //这里只是描述了个任务
        Runnable runnable = new MyRunnable();
        //把这个任务交给线程来执行
        Thread t = new Thread(runnable);
        t.start();
    }
}
