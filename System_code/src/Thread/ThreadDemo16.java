package Thread;

public class ThreadDemo16 {
    public static void main(String[] args) throws InterruptedException {
        Object object = new Object();
        synchronized (object) {
            System.out.println("wait之前");
            object.wait(1000);
            System.out.println("wait之后");
        }
    }
}

