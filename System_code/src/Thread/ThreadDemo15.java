package Thread;

import java.util.Scanner;

class MyCounter{
    volatile public int flag = 0;
}
public class ThreadDemo15 {
    public static void main(String[] args) {
        MyCounter myCounter = new MyCounter();
        Thread t1 = new Thread(()->{
            while (myCounter.flag == 0){
                //这个循环咱就空着
            }
            System.out.println("循环结束");
        });
        Thread t2 = new Thread(()->{
            Scanner scanner = new Scanner(System.in);
            System.out.println("请输入一个数字:");
            myCounter.flag = scanner.nextInt();
        });
        t1.start();
        t2.start();

    }

}
