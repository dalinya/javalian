package Thread;

public class ThreadDemo8 {
    private static boolean flag = true;
    public static void main(String[] args) {
        Thread t = new Thread(()->{
        while (flag){
            System.out.println("hello thread");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }});
        t.start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        flag = false;
    }

}
