package Thread;
class Counter{
    public int count = 0;
    public synchronized void add(){
            count++;
    }
    public synchronized void add2(){
        count++;
    }
}
public class ThreadDemo13 {
    public static void main(String[] args) {
        Counter counter = new Counter();
        Thread t1 = new Thread(()->{
            for (int i = 0; i < 50000; i++) {
                counter.add();
            }
        });
        Thread t2 = new Thread(()->{
            for (int i = 0; i < 50000; i++) {
                counter.add2();
            }
        });
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("count = " + counter.count);
    }
}
