package Thread;

public class ThreadDemo17 {
    public static void main(String[] args) {
        Object object = new Object();
        Thread t1 = new Thread(()->{
            //负责等待的线程
            System.out.println("wait之前");
            try {
                synchronized (object) {
                    object.wait();
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("wait之后");
        });
        Thread t2 = new Thread(()->{
            System.out.println("t2:notify之前");
            /*try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            synchronized (object) {
                object.notify();
            }
            System.out.println("t2:notify之后");
        });
        t1.start();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        t2.start();
    }
}
