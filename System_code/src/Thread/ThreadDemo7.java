package Thread;

public class ThreadDemo7 {
    public static void main(String[] args) {//线程抢占式执行
        Thread t = new Thread(()->{
            for (int i = 0; i < 300; i++) {
                System.out.println("hello Thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        t.start();
        while (true){
            try {
                Thread.sleep(1000);
                System.out.println("hello main");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

    }

}
