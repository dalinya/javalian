package com.example.interviewquestions.Thread;

import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class VirtualThread {
    @Test
    public void testCreateVirtualThread1() throws InterruptedException {
        //创建并启动虚拟线程
        Thread.startVirtualThread(() -> {
            System.out.println("Do virtual thread1");
        });
        Thread.sleep(1000);
    }
    @Test
    public void testCreateVirtualThread2() throws InterruptedException {
        //创建虚拟线程
        Thread vt = Thread.ofVirtual().unstarted(()->{
            System.out.println("Do virtual thread2");
        });
        //运行虚拟线程
        vt.start();
        Thread.sleep(1000);
    }
    @Test
    public void testCreateVirtualThread3() throws InterruptedException {
        //创建线程工厂
        ThreadFactory tf = Thread.ofVirtual().factory();
        //创建虚拟线程
        Thread vt = tf.newThread(()->{
            System.out.println("Do virtual thread3");
        });
        //运行虚拟线程
        vt.start();
        Thread.sleep(1000);
    }
    @Test
    public void testCreateVirtualThread4() throws InterruptedException {
        //创建一个支持虚拟线程的线程池
        ExecutorService executor = Executors.newVirtualThreadPerTaskExecutor();
//        executor.submit(()->{
//            System.out.println("Do virtual thread4");
//        });
        //提交任务到线程池
        for (int i = 0; i < 5; i++) {
            executor.submit(()->{
                Thread thread = Thread.currentThread();
                System.out.println("当前线程 Id：" + thread.threadId() +
                        "| 是否为虚拟线程：" + thread.isVirtual());
            });
        }
        Thread.sleep(1000);
    }
}
