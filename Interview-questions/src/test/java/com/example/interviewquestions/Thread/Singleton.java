package com.example.interviewquestions.Thread;

public class Singleton {
    private Singleton(){

    }
    //使用 volatile 禁止指令重排序
    private static volatile Singleton instance = null;
    public static Singleton getInstance(){
        if (instance == null){//避免初始化后还重复加锁
            synchronized (Singleton.class){
                if (instance == null){//避免重复初始化
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}
