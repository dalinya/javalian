package com.example.interviewquestions.Thread;

import org.junit.jupiter.api.Test;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 线程间的通信
 */
public class correspondence {
    @Test
    public void testObject() throws InterruptedException {
        Object lock = new Object();
        Object lock2 = new Object();
        //创建线程并执行
        new Thread(()->{
            System.out.println("线程1：开始执行");
            synchronized (lock){
                try {
                    System.out.println("线程1：进入等待");
                    lock.wait();
                    System.out.println("线程1：继续执行");
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("线程1：执行结束");
            }
        }).start();
        Thread.sleep(1000);
        synchronized (lock){
            //唤醒线程
            System.out.println("执行 notifyAll()");
            lock.notifyAll();
        }

    }
    @Test
    public void testCondition(){
        Lock lock = new ReentrantLock();
        //生产者的Condition对象
        Condition producerCondition = lock.newCondition();
        //消费者的Condition对象
        Condition consumerCondition = lock.newCondition();
        //加锁
        lock.lock();
        try {
            //1.进入等待状态
            System.out.println("进入等待状态: consumerCondition.await()");
            consumerCondition.await();
            //2.唤醒操作
            consumerCondition.signal();
        }catch (InterruptedException e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }
    @Test
    public void test() throws InterruptedException {
        Thread t1 = new Thread(()->{
            System.out.println("线程1休眠");
            LockSupport.park();
            System.out.println("线程1结束");
        },"线程1");
        t1.start();
        Thread t2 = new Thread(()->{
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("唤醒线程1");
            LockSupport.unpark(t1);
            System.out.println("线程2结束");
        },"线程2");
        t2.start();
        Thread.sleep(3000);
    }

}
