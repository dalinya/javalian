package Greedy;

public class Solution_122{
    //贪心：连续上涨：最低点买入，最高点买出，
    //连续下跌：不进行任何操作。
    public int maxProfit(int[] prices) {
        int profit = 0;
        for(int i = 1;i < prices.length;i++){
            //价格趋势
            if(prices[i] > prices[i - 1]){
                profit += prices[i] - prices[i-1];
            }
        }
        return profit;
    }


}
