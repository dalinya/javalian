package Greedy;

import java.util.*;

public class TestSome {
    public static int getMatTime(int[] taskTime,int m){
        int n = taskTime.length;
        //排序:递增排序
        Arrays.sort(taskTime);
        int[] machines = new int[m];
        if (n <= m){
            return taskTime[n - 1];
        }else {
            for (int i = n - 1; i >= 0; i--) {
                int min = 0;//最小数据的下标
                for (int j = 0; j < m; j++) {
                    if (machines[min] > machines[j]){
                        min = j;
                    }
                }
                //找一个最先结束的机器，分配新的任务
                /*int finish = 0;
                int machineTime = machines[finish];
                for (int j = 0; j < m; j++) {//找到一个最小的数据
                    if (machines[j] < machineTime){
                        finish = j;
                        machineTime = machines[j];
                    }
                }*/
                //新的任务分配给最先结束的机器
                machines[min] += taskTime[i];
            }
        }
        return findMax(machines);


    }

    private static int findMax(int[] machines) {
        int ret = machines[0];
        for (int i = 0; i < machines.length; i++) {
            ret = Math.max(ret,machines[i]);
        }
        return ret;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入任务和机器个数");
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        System.out.println("输入任务的时间");
        int[] taskTime = new int[n];
        for (int i = 0; i < n; i++) {
            taskTime[i] = scanner.nextInt();
        }
        System.out.println("最短时间" + getMatTime(taskTime,m));
    }
}



