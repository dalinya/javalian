package Greedy;

public class Solution_55 {
    public boolean canJump(int[] nums) {
        int max = 0;
        int pos = nums.length - 1;
        for(int i = 0; i <= pos; i++){
            //判断是否可以到达当前位置
            if(i <= max){
                max = Math.max(i + nums[i],max);
                //判断最远的位置是否包含最后一个位置
                if(max >= pos){
                    return true;
                }
            }else{

            }
        }
        return false;
    }
}
