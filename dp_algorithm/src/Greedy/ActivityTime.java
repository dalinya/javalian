package Greedy;

import java.util.Arrays;
import java.util.Comparator;

class Cmp implements Comparator<int[]> {

    @Override
    public int compare(int[] o1, int[] o2) {
        return o1[1] - o2[1];
    }
}
public class ActivityTime {

    public static void main(String[] args) {
        int[][] events = {{2,5},{3,4},{1,6},{5,8},{5,7},{3,9},{7,10}};
        //求一天安排最多的活动数量
        int num = getMaxNum(events);

        System.out.println(num);
    }

    private static int getMaxNum(int[][] events) {
        //按照结束时间进行递增排序
        Arrays.sort(events,new Cmp());
/*        for (int i = 0; i < events.length; i++) {
            System.out.println(events[i][0] + "    " + events[i][1]);
            System.out.println();
        }*/
        int count = 1;//一天举行活动的最大数量
        int i = 0;//举行该活动的下表
        for (int j = 0; j < events.length; j++) {
            //下一个活动的起始时间，不小于当前活动的结束时间。
            if (events[j][0] >= events[i][1]){
                i = j;
                count++;
            }
        }
        return count;
    }

}
