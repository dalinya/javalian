package Greedy;

import java.util.Arrays;
import java.util.Scanner;


public class moneyUse{
   /* public static int maxNum(int money,int[][] moneyMat){
        int count = 0;
        for (int i = moneyMat.length - 1; i >= 0; i--) {
            int c = Math.min(money/moneyMat[i][0],moneyMat[i][1]);
            money -= c * moneyMat[i][0];
            count += c;
        }
        if (money != 0){
            return -1;
        }
        return count;

    }*/

    public static void main(String[] args) {
        int[][] moneyCount = { { 1, 3 }, { 2, 1 }, { 5, 4 }, { 10, 3 }, { 20, 0 }  ,{50, 1}, { 100, 10 } };
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入要支付的钱");
        int count = maxNum(scanner.nextInt(), moneyCount);//通过maxNum方法计算出最小张数量
        if (count == -1){
            System.out.println("No");
        }else {
            System.out.println("最小张数" + count);
        }
    }

    private static int maxNum(int money, int[][] moneyCount) {
        int count = 0;//记录当前花费钞票的张数
        for (int i = moneyCount.length - 1; i >= 0; i--) {
            int num = Math.min(money / moneyCount[i][0],moneyCount[i][1]);
            count += num;
            money -= num * moneyCount[i][0];
            if (money == 0){
                return count;
            }
        }
        return -1;
    }


}
