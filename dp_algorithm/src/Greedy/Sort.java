package Greedy;

public class Sort {
    public static void selectSort(int[] array,int n){
        for (int i = 0; i < n; i++) {

            int minIdx = i;
            //贪心：每次从未排序数组中找到最小值
            for (int j = i; j < n; j++) {
                if (array[j] < array[minIdx]){
                    minIdx = j;
                }
            }
            swap(array,i,minIdx);
        }
    }
    private static void swap(int[] array, int i, int minIdx) {
        int tmp = array[i];
        array[i] = array[minIdx];
        array[minIdx] = tmp;
    }

}
