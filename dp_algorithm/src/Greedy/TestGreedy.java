package Greedy;

import java.util.*;

public class TestGreedy {
    public static void main(String[] args) {
        int[] arr = {9,8,4,6,2,5};
        selectSort(arr);
        System.out.println(Arrays.toString(arr));
    }

    private static void selectSort(int[] arr) {//插入排序
        for (int i = 0; i < arr.length; i++) {
            int min = i;
            for (int j = i; j < arr.length; j++) {
                if (arr[j] < arr[min]){
                    min = j;
                }
            }
            swap(arr,i,min);
        }
    }

    private static void swap(int[] arr, int i, int min) {
        int tmp = arr[i];
        arr[i] = arr[min];
        arr[min] = tmp;
    }


    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int[] taskTime = new int[n];
        for (int i = 0; i < n; i++) {
            taskTime[i] = scanner.nextInt();
        }
        int num = greedStrategy(m,taskTime);
        System.out.println("处理的最短时间" + num);
    }

    private static int greedStrategy(int m, int[] taskTime) {
        Arrays.sort(taskTime);
        if (m >= taskTime.length){//当机器数量大于等于任务数量
            return taskTime[taskTime.length - 1];
        }
        int[] machines = new int[m];
        for (int i = taskTime.length - 1; i >= 0; i--) {
            int num = taskTime[i];
            int min = 0;
            for (int j = 0; j < m; j++) {//找到机器中最先运行结束的机器
                if (machines[j] < machines[min]){
                    min = j;
                }
            }
            machines[min] += num;//最先结束运行的机器加上需要执行的任务
        }
        int max = 0;
        for (int i = 0; i < m; i++) {
            if (max < machines[i]){
                max = machines[i];
            }
        }
        return max;
    }


    public boolean canJump(int[] nums) {
        int maxStep = 0;
        int far = nums.length  - 1;
        for (int i = 0; i < nums.length; i++) {
            if (maxStep >= i){//可以到达i下标
                if (i + nums[i] > maxStep){//在该位置重新计算maxStep
                    maxStep = i + nums[i];
                }
                if (maxStep >= far){
                    return true;
                }
            }
        }
        return false;

    }
    public boolean canJump1(int[] nums) {
        int maxStep = nums[0];
        int far = nums.length - 1;
        for (int i = 1; i < nums.length; i++) {
            if (maxStep < i){
                return false;
            }
            if (i + nums[i] > maxStep){
                maxStep = i + nums[i];
            }
            if (maxStep >= far){
                return true;
            }
        }
        return true;
    }
    public int maxProfit(int[] prices) {
        int[][] dp = new int[prices.length][2];
        //初始化
        dp[0][1] = -prices[0];
        for (int i = 1; i < prices.length; i++) {
            dp[i][0] = Math.max(prices[i] + dp[i-1][1],dp[i-1][0]);
            dp[i][1] = Math.max(dp[i-1][1],dp[i-1][0] - prices[i]);
        }
        return dp[prices.length - 1][0];
    }

    public int maxProfit1(int[] prices) {
        int money = 0;
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] > prices[i-1]){//表示当天股票是对比昨天上涨的
                money += prices[i] - prices[i-1];
            }

        }
        return money;
    }

    public int balancedStringSplit(String s) {
        int countL = 0;
        int countR = 0;
        int count = 0;//平衡字符字串的个数
        for (char ch: s.toCharArray()) {
            if (ch == 'R'){
                countR++;
            }else if (ch == 'L'){
                countL++;
            }
            if (countL == countR){
                count++;
                countR = 0;
                countL = 0;
            }
        }
        return count;
    }
    public int eraseOverlapIntervals(int[][] intervals) {
        Arrays.sort(intervals,(int[] arr1,int[] arr2)-> arr1[1] - arr2[1] );//对区间结束位置进行升序排序
        int count = 0;//记录不需要被移除的区间数量。
        int end = intervals[0][1];//结束时间早的区间结束位置
        for (int i = 1; i < intervals.length; i++) {
            if (end <= intervals[i][0]){
                count++;
                end = intervals[i][1];
            }
        }
        return intervals.length - count;

    }
    public int eraseOverlapIntervals1(int[][] intervals) {
        int count = 0;//移除区间的个数
        Arrays.sort(intervals,(int[] arr1,int[] arr2)-> arr1[0] - arr2[0]);//把区间数组按起始位置进行升序排列。
        int end = intervals[0][1];
        for (int i = 1; i < intervals.length; i++) {
            if (intervals[i][1] < end) {//区间重叠情况1：删除前者。
                end = intervals[i][1];
                count++;
            } else if (intervals[i][0] < end){//重区间叠情况2，删除后者。
                count++;
            }else{//不重叠情况
                end = intervals[i][1];
            }
        }
        return count;

    }


    public int maxEvents(int[][] events) {
        //以开始时间为升序进行排序
        Arrays.sort(events,(int[] arr1,int[] arr2)-> {return arr1[0] - arr2[0];});
        PriorityQueue<Integer> pq = new PriorityQueue<>();//存放结束时间的优先级队列
        int count = 0;//记录可以安排的活动数量
        int i = 0;//遍历活动数组的下标
        int day = 0;//判断每一天是否可以参加会议，是第几天。
        while (!pq.isEmpty() || i < events.length){
            while (i < events.length && day == events[i][0]){//对相同起始时间的会议加入到优先级队列中
                pq.offer(events[i][1]);
                i++;
            }
            while (!pq.isEmpty() && day > pq.peek()){//删除结束时间小于当前天数的活动，也就是说删除不能够举行的会议
                pq.poll();
            }
            if (!pq.isEmpty() && day <= pq.peek()){//对结束时间最小的会议进行分配时间
                pq.poll();
                count++;
            }
            day++;
        }
        return count;
    }

    public static void main1(String[] args) {
        int[][] events = {{2,5},{3,4},{1,6},{5,8},{5,7},{3,9},{7,10}};
        int max = getMax(events);
        System.out.println(max);
    }

    private static int getMax(int[][] events) {
        //进行以结束时间为主，开始时间为辅的升序排序。
        Arrays.sort(events,(int[] arr1,int[] arr2)->  { return arr1[1] - arr2[1] == 0 ? arr1[0] - arr2[0]:arr1[1] - arr2[1];});
        int count = 1;
        int cur = 0;
        for (int i = 1; i < events.length; i++) {
            if (events[i][0] >= events[cur][1]){
                count++;
                cur = i;
            }
        }
        return count;
    }

}
