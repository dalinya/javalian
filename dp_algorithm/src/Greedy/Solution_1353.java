package Greedy;

import java.util.Arrays;
import java.util.PriorityQueue;

public class Solution_1353 {
    public int maxEvents(int[][] events){
        int n = events.length;
        Arrays.sort(events,(int[] o1,int[] o2) -> o1[0] - o2[0]);//按照开始时间进行排序，方便选择开始时间相同的会议。
        PriorityQueue<Integer> pq = new PriorityQueue<>();//存储能参加会议的结束时间
        int count = 0;//可以参加会议的数量
        int currDay = 1;//判断每一天是否可以参加会议的，是第几天
        int i = 0;//记录放进优先级队列数组的下标
        while (i < n || !pq.isEmpty()){//从第一天开始判断每一天是否可以参加会议
            while (i < n && events[i][0] == currDay){//将开始时间等于当前天的会议的结束时间放到优先级队列中
                pq.offer(events[i][1]);
                i++;
            }
            //将结束时间小于当前天的会议从优先级队列中取出
            //也就是已经不能参加的会议
            while (!pq.isEmpty() && pq.peek() < currDay){
                pq.poll();
            }
            //贪心思想：在能参加的会议中选择结束时间最早的会议进行参加
            if (!pq.isEmpty()){
                pq.poll();
            }
            currDay++;
        }
        return count;
    }


    //贪心思想：在所有开始时间相同的会议中，尽量选择结束时间最小的会议。
    public int maxEvents1(int[][] events) {
        int n = events.length;
        Arrays.sort(events,(int[] o1, int[] o2) -> o1[0] - o2[10]);//按照开始时间进行升序排序
        int count = 0;//参加的会议数量
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        int currDay = 1;//循环的天数
        int i = 0;
        while (i < n && !pq.isEmpty()){
            //将所有开始时间等于 currDay的会议结束时间放到小根堆
            while (i < n && events[i][0] == currDay){
                pq.offer(events[i][1]);
                i++;
            }
            //将已经结束的会议弹出堆中，即堆顶的结束时间小于 currDay
            while (!pq.isEmpty() && pq.peek() < currDay){
                pq.poll();
            }
            //贪心算法选择结束时间小的会议参加
            if (!pq.isEmpty()){
                //参加的会议，就从堆中删除
                pq.poll();
                count++;
            }
            currDay++;
        }

        return count;
    }

}
