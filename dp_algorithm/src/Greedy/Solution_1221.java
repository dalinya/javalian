package Greedy;

public class Solution_1221 {
    public int balancedStringSplit(String s) {//不让字串嵌套，有一个字串平行就重新开始
        int balance = 0;
        int count = 0;
        for(int i = 0; i < s.length();i++){
            if(s.charAt(i) == 'L'){
                balance++;
            }else{
                balance--;
            }
            if(balance == 0){
                count++;
            }
        }
        return count;
    }
}
