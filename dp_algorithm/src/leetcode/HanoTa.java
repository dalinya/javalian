package leetcode;

import java.util.List;

public class HanoTa {
    public void hanota(List<Integer> A, List<Integer> B, List<Integer> C) {
        int length = A.size();
        swap(length,A,B,C);
    }

    private void swap(int length, List<Integer> a, List<Integer> b, List<Integer> c) {
        if (length == 1){
            c.add(a.remove(0));
        }
        swap(length - 1,a,c,b);
        c.add(a.remove(a.size()-1));
        swap(length-1,b,a,c);

    }

    public static void main(String[] args) {
        hanota2(3,"A","B","C");

    }

    private static void hanota2(int i, String pos1, String pos2, String pos3) {
        if (i == 1){
            System.out.println(pos1 + "--> " + pos3);
            return;
        }
        hanota2(i-1,pos1,pos3,pos2);
        System.out.println(pos1 + "--> " + pos3);
        hanota2(i-1,pos2,pos1,pos3);
    }
}
