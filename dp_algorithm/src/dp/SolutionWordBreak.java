package dp;

import java.util.Set;

public class SolutionWordBreak {
    public boolean wordBreak(String s, Set<String> dict) {
        boolean[] canBreak = new boolean[s.length() + 1];
        //初始化辅助
        canBreak[0] = true;
        for(int i = 0;i <= s.length(); ++i){
            for(int j = 0; j < i; j++){
                if(canBreak[j] && dict.contains(s.substring(j,i))){
                    canBreak[i] = true;
                    break;
                }
            }
        }
        return canBreak[s.length()];
    }

}
