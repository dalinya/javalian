package dp;

public class SolutionMinDistance {
    public int minDistance(String word1, String word2) {
        int row = word1.length();
        int col = word2.length();
        int[][] minD = new int[row + 1][col + 1];
        //初始化 F(i,0): i    F(0,j) : j
        for (int i = 0; i <= col; i++) {
            minD[0][i] = i;
        }
        for (int i = 1; i <= row; i++) {
            minD[i][0] = i;
        }
        for (int i = 1; i <= row; i++) {
            for (int j = 1; j <= col; j++) {
                //插入和删除的代价
                minD[i][j] = Math.min(minD[i][j - 1], minD[i - 1][j]) + 1;
                //替换的代价
                if (word1.charAt(i - 1) == word2.charAt(j - 1)) {
                    minD[i][j] = Math.min(minD[i][j], minD[i - 1][j - 1]);
                } else {
                    minD[i][j] = Math.min(minD[i][j], minD[i - 1][j - 1] + 1);
                }
            }
        }
        return minD[row][col];
    }
}
