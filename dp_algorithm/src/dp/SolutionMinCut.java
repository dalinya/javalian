package dp;

public class SolutionMinCut {
    public int minCut(String s) {//优化时间复杂度
        int len = s.length();
        if(len == 0 || isPal(s,0,len - 1)){
            return 0;
        }
        boolean[][] Mat = getMat(s);
        int[] minCut = new int[len + 1];
        for (int i = 0; i <= len; i++){
            minCut[i] = i - 1;
        }

        for (int i = 2; i <= len; i++) {
            //j < i && [j+1,i]是否是回文
            for (int j = 0; j < i; j++) {
                if (Mat[j][i-1]){
                    minCut[i] = Math.min(minCut[i],minCut[j] + 1);
                }
            }
        }
        return minCut[len];
    }
    public boolean[][] getMat(String s){
        int n = s.length();
        boolean[][] Mat = new boolean[n][n];
        for (int i = n - 1; i >= 0; i--) {
            for (int j = i; j < n; j++) {
                if (j == i){
                    Mat[i][j] = true;
                } else if (j == i + 1) {
                    Mat[i][j] = s.charAt(i) == s.charAt(j);
                }else {
                    Mat[i][j] = (s.charAt(i) == s.charAt(j)) && (Mat[i + 1][j - 1]);
                }
            }
        }
        return Mat;
    }


    public int minCut2(String s) {//minCut[0]用起来
        int len = s.length();
        if(len == 0 || isPal(s,0,len - 1)){
            return 0;
        }
        int[] minCut = new int[len + 1];
        for (int i = 0; i <= len; i++){
            minCut[i] = i - 1;
        }
        for (int i = 2; i <= len; i++) {
            //j < i && [j+1,i]是否是回文
            for (int j = 0; j < i; j++) {
                if (isPal(s,j,i-1)){
                    minCut[i] = Math.min(minCut[i],minCut[j] + 1);
                }
            }
        }
        return minCut[len];
    }

    public int minCut1(String s) {//minCut[0]闲置
        int len = s.length();
        if(len == 0 || isPal(s,0,len - 1)){
            return 0;
        }
        int[] minCut = new int[len + 1];
        for (int i = 1; i <= len; i++){
            minCut[i] = i - 1;
        }
        for (int i = 2; i <= len; i++) {
            //整体是否位回文
            if(isPal(s,0,i-1)){
                minCut[i] = 0;
                continue;
            }
            //j < i && [j+1,i]是否是回文
            for (int j = 1; j < i; j++) {
                if (isPal(s,j,i-1)){
                    minCut[i] = Math.min(minCut[i],minCut[j] + 1);
                }
            }
        }
        return minCut[len];
    }
    boolean isPal(String s,int start,int end){//判断是否回文
        while (start < end){
            if (s.charAt(start) != s.charAt(end)){
                return false;
            }
            start++;
            end--;
        }
        return true;
    }
}
