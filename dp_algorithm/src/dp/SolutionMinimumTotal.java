package dp;

import java.util.ArrayList;

public class SolutionMinimumTotal {
    public int minimumTotal(ArrayList<ArrayList<Integer>> triangle) {
        //自低向上
        if(triangle.isEmpty()){
            return 0;
        }
        int row = triangle.size();
        for (int i = row - 2; i >= 0; --i) {
            int curSum = 0;
            for (int j = 0; j <= i; j++) {
                curSum = Math.min(triangle.get(i+1).get(j+1), triangle.get(i + 1).get(j));
                triangle.get(i).set(j,triangle.get(i).get(j) + curSum);
            }
        }
        return triangle.get(0).get(0);
    }
    public int minimumTotal1(ArrayList<ArrayList<Integer>> triangle) {
        if(triangle.isEmpty()){
            return 0;
        }
        int row = triangle.size();
        for (int i = 0; i < row; i++) {
            int curSum = 0;
            for (int j = 0; j <= i; j++) {
                if(j==0){
                    curSum = triangle.get(i-1).get(j);
                } else if(j == i){
                    curSum = triangle.get(i-1).get(j-1);
                }else {
                    curSum = Math.min(triangle.get(i-1).get(j-1), triangle.get(i - 1).get(j));
                }
                triangle.get(i).set(j,triangle.get(i).get(j) + curSum);
            }
        }
        int minSum = triangle.get(row - 1).get(0);
        for (int i = 1; i < triangle.size(); i++) {
            minSum = Math.min(minSum,triangle.get(row - 1).get(i));
        }
        return minSum;
    }
}
















