package dp;

public class SolutionMumDistinct {
    public int numDistinct(String s, String t) {
        int row = s.length();
        int col = t.length();

        /*int[][] numD = new int[row + 1][col + 1];
        numD[0][0] = 1;
        for (int i = 1; i <= row; i++) {
            numD[i][0] = 1;
            for (int j = 1; j <= col; j++) {
                if (s.charAt(i - 1) == t.charAt(j-1)){
                    numD[i][j] = numD[i-1][j-1] + numD[i-1][j];
                }else {
                    numD[i][j] = numD[i-1][j];
                }
            }
        }
        */
        int[] numD = new int[col + 1];
        numD[0] = 1;
        for (int i = 1; i <= row; i++) {
            for (int j = col; j > 0; j--) {//用到了前面列的结果，所以必须从后向前更新
                if (s.charAt(i - 1) == t.charAt(j-1)){
                    numD[j] = numD[j-1] + numD[j];
                }else {
                    numD[j] = numD[j];
                }
            }
        }
        return numD[col];
    }
}
