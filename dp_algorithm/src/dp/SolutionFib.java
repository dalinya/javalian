package dp;

public class SolutionFib {
    public int fib1(int n) {
        // if(n == 0){//递归
        //     return 0;
        // }
        // if(n == 1 || n == 2){
        //     return 1;
        // }
        // return fib(n - 1) + fib(n - 2);
        if(n == 0){//动态规划
            return 0;
        }
        int[] F = new int[n + 1];
        F[0] = 0;
        F[1] = 1;
        for(int i = 2;i <= n;i++){
            F[i] = F[i - 1] % 1000000007 + F[i - 2] % 1000000007;
        }
        return F[n] % 1000000007;
    }
    public int fib(int n) {//迭代法
        if(n == 0){
            return 0;
        }
        if(n == 1){
            return 1;
        }
        int fn1 = 1;
        int fn2 = 0;
        int fn;
        for(int i = 2; i <= n; ++i){
            fn = (fn1 + fn2) % 1000000007;
            //更新中间状态
            fn2 = fn1;
            fn1 = fn;
        }
        return fn1;
    }
}
