package dp;

public class SolutionBackPackII {
    public int backPackII(int m, int[] a, int[] v) {
        // write your code here

        int n = a.length;
        int[] maxValue = new int[m+1];
        for (int i = 1; i <= n; i++) {
            for (int j = m; j >= 0; j--) {
                if(a[i  -1] <= j){
                    maxValue[j] = Math.max(maxValue[j],maxValue[j-a[i-1]] + v[i-1]);
                }
            }
        }
        return maxValue[m];
    }

    /*public int backPackII(int m, int[] a, int[] v) {
        // write your code here

        int n = a.length;
        int[][] maxValue = new int[n+1][m+1];
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if(a[i  -1] <= j){
                    maxValue[i][j] = Math.max(maxValue[i-1][j],maxValue[i-1][j-a[i-1]] + v[i-1]);
                }else {
                    maxValue[i][j] = maxValue[i-1][j];
                }
            }
        }
        return maxValue[n][m];
    }*/
}
