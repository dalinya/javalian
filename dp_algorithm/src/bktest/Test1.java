package bktest;

import java.util.Arrays;
import java.util.Scanner;

public class Test1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int[] taskTime = new int[n];
        for (int i = 0; i < n; i++) {
            taskTime[i] = scanner.nextInt();
        }
        int num = greedStrategy(m,taskTime);
        System.out.println("处理的最短时间" + num);
    }

    private static int  greedStrategy(int m , int[] array) {
        Arrays.sort(array);//进行升序排序
        int n = array.length;
        if(n <= m){//作业数小于等于机器数
            return array[n-1];
        }
        //作业数大于机器数
        int[] machines = new int[m];
        for (int i = array.length - 1; i >= 0; i--) {
            //寻找machines中最下值的下标,也就是分配任务最先结束的机器
            int min = 0;
            for (int j = 0; j < m; j++) {
                if (machines[min] > machines[j]) {
                    min = j;
                }
            }
            //最先处理完的机器 + 一个任务
            machines[min] += array[i];
        }
        return findMax(machines);
    }

    private static int findMax(int[] machines) {
       int max = machines[0];
        for (int i = 1; i < machines.length; i++) {
            if (max < machines[i]){
                max = machines[i];
            }
        }
        return max;
    }


    public static void main1(String[] args) {
        int[] arr = {9,8,4,6,2,5};
        selectSort(arr);
        System.out.println(Arrays.toString(arr));

    }

    private static void selectSort(int[] arr) {
        //从下标0开始一直缩小未排序数据的范围，
        for (int i = 0; i < arr.length; i++) {
            //找到未排序数据中的最小值的索引。
            int min = i;
            for (int j = i; j < arr.length; j++) {
                if (arr[j] < arr[min]){
                    min = j;
                }
            }
            //最小值和未排序数据最小下标的值进行互换
            swap(arr,min,i);
        }
    }

    private static void swap(int[] arr, int min, int i) {
        int tmp = arr[min];
        arr[min] = arr[i];
        arr[i] = tmp;
    }
}
