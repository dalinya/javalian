package bktest;

import java.util.*;

public class Test7 {

    public static void main(String[] args) {
        int a = Integer.parseInt("ABCDE",16);
        System.out.printf("%15d\n",a);
        int b = 2000000000 + 2000000000 + 2000000000;
        System.out.println(b);
    }

    public void solveSudoku(char[][] board) {
        backtracking(board);
    }

    private boolean backtracking(char[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == '.'){
                    for (char k = '1'; k <= '9'; k++) {
                        if (isValid(board,i,j,k)){
                            board[i][j] = k;
                            boolean result = backtracking(board);
                            if (result){
                                return true;
                            }
                            board[i][j] = '.';
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isValid(char[][] board, int x, int y, char num) {
        for (int i = 0; i < 9; i++) {//判断列
            if (board[i][y] == num){
                return false;
            }
        }
        for (int j = 0; j < 9; j++) {//判断行
            if(board[x][j]==num){
                return false;
            }
        }
        int startX = (x / 3)*3;
        int startY = (y / 3)*3;
        for (int i = startX; i < startX + 3; i++) {//判断九宫格
            for (int j = startY; j < startY + 3; j++) {
                if (board[i][j] == num){
                    return false;
                }
            }
        }
        return true;
    }

/*    public List<List<String>> solveNQueens(int n) {
        List<List<String>> result = new ArrayList<>();
        char[][] board = new char[n][n];
        for(char[] chs:board){
            Arrays.fill(chs,'.');
        }
        backtracking(result,board,0,n);
        return result;
    }

    private void backtracking(List<List<String>> result, char[][] board, int row, int n) {
        if (row == n){
            result.add(Array2List(board));
        }
        for (int i = 0; i < n; i++) {
            if (isValid(board,row,i)){
                board[row][i] = 'Q';
                backtracking(result,board,row + 1,n);
                board[row][i] = '.';
            }
        }
    }

    private boolean isValid(char[][] board, int x, int y) {
        for (int i = 0; i < x; i++) {//检查列
            if(board[i][y] == 'Q'){
                return false;
            }
        }
        for (int i = x - 1 , j = y -1; i >= 0 && j >= 0; i--,j--) {//检查45°是否有皇后
            if (board[i][j] == 'Q'){
                return false;
            }
        }
        for (int i = x - 1, j = y + 1; i >= 0 && j < board.length; i--, j++) {//检查135°是否有皇后
            if (board[i][j] == 'Q'){
                return false;
            }
        }
        return true;
    }

    private ArrayList<String> Array2List(char[][] board) {
        ArrayList<String> list = new ArrayList<>();
        for (char[] child: board) {
            String str = "";
            for (char ch:child) {
                str += ch;
            }
            list.add(str);
        }
        return list;
    }*/


/*    List<List<Integer>> result = new ArrayList<>();
    List<Integer> path = new ArrayList<>();
    public List<List<Integer>> permuteUnique(int[] nums) {
        Arrays.sort(nums);
        boolean[] used = new boolean[nums.length];
        backtracking(nums,used);
        return result;
    }

    private void backtracking(int[] nums, boolean[] used) {
        if (nums.length == path.size()){
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            if (i > 0 && nums[i] == nums[i-1] && used[i-1]||used[i]){
                continue;
            }
            used[i] = true;
            path.add(nums[i]);
            backtracking(nums,used);
            path.remove(path.size()-1);
            used[i] = false;
        }
    }*/


//    public List<List<Integer>> permute(int[] nums) {
//        boolean[] used = new boolean[nums.length];
//        backtracking(nums,used);
//        return result;
//    }
//
//    private void backtracking(int[] nums, boolean[] used) {
//        if (nums.length == path.size()){
//            result.add(new ArrayList<>(path));
//            return;
//        }
//        for (int i = 0; i < nums.length; i++) {
//            if (used[i] == true){
//                continue;
//            }
//            used[i] = true;
//            path.add(nums[i]);
//            backtracking(nums,used);
//            path.remove(path.size() - 1);
//            used[i] = false;
//        }
//
//    }
/*
    public List<List<Integer>> findSubsequences(int[] nums) {

        backtracking(nums,0);
        return result;
    }

    private void backtracking(int[] nums, int startIndex) {
        if (path.size()>1){
            result.add(new ArrayList<>(path));
        }
        if (startIndex >= nums.length){
            return;
        }
        Set<Integer> set = new HashSet<>();
        for (int i = startIndex; i < nums.length; i++) {
            if (set.contains(nums[i])){
                continue;
            }
            if (!path.isEmpty() && path.get(path.size()-1) > nums[i]){
                continue;
            }
            set.add(nums[i]);
            path.add(nums[i]);
            backtracking(nums,i+1);
            path.remove(path.size()-1);
        }
    }

    public List<List<Integer>> subsetsWithDup(int[] nums) {
        Arrays.sort(nums);
        boolean[] used = new boolean[nums.length];
        backtracking6(nums,0,used);
        return result;
    }

    private void backtracking6(int[] nums, int startIndex, boolean[] used) {
        result.add(new ArrayList<>(path));
        if (startIndex>=nums.length);
        for (int i = startIndex; i < nums.length; i++) {
            if(i>0 && nums[i] == nums[i-1] && !used[i-1]){
                continue;
            }
            path.add(nums[i]);
            used[i] = true;
            backtracking6(nums,i+1,used);
            used[i]=false;
            path.remove(path.size()-1);
        }
    }

*/


//    public List<List<Integer>> subsets(int[] nums) {
//        backtracking(nums,0);
//        return result;
//    }
//
//    private void backtracking(int[] nums, int startIndex) {
//        result.add(new ArrayList<>(path));
//        if (startIndex >= nums.length){
//            return;
//        }
//        for (int i = startIndex; i < nums.length; i++) {
//            path.add(nums[i]);
//            backtracking(nums, i + 1);
//            path.remove(path.size() - 1);
//        }
//    }
/*
    List<String> result = new ArrayList<>();
    public List<String> restoreIpAddresses(String s) {
        backtracking(s,0,0);
        return result;
    }

    private void backtracking(String s, int startIndex, int pointNum) {
        if (pointNum == 3){
            if (isValid(s,startIndex,s.length()-1)){
                result.add(s);
            }
            return;
        }
        for (int i = startIndex; i < s.length(); i++) {
            if (isValid(s,startIndex,i)){
                s = s.substring(0,i+1) + '.' + s.substring(i+1);
                backtracking(s,i+1,pointNum+1);
                s = s.substring(0,i+1) + s.substring(i+2);
            }
        }
    }

    private boolean isValid(String s, int startIndex, int right) {
        if (right > startIndex && s.charAt(startIndex) == '0'){
                return false;
        }
        int num = Integer.parseInt(s.substring(startIndex,right));
        return num <= 255 && num >= 0;
    }
*/

/*
    List<List<String>> result = new ArrayList<>();
    List<String> path = new ArrayList<>();
    public List<List<String>> partition(String s) {
        backtracking6(s,0);
        return result;
    }

    private void backtracking6(String s, int startIndex) {
        if (s.length() <= startIndex){
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = startIndex; i < s.length(); i++) {
            if (isPal(s,startIndex,i)){
                path.add(s.substring(startIndex,i+1));
            }else {
                continue;
            }
            backtracking6(s,i+1);
            path.remove(path.size()-1);
        }
    }
*/

    private boolean isPal(String s, int left, int right) {
        while (left < right){
            if (s.charAt(left) != s.charAt(right)){
                return false;
            }
            left++;
            right--;
        }
        return true;
    }

    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        Arrays.sort(candidates);
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        Boolean[] used = new Boolean[candidates.length];
        Arrays.fill(used,false);
        backtracking5(candidates,target,0,0,result,path,used);
        return result;
    }

    private void backtracking5(int[] candidates, int target, int pathSum, int startIndex, List<List<Integer>> result, List<Integer> path, Boolean[] used) {
        if (target < pathSum){
            return;
        }
        if (target == pathSum){
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = startIndex; i < candidates.length && pathSum + candidates[i]
                <= target; i++) {
            if (i > 0 && candidates[i] == candidates[i-1] && !used[i-1]){
                continue;
            }
            used[i] = true;
            path.add(candidates[i]);
            backtracking5(candidates,target,pathSum + candidates[i] , i + 1,result,path,used);
            path.remove(path.size()-1);
            used[i] = false;
        }
    }


    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        Arrays.sort(candidates);
        backtracking4(candidates,target,0,0,result,path);
        return result;
    }

    private void backtracking4(int[] candidates, int targetSum, int startIndex, int pathSum, List<List<Integer>> result, List<Integer> path) {
        if (pathSum >= targetSum){
            return;
        }
        if (pathSum == targetSum){
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = startIndex; i < candidates.length && pathSum + candidates[i] <= targetSum; i++) {
            path.add(candidates[i]);
            backtracking4(candidates,targetSum,i,pathSum+candidates[i],result,path);
            path.remove(path.size()-1);
        }
    }

    public List<String> letterCombinations(String digits) {
        List<String> result = new ArrayList<>();
        backtracking3(digits,0,"",result);

        return result;
    }
    String[] strMap = {"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
    private void backtracking3(String digits, int Index, String Path, List<String> result) {
        if (digits.length() == Index){
            if(Index != 0){
                result.add(Path);
            }
            return;
        }
        int num = Integer.parseInt(digits.charAt(Index) + "");
        String str = strMap[num];
        for (int i = 0; i < str.length(); i++) {
            backtracking3(digits,Index+1,Path+str.charAt(i),result);
        }
    }

    public List<List<Integer>> combinationSum3(int k, int n) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        backtracking2(k,n,1,0,result,path);
        return result;
    }

    private void backtracking2(int k, int targetSum, int curIndex, int sum, List<List<Integer>> result, List<Integer> path) {
        if (targetSum < sum){
            return;
        }
        if (path.size() == k){
            if (sum == targetSum){
                result.add(new ArrayList<>(path));
            }
            return;
        }
        for (int i = curIndex; i <= 9 - (k-path.size()) + 1; i++) {
            path.add(i);
            backtracking2(k,targetSum,i+1,sum+i,result,path);
            path.remove(path.size()-1);
        }
    }

    public List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> allRet = new ArrayList<>();
        List<Integer> curRet = new ArrayList<>();
        backtracking1(n,k,1,allRet,curRet);
        return allRet;
    }

    private void backtracking1(int n, int k, int curIndex, List<List<Integer>> allRet, List<Integer> curRet) {
        if (curRet.size() == k){//终止条件
            allRet.add(new ArrayList<>(curRet));
            return;
        }
        for (int i = curIndex; i <= n; i++) {//单层递归逻辑
            curRet.add(i);
            backtracking1(n,k,i+1,allRet, curRet);
            curRet.remove(curRet.size()-1);
        }
    }


}
