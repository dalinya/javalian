package bktest;

import java.util.Arrays;
import java.util.List;
class Main2{
    public static void main(String[] args) {
        int[] arr = {9,7,5,6,2,4,3,1};
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] > arr[j+1]){
                    int a = arr[j];
                    arr[j] = arr[j+1];
                    arr[j + 1] = a;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }
    public static void main1(String[] args) {
        new Test5().minCut("aab");
    }
}

public class Test5 {
    public int numDistinct(String s, String t) {
        int n = s.length();
        int m = t.length();
        int[][] dp = new int[n + 1][m + 1];
        for (int i = 0; i <= n; i++) {
            dp[i][0] = 1;
        }
        for (int i = 1; i <= n; i++) {

            for (int j = 1; j <= i && j <= m; j++) {
                if (s.charAt(i) == t.charAt(j)){
                    dp[i][j] = dp[i - 1][j] + dp[i - 1][j - 1];
                }else {
                    dp[i][j] = dp[i - 1][j];
                }
            }
        }
        return dp[n][m];

    }
    public int minDistance(String word1, String word2) {
        int m = word1.length();
        int n = word2.length();
        int[][] dp = new int[m + 1][n + 1];
        //对动态规划数组进行初始化
        for (int i = 0; i <= m; i++) {
            dp[i][0] = i;
        }
        for (int j = 0; j <= n; j++) {
            dp[0][j] = j;
        }
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                dp[i][j] = Math.min(dp[i-1][j] + 1,dp[i][j-1] + 1);

                if (word1.charAt(i-1) == word2.charAt(j-1)){
                    dp[i][j] = Math.min(dp[i][j],dp[i-1][j-1]);
                }else {
                    dp[i][j] = Math.min(dp[i][j],dp[i-1][j-1] + 1);
                }
            }

        }
        return dp[m][n];
    }


    public int minCut(String s) {
//        int[] minCutArr = new int[s.length() + 1];
//        int len = s.length();
//        if (isPal(s,0,len - 1)){
//            return 0;
//        }
//        //对动态规划数组进行初始化
//        for (int i = 1; i <= len; i++) {
//            minCutArr[i] = i-1;
//        }
//        //进行状态转移
//        for (int i = 2; i <= len; i++) {
//            if (isPal(s,0,i-1)){
//                minCutArr[i] = 0;
//                continue;
//            }
//            for (int j = 1; j < i; j++) {
//                if (isPal(s,j,i-1)){
//                    minCutArr[i] = Math.min(minCutArr[i],minCutArr[j] + 1);
//                }
//            }
//        }
//
//        return minCutArr[len];

        int len = s.length();
        int[] minCutArr = new int[len + 1];
        boolean[][] Mat = getMat(s);
        for (int i = 0; i <= len; i++) {
            minCutArr[i] = i - 1;
        }
        for (int i = 2; i <= len; i++) {
            if (Mat[0][i]) {
                minCutArr[i] = 0;
                continue;
            }

            for (int j = 1; j < i; j++) {
                if (Mat[j][i - 1]) {
                    minCutArr[i] = Math.min(minCutArr[i], minCutArr[j] + 1);
                }
            }
        }
        return minCutArr[len];
    }

    private boolean[][] getMat(String s) {
        int len = s.length();
        boolean[][] Mat = new boolean[len][len];
        for (int i = len - 1; i >= 0; i--) {
            for (int j = i; j < len; j++) {
                if (i == j){
                    Mat[i][j] = true;
                }else if (i + 1 == j){
                    Mat[i][j] = (s.charAt(i) == s.charAt(j));
                }else {
                    Mat[i][j] = s.charAt(i) == s.charAt(j) && Mat[i + 1][j - 1];
                }
            }
        }
        return Mat;
    }


    private boolean isPal(String s, int left, int right) {
        while (left < right){
            if (s.charAt(left) != s.charAt(right)){
                return false;
            }
            left++;
            right--;
        }
        return true;
    }



    public int backPackII(int m, int[] a, int[] v) {
        // write your code here
        int n = a.length;
        int[][] maxVal = new int[n + 1][m + 1];
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (a[i - 1] <= j){
                    maxVal[i][j] = Math.max(maxVal[i-1][j],maxVal[i-1][j-a[i-1]] + v[i-1]);
                }else {
                    maxVal[i][j] = maxVal[i-1][j];
                }
            }
        }
        return maxVal[n][m];

    }


    public int minPathSum(int[][] grid) {
        int row  = grid.length;
        int col = grid[0].length;
        //对第一行和第一列进行递推
        for (int i = 1; i < row; i++) {
            grid[i][0] = grid[i - 1][0] + grid[i][0];
        }
        for (int j = 1; j < col; j++) {
            grid[0][j] = grid[0][j - 1] + grid[0][j];
        }
        //对其他部分进行递推
        for (int i = 1; i < row; i++) {
            for (int j = 1; j < col; j++) {
                grid[i][j] = Math.min(grid[i-1][j],grid[i][j-1]) + grid[i][j];
            }
        }
        return grid[row - 1][col - 1];
    }

    public int uniquePaths(int m, int n) {
        //创建一个数组存储每个位置有多少条路径
        int[][] dp = new int[m][n];
        //对数组首行首列进行初始化
        for (int i = 0; i < m; i++) {
            dp[i][0] = 1;
        }
        for (int j = 1; j < n; j++) {
            dp[0][j] = 1;
        }
        //进行状态转移
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                dp[i][j] = dp[i][j-1] + dp[i-1][j];
            }
        }
        return dp[m-1][n-1];
    }

    public int minimumTotal(List<List<Integer>> triangle) {
        int row = triangle.size();
        for (int i = row - 2; i >= 0; i--) {
            for (int j = 0; j <= i; j++) {
                int curMinSum = 0;
                curMinSum = Math.min(triangle.get(i+1).get(j),triangle.get(i+1).get(j + 1)) + triangle.get(i).get(j);
                triangle.get(i).set(j,curMinSum);
            }
        }
        return triangle.get(0).get(0);
    }

    public int minimumTotal1(List<List<Integer>> triangle) {
        int row = triangle.size();
        for (int i = 1; i < row; i++) {
            for (int j = 0; j <= i; j++) {
                int curMinSum = 0;//判断该位置的最小路径和
                if (j == 0){//在左边界
                    curMinSum = triangle.get(i - 1).get(0) + triangle.get(i).get(0);
                } else if (j == i){//在右边界
                    curMinSum = triangle.get(i - 1).get(j - 1) + triangle.get(i).get(j);
                }else {
                    curMinSum = Math.min(triangle.get(i - 1).get(j - 1),triangle.get(i - 1).get(j)) + triangle.get(i).get(j);
                }
                triangle.get(i).set(j,curMinSum);
            }
        }
        int minSum = triangle.get(row - 1).get(0);
        for (int i = 1; i < triangle.get(row - 1).size(); i++) {
            minSum = Math.min(minSum,triangle.get(row - 1).get(i));
        }
        return minSum;
    }


    public boolean wordBreak(String s, List<String> wordDict) {
        boolean[] canBreak = new boolean[s.length() + 1];
        canBreak[0] = true;//只是一个辅助状态，
        for (int i = 1; i < s.length(); i++) {
            for (int j = 0; j < i; j++) {
                if (canBreak[j] && wordDict.contains(s.substring(j,i))){
                    canBreak[i] = true;
                }
            }
        }
        return canBreak[s.length()];
    }

}
