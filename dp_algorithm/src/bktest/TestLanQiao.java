package bktest;

import java.time.LocalDate;
import java.util.Scanner;

public class TestLanQiao {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int count = scan.nextInt();
        int[] arr = new int[count];
        int sum = 0;
        int max = 0;
        int min = 100;
        for (int i = 0; i < count; i++) {
            arr[i] = scan.nextInt();
            if (arr[i] > max){
                max = arr[i];
            }
            if (arr[i] < min){
                min = arr[i];
            }
            sum += arr[i];
        }
        System.out.println(max);
        System.out.println(min);
        System.out.printf("%.2f",sum/(count*1.0));
        scan.close();
    }
    public static void main4(String[] args) {
        int count = 0;
        for (int i = 1; i <= 2020; i++) {
            String curStr = "" + i;
            for (char ch: curStr.toCharArray()) {
                if (ch == '2'){
                    count++;
                }
            }
        }
        System.out.println(count);
    }
    public static void main3(String[] args) {
        Scanner scan = new Scanner(System.in);
        int count = scan.nextInt();
        int[] arr = new int[count];
        for (int i = 0; i < count; i++) {
            arr[i] = scan.nextInt();
        }
        int priceA = 0;
        int priceB = 0;
        for (int i = 0; i < count; i++) {
            if (arr[i] >= 85){
                priceA ++;
            }
            if (arr[i] >= 60){
                priceB++;
            }
        }
        System.out.println((int) (priceB/(count * 1.0) * 100 + 0.5) + "%");
        System.out.println((int) (priceA/(count * 1.0) * 100 + 0.5) + "%");

        //在此输入您的代码...
        scan.close();
    }
    public static void main2(String[] args) {
        Scanner scan = new Scanner(System.in);
        //在此输入您的代码...
        String str = scan.next();
        int[] monthDay = {0,31,28,31,30,31,30,31,31,30,31,30,31};
        int year = Integer.parseInt(str.substring(0,4));
        int month = Integer.parseInt(str.substring(4,6));
        int day = Integer.parseInt(str.substring(6,8));
        LocalDate date = LocalDate.now();
        date = date.withYear(year);
        date = date.withMonth(month);
        date = date.withDayOfMonth(day);
        date = date.plusDays(1);
        boolean flg = false;
        String a = null;
        String b = null;
        while (true){
            String dateStr = date.toString().replace("-","");
            if (!flg && huiWen(dateStr)){
                a = dateStr;
                flg = true;
            }
            if (is_ababbaba(dateStr)){
                b = dateStr;
                break;
            }
            date.plusDays(1);
        }
        System.out.println(a);
        System.out.println(b);
        scan.close();
    }

    private static boolean is_ababbaba(String dateStr) {
        char ch1 = dateStr.charAt(0);
        char ch2 = dateStr.charAt(1);
        if (dateStr.charAt(2) == ch1 && dateStr.charAt(3) == ch2
                && dateStr.charAt(4) == ch2 && dateStr.charAt(5) == ch1
                &&dateStr.charAt(6) == ch2 && dateStr.charAt(7) == ch1){
            return true;
        }else {
            return false;
        }
    }

    private static boolean huiWen(String curStr) {
        int left = 0;
        int right = curStr.length() - 1;
        while (left < right){
            if (curStr.charAt(left) != curStr.charAt(right)){
                return false;
            }
            left++;
            right--;
        }
        return true;
    }

    private static boolean isLeapYear(int year) {
        return year % 400 == 0 || (year % 4 == 0 && year %100 != 0);
    }

    public static void main1(String[] args) {
//        System.out.println(Integer.parseInt("2022",9));
        int[] map = new int[26];
        Scanner sc = new Scanner(System.in);
        String str = sc.next();

        for (int i = 0; i < str.length(); i++) {
            int index = str.charAt(i) - 'a';
            map[index]++;
        }
        int max = 0;
        int index = 0;
        for (int i = 0; i < 26; i++) {
            if (map[i] > max){
                index = i;
                max = map[i];
            }
        }
        char ch = (char) ('a' + index);

        System.out.println(ch);
        System.out.println(max);
    }
}
