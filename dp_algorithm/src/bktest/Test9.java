package bktest;

import java.util.Arrays;

public class Test9 {
    public int findMaxForm(String[] strs, int m, int n) {
        int[][] dp = new int[m + 1][n + 1];//i个0 和 j个1的最大子集
        dp[0][0] = 0;
        for (String str :strs) {
            int x = 0,y = 0;
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) == '0'){
                    x++;
                }else {
                    y++;
                }
            }
            for (int i = m; i >= x; i--) {
                for (int j = n; j >= y; j--) {
                    dp[i][j] = Math.max(dp[i-x][j-y] + 1,dp[i][j]);
                }
            }
        }
        return dp[m][n];
    }
    public int findTargetSumWays(int[] nums, int target) {
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
        }
        if ((sum + target)%2 != 0){
            return 0;
        }
        if (Math.abs(target) > sum){
            return 0;
        }
        int num = (sum + target)/2;
        int[] dp = new int[num + 1];//存放和为num 的元素有多少种
        for (int i = 0; i < nums.length; i++) {//遍历背包
            for (int j = num; j >= nums[i]; j--) {//遍历种类个数
                dp[j] += dp[j - nums[i]];
            }

        }
        return dp[num];
    }



    public int lastStoneWeightII(int[] stones) {
        int sum = 0;
        for (int i = 0; i < stones.length; i++) {
            sum += stones[i];
        }
        int target = sum/2;
        int[] dp = new int[target + 1];
        for (int i = 0; i < stones.length; i++) {
            for (int j = target; j >= stones[i]; j--) {
                dp[j] = Math.max(dp[j],dp[j - stones[i]] + stones[i]);
            }
        }
        return sum - 2 * dp[target];
    }

    public boolean canPartition(int[] nums) {
        int target = 0;
        for (int i = 0; i < nums.length; i++) {
            target += nums[i];
        }
        if (target % 2 == 0){
            return false;
        }
        target /= 2;
        int[] dp = new int[target + 1];
        for (int i = 0; i < nums.length; i++) {
            for (int j = target; j >= nums[i]; j--) {
                dp[i] = Math.max(dp[i],dp[i - nums[i]] + nums[i]);
            }
        }
        if (dp[target - 1] == target){
            return true;
        }
        return false;
    }
    public int numTrees(int n) {
        int[] dp = new int[n + 1];//dp数组的含义：i个元素的搜索二叉树有多少个
        dp[0] = 1;//初始化：辅助状态，null结点是一个搜索二叉树
        //状态转移
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= i; j++) {
                dp[i] += dp[j-1] + dp[i - j];
            }
        }
        return dp[n];
    }
    public int integerBreak(int n) {
        int[] dp = new int[n + 1];
        dp[2] = 1;
        for (int i = 3; i <= n; i++) {
            for (int j = 1; j < i; j++) {
                dp[i] = Math.max(Math.max(j*dp[i-j],j*(i-j)),dp[i]);
            }
        }
        return dp[n];
    }
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int m = obstacleGrid.length;
        int n = obstacleGrid[0].length;
        int[][] dp = new int[m][n];//含义，到i,j位置的路径
        //初始化：第一行和第一列路径都为1
        for (int i = 0; i < m && obstacleGrid[i][0] == 0; i++) {
            dp[i][0] = 1;
        }
        for (int j = 1; j < n && obstacleGrid[0][j] == 0; j++) {
            dp[0][j] = 1;
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                if (obstacleGrid[i][j] == 0){
                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
                }else {
                    dp[i][j] = 0;
                }
            }
        }
        System.out.println(Arrays.deepToString(dp));
        return dp[m-1][n-1];
    }
    public int uniquePaths(int m, int n) {
        int[][] dp = new int[m][n];//含义，到i,j位置的路径
        //初始化：第一行和第一列路径都为1
        for (int i = 0; i < m; i++) {
            dp[i][0] = 1;
        }
        for (int j = 1; j < n; j++) {
            dp[0][j] = 1;
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }
        return dp[m-1][n-1];
    }

    public int minCostClimbingStairs(int[] cost) {
        int[] dp = new int[cost.length + 1];//代表的是到达i层最少需要花费的钱
        //可以从第0层和第1层开始向上趴
        dp[0] = 0;
        dp[1] = 0;
        for (int i = 2; i <= cost.length; i++) {
            dp[i]  = Math.min(dp[i-2] + cost[i-2],dp[i-1] + cost[i-1]);
        }
        return dp[cost.length];
    }
    public int climbStairs(int n) {
        if (n == 1){
            return 1;
        }
        int[] dp = new int[n + 1];
        dp[1] = 1;
        dp[0] = 1;
        for (int i = 0; i <= n; i++) {
            dp[i] = dp[i-1] + dp[i-2];
        }
        return dp[n];
    }
    public int fib(int n) {
        if (n == 0 || n == 1){
            return n;
        }
        int[] dp = new int[2];
        dp[1] = 1;
        int sum = 0;
        for (int i = 2; i <= n; i++) {
            sum = dp[0] + dp[1];
            dp[1] = sum;
            dp[0] = dp[1];
        }
        return dp[2];
    }
}
