package bktest;

import java.util.*;
//5：00  301和302的区别
class Employee {
    public int id;
    public int importance;
    public List<Integer> subordinates;
};

class Main{
    public static void main(String[] args) {
        new Test2().solveNQueens(4);
    }
}

class Pair{
    int x;
    int y;

    public Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
public class Test2 {
    int[][] nextP = {{1,0},{-1,0},{0,1},{0,-1}};//方向数组，下一步从哪个方向搜索 下 上 右 左

    public List<List<String>> solveNQueens(int n) {
        List<List<Pair>> allRet = new ArrayList<>();
        List<Pair> curRet = new ArrayList<>();
        int curRow = 0;
        DFS(allRet,curRet,n,curRow);
        return invert(allRet,n);
    }



    private void DFS(List<List<Pair>> allRet, List<Pair> curRet, int n, int curRow) {
        if (curRet.size() == n){
            allRet.add(new ArrayList<>(curRet));//把List类型转化为ArrayList类型，避免curRet中的数据丢失
        }
        for (int i = 0; i < n; i++) {
            if (isLawful(curRet,curRow,i)){
                curRet.add(new Pair(curRow,i));
                DFS(allRet,curRet,n,curRow + 1);
                curRet.remove(curRet.size() - 1);
            }
        }
    }

    private boolean isLawful(List<Pair> curRet, int curRow, int curCol) {
        for (Pair pair : curRet) {
            if (pair.y == curCol
                || pair.x - pair.y == curRow - curCol
                || pair.x + pair.y == curRow + curCol){
                return false;
            }
        }
        return true;
    }
    private List<List<String>> invert(List<List<Pair>> allRet, int n) {
        List<List<String>> allInvert = new ArrayList<>();
        for (List<Pair> curList: allRet) {
            List<String> curInvert = new ArrayList<>();
            for (Pair pair: curList) {
                System.out.print(pair.x + " " + pair.y + "    ");
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < n; i++) {
                    sb.append(".");
                }
                sb.setCharAt(pair.y,'Q');
                curInvert.add(sb.toString());
            }
            allInvert.add(curInvert);
        }
        System.out.println();
        return allInvert;
    }



    public int numTilePossibilities(String tiles) {
        Set<String> set = new HashSet<>();
        String curStr = "";
        int[] book = new int[tiles.length()];//标记字符串中的字符是否已经搜索过了。
        DFS6(tiles,set,curStr,book);
        return set.size();
    }

    private void DFS6(String tiles, Set<String> set, String curStr, int[] book) {
        if (curStr.length() != 0){
            set.add(curStr);
        }
        if (curStr.length() == tiles.length()){
            return;
        }
        for (int i = 0; i < tiles.length(); i++) {
            if (book[i] == 0){
                book[i] = 1;
                //进行深度优先搜索
                DFS6(tiles,set,curStr + tiles.charAt(i),book);
                //进行回溯
                book[i] = 0;
            }
        }
    }

    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> allRet = new ArrayList<>();
        List<Integer> curRet = new ArrayList<>();
        int curSum = 0;
        int index = 0;
        DFS5(candidates,allRet,curRet,target,curSum,index);
        return allRet;
    }

    private void DFS5(int[] candidates, List<List<Integer>> allRet, List<Integer> curRet, int target, int curSum, int index) {
        if (curSum >= target){
            if (curSum == target){
                allRet.add(new ArrayList<>(curRet));
            }
            return;
        }
        for (int i = index; i < candidates.length; i++) {
            curRet.add(candidates[i]);
            DFS5(candidates,allRet,curRet,target,curSum + candidates[i], i);
            curRet.remove(curRet.size() - 1);
        }

    }

    String[] strMap = {"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
    public List<String> letterCombinations(String digits) {
        List<String> list = new ArrayList<>();
        int length = digits.length();
        int curLength = 0;
        String curStr = "";
        DFS(list,digits,curStr,curLength,length);
        return list;
    }

    private void DFS(List<String> list, String digits, String curStr , int curLength, int length) {
        if (curLength == length){
            if (length != 0){
                list.add(curStr);
            }
            return;
        }
        String str =  strMap[Integer.parseInt(digits.charAt(curLength) + "")];
        for (int i = 0; i < str.length(); i++) {
            DFS(list,digits,curStr + str.charAt(i),curLength + 1,length);
        }
    }


    public int numIslands(char[][] grid) {
        int row = grid.length;
        int col = grid[0].length;
        int count = 0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == '1'){
                    DFS4(grid,i,j,row,col);
                    count++;
                }
            }
        }
        return count;
    }

    private void DFS4(char[][] grid,int posX, int poxY ,int row, int col) {
        grid[posX][poxY] = '*';//对已经搜索过的陆地进行标记，以防重复搜索造成死递归。
        for (int i = 0; i < 4; i++) {
            int newX = posX + nextP[i][0];
            int newY = poxY + nextP[i][1];
            if (newX < 0 || newX >= row
                    || newY < 0 || newY >= col) {//判断是否是越界了
                continue;
            }
            if (grid[newX][newY] == '1'){//是未被搜索的陆地，就进行下一步搜索
                DFS4(grid, newX, newY, row, col);
            }
        }
    }

    public void solve(char[][] board) {
        int row = board.length;
        int col = board[0].length;
        //用逆向思维的方式求未被包围的区域，间接求出被包围的区域
        for (int i = 0; i < row; i++) {//寻找左列边界的区域中‘O’的位置
            if (board[i][0] == 'O'){
                DFS3(board,i,0,row,col);
            }
            if (board[i][col - 1] == 'O'){//寻找右列边界的区域中‘O’的位置
                DFS3(board,i,col - 1,row,col);
            }
        }
        for (int j = 0; j < col; j++) {
            if (board[0][j] == 'O'){//判断上边界是否有‘O’
                DFS3(board,0,j,row,col);
            }
            if (board[row - 1][j] == 'O'){//判断下边界是否有‘O’
                DFS3(board,row - 1,j,row,col);
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (board[i][j] == 'O'){
                    board[i][j] = 'X';
                }
                if (board[i][j] == '*'){
                    board[i][j] = 'O';
                }
            }
        }
    }

    private void DFS3(char[][] board, int x, int y, int row, int col) {
        board[x][y] = '*';//代表该‘O’已经被搜索过了
        for (int i = 0; i < 4; i++) {
            int newX = x + nextP[i][0];
            int newY = y + nextP[i][1];
            if (newX < 0 || newX >= row
                ||newY < 0 || newY >= col){//判断探索位置是否越界
                continue;
            }
            if (board[newX][newY] == 'O'){//判断该位置是否需要进行探索
                DFS3(board,newX,newY,row,col);
            }
        }
    }

    public int[][] floodFill(int[][] image, int sr, int sc, int color) {
        int row = image.length;
        int col = image[0].length;
        int oldColor = image[sr][sc];
        int[][] book = new int[row][col];//设计标记数组，是为了避免新颜色和旧颜色相同会出现死递归的情况
        DFS2(image,book,sr,sc,color,row,col,oldColor);
        return image;
    }

    private void DFS2(int[][] image, int[][] book, int sr, int sc, int color, int row, int col, int oldColor) {
        image[sr][sc] = color;
        book[sr][sc] = 1;
        for (int i = 0; i < 4; i++) {
            int newX = sr + nextP[i][0];
            int newY = sc + nextP[i][1];
            if (newX < 0 || newX >= row
                || newY < 0 || newY >= col){//判断新的下表是否越界
                continue;//越界该的方向不进行处理，进行下一个的方向。
            }
            if (image[newX][newY] == oldColor
                && book[newX][newY] == 0){//对新的覅和满足条件的位置进行深度优先搜索
                DFS2(image, book, newX,newY,color,row,col, oldColor);
            }
        }
    }

    public int getImportance(List<Employee> employees, int id) {
        Map<Integer,Employee> empMap = new HashMap<>();
        for (Employee employee: employees) {
            empMap.put(employee.id,employee);
        }
        return DFS1(empMap,id);
    }

    private int DFS1(Map<Integer, Employee> empMap, int id) {
        int sum = empMap.get(id).importance;
        for (int curId:empMap.get(id).subordinates) {
            sum += DFS1(empMap,curId);
        }
        return sum;
    }


    public static void main1(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();//盒子和牌的个数
        int[] book = new int[n + 1];
        int[] box = new int[n + 1];
        Dfs(1,book,box,n);
    }

    private static void Dfs(int i, int[] book, int[] box, int n) {
        if (i == n + 1){
            for (int j = 1; j <= n; j++) {
                System.out.print(box[j] + " ");
            }
            System.out.println();
            return;
        }
        for (int j = 1; j <= n; j++) {
            if (book[j] == 0){
                book[j] = 1;
                box[i] = j;
                Dfs(i + 1,book,box,n);
                book[j] = 0;
            }
        }

    }
}
