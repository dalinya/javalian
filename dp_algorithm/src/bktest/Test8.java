package bktest;

import java.util.*;

//20：20
public class Test8 {
    public int monotoneIncreasingDigits(int n) {
        StringBuilder result = new StringBuilder(n + "");
        int flg = result.length();
        for (int i = result.length() - 1; i > 0; i--) {
            if (result.charAt(i) < result.charAt(i-1)){
                flg = i;
                char tmp = result.charAt(i-1);
                tmp--;
                result.setCharAt(i-1,tmp);
            }
        }
        for (int i = flg; i < result.length(); i++) {
            result.setCharAt(i,'9');
        }
        return Integer.parseInt(result.toString());
    }
    public int monotoneIncreasingDigits1(int n) {
        String result = n + "";
        int flg = result.length();
        int len = result.length();
        for (int i = len - 1; i > 0; i--) {
            if (result.charAt(i) < result.charAt(i-1)){
                flg = i;
                char ch = result.charAt(i-1);
                ch--;
                result = result.substring(0,i-1) + ch + result.substring(i,len);
            }
        }
        for (int i = flg; i < len; i++) {
            result = result.substring(0,i) + '9';
        }
        return Integer.parseInt(result);
    }
    public int[][] merge(int[][] intervals) {
        ArrayList<int[]> list = new ArrayList<>();
        int count = 0;
        Arrays.sort(intervals,(a,b)->a[0]-b[0]);
        int left = intervals[0][0];
        int right = intervals[0][1];
        for (int i = 1; i < intervals.length; i++) {
            if (intervals[i][0] <= right) {//重叠的时候
                right = Math.max(right,intervals[i][1]);
            }else {//不重叠
                list.add(new int[]{left, right});
                //更新左右区间的位置
                right = Math.max(right,intervals[i][1]);
                left = intervals[i][0];
            }
        }
        list.add(new int[]{left, right});
        return list.toArray(new int[count][]);
    }
    public List<Integer> partitionLabels(String s) {
        List<Integer> result = new ArrayList<>();
        int right = 0;
        int left = 0;
        int[] hash = new int[27];
        for (int i = 0; i < s.length(); i++) {
            hash[s.charAt(i) - 'a'] = i;
        }
        for (int i = 0; i < s.length(); i++) {
            right =  Math.max(right,hash[s.charAt(i) - 'a']);
            if (right == i){
                result.add(right - left + 1);
                left = i + 1;
            }
        }
        return result;
    }
    public int eraseOverlapIntervals(int[][] intervals) {
        Arrays.sort(intervals,(a,b)->{return a[1]- b[1];});//按着结束时间排序
        int right = intervals[0][1];
        int result = 0;
        for (int i = 1; i < intervals.length; i++) {
            if (right > intervals[i][0]){//有重叠区间
                result++;
                right = Math.min(right,intervals[i][1]);
            }else {
                right = intervals[i][1];
            }
        }
        return result;
    }
    public int findMinArrowShots(int[][] points) {
        Arrays.sort(points,(a,b)->{return a[0]- b[0];});
        int right = points[0][1];
        int result = 1;
        for (int i = 1; i < points.length; i++) {
            if (points[i][0] > right){
                right = points[i][1];
                result++;
            }else {
                right = Math.min(right,points[i][1]);
            }
        }
        return result;
    }
    public int[][] reconstructQueue(int[][] people) {
        Arrays.sort(people,(a, b) -> {
            if (a[0] == b[0]) return a[1] - b[1];
            return b[0] - a[0];});
        LinkedList<int[]> list = new LinkedList<>();
        for (int[] array:people) {
            list.set(array[1],array);
        }
        return list.toArray(new int[people.length][]);
    }
    public boolean lemonadeChange(int[] bills) {
        int five = 0;
        int ten = 0;
        for (int money: bills) {
            if (money == 5){
                five++;
            }
            if (money == 10){
                if (five > 0){
                    five--;
                    ten++;
                }else {
                    return false;
                }
            }
            if (money == 20){
                if (five > 0 && ten > 0){
                    five--;
                    ten--;
                }else if (five >= 3){
                    five -= 3;
                }else {
                    return false;
                }
            }
        }
        return true;
    }


    public int candy(int[] ratings) {
        int[] candyVal = new int[ratings.length];
        Arrays.fill(candyVal,1);
        for (int i = 1; i < ratings.length; i++) {
            if (ratings[i] > ratings[i-1]){
                candyVal[i] = candyVal[i-1]+1;
            }
        }
        for (int i = ratings.length - 2; i >= 0; i--) {
            if (ratings[i] > ratings[i + 1]){
                candyVal[i] = Math.max(candyVal[i],candyVal[i+1]+1);
            }
        }
        int result = 0;
        for (int i = 0; i < candyVal.length; i++) {
            result += candyVal[i];
        }
        return result;
    }

    public int canCompleteCircuit(int[] gas, int[] cost) {
        int curSum = 0;
        int totalSum = 0;
        int start = 0;
        for (int i = 1; i < gas.length; i++) {
            curSum += gas[i] - cost[i];
            totalSum += gas[i] - cost[i];
            if (curSum < 0){
                start = i + 1;
                curSum = 0;
            }

        }
        if (totalSum < 0){
            return -1;
        }
        return start;
    }

    public int largestSumAfterKNegations(int[] nums, int k) {
        Arrays.sort(nums);
        int i = 0;
        for (i = 0; i < nums.length; i++) {
            if (nums[i] < 0 && k > 0){
                nums[i] *= -1;
                k--;
            }
            if (k < 0 || nums[i] >= 0){
                break;
            }
        }
        for (int j = 0; j < nums.length; j++) {
            System.out.println(nums[i]);
        }
        if (k % 2 == 1){
            if (i > 0){
                if (i == nums.length){
                    i--;
                }
                if (nums[i] > nums[i-1]){
                    i--;
                }
            }
            nums[i] *= -1;
        }
        int result = 0;
        for (int j = 0; j < nums.length; j++) {
            System.out.println(nums[i]);
            result += nums[i];
        }
        return result;
    }


    public int jump(int[] nums) {
        if (nums.length == 1){
            return 0;
        }
        int result = 0;
        int next = 0;
        int cur = 0;
        for (int i = 0; i < nums.length; i++) {
            next = Math.max(next,i+nums[i]);
            if (i == cur){//下标移动到了最远的覆盖范围
                cur = next;//最远覆盖范围更新
                result++;
                if (cur >= nums.length - 1){//判断更新的范围是否能够覆盖最后
                    break;
                }
            }
        }
        return result;
    }
    public boolean canJump(int[] nums) {
        int cover = 0;
        for (int i = 0; i <= cover; i++) {
            cover = Math.max(cover,i+nums[i]);
            if (cover >= nums.length - 1){
                return true;
            }
        }
        return false;
    }
    public int maxProfit(int[] prices) {
        int result = 0;
        for (int i = 1; i < prices.length; i++) {
            result += Math.max(0,prices[i] - prices[i-1]);
        }
        return result;
    }
    public int maxSubArray(int[] nums) {
        int result = Integer.MIN_VALUE;
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (count < 0){
                count = nums[i];
            }else {
                count += nums[i];
            }
            if (count > result){
                result = count;
            }
        }
        return result;
    }

    public int lastRemaining(int n, int m) {
        Queue<Integer> qu = new LinkedList<>();
        for (int i = 1; i <= n; i++) {
            qu.offer(i);
        }
        int i = 0;
        while (qu.size() > 1){
            i++;
            int tmp = qu.poll();
            if (i != n){
                qu.offer(tmp);
            }else {
                i = 0;
            }
        }
        return qu.peek();
        
    }

    public int wiggleMaxLength(int[] nums) {
        int result = 1;
        int preDiff = 0;
        int curDiff = 0;
        if (nums.length > 1){
            if (nums[0] != nums[1]){
                result++;
                preDiff = nums[1] - nums[0];
            }
        }
        for (int i = 1; i < nums.length - 1; i++) {
            curDiff = nums[i+1] - nums[i];
            if (preDiff < 0 && curDiff > 0
                ||preDiff > 0 && curDiff <0){
                preDiff = curDiff;
                result++;
            }
        }
        return result;
    }


    public int findContentChildren(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        int index = 0;//最小的饼干下标
        int result = 0;//满足小孩的数量
        for (int i = 0; i < s.length; i++) {
            if (index < g.length && s[i] >= g[index]){
                result++;
                index++;
            }

        }
        return result;
    }
}
