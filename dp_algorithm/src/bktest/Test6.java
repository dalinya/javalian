package bktest;

import java.util.List;

public class Test6 {

    public int numDistinct(String s, String t) {
        //状态方程：dp[i][j]：s1前i个字符到s2前j个字符的转化过程
        //状态转化：s[i] != t[j]:dp[i][j]=dp[i-1][j]
        //        s[i] == t[j]: 选第i个字符：dp[i-1][j-1] 不选第i个字符：dp[i-1][j]  ---》dp[i][i] = dp[i-1][j-1] + dp[i-1][j]
        //初始状态：dp[i][0]=1  j!=0 dp[0][j]=0
        //返回结果：dp[row][col]
        int n = s.length();
        int m = t.length();
        int[][] dp = new int[n + 1][m + 1];
        for (int i = 0; i <= n; i++) {
            dp[i][0] = 1;
        }
        for (int j = 1; j <= m; j++) {
            dp[0][j] = 0;
        }
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (s.charAt(i-1) == t.charAt(j-1)){
                    dp[i][j] = dp[i-1][j] + dp[i-1][j-1];
                }else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return dp[n][m];
    }
    public int minDistance(String word1, String word2) {
        //状态方程：word1前i个字符转化word2前j个字符需要的最少步骤
        //状态转化：dp[i][j] = min(r:dp[i][j]+1    d:dp[i-1][j]+1   i:dp[i-1][j-1]+1)
        //初始化：dp[i][0]=i,dp[0][j]=j
        //返回结果：dp[row][col];
        int row  = word1.length();
        int col  = word2.length();
        int[][] dp = new int[row+1][col+1];
        for (int i = 0; i <= row; i++) {//第一列进行初始化
            dp[i][0] = i;
        }
        for (int j = 0; j <= col; j++) {//第一行进行初始化
            dp[0][j] = j;
        }
        for (int i = 1; i <= row; i++) {
            for (int j = 1; j <= col; j++) {
                dp[i][j] = Math.min(dp[i-1][j],dp[i][j-1])+1;
                if (word1.charAt(i-1)==word2.charAt(j-1)){
                    dp[i][j] = Math.min(dp[i][j],dp[i-1][j-1]);
                }else {
                    dp[i][j] = Math.min(dp[i][j],dp[i-1][j-1] + 1);
                }
            }
        }
        return dp[row][col];
    }
    public int minCut(String s) {
        //状态方程：前i个字符最少需要分割的次数
        //状态转化：if(j,i是回文)--->f[i]=min(f[j]+1)
        //初始状态：f[i] = i-1
        int len = s.length();
        boolean[][] ifPalMat = getMat(s,len);
        int[] dp = new int[len + 1];
        //初始化
        for (int i = 0; i <= len; i++) {
            dp[i] = i-1;
        }

        //状态转化
        for (int i = 2; i <= len; i++) {
            for (int j = 0; j < i; j++) {
                if (ifPalMat[j][i-1]){//(i,j)是否回文
                    dp[i] = Math.min(dp[j] + 1,dp[i]);
                }
            }
        }
        return dp[len];

    }

    private boolean[][] getMat(String s, int len) {

        boolean[][] palMat = new boolean[len][len];
        for (int i = len-1; i >= 0; i--) {
            for (int j = i; j < len; j++) {
                if (i==j){
                    palMat[i][j] = true;
                }else if (i+1==j){
                    palMat[i][j] = s.charAt(i) == s.charAt(j);
                }else {
                    palMat[i][j] = palMat[i+1][j-1] && (s.charAt(i) == s.charAt(j));
                }

            }
        }
        return palMat;
    }

    public int minCut1(String s) {
        //状态方程：前i个字符最少需要分割的次数
        //状态转化：if(j,i是回文)--->f[i]=min(f[j]+1)
        //初始状态：f[i] = i-1
        int len = s.length();
        int[] dp = new int[len + 1];
        //初始化
        for (int i = 1; i <= len; i++) {
            dp[i] = i-1;
        }

        //状态转化
        for (int i = 2; i <= len; i++) {
            if (isPal(s,0,i-1)){//判断整体是否回文
                dp[i] = 0;
                continue;
            }
            for (int j = 1; j < i; j++) {
                if (isPal(s,j,i-1)){//(i,j)是否回文
                    dp[i] = Math.min(dp[j] + 1,dp[i]);
                }
            }
        }
        return dp[len];
    }

    private boolean isPal(String s, int left, int right) {
        while (left < right){
            if (s.charAt(left) != s.charAt(right)){
                return false;
            }
            left++;
            right--;
        }
        return true;
    }

    public int backPackII(int m, int[] a, int[] v) {
        // write your code here
        int n = a.length;
        int[][] dp = new int[n+ 1][m + 1];
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (j >= a[i-1]){
                    dp[i][j] = Math.max(dp[i-1][j],dp[i-1][j-a[i-1]]+v[i-1]);//放和不放进行价值比较
                }else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return dp[n][m];
    }


    public int minPathSum(int[][] grid) {
        //状态方程：在dp[i][j]位置定的路径和
        //状态转化：dp[i][j] = grid[i][j]  + Math.min(dp[i-1][j] , dp[i][j-1])
        //         第一行 : dp[0][j] = grid[0][j] + dp[0][j-1]
        //         第一列 : dp[i][0] = grid[i][0] + dp[i-1][0]
        //初始状态：dp[0][0] = grid[0][0]
        //返回结果：dp[row-1][col-1]

        int row = grid.length;
        int col = grid[0].length;
        int[][] dp = new int[row][col];
        //初始化
        dp[0][0] = grid[0][0];
        for (int i = 1; i < row; i++) {
            dp[i][0] = grid[i][0] + dp[i - 1][0];
        }
        for (int j = 1; j < col; j++) {
            dp[0][j] = grid[0][j] + dp[0][j - 1];
        }


        for (int i = 1; i < row; i++) {
            for (int j = 1; j < col; j++) {
                dp[i][j] = grid[i][j]  + Math.min(dp[i-1][j] , dp[i][j-1]);
            }
        }
        return dp[row - 1][col - 1];
    }

    public int uniquePaths(int m, int n) {
        //子问题：在中间位置的方法为多少
        //状态方程：dp[i][j]从(0,0)到(i,j)共有多少种方法
        //状态转化：dp[i][j]=dp[i-1][j] + dp[i][j-1]
        //初始状态：dp[0][j] = dp[i][0] = 1
        //返回结果：dp[m-1][n-1]
        int[][] dp = new int[m][n];
        for (int i = 0; i < m; i++) {//初始化行
            dp[i][0] = 1;
        }
        for (int j = 1; j < n; j++) {//初始化列
            dp[0][j] = 1;
        }
        //进行状态转化
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < m; j++) {
                dp[i][j] = dp[i-1][j] + dp[i][j-1];
            }
        }
        return dp[m-1][n-1];
    }

    public int minimumTotal(List<List<Integer>> triangle) {
        //状态方程：从最后一行到(0,0)的最小路径和
        //状态转化：dp[i][j] = min(dp[i+1][j],dp[i+1][j+1])
        //初始状态：dp[0][0] = triangle[0][0]
        //返回结果：dp[0][0]
        int row = triangle.size();
        for (int i = row - 2; i >= 0; i--) {
            for (int j = 0; j <= i; j++) {
                int minValue = Math.min(triangle.get(i+1).get(j),triangle.get(i+1).get(j+1));
                triangle.get(i).set(j,minValue + triangle.get(i).get(j));
            }
        }
        return triangle.get(0).get(0);
    }
    public int minimumTotal1(List<List<Integer>> triangle) {//自顶向下
        //状态方程：从(0,0)到第i行第j列的最小路径和
        //状态转化：一般情况：dp[i][j]=min(dp[i-1][j-1],dp[i-1][j]) 当 i==0 时 dp[i][j]=dp[i-1][j] || i==j 时 dp[i][j]=dp[i-1][j-1]
        //初始状态：dp[0][0] = triangle[0][0]
        //返回结果：min(triangle[row-1][j])
        int row = triangle.size();
        for (int i = 1; i < row; i++) {
            for (int j = 0; j < i; j++) {
                int minPath = 0;
                if (j == 0){
                    minPath = triangle.get(i-1).get(0);
                }else if (j == i){
                    minPath = triangle.get(i-1).get(j-1);
                }else {
                    minPath = Math.min(triangle.get(i-1).get(j-1),triangle.get(i-1).get(j));
                }
                triangle.get(i).set(j,triangle.get(i).get(j) + minPath);
            }
        }
        List<Integer> curList = triangle.get(row);
        int minValue = curList.get(0);
        for (int curNum:curList) {
            if (curNum < minValue){
                minValue = curNum;
            }
        }
        return minValue;
    }
    public boolean wordBreak(String s, List<String> wordDict) {
        //子问题：前i个字符是否可以用字典中的字符串拼接出来
        //状态方程：dp(i)：前i个字符是否可以是否可以被分割
        //状态转化：dp[i] = dp[j] + [j,i] ? 1 : 下一次循环； ( 0 <= j < i)
        //装填初始：dp[0] = true;//辅助状态，为了判断整体是否是字典中的字符
        //返回结果：dp[s.length];
        boolean[] canBreak = new boolean[s.length() + 1];
        canBreak[0] = true;
        for (int i = 1; i <= s.length(); i++) {
            for (int j = 0; j < i; j++) {
                if (canBreak[j] && wordDict.contains(s.substring(j,i))){//默认是左闭右开
                    canBreak[i] = true;
                    break;
                }
            }
        }
        return canBreak[s.length()];
    }

    public int fib(int n) {
        //动态规划
        //状态方程：第i个位置的斐波那契数列
        //状态转化：f(n) = f(n-1) + f(n-2)
        if (n == 0){
            return 0;
        }
        int[] dp = new int[n + 1];
        dp[0] = 0;
        dp[1] = 1;
        for (int i = 2; i <= n; i++) {
            dp[i] = (dp[i-1] + dp[i-2])%1000000007;
        }
        return dp[n];

    }
    public int fib1(int n) {
        //一般方法
        if (n == 0){
            return 0;
        }
        int a = 0;
        int b = 1;
        int c = b;
        for (int i = 2; i <= n; i++) {
            c = (a + b)%1000000007;
            a = b;
            b = c;
        }
        return c;
    }
}
