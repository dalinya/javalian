package bktest;

public class Test11 {
    public int findTargetSumWays(int[] nums, int target) {
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
        }
        //left + right = sum    left - right = target
        //left = (sum + target)/2 --》为正数的和为left
        if (Math.abs(target) > sum){
            return 0;
        }
        if ((sum + target) % 2 != 0){//为奇数是没有解的
            return 0;
        }
        int bagWeight = (sum + target)/2;
        int[] dp = new int[bagWeight + 1];//总数和为i的组合种类
        dp[0] = 1;
        for (int i = 0; i < nums.length; i++) {//遍历物品
            for (int j = bagWeight; j >= nums[i]; j--) {//遍历背包的容量
                dp[j] += dp[j-nums[i]];//物品的重量nums[i]，物品的价值1
            }
        }
        return dp[bagWeight];
    }

}
