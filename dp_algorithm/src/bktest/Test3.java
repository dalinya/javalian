package bktest;


import java.util.*;

class pair{
    int x;
    int y;

    public pair(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
class Node {
    public int val;
    public List<Node> children;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, List<Node> _children) {
        val = _val;
        children = _children;
    }
};

public class Test3 {


    public List<List<Integer>> levelOrder(Node root) {
        List<List<Integer>> treeMat = new LinkedList<>();
        if (root == null){//如果根节点为null，就直接返回一个什么也没有的List
            return treeMat;
        }
        Queue<Node> qu = new LinkedList<>();//存放当前层次的遍历结果
        qu.offer(root);
        while (!qu.isEmpty()){
            List<Integer> rowV = new ArrayList<>();//存放当前层的遍历结果，最后存储到treeMat
            int size = qu.size();
            while (size > 0){//对当前层的结点进行遍历，搜索下一层的子节点
                rowV.add(qu.peek().val);
                List<Node> curNode = qu.poll().children;//孩子节点是个链表
                for (Node ch:curNode) {
                    qu.add(ch);
                }
                size--;
            }
            treeMat.add(rowV);
        }
        return treeMat;
    }
    public int openLock(String[] deadends, String target) {
        int count = 0;
        Queue<String> qu = new LinkedList<>();
        qu.offer("0000");
        Set<String> deadSet = new HashSet<>();
        for (int i = 0; i < deadends.length; i++) {
            deadSet.add(deadends[i]);
        }
        if (deadSet.contains("0000")){
            return -1;
        }
        Set<String> book = new HashSet<>();//存放已经搜索过的字符串
        while (!qu.isEmpty()){
            int size = qu.size();
            while (size-- > 0){
                String str = qu.poll();
                if (str.equals(target)){
                    return count;
                }

                for (int i = 0; i < 4; i++) {
                    StringBuilder sb = new StringBuilder(str);
                    char ch1 = str.charAt(i);
                    char ch2 = str.charAt(i);
                    if (ch1 == '9'){
                        ch1 = '0';
                    }else {
                        ch1++;
                    }
                    if (ch2 == '0'){
                        ch2 = '9';
                    }else {
                        ch2--;
                    }
                    sb.setCharAt(i,ch1);
                    if (!deadSet.contains(sb.toString()) && !book.contains(sb.toString())){
                        book.add(sb.toString());
                        qu.offer(sb.toString());
                    }

                    sb.setCharAt(i,ch2);
                    if (!deadSet.contains(sb.toString()) && !book.contains(sb.toString())){
                        book.add(sb.toString());
                        qu.offer(sb.toString());
                    }
                }
            }
            count++;
        }
        return -1;
    }
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Queue<String> qu = new LinkedList<>();//存放已经搜索的字符串
        qu.offer(beginWord);
        Set<String> book = new HashSet<>();//存放字典中已经搜索过的字符串
        book.add(beginWord);
        Set<String> dict = new HashSet<>(wordList);
        int count = 0;//记录搜素的次数
        while (!qu.isEmpty()){
            int size = qu.size();
            while (size-- > 0){
                String str = qu.poll();
                if (str.equals(endWord)){
                    return count;
                }
                for (int i = 0; i < str.length(); i++) {
                    StringBuilder sb = new StringBuilder(str);
                    for (char j = 'a'; j <= 'z'; j++) {
                        sb.setCharAt(i,j);
                        if (!book.contains(sb.toString()) && dict.contains(sb.toString())){
                            qu.offer(sb.toString());
                            book.add(sb.toString());
                        }
                    }
                }
            }
            count++;
        }
        return 0;
    }
    public int orangesRotting(int[][] grid) {
        int row = grid.length;
        int col = grid[0].length;
        return BFS(grid,row,col);
    }
    int[][] nextP = {{1,0},{-1,0},{0,1},{0,-1}};
    private int BFS(int[][] grid, int row, int col) {
        int count = 0;
        Queue<Pair> qu = new LinkedList<>();//存放在该此搜索时候坏掉的桔子
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 2){//把腐烂的桔子放到队列中
                    qu.offer(new Pair(i,j));
                }
            }
        }
        while (!qu.isEmpty()){
            int size = qu.size();
            boolean flg = false;
            while (size-- != 0){
                Pair cur = qu.poll();
                for (int i = 0; i < 4; i++) {
                    int newX = cur.x + nextP[i][0];
                    int newY = cur.y + nextP[i][1];
                    if (newX < 0 || newX >= row
                        ||newY < 0 || newY >= col){//判断新的位置是否越界
                        continue;
                    }
                    if (grid[newX][newY] == 1){
                        flg = true;
                        grid[newX][newY] = 2;
                        qu.offer(new Pair(newX,newY));//将刚变坏的桔子放到队列中去
                    }
                }
            }
            if (flg){//如果此次BFS有变坏水果
                count++;
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 1){//把腐烂的桔子放到队列中
                    return -1;
                }
            }
        }
        return count;
    }

    public static void main1(String[] args) {
        int[][] Mat = {{0,0,1,0},
                {1,0,0,1},
                {0,0,0,0},
                {1,1,0,0}};
        Scanner sc = new Scanner(System.in);
        while (true){
            System.out.println("起点，终点");
            int sx = sc.nextInt();
            int sy = sc.nextInt();
            int ex = sc.nextInt();
            int ey = sc.nextInt();
            System.out.println(BFS1(Mat,sx,sy,ex,ey));
        }
    }
    private static boolean BFS1(int[][] mat, int sx, int sy, int ex, int ey) {
        Queue<pair> qu = new LinkedList<>();//创建存储当前广度优先步数的位置个数
        int[][] nextP = {{1,0},{-1,0},{0,1},{0,-1}};//搜索数组
        qu.offer(new pair(sx,sy));//将初始位置放进去
        Stack<pair> stack = new Stack<>();
        while (!qu.isEmpty()){//队列中的元素不为空
            int size = qu.size();//记录当前队列的大小，以便我们把队列中的元素全部拿出来，然后用队列存储下一步的所有位置。
            while (size-- != 0){
                pair pair = qu.poll();
                stack.add(pair);
                if (pair.x == ex && pair.y == ey){//判断改位置是否是终点
                    for (pair p : stack) {
                        System.out.println(p.x + "      " + p.y);
                    }
                    return true;
                }
                for (int i = 0; i < 4; i++) {
                    int newX = pair.x + nextP[i][0];
                    int newY = pair.y + nextP[i][1];
                    if (newX < 0 || newX >= mat.length
                        ||newY < 0 || newY >= mat[0].length){
                        continue;
                    }
                    if (mat[newX][newY] == 0){

                        mat[newX][newY] = 2;//标记此位置已经被搜索过了。
                        System.out.println(newX + " " + newY);
                        qu.offer(new pair(newX,newY));
                    }
                }
                stack.pop();
            }
        }

        return false;
    }
}
