package bktest;

import java.util.*;

public class Test4 {

    public int openLock(String[] deadends, String target) {
        Set<String> deadSet = new HashSet<>(Arrays.asList(deadends));//将死亡数组存放到一个哈希表中
        Set<String> book = new HashSet<>();//存放已经搜索过字符串，避免重复搜索
        book.add("0000");
        if (deadSet.contains("0000")){//如果"0000"字符串在死亡数组中，就不会转动，不会找到目标数组
            return -1;
        }
        int step = 0;
        Queue<String> qu = new LinkedList<>();//存放当前情况下的需要进行搜索的字符串
        qu.offer("0000");
        while (!qu.isEmpty()){//进行BFS
            int size = qu.size();
            while (size-- > 0){
                String str = qu.poll();
                if (str.equals(target)){//判断该搜索字符串是不是目标字符串
                    return step;
                }
                for (int i = 0; i < 4; i++) {//对四个数字中的每个都进行一次上下翻转
                    StringBuilder sb = new StringBuilder(str);
                    char ch1 = str.charAt(i);
                    char ch2 = str.charAt(i);
                    if (ch1 == '9'){
                        ch1 = '0';
                    }else {
                        ch1++;
                    }
                    if(ch2 == '0'){
                        ch2 = '9';
                    }else {
                        ch2--;
                    }
                    sb.setCharAt(i,ch1);
                    if (!book.contains(sb.toString())&&!deadSet.contains(sb.toString())){//判断是否不在死亡数组中，并且没有被搜索到
                        book.add(sb.toString());
                        qu.offer(sb.toString());
                    }
                    sb.setCharAt(i,ch2);
                    if (!book.contains(sb.toString())&&!deadSet.contains(sb.toString())){
                        book.add(sb.toString());
                        qu.offer(sb.toString());
                    }
                }
            }
            step++;
        }
        return -1;
    }

    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Queue<String> qu = new LinkedList<>();//存放应该根据其搜索的字符串
        qu.offer(beginWord);
        Set<String> dict = new HashSet<>(wordList);//存放字典中的字符串。
        Set<String> book = new HashSet<>();//标记已经搜索过的字符串
        book.add(beginWord);
        int step = 1;//最短转换数列的数目
        while (!qu.isEmpty()){//进行BFS
            int size = qu.size();
            while (size-- > 0){
                String str = qu.poll();
                if (str.equals(endWord)){//判断队列中的字符串是否等于endWord
                    return step;
                }
                for (int i = 0; i < str.length(); i++) {//一个单词中一个字母需要进行依次替换
                    StringBuilder sb = new StringBuilder(str);//保证替换下一个的时候，其他位置的字母相对str不会改变
                    for (char ch = 'a'; ch <= 'z'; ch++) {//对一个位置的字母进行26次变化
                        sb.setCharAt(i,ch);
                        if (!book.contains(sb.toString()) && dict.contains(sb.toString())){//判断经过此次变化的字符串是否在字典中，并且未被搜索过
                            book.add(sb.toString());
                            qu.offer(sb.toString());
                        }
                    }
                }
            }
            step++;
        }
        return -1;
    }
    int[][] nextP = {{1,0},{-1,0},{0,-1},{0,1}};
    public int orangesRotting(int[][] grid) {
        Queue<Pair> qu = new LinkedList<>();//存储需要搜索的坏桔子
        int row = grid.length;
        int col = grid[0].length;
        for (int i = 0; i < row; i++) {//把坏掉的桔子存放到队列中去
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 2){
                    qu.offer(new Pair(i,j));
                }
            }
        }
        int step  = 0;
        while (!qu.isEmpty()){//进行BFS
            int size = qu.size();
            while (size-- > 0){
                Pair pair = qu.poll();
                for (int i = 0; i < 4; i++) {//对坏橘子四周进行搜索
                    int newX = pair.x + nextP[i][0];
                    int newY = pair.y + nextP[i][1];
                    if (newX < 0 || newX >= row
                        || newY < 0 || newY >= col){//判断坏桔子的位置是否越界
                        continue;
                    }
                    if (grid[newX][newY] == 1){//坏桔子周围的好桔子放到队列中并且标记该位置为坏桔子
                        grid[newX][newY] = 2;
                        qu.offer(new Pair(newX,newY));
                    }
                }
            }
            //坏桔子四周如果有好桔子的话，我们才会对分钟数+1，
            // 如果队列不为null，说明此次搜索周围有桔子变坏
            if (!qu.isEmpty()){
                step++;
            }
        }

        //判断是否还有未坏掉的桔子，如果有就返回-1
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 1){
                    return -1;
                }
            }
        }
        return step;
    }
}
