package bktest;

import java.util.*;

public class Test10 {
    public void solveSudoku(char[][] board) {
        backtracking(board);
    }

    private boolean backtracking(char[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == '.'){
                    for (char k = '1'; k <= '9'; k++) {
                        if (isValid2(board,i,j,k)){
                            board[i][j] = k;
                            boolean result = backtracking(board);
                            if (result){
                                return true;
                            }
                            board[i][j] = '.';
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isValid2(char[][] board, int row, int col, char ch) {
        for (int i = 0; i < 9; i++) {//判断列是否有相同的
            if (board[i][col] == ch){
                return false;
            }
        }
        for (int j = 0; j < 9; j++) {//判断行是否有相同的
            if (board[row][j] == ch){
                return false;
            }
        }
        int x = (row / 3) * 3;
        int y = (col / 3) * 3;
        for (int i = x; i < x + 3; i++) {//判断 3 * 3 是否满足
            for (int j = y; j < y + 3; j++) {
                if (board[i][j] == ch){
                    return false;
                }
            }
        }
        return true;
    }

    public List<List<String>> solveNQueens(int n) {
        List<List<String>> result = new ArrayList<>();
        char[][] path = new char[n][n];
        for (int i = 0; i < n; i++) {
            Arrays.fill(path[i],'.');
        }
        backtracking14(result,path,0,0);

        return result;
    }

    private void backtracking14(List<List<String>> result, char[][] path, int index, int n) {
        if (index == n){
            result.add(toList(path));
            return;
        }
        for (int i = 0; i < n; i++) {
            if (isValid(path,index,i)){
                path[index][i] = 'Q';
                 backtracking14(result,path,index+1,n);
                path[index][i] = '.';
            }
        }
    }

    private boolean isValid(char[][] path, int row, int col) {
        for (int i = row; i >= 0; i--) {//判断列
            if (path[i][col] == 'Q'){
                return false;
            }
        }
        for (int j = col; j >= 0; j--) {//判断列
            if (path[row][j] == 'Q'){
                return false;
            }
        }
        for (int i = row,j = col; i >= 0 && j >= 0; i--,j--) {//判断125°角
            if (path[i][j] == 'Q'){
                return false;
            }
        }
        for (int i = row, j = col; i >= 0 && j < path.length; i--,j++) {//判断45°角
            if (path[i][j] == 'Q'){
                return false;
            }
        }
        return true;
    }

    private ArrayList<String> toList(char[][] path) {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < path.length; i++) {
            String str = "";
            for (int j = 0; j < path[i].length; j++) {
                str += path[i][j];
            }
            list.add(str);
        }
        return list;
    }

    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        boolean[] used = new boolean[nums.length];
        backtracking13(result,path,nums,used);

        return result;
    }

    private void backtracking13(List<List<Integer>> result, List<Integer> path, int[] nums, boolean[] used) {
        if (path.size() == nums.length){
            result.add(new ArrayList<>(path));
            return;
        }
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            if (set.contains(nums[i]) || used[i]){
                continue;
            }
            set.add(nums[i]);
            used[i] = true;
            path.add(nums[i]);
            backtracking13(result,path,nums,used);
            path.remove(path.size() - 1);
            used[i] = false;
        }

    }

    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        boolean[] used = new boolean[nums.length];
         backtracking12(result,path,nums,used);
        return result;
    }

    private void backtracking12(List<List<Integer>> result, List<Integer> path, int[] nums, boolean[] used) {
        if (path.size() == nums.length){
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            if (used[i]){
                continue;
            }
            used[i] = true;
            path.add(nums[i]);
            backtracking12(result,path,nums,used);
            path.remove(path.size() - 1);
            used[i] = false;
        }

    }

    public List<List<Integer>> findSubsequences(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        backtracking11(result,path,nums,0);
        return result;
    }

    private void backtracking11(List<List<Integer>> result, List<Integer> path, int[] nums, int startIndex) {
        if (path.size() > 1){
            result.add(new ArrayList<>(path));
        }
        boolean[] set = new boolean[201];
        Set<Integer> usedSet = new HashSet<>();
        for (int i = startIndex; i < nums.length; i++) {
            if (set[nums[i] + 100] || !path.isEmpty() && path.get(path.size() - 1) > nums[i]){
                continue;
            }
            set[nums[i] + 100] = true;
            path.add(nums[i]);
            backtracking11(result,path,nums,i + 1);
            path.remove(path.size() - 1);
        }



    }

    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        Arrays.sort(nums);
        boolean[] used = new boolean[nums.length];
        backtracking10(result,path,nums,0,used);

        return result;
    }

    private void backtracking10(List<List<Integer>> result, List<Integer> path, int[] nums, int startIndex, boolean[] used) {
        result.add(new ArrayList<>(path));
        if (startIndex >= nums.length){
            return;
        }
        for (int i = startIndex; i < nums.length; i++) {
            if (i > 0 && nums[i] == nums[i-1] && !used[i-1]){
                continue;
            }
            used[i] = true;
            path.add(nums[i]);
            backtracking10(result,path,nums,i + 1,used);
            path.remove(path.size() - 1);
            used[i] = false;
        }
    }

    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        backtracking9(result,path,nums,0);

        return result;

    }

    private void backtracking9(List<List<Integer>> result, List<Integer> path, int[] nums, int startIndex) {
        result.add(new ArrayList<>(path));
        for (int i = startIndex; i < nums.length; i++) {
            path.add(nums[i]);
            backtracking9(result,path,nums,i+1);
            path.remove(path.size()-1);
        }
    }

    public List<String> restoreIpAddresses(String s) {
        List<String> result = new ArrayList<>();
        backtracking8(result,s,0,0);

        return result;
    }

    private void backtracking8(List<String> result, String s, int pont, int startIndex) {

        if (pont == 3){
            if (isOk(s,startIndex,s.length() - 1)){
                result.add(s);
            }
            return;
        }
        for (int i = startIndex; i < s.length(); i++) {
            if (isOk(s,startIndex,i)){
                s = s.substring(0,i + 1) + '.' + s.substring(i + 1);
                backtracking8(result,s,pont + 1,i+2);
                s = s.substring(0,i + 1)  + s.substring(i + 2);
            }else {
                break;
            }
        }
    }

    private boolean isOk(String s, int start, int end) {
        if (start > end){//因为startIndex是每次加2,当在最后一个字符后面加.就会超过end
            return false;
        }
        if (end > start && s.charAt(start) == '0'){//前导不能为0
            return false;
        }
        if (end - start > 3){//代表有了三个点后面的数字太大了
            return false;
        }
        int num = Integer.parseInt(s.substring(start,end + 1));
        return num <= 255;
    }

    public List<List<String>> partition(String s) {
        List<List<String>> result = new ArrayList<>();
        List<String> path = new ArrayList<>();
        boolean[][] pal = getMat(s);
        System.out.println(Arrays.deepToString(pal));
        backtracking7(result,path,s,0,pal);
        return result;
    }

    private boolean[][] getMat(String s) {
        boolean[][] dp = new boolean[s.length()][s.length()];
        for (int i = s.length() - 1; i >= 0; i--) {
            for (int j = i; j < s.length(); j++) {
                if (i == j){
                    dp[i][j] = true;
                }else if (i + 1 == j){
                    dp[i][j] = s.charAt(i) == s.charAt(j);
                }else {
                    dp[i][j] = dp[i+1][j-1] && s.charAt(i) == s.charAt(j);
                }
            }
        }
        return dp;
    }

    private void backtracking7(List<List<String>> result, List<String> path, String s, int startIndex, boolean[][] pal) {
        if (startIndex == s.length()){
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = startIndex; i < s.length(); i++) {
            if (pal[i][startIndex]){
                path.add(s.substring(startIndex,i + 1));
                backtracking7(result,path,s,i + 1, pal);
                path.remove(path.size() - 1);
            }
        }
    }

    private boolean isPal(String str, int left, int right) {
        while (left < right){
            if (str.charAt(left) != str.charAt(right)){
                return false;
            }
            left++;
            right--;
        }
        return true;
    }


    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        Arrays.sort(candidates);
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        boolean[] used = new boolean[candidates.length];
        backtracking6(result,path,candidates,target,used,0,0);


        return result;
    }

    private void backtracking6(List<List<Integer>> result, List<Integer> path, int[] candidates, int target, boolean[] used, int pathSum, int startIndex) {
        if (target < pathSum){
            return;
        }
        if (target == pathSum){
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = startIndex; i < candidates.length && pathSum + candidates[i] <= target; i++) {
            if (i > 0 && candidates[i] == candidates[i-1] && !used[i]){
                continue;
            }
            path.add(candidates[i]);
            used[i] = true;
            backtracking6(result,path,candidates,target, used, pathSum + candidates[i],i + 1);
            path.remove(path.size() - 1);
            used[i] = false;
        }
    }

    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        Arrays.sort(candidates);
        backtracking5(result,path,candidates,target,0,0);

        return result;
    }

    private void backtracking5(List<List<Integer>> result, List<Integer> path, int[] candidates, int target, int sum, int index) {
        if (sum > target){
            return;
        }
        if (target == sum){
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = index; i < candidates.length && candidates[i] + sum <= target; i++) {
            path.add(candidates[i]);
            backtracking5(result,path,candidates,target,sum+candidates[i], i);
            path.remove(path.size() - 1);
        }
    }

    String[] strMap = {"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
    public List<String> letterCombinations(String digits) {
        List<String> result = new ArrayList<>();
        if (digits.length() == 0){
            return result;
        }
        backtracking4(result,digits,"",0);
        return result;
    }

    private void backtracking4(List<String> result, String digits, String pathStr, int index) {
        if (pathStr.length() == digits.length()){
            result.add(pathStr);
            return;
        }
        int num = Integer.parseInt(digits.charAt(index) + "");
        String str = strMap[num];
        for (int i = 0; i < str.length(); i++) {
            backtracking4(result,digits,pathStr + str.charAt(i),index + 1);
        }
    }

    public List<List<Integer>> combinationSum3(int k, int n) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        backtracking2(result,path,1,k,n,0);
        return result;
    }

    private void backtracking2(List<List<Integer>> result, List<Integer> path, int startIndex, int k, int n, int sum) {
        if (sum > n){//剪枝操作
            return;
        }
        if (path.size() == k){
            if (sum == n){
                result.add(new ArrayList<>(path));
            }
            return;
        }

        for (int i = startIndex; i <= 9 - (k - path.size()) + 1; i++) {
            path.add(i);
            backtracking2(result,path,i + 1,k,n,sum + i);
            path.remove(path.size() - 1);
        }
    }

    public List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        backtracking1(result,path,1,k,n);
        return result;
    }

    private void backtracking1(List<List<Integer>> result, List<Integer> path, int startIndex, int k, int n) {
        if (path.size() == k){
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = startIndex; i <= n - (k - path.size()) + 1; i++) {
            path.add(i);
            backtracking1(result,path,i+1,k,n);
            path.remove(path.size()-1);
        }
    }

}
