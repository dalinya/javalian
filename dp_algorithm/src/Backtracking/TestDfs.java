package Backtracking;

import java.util.Scanner;

public class TestDfs {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();//盒子和牌的个数
        int[] book = new int[n + 1];
        int[] box = new int[n + 1];
        Dfs(1,book,box,n);


    }

    private static void Dfs(int idx, int[] book, int[] box, int n) {
        if (idx == n + 1){//此时已经完成了一次深度优先搜索
            for (int i = 1; i <= n; i++) {
                System.out.print(box[i] + "  ");
            }
            System.out.println();
            return;
        }

        for (int i = 1; i <= n; i++) {//深度优先搜索，去放置牌
            if(book[i] == 0){
                box[idx] = i;//idx个盒子放第i个牌
                book[i] = 1;//代表第i个牌已经使用了
                Dfs(idx + 1, book, box, n);//处理下一个盒子
                book[i] = 0;//第i张牌重新拿到手里
            }
        }

    }


}
