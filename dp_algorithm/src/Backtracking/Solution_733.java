package Backtracking;

public class Solution_733 {
    int[][] nextP= {{1,0},{-1,0},{0,-1},{0,1}};//偏移数组，通过原位置找到相邻的四个位置
    public int[][] floodFill(int[][] image, int sr, int sc, int color) {
        int oldColor = image[sr][sc];//记录要修改坐标的旧颜色
        int row = image.length;
        int col = image[0].length;
        int[][] book = new int[row][col];//创建一个标记数组
        DFS(image,book,sr,sc,oldColor,color,row,col);//深度优先搜索寻找是否有相同颜色的位置

        return image;
    }

    private void DFS(int[][] image, int[][] book, int sr, int sc, int oldColor, int color,int row,int col) {
        image[sr][sc] = color;//修改当前位置的颜色
        book[sr][sc] = 1;//标记当前位置已经被修改过了
        for (int i = 0; i < 4; i++) {//搜索上下左右四个位置的颜色是否符合原颜色
            int newX = sr + nextP[i][0];
            int newY = sc + nextP[i][1];
            if (newX < 0 || newX >= row
                || newY < 0 || newY >= col) {//判断新坐标是否越界
                continue;
            }
            if (image[newX][newY] == oldColor && book[newX][newY] == 0){
                //判断当前位置是否需要进行图像渲染
                DFS(image,book,newX,newY,oldColor,color,row,col);
            }
        }
    }
}