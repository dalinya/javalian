package Backtracking;

class Solution_200 {
    public int numIslands(char[][] grid) {
        int count = 0;
        int row = grid.length;
        int col = grid[0].length;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if(grid[i][j] == '1'){
                    count++;
                    DFS(grid,i,j,row,col);
                }
            }
        }

        return count;
    }
    int[][] nextP = {{1,0},{-1,0},{0,-1},{0,1}};//四周搜索的方向数组
    private void DFS(char[][] grid, int i, int j, int row, int col) {
        grid[i][j] = '*';
        for (int k = 0; k < 4; k++) {
            int newX = i + nextP[k][0];
            int newY = j + nextP[k][1];
            //判断新下表是否越界
            if (newX < 0 || newX >= row
                || newY < 0 || newY >= col){
                continue;
            }

            //判断新位置是否需要进行搜索
            if (grid[newX][newY] == '1'){
                //对为探索的陆地进行深度优先搜索
                DFS(grid,newX,newY,row,col);
            }
        }

    }
}