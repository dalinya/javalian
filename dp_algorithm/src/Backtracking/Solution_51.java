package Backtracking;

import java.util.ArrayList;
import java.util.List;
class pair{
    public int x;
    public int y;

    public pair(int x, int y) {
        this.x = x;
        this.y = y;
    }
}

public class Solution_51 {
    public List<List<String>> solveNQueens(int n) {
        List<List<pair>> allRet = new ArrayList<>();
        List<pair> curRet = new ArrayList<>();
        DFS(allRet,curRet,0,n);//存放一种方案中皇后可以存放的位置
        //System.out.println(allRet.toString());
        return transResult(allRet,n);
    }
    void DFS(List<List<pair>> allRet,List<pair> curRet,int curRow,int n){
        //如果每一行都没有冲突,则是一种可行方案
        if (curRow == n){
            allRet.add(new ArrayList<>(curRet));
            return;
        }

        //确定当前行的每一个位置是否和已经确定的位置有冲突
        for (int i = 0; i < n; i++) {
            if(isValidPos(curRet,curRow,i)){
                curRet.add(new pair(curRow,i));//把当前位置存放到当前情况中
                //处理下一行
                DFS(allRet,curRet,curRow + 1,n);
                //回溯
                curRet.remove(curRet.size() - 1);//尾删
            }

        }
    }

    private boolean isValidPos(List<pair> curRet, int row, int col) {
        for (pair pos: curRet) {
            if (pos.y == col || pos.x + pos.y == row + col
                    || pos.x - pos.y == row - col){
                return false;
            }
        }
        return true;
    }
    private List<List<String>> transResult(List<List<pair>> allRet, int n) {
        List<List<String>> allMet = new ArrayList<>();
        for (List<pair> curRet : allRet) {
            List<String> curMat = new ArrayList<>();
            for (pair pos: curRet) {
                StringBuilder str = new StringBuilder();
                for (int i = 0; i < n; i++) {
                    str.append('.');
                }
                str.setCharAt(pos.y,'Q');
                curMat.add(str.toString());
            }
            allMet.add(curMat);
        }
        return allMet;
    }

/*
     public List<List<String>> solveNQueens(int n) {
        List<List<int[]>> allRet = new ArrayList<>();
        List<int[]> curRet = new ArrayList<>();
        DFS(allRet,curRet,0,n);
        //System.out.println(allRet.toString());
        return transResult(allRet,n);
    }
    void DFS(List<List<int[]>> allRet,List<int[]> curRet,int curRow,int n){
        //如果每一行都没有冲突,则是一种可行方案
        if (curRow == n){
            allRet.add(new ArrayList<>(curRet));
            return;
        }

        //确定当前行的每一个位置是否和已经确定的位置有冲突
        for (int i = 0; i < n; i++) {
            if(isValidPos(curRet,curRow,i)){
                curRet.add(new int[]{curRow,i});//把当前位置存放到当前情况中
                System.out.println(curRet.get(0));
                //处理下一行
                DFS(allRet,curRet,curRow + 1,n);
                //回溯
                curRet.remove(curRet.size() - 1);//尾删
            }

        }
    }

    private boolean isValidPos(List<int[]> curRet, int row, int col) {
        for (int[] pos: curRet) {
            if (pos[1] == col || pos[0] + pos[1] == row + col
            || pos[0] - pos[1] == row - col){
                return false;
            }
        }
        return true;
    }
    private List<List<String>> transResult(List<List<int[]>> allRet, int n) {
        List<List<String>> allMet = new ArrayList<>();
        for (List<int[]> curRet : allRet) {
            List<String> curMat = new ArrayList<>();
            for (int[] pos: curRet) {
                StringBuilder str = new StringBuilder();
                for (int i = 0; i < n; i++) {
                    str.append('.');
                }
                str.setCharAt(pos[1],'Q');
                curMat.add(str.toString());
            }
            allMet.add(curMat);
        }
        return allMet;
    }*/

}