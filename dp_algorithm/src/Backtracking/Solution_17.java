package Backtracking;

import java.util.ArrayList;
import java.util.List;

class Solution_17 {
    public List<String> letterCombinations(String digits) {
        List<String> list = new ArrayList<>();
        String curStr = "";//每次搜索得到的字符加到该字符串中，为临时字符串
        int len = digits.length();//所需获得字符串的长度
        DFS(digits,list,curStr,0,len); //第三个参数代表的是临时字符串的长度
        return list;
    }
    String[] strings = {"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
    private void DFS(String digits, List<String> list, String curStr, int curlen,int len) {
        if (len == curlen){
            if (len != 0){
                list.add(curStr);
            }
            return;
        }
        //进行深度优先搜索,
        int num = Integer.parseInt(digits.charAt(curlen) + "");//获取当前数字映射的字符串
        for (char ch: strings[num].toCharArray()) {
            DFS(digits,list,curStr + ch,curlen + 1,len);
        }


    }

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        for (String str:
             list) {
            System.out.println("\"" + str);

        }
    }

}