package Backtracking;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Definition for Employee.
class Employee {
    public int id;
    public int importance;
    public List<Integer> subordinates;
};

public class Solution_690 {
    public int getImportance(List<Employee> employees, int id) {
        if (employees.size() == 0){
            return 0;
        }
        Map<Integer,Employee> map = new HashMap<>();
        for (Employee employee: employees) {//把list中的数据存放到map中去
            map.put(employee.id,employee);
        }
        return Dfs(map,id);

    }

    private int Dfs(Map<Integer,Employee> map, int id) {
        int sum = map.get(id).importance;//得到该id员工的重要度
        for (int i: map.get(id).subordinates) {//遍历该id员工的下属结点
            sum += Dfs(map,i);//对该下属结点进行一次深度优先搜索
        }
        return sum;
    }
}