package Backtracking._BFS;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;



public class Solution_test {

    public static void main(String[] args) {
        int[][] Mat = {{0,0,1,0},
                        {1,0,0,1},
                        {0,0,0,0},
                        {1,1,0,0}};
        Scanner sc = new Scanner(System.in);
        while (true){
            System.out.println("起点，终点");
            int sx = sc.nextInt();
            int sy = sc.nextInt();
            int ex = sc.nextInt();
            int ey = sc.nextInt();
            System.out.println(BFS(Mat,sx,sy,ex,ey));
        }
        
    }
    private static boolean BFS(int[][] Mat, int startX, int startY, int endX, int endY) {
        Queue<Pair> qu = new LinkedList<>();
        qu.offer(new Pair(startX,startY));
        int row = Mat.length;
        int col = Mat[0].length;
        int[][] book = new int[row][col];//标记该位置是否被搜索过
        book[startX][startY] = 1;
        int[][] nextP = {{1,0},{-1,0},{0,1},{0,-1}};//四周搜索数组
        while (!qu.isEmpty()){
            Pair curPos = qu.peek();
            qu.poll();
            System.out.println(curPos.x + " " + curPos.y);
            if (curPos.x == endX && curPos.y == endY){
                return true;
            }
            for (int i = 0; i < 4; i++) {
                int nextX = curPos.x + nextP[i][0];
                int nextY = curPos.y + nextP[i][1];
                if (nextX < 0 || nextX >= row
                    || nextY < 0 || nextY >= col){
                    continue;
                }
                if (book[nextX][nextY] == 0 && Mat[nextX][nextY] == 0){
                    qu.offer(new Pair(nextX,nextY));
                    book[nextX][nextY] = 1;
                }
            }
        }
        return false;
    }
}
