package Backtracking._BFS;

import java.util.LinkedList;
import java.util.Queue;

//15:25
class Pair{
    int x;
    int y;

    public Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }
}


public class Solution_994 {
    public int orangesRotting(int[][] grid) {
        if (grid == null){
            return 0;
        }
        int row = grid.length;
        int col = grid[0].length;
        Queue<Pair> qu = new LinkedList<>();//存放坏掉水果的队列
        for (int i = 0; i < row; i++) {//把坏掉的水果放到队列中去
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 2){
                    qu.offer(new Pair(i,j));
                }
            }
        }
        int sept = 0;
        int[][] nextP = {{1,0},{-1,0},{0,1},{0,-1}};//搜索数组
        while (!qu.isEmpty()){
            boolean flg = false;//标志是否有水果又坏了
            int size = qu.size();
            while (size-- >0){
                Pair cur = qu.poll();
                for (int i = 0; i < 4; i++) {
                    int nextX =  cur.x + nextP[i][0];
                    int nextY = cur.y + nextP[i][1];
                    if (nextX < 0 || nextX >= row
                        || nextY < 0 || nextY >= col){//判断下一个位置是否越界
                        continue;
                    }
                    if (grid[nextX][nextY] == 1){//好的苹果变坏
                        flg = true;
                        grid[nextX][nextY] = 2;
                        qu.add(new Pair(nextX,nextY));//将新的坏的水果放到队列中
                    }
                }
            }
            if (flg){
                sept++;
            }
        }
        for (int i = 0; i < row; i++) {//遍历是否有没坏掉的水果
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 1){
                    return -1;
                }
            }
        }
        return sept;
    }
}