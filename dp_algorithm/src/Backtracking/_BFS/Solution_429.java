package Backtracking._BFS;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

// Definition for a Node.
class Node {
    public int val;
    public List<Node> children;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, List<Node> _children) {
        val = _val;
        children = _children;
    }
};

class Solution {
    public List<List<Integer>> levelOrder(Node root) {
        List<List<Integer>> treeMat = new LinkedList<>();
        if (root == null){
            return treeMat;
        }
        Queue<Node> qu = new LinkedList<>();
        qu.offer(root);
        while (!qu.isEmpty()){
            List<Integer> rowV = new ArrayList<>();
            int size = qu.size();//队列中元素的个数
            while (size > 0){
                rowV.add(qu.peek().val);//保存当前元素在同一行
                List<Node> curNode = qu.poll().children;
                for (Node ch:curNode) {//孩子节点入队
                    qu.add(ch);
                }
                size--;
            }

            treeMat.add(rowV);
        }
        return treeMat;
    }
}