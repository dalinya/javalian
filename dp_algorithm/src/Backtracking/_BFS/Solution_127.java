package Backtracking._BFS;

import java.util.*;

public class Solution_127 {
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        int step = 1;//在字典中查询的步数
        Queue<String> curQu = new LinkedList<>();//存放当前过程放的字符串
        curQu.offer(beginWord);
        Set<String> book = new HashSet<>();
        book.add(beginWord);
        Set<String> dict = new HashSet<>(wordList);
        while (!curQu.isEmpty()) {
            int size = curQu.size();
            while (size-- != 0) {
                String curStr = curQu.peek();
                curQu.poll();
                if (curStr.equals(endWord)) {
                    return step;
                }
                //查看当前单词能一步转化 的单词是否在字典中
                for (int i = 0; i < curStr.length(); i++) {
                    StringBuilder str = new StringBuilder(curStr);
                    //每一个位置用26英文字母
                    for (char ch = 'a'; ch <= 'z'; ch++) {
                        str.setCharAt(i, ch);
                        String tmpStr = str.toString();
                        if (dict.contains(tmpStr)
                            &&!book.contains(tmpStr)) {//判断该字符串是否在字典中并且没被搜索过
                            curQu.add(tmpStr);
                            book.add(tmpStr);//标记该字符串已经被使用过了
                        }
                    }
                }
            }
            step++;
        }
        //如果不成功转化为0
        return 0;
    }
}