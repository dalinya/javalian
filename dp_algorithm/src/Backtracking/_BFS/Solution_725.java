package Backtracking._BFS;

import java.util.*;

public class Solution_725 {



    public int openLock(String[] deadends, String target) {
        //建立一个存储死亡数组元素的哈希表，这样搜索效率更高
        Set<String> deadDict = new HashSet<>();
        for (String str:deadends) {
            deadDict.add(str);
        }

        if (deadDict.contains("0000")){
            return -1;
        }
        //建立一个存放遍历完成等待下一步搜索和判断的字符串的一个队列
        Queue<String> qu = new LinkedList<>();
        //建立一个标记数组，存放已经搜索过的字符串
        Set<String> book = new HashSet<>();
        qu.offer("0000");
        book.add("0000");

        int step = 0;//存放变化的次数

        while (!qu.isEmpty()){
            int size = qu.size();//代表该旋转次数的字符串的个数
            while (size-- != 0){
                String str = qu.poll();//取出需要操作的字符串
                if (str.equals(target)){
                    return step;
                }
                for (int i = 0; i < str.length(); i++) {
                    char ch1 = str.charAt(i);
                    char ch2 = str.charAt(i);
                    if (ch1 == '9'){
                        ch1 = '0';
                    }else {
                        ch1++;
                    }
                    if (ch2 =='0'){
                        ch2 = '9';
                    }else {
                        ch2--;
                    }
                    StringBuilder sb1 = new StringBuilder(str);
                    StringBuilder sb2 = new StringBuilder(str);
                    sb1.setCharAt(i,ch1);
                    sb2.setCharAt(i,ch2);
                    if (!book.contains(sb1.toString()) && !deadDict.contains(sb1.toString())){
                        book.add(sb1.toString());
                        qu.offer(sb1.toString());
                    }
                    if (!book.contains(sb2.toString()) && !deadDict.contains(sb2.toString())){
                        book.add(sb2.toString());
                        qu.offer(sb2.toString());
                    }

                }

            }
            step++;
        }
        return -1;
    }
}