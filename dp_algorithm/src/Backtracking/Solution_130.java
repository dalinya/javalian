package Backtracking;

public class Solution_130 {

    public void solve(char[][] board) {
        int row = board.length;
        int col = board[0].length;
        //找没被包围的O,把没被包围的O设置成 * ，最后把被包围的O变成X，把*改回来O，
        for (int i = 0; i < row; i++) {//查找第一列和最后一列中的O
            if (board[i][0] == 'O'){
                DFS(board,row,col,i,0);
            }
            if (board[i][col - 1] =='O'){
                DFS(board,row,col,i,col-1);
            }
        }
        for (int j = 1; j < col - 1; j++) {//查找第一行和最后一行中的O
            if (board[0][j] == 'O'){
                DFS(board,row,col,0,j);
            }
            if (board[row - 1][j] == 'O'){
                DFS(board,row,col,row - 1,j);
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (board[i][j] == '*'){
                    board[i][j] = 'O';
                }else if (board[i][j] == 'O'){
                    board[i][j] = 'X';
                }
            }
        }
    }

    int[][] nextP = {{1,0},{-1,0},{0,-1},{0,1}};//四周搜索数组
    private void DFS(char[][] board, int row, int col, int i, int j) {
        board[i][j] = '*';//代表此位置已经被搜索到了。
        //搜索新的位置
        for (int k = 0; k < 4; k++) {
            int newX = i + nextP[k][0];
            int newY = j + nextP[k][1];
            //判断新下标是否越界
            if (newX < 0 || newX >= row
                || newY < 0 || newY >= col){
                continue;
            }
            //判断该位置是否有联通
            if (board[newX][newY] == 'O'){
                DFS(board,row,col,newX,newY);
            }
        }
    }
}