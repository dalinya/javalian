package Backtracking;

import java.util.ArrayList;
import java.util.List;

class Solution_39 {
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> allRet = new ArrayList<>(new ArrayList<>());
        List<Integer> curRet = new ArrayList<>();
        int curSum = 0;//搜索当前情况下的数字和
        int prev = 0;//当前你所需要的DFS时加和的下标
        DFS(candidates,target,allRet,curRet,curSum,prev);

        return allRet;
    }

    private void DFS(int[] candidates, int target, List<List<Integer>> allRet, List<Integer> curRet, int curSum, int prev) {
        if (curSum >= target){
            if (curSum == target){
                //保存当前的解集
                allRet.add(new ArrayList<>(curRet));
            }
            return;
        }
        //累加的起始位置为上一项的位置
        for (int i = prev; i < candidates.length; i++) {
            if (candidates[i] > target){
                continue;
            }
            //累加当前项
            curRet.add(candidates[i]);
            DFS(candidates,target,allRet,curRet,curSum + candidates[i],i);
            //回溯
            curRet.remove(curRet.size() - 1);
        }

    }


}