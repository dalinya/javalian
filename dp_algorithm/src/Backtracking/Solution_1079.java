package Backtracking;

import java.util.HashSet;
import java.util.Set;

class Solution_1079 {
    public int numTilePossibilities(String tiles) {
        Set<String> set = new HashSet<>();
        String curStr = "";
        int[] book = new int[tiles.length()];//标记该位置的字符是否被使用
        DFS(tiles,set,curStr,book);
        return set.size();
    }

    private void DFS(String tiles, Set<String> set, String curStr, int[] book) {
        if (!curStr.isEmpty()){
            set.add(curStr);
        }
        for (int i = 0; i < tiles.length(); i++) {
            if (book[i] == 0){
                book[i] = 1;
                DFS(tiles,set,curStr + tiles.charAt(i), book);
                //回溯
                book[i] = 0;
            }

        }
    }
}