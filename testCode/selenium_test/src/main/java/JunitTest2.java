import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class JunitTest2 {
    @Order(1)
    @Test
    void Test1(){
        System.out.println("这是Test1测试用例");
    }
    @Order(2)
    @Test
    void Test2(){
        System.out.println("这是Test2测试用例");
    }
    @Order(3)
    @Test
    void A(){
        System.out.println("这是TestA测试用例");
    }
    @Order(4)
    @Test
    void B(){
        System.out.println("这是TestB测试用例");
    }
}
