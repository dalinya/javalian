import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class JunitTest1 {
    @BeforeEach
    void BeforeEachTest() {
        System.out.println("这是BeforeEach里面的语句");
    }

    @AfterEach
    void AfterEachTest() {
        System.out.println("这是AfterEach里面的语句");
    }

    @ParameterizedTest
    @ValueSource(ints = {1,2,3})//类型的限制~
    void test4(int num){
        System.out.println(num);
    }


}
