import Package1.TestDemo1;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;

@Suite
//在同一个包中，依次执行JunitTest和JunitTest1和JunitTest2
//@SelectClasses({TestDemo1.class, JunitTest1.class,JunitTest2.class})
@SelectPackages(value = {"Package1","Package2"})
public class RunSuit {

}
