import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

public class JunitTest {
    @ParameterizedTest
    @CsvSource({"1,2,3"})
    void test5(String x,String y,String z){
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
    }
    @ParameterizedTest
    @CsvFileSource(resources = "test01.csv")
    void test5(String name){
        System.out.println(name);
    }
}
