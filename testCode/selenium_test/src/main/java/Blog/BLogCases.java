package Blog;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import static java.lang.Thread.sleep;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BLogCases extends InitAndEnd{
    public static Stream<Arguments> Generator() {
        //每次测试运行之后最前面的博客id就会变成之前的+1
        return Stream.of(
                Arguments.arguments("http://82.157.236.116:8080/servlet_blog_system/blog_detail.html",
                       "博客详情页","数据库的基本操作")
        );
    }

    /**
     * 输入正确账号，密码登录成功
     */
    @Order(1)
    @ParameterizedTest
    @CsvFileSource(resources = "LoginSuccess.csv")
    void LoginSuccess(String username,String password,String blog_list_utl) throws InterruptedException {
        //打开博客登录页面
        webDriver.get("http://82.157.236.116:8890/login.html");
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //输入账号
        webDriver.findElement(By.cssSelector("#username")).sendKeys(username);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //输入密码
        webDriver.findElement(By.cssSelector("#password")).sendKeys(password);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //点击提交按钮
        webDriver.findElement(By.cssSelector("#submit")).click();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //跳转到列表页
        //获取当前页面的url,
        String cur_url = webDriver.getCurrentUrl();
        //如果url=http://82.157.236.116:8890/myblog_list.html测试通过，反之不通过
        Assertions.assertEquals(blog_list_utl,cur_url);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        sleep(3000);
        //列表页显示的登录信息是admin
        //用户名是zhangsan测试通过，反之不通过
        String cur_admin = webDriver.findElement(By.cssSelector("body > div.container > div.container-left > div > h3")).getText();
        Assertions.assertEquals("admin",cur_admin);
    }
    /**
     * 博客列表页博客数量不为0
     */
    @Order(2)
    @Test
    void BlogList(){
        //打开博客列表页
        webDriver.get("http://82.157.236.116:8080/servlet_blog_system/blog_list.html");
        //获取页面上所有博客标题对应的元素
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        int title_num = webDriver.findElements(By.cssSelector("body > div.container > div.container-right > div:nth-child(1) > div.title")).size();
        //如果元素数量不为为0，测试通过
        Assertions.assertNotEquals(title_num,0);
    }
    /**
     * 博客详情页校验
     * url
     * 博客标题
     * 页面title是“博客详情页”
     */
    @Order(3)
    @ParameterizedTest
    @MethodSource("Generator")
    void BlogDetail(String expected_url,String expected_title,String expected_blog_title){
        //找到第一篇博客对应的查看全文按钮。
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        webDriver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/a")).click();
        //获取当前页面的URL，
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        String cur_url = webDriver.getCurrentUrl();
        //获取当前页面的title
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        String cur_title = webDriver.getTitle();
        //获取博客标题
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        String blog_title = webDriver.findElement(By.cssSelector("body > div.container > div.container-right > div > div.title")).getText();
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        //Assertions.assertEquals(expected_url,cur_url);
        System.out.println(expected_blog_title +" ::: " + blog_title);
        Assertions.assertEquals(expected_title,cur_title);
        Assertions.assertEquals(expected_blog_title,blog_title);
        if (cur_url.contains(expected_url)){
            System.out.println("测试通过");
        }else {
            System.out.println("测试不通过");
        }
    }
    /**
     * 写博客
     */
    @Order(4)
    @Test
    void EditBlog() throws InterruptedException {
        //找到写博客按钮，点击
        webDriver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)")).click();
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        //找到输入框输入标题
        webDriver.findElement(By.cssSelector("#title")).sendKeys("");
        //通过js将标题进行输入
        ((JavascriptExecutor)webDriver).executeScript("document.getElementById(\"title\").value = \"自动化测试代码\"");
        sleep(3000);
        //点击发布
        webDriver.findElement(By.cssSelector("#submit")).click();
        sleep(3000);
        //获取当前页面的url
        String cur_url = webDriver.getCurrentUrl();
        Assertions.assertEquals("http://82.157.236.116:8080/servlet_blog_system/blog_list.html",cur_url);
    }
    /**
     * 校验已发布博客的标题
     * 校验已发布博客的时间
     */
    @Order(5)
    @Test
    void BlogInfoChecked(){
        webDriver.get("http://82.157.236.116:8080/servlet_blog_system/blog_list.html");
        //获取第一篇博客的标题
        String first_blog_title =  webDriver.findElement(By.cssSelector("body > div.container > div.container-right > div:nth-child(1) > div.title")).getText();
        //获取第一篇博客发布时间
        String first_blog_time = webDriver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div[2]")).getText();
        //校验博客标题是不是自动化测试
        Assertions.assertEquals("自动化测试代码",first_blog_title);
        // 如果时间是当天发布的，就测试通过
        if (first_blog_time.contains("2023-09-17")){
            System.out.println("测试通过");
        }else{
            System.out.println("当前时间：" + first_blog_title);
            System.out.println("测试不通过");
        }
    }
    /**
     * 删除刚刚发布的博客
      */
    @Order(6)
    @Test
    void DeleteBlog() throws InterruptedException {
        //打开博客列表页
        webDriver.get("http://82.157.236.116:8080/servlet_blog_system/blog_list.html");
        //点击全文按钮
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        webDriver.findElement(By.cssSelector("body > div.container > div.container-right > div:nth-child(1) > a")).click();
        //点击删除按钮
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        webDriver.findElement(By.cssSelector("#deleteBlog")).click();
        sleep(3000);
        //获取博客列表第一篇标题不是“自动化测试”
        String first_blog_title = webDriver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div[1]")).getText();
        //校验当前博客标题不等于“自动化测试”
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        Assertions.assertNotEquals(first_blog_title,"自动化测试代码");
    }

    /**
     * 校验注销功能
     */
    @Order(7)
    @Test
    void Logout(){
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        webDriver.findElement(By.cssSelector("body > div.nav > a:nth-child(6)")).click();
        //校验url(登录url)
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        String cur_url = webDriver.getCurrentUrl();
        Assertions.assertEquals("http://82.157.236.116:8080/servlet_blog_system/login.html",cur_url);
        //校验提交按钮
        WebElement webElement = webDriver.findElement(By.cssSelector("#submit"));
        Assertions.assertNotNull(webElement);
    }

}
