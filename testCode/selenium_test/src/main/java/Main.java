import javafx.util.Pair;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;


public class Main {
    public static void main(String[] args) throws InterruptedException, IOException {
        //创建驱动
        WebDriver webDriver = new ChromeDriver();
        // 打开网页
        webDriver.get("https://www.baidu.com");
        webDriver.findElement(By.cssSelector("#kw")).sendKeys("软件测试");
        webDriver.findElement(By.cssSelector("#su")).click();
        sleep(3000);
        File file = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("D://20230911jietu.png"));
    }
    public List<String> topKFrequent(String[] words, int k) {
        HashMap<String,Integer> map = new HashMap<>();//统计单词出现的频次
        for (int i = 0; i < words.length; i++) {
            map.put(words[i],map.getOrDefault(words[i],0) + 1);
        }

        //频次：小根堆，字典序：大根堆
        PriorityQueue<Pair<String,Integer>> heap = new PriorityQueue<>(
                (a,b)->{
                    return a.getValue().equals(b.getValue()) ?
                            b.getKey().compareTo(a.getKey()) : a.getValue().compareTo(b.getValue());
                });
        for (Map.Entry<String,Integer> entry: map.entrySet()) {
            heap.offer(new Pair<>(entry.getKey(),entry.getValue()));
            if (heap.size() > k){
                heap.poll();
            }
        }
        List<String> result = new ArrayList<>();
        while (!heap.isEmpty()){
            result.add(heap.poll().getKey());
        }
        Collections.reverse(result);
        return result;
    }
    class MedianFinder {
        PriorityQueue<Integer> heapLeft;//小根堆
        PriorityQueue<Integer> heapRight;//大根堆
        public MedianFinder() {
            heapLeft = new PriorityQueue<>();//小根堆
            heapRight = new PriorityQueue<>((a,b) -> b - a);//大根堆
        }

        public void addNum(int num) {
            int m = heapLeft.size(),n = heapRight.size();
            if(m == n){
                if (m == 0||num <= heapLeft.peek()){
                    heapLeft.offer(num);
                }else {
                    heapRight.offer(num);
                    int y = heapRight.poll();
                    heapLeft.offer(y);
                }
            }else if (m == n + 1){
                if (num <= heapLeft.peek()){//先进再出到heapRight中
                    heapLeft.offer(num);
                    int x = heapLeft.poll();
                    heapRight.offer(x);
                }else {//直接进
                    heapRight.offer(num);
                }
            }
        }

        public double findMedian() {
            if (heapLeft.size() == heapRight.size()){
                return (heapLeft.peek() + heapRight.peek()) / 2.0;
            }
            return heapLeft.peek();
        }
    }
}