import javafx.util.Pair;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        System.out.println(calculate("3 / 2"));
    }
    public static int calculate(String ss) {
        ss = ss.replace(" ","");
        char[] s = ss.toCharArray();
        Stack<Integer> stack = new Stack<>();
        int tmp = 0;
        char op = ' ';
        for (int i = 0; i <= s.length; i++) {
            if (i < s.length && s[i] >= '0' && s[i] <= '9'){//判断是否数字
                tmp = tmp * 10 + (s[i] - '0');
            }else {
                if (op == '/'){
                    tmp = stack.pop() / tmp;
                }else if (op == '*'){
                    tmp = stack.pop() * tmp;
                }else if (op == '-'){
                    tmp *= -1;
                }
                if (i < s.length){
                    op = s[i];
                }
                stack.push(tmp);
                tmp = 0;
            }
        }
        int result = 0;
        while (!stack.isEmpty()){
            result += stack.pop();
        }
        return result;
    }
    public String decodeString(String ss) {
        char[] s = ss.toCharArray();
        //用栈来模拟
        Stack<Integer> stackNum = new Stack<>();//数字栈
        Stack<String> stackStr = new Stack<>();//字符串栈
        stackStr.push("");
        int index = 0,len = s.length;
        while (index < len){
            if (s[index] >= '0' && s[index] <= '9'){//1.遇到数字，将这个数字放到数字栈中
                int num = 0;
                while (index < len && s[index] >= '0' && s[index] <= '9'){
                    num = num * 10 + (s[index++] - '0');
                }
                stackNum.push(num);
            }else if (s[index] == '['){//2.遇到[把后面的字符串提取出来，放到字符串栈中。
                index++;
                String str = "";
                while (index < len && s[index] >= 'a' && s[index] <= 'z'){
                    str += s[index++];
                }
                stackStr.push(str);
            }else if (s[index] == ']'){//3.遇到]进行解析 ，然后放到字符串栈顶元素的后面
                index++;
                String str = stackStr.pop();
                int num = stackNum.pop();
                String input = "";
                for (int i = 0; i < num; i++) {
                    input += str;
                }
                stackStr.push(stackStr.pop() + input);
            }else {//3.遇到单独的字符串就直接放到字符串栈顶元素的最后面
                String input = "";
                while (index < len && s[index] >= 'a' && s[index] <= 'z'){
                       input += s[index++];
                }
                stackStr.push(stackStr.pop() + input);
            }

        }
        return stackStr.peek();
    }
    public boolean backspaceCompare(String ss, String tt) {
        char[] s = ss.toCharArray();
        char[] t = tt.toCharArray();
        return changeStr(s).equals(changeStr(t));
    }
    private String changeStr(char[] s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length; i++) {
            if (s[i] == '#'){
                if (sb.length() > 0){//删除最后一个字符
                    sb.deleteCharAt(sb.length() - 1);
                }
            }else {
                sb.append(s[i]);
            }
        }
        return sb.toString();
    }
    public boolean validateStackSequences(int[] pushed, int[] popped) {
        Stack<Integer> stack = new Stack<>();
        int pop = popped.length - 1;
        for (int i = 0; i < pushed.length; i++) {
            stack.push(pushed[i]);
            while (pop > 0 && (!stack.isEmpty()) &&stack.peek().equals(popped[pop])){
                pop--;
                stack.pop();
            }
        }
        return stack.isEmpty();
    }
    public List<List<Integer>> levelOrder(Node root) {
        List<List<Integer>> result = new ArrayList<>();
        if(root == null){
            return result;
        }
        Queue<Node> qu = new LinkedList<>();
        qu.offer(root);//将根节点放入到队列中
        boolean flg = false;
        while (!qu.isEmpty()){
            int size = qu.size();
            List<Integer> path = new ArrayList<>();
            while (size-- > 0){
                Node cur = qu.poll();//将队列中的元素弹出
                for (Node node:cur.children) {
                    qu.offer(node);
                }
                path.add(cur.val);
            }
            if (flg){
                Collections.reverse(path);
            }
            flg = !flg;
            result.add(path);
        }
        return result;
    }
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if(root == null){
            return result;
        }
        Queue<TreeNode> qu = new LinkedList<>();
        qu.offer(root);//将根节点放入到队列中
        boolean flg = false;
        while (!qu.isEmpty()){
            int size = qu.size();
            List<Integer> path = new ArrayList<>();
            while (size-- > 0){
                TreeNode cur = qu.poll();//将队列中的元素弹出
                if (cur.left != null){
                    qu.offer(cur.left);
                }
                if (cur.right != null){
                    qu.offer(cur.right);
                }
                path.add(cur.val);
            }
            if (flg){
                Collections.reverse(path);
            }
            flg = !flg;
            result.add(path);
        }
        return result;
    }

    public int widthOfBinaryTree(TreeNode root) {
        List<Pair<TreeNode,Integer>> qu = new ArrayList<>();
        int result = 0;
        qu.add(new Pair<>(root,1));
        while (!qu.isEmpty()){
            Pair<TreeNode,Integer> t1 = qu.get(0);//记录该层的第一个节点
            Pair<TreeNode,Integer> t2 = qu.get(qu.size()-1);//记录该层的最后一个结点
            List<Pair<TreeNode,Integer>> tmp = new ArrayList<>();//重新创建一个顺序表模拟队列
            result = Math.max(result,t2.getValue() - t1.getValue());
            for (Pair<TreeNode,Integer> t: qu) {//将该层的结点取出
                TreeNode node = t.getKey();
                int index = t.getValue();
                if (node.left != null){
                    tmp.add(new Pair<>(node.left,2 * index));
                }
                if (node.right != null){
                    tmp.add(new Pair<>(node.right,2 * index + 1));
                }
            }
            qu = tmp;//将队列重新赋值
        }
        return result;
    }
    public List<Integer> largestValues(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        if (root == null){
            return result;
        }
        Queue<TreeNode> qu = new LinkedList<>();
        qu.offer(root);
        while (!qu.isEmpty()){
            int size = qu.size();
            int max = Integer.MIN_VALUE;
            while (size-- > 0){
                TreeNode cur = qu.poll();
                max = Math.max(max,cur.val);
                if (cur.left != null){
                    qu.offer(cur.left);
                }
                if (cur.right != null){
                    qu.offer(cur.right);
                }
            }
            result.add(max);
        }
        return result;
    }
    public int lastStoneWeight(int[] stones) {
        //优先级队列
        PriorityQueue<Integer> pq = new PriorityQueue<>((a,b)->b-a);
        for (int i = 0; i < stones.length; i++) {
            pq.offer(stones[i]);
        }
        while (pq.size() > 1){
            int a = pq.poll();
            int b = pq.poll();
            int diff = Math.abs(a - b);
            if (diff != 0){
                pq.offer(diff);
            }
        }
        return pq.isEmpty() ? 0 :pq.peek();
    }

    class KthLargest {
        //求第k小的用大根堆
        PriorityQueue<Integer> pq = null;
        int _k = 0;
        public KthLargest(int k, int[] nums) {
            pq = new PriorityQueue<>();
            this._k = k;
            for (int i = 0; i < nums.length; i++) {
                pq.offer(nums[i]);
                if (pq.size() > _k) {
                    pq.poll();
                }
            }
        }

        public int add(int val) {
            pq.offer(val);
            if (pq.size() > _k) {
                pq.poll();
            }
            return pq.peek();
        }
    }
    public boolean evaluateTree(TreeNode root) {
        //3.递归出口：当前结点是叶子结点
        if(root.right == null && root.left == null){
            return root.val == 1;//叶子结点0 -- False 1 -- True
        }
        boolean left = evaluateTree(root.left);
        boolean right = evaluateTree(root.right);
        return root.val == 2 ? left || right:left && right;
    }

    public int sumNumbers(TreeNode root) {
        return dfs(root,0);
    }
    //1.函数体：计算当前结点的数字和
    private int dfs(TreeNode root, int num) {
        //2.1函数体：计算当前结点的num
        num = num * 10 + root.val;
        //3. 递归出口:当前结点是叶子结点
        if (root.left == null && root.right == null){
            return num;
        }
        int sum = 0;
        //2.2：获取左子树的sum
        if (root.left != null){
            sum += dfs(root.left,num);
        }
        //2.3：获取右子树的sum
        if (root.right != null){
            sum += dfs(root.right,num);
        }
        //2.3返回当前树的sum
        return sum;
    }
    public TreeNode pruneTree(TreeNode root) {
        if (root == null){
            return null;
        }
        //处理左子树
        root.left = pruneTree(root.left);
        //处理右子树
        root.right = pruneTree(root.right);
        if (root.right == null && root.left == null && root.val == 0){
            return null;
        }
        return root;
    }

    long prev = Long.MIN_VALUE;//全局变量
    public boolean isValidBST(TreeNode root) {
        if(root == null){
            return true;
        }
        if (!isValidBST(root.left)){
            return false;
        }
        if (prev < root.val){
            prev = root.val;
        }else {
            return false;
        }
        return isValidBST(root.right);
    }
    int count,result = 0;
    public int kthSmallest(TreeNode root, int k) {
        count = k;
        dfs(root);
        return result;
    }

    private void dfs(TreeNode root) {
        if (root == null || count == 0){
            return;
        }
        dfs(root.left);
        count--;
        if (count == 0){
            result = root.val;
            return;
        }
        dfs(root.right);
    }

}
class Solution257 {
    List<String> result;//全局变量存放结果
    public List<String> binaryTreePaths(TreeNode root) {
        result = new ArrayList<>();
        String path = "";//路径
        bfs(root,path);
        return result;
    }

    private void bfs(TreeNode root, String path) {
        if (root == null){
            return;
        }
        path += root.val;
        if (root.left == null && root.right == null){//递归出口
            result.add(path);
            return;
        }
        bfs(root.left,path + "->");
        bfs(root.right,path + "->");
    }
}
class Solution46 {
    List<List<Integer>> result;//最终结果
    List<Integer> path;//路径中的值
    boolean[] check;//标记某下表的元素是否被用过了
    public List<List<Integer>> permute(int[] nums) {
        result = new ArrayList<>();
        path = new ArrayList<>();
        check = new boolean[nums.length];
        dfs(nums);
        return result;
    }

    private void dfs(int[] nums) {
        if (nums.length == path.size()){
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            if (!check[i]){//当前结点没有使用过
                path.add(nums[i]);
                check[i] = true;
                dfs(nums);
                check[i] = false;
                path.remove(path.size() - 1);
            }
        }
    }

}
class Solution78_1 {
    //方式1选/不选
    List<List<Integer>> result;
    List<Integer> path;
    public List<List<Integer>> subsets(int[] nums) {
        result = new ArrayList<>();
        path = new ArrayList<>();
        int index = 0;
        dfs(nums,index);
        return result;
    }

    private void dfs(int[] nums, int index) {
        if (index == nums.length){
            result.add(new ArrayList<>(path));
            return;
        }
        dfs(nums,index + 1);
        path.add(nums[index]);
        dfs(nums,index + 1);
        path.remove(path.size()-1);
    }
}
class Solution78_2 {
    //只遍历后面的。
    List<List<Integer>> result;
    List<Integer> path;
    public List<List<Integer>> subsets(int[] nums) {
        result = new ArrayList<>();
        path = new ArrayList<>();
        int pos = 0;//代表当前回溯最后一个元素下标的位置
        dfs(nums,pos);
        return result;
    }

    private void dfs(int[] nums, int pos) {
        result.add(new ArrayList<>(path));
        for (int i = pos + 1; i < nums.length; i++) {
            path.add(nums[pos]);
            dfs(nums,i);
            path.remove(path.size() - 1);//回溯
        }
    }
}
class Solution1863 {
    int sum;
    int path;//存放子集的亦数值
    public int subsetXORSum(int[] nums) {
        dfs(nums,0);
        return sum;
    }

    private void dfs(int[] nums, int index) {
        sum += path;
        for (int i = index; i < nums.length; i++) {
            path ^= nums[index];
            dfs(nums,i + 1);
            path ^= nums[index];//恢复现场
        }
    }
}
class Solution47 {
    List<List<Integer>> result;
    List<Integer> path;
    boolean[] check;
    public List<List<Integer>> permuteUnique(int[] nums) {
        result = new ArrayList<>();
        path = new ArrayList<>();
        check = new boolean[nums.length];
        Arrays.sort(nums);//去重的前置操作，对数组进行排序
        dfs(nums);
        return result;
    }

    private void dfs(int[] nums) {
        if (path.size() == nums.length){//递归出口
            System.out.println(path.size());
            result.add(new ArrayList<>(path));
        }
        for (int i = 0; i < nums.length; i++) {
            //合法分支：
            //check[i] == false:同一个数只能使用一次
            //(i > 0 && nums[i] != nums[i - 1]) || check[i-1] == true : 同一个节点的所有分支中，相同的元素只能选择一次
            if (check[i] == false &&(i == 0 || nums[i] != nums[i - 1] || check[i-1] == true)){
                check[i] = true;
                path.add(nums[i]);
                //递归出口
                path.remove(path.size() - 1);
                check[i] = false;
            }

        }
    }
}
class Solution17 {
    List<String> result;
    String path;
    //数字与字符串间的映射关系
    String[] hash = {"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
    public List<String> letterCombinations(String digits) {
        path = "";
        result = new ArrayList<>();
        int pos = 0;//当前遍历的数字下标
        dfs(digits.toCharArray(),pos);
        return result;
    }

    private void dfs(char[] digits, int pos) {
        if (path.length() == digits.length){//递归出口
            result.add(path);
            return;
        }
        for (char ch: hash[digits[pos]-'0'].toCharArray()) {
            path += ch;
            dfs(digits,pos + 1);
            //恢复现场
            path = path.substring(0,path.length() - 1);
        }
    }
}
class Solution22 {
    int left,right;
    String path;
    List<String> result;
    public List<String> generateParenthesis(int n) {
        result = new ArrayList<>();
        path = "";
        dfs(n);
        return result;
    }

    private void dfs(int n) {
        if (n == right){//递归出口：先放left,再放right,right放完了，left肯定也放完了。
            result.add(path);
            return;
        }
        if (left >= right){
            path += '(';
            left++;
            dfs(n);
            //恢复现场
            path = path.substring(0,path.length() - 1);
            left--;
        }
        if (left > right){
            path += ')';
            right++;
            dfs(n);
            //恢复现场
            path = path.substring(0,path.length() - 1);
            right--;
        }
    }
}
class Solution {
    List<List<Integer>> result;
    List<Integer> path;
    public List<List<Integer>> combine(int n, int k) {
        result = new ArrayList<>();
        path = new ArrayList<>();
        int pos = 1;
        dfs(n,k,pos);
        return result;
    }

    private void dfs(int n, int k, int pos) {
        if (path.size() == k){//递归出口
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = pos; i <= n && (k - path.size() <= n - i + 1); i++) {
            path.add(i);
            dfs(n,k,i+1);
            //恢复现场
            path.remove(path.size() - 1);
        }
    }
}
class Solution494 {
    int result = 0;
    public int findTargetSumWays(int[] nums, int target) {
        int sum = 0;
        dfs(nums,target,sum,0);
        return result;
    }

    private void dfs(int[] nums, int target, int sum, int pos) {
        if (sum == target){
            result++;
        }
        if (pos == nums.length){//递归出口
            return;
        }
        dfs(nums,target,sum + nums[pos],pos + 1);
        dfs(nums,target,sum - nums[pos],pos + 1);
    }
}
class Solution39_1 {
    List<List<Integer>> result;
    List<Integer> path;
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        result = new ArrayList<>();
        path = new ArrayList<>();
        Arrays.sort(candidates);
        dfs(candidates,target,0,0);
        return result;
    }

    private void dfs(int[] candidates, int target, int index, int sum) {
        if (sum == target){
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = index; i < candidates.length; i++) {
            if (sum + candidates[i] > target){
                break;
            }
            path.add(candidates[i]);
            dfs(candidates,target,i,sum + candidates[i]);
            path.remove(path.size() - 1);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.reverse();
        }
    }
}
class Solution39_2 {
    List<List<Integer>> result;
    List<Integer> path;
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        result = new ArrayList<>();
        path = new ArrayList<>();
        Arrays.sort(candidates);
        dfs(candidates,target,0,0);
        return result;
    }

    private void dfs(int[] candidates, int target, int pos, int sum) {
        if (candidates.length <= pos){//递归出口
            return;
        }
        int count = 0;
        for (int k = 0; k * candidates[pos] + sum < target; k++) {
            dfs(candidates,target,pos+1,sum + k * candidates[pos]);
        }
        //恢复现场
        for (int k = 1; k * candidates[pos] + sum < target; k++) {
            if (k != 0){
                path.add(candidates[pos]);
            }
            path.remove(path.size() - 1);
        }
    }
}
class Solution784 {
    List<String> result;
    StringBuilder path;
    public List<String> letterCasePermutation(String ss) {
        path = new StringBuilder();
        result = new ArrayList<>();
        char[] s = ss.toCharArray();
        int pos = -1;
        for (int i = 0; i < s.length; i++) {
            if ((s[i] >= 'a' && s[i] <= 'z') || (s[i] >= 'A' && s[i] <= 'Z')){
                pos = i;
                break;
            }
        }
        dfs(s,pos);
        return result;
    }

    private void dfs(char[] s, int pos) {
        if (pos == s.length){//递归出口
            result.add(path.toString());
            return;
        }
        //不改变
        path.append(s[pos]);
        dfs(s,pos + 1);
        path.deleteCharAt(path.length() - 1);//恢复现场
        //改变
        if (s[pos] <'0' || s[pos] > '9') {
            path.append(change(s[pos]));
            dfs(s,pos + 1);
            path.deleteCharAt(path.length() - 1);//恢复现场
        }
    }

    private char change(char ch) {
        if (ch >= 'a' && ch <= 'z'){
            return (char) (ch - 32);
        }
        return (char) (ch + 32);
    }

}
class Solution526 {
    boolean[] check;
    int result;
    public int countArrangement(int n) {
        check = new boolean[n + 1];
        int pos = 1;
        dfs(n,pos);
        return result;
    }

    private void dfs(int n, int pos) {
        if (pos == n + 1){//终止条件，当要放的位置为n + 1。说明之前的位置已经放满了。
            result++;
            return;
        }
        for (int i = 1; i <= n; i++) {
            if (check[i] == false  && (pos % i == 0 ||i % pos == 0)){
                check[i] = true;
                dfs(n,pos + 1);
                check[i] = false;
            }
        }
    }
}
class Solution526_2 {
    boolean[] check;
    int result;
    public int countArrangement(int n) {
        check = new boolean[n + 1];
        int pos = 1;
        dfs(n,pos);

        return result;
    }

    private void dfs(int n, int pos) {
        if (pos == n + 1){//拥有正确结果
            result++;
            return;
        }
        for (int i = 1; i <= n; i++) {
            if (check[i] == false && (pos % i == 0 || i % pos == 0)){//满足完美数字的要求。
                check[i] = true;
                dfs(n,pos + 1);
                check[i] = false;
            }
        }
    }
}
class Node {
    public int val;
    public List<Node> children;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, List<Node> _children) {
        val = _val;
        children = _children;
    }
};
class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }

}

