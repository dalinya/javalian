package common;

import common.TestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import testDemo.InitAndEnd;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class LoginAuxTest {
    static WebDriver webDriver = null;
    @BeforeAll
    static void init(){
         webDriver = TestUtils.chromeDriver();
        //打开博客登录页
        webDriver.get("http://82.157.236.116:8890/login.html");
    }
    @ParameterizedTest
    @CsvSource({"adminlisi,123456,http://82.157.236.116:8890/myblog_list.html"})
    void LoginSuccessTest(String username,String password,String blog_list_url) throws InterruptedException, IOException {
        // 在每次登录之后都要进行清空，然后才能重新输入
        webDriver.findElement(By.cssSelector("#username")).clear();
        webDriver.findElement(By.cssSelector("#password")).clear();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        //输入账号
        webDriver.findElement(By.cssSelector("#username")).sendKeys(username);
        //输入密码
        webDriver.findElement(By.cssSelector("#password")).sendKeys(password);
        //点击提交按钮
        webDriver.findElement(By.cssSelector("#submit")).click();
        //登录成功后进行检查
        sleep(1000);
        //跳转到列表页
        //获取当前页面的url,
        String cur_url = webDriver.getCurrentUrl();
        //如果url=http://82.157.236.116:8890/myblog_list.html测试通过，反之不通过
        Assertions.assertEquals(blog_list_url,cur_url);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        //找到个人信息
        String user_text = webDriver.findElement(By.cssSelector("#username")).getText();
        //进行检查
        Assertions.assertEquals(username,user_text);
        sleep(1000);
    }
}
