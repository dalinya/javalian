package common;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class TestUtils {
    public static volatile ChromeDriver driver = null;//避免出现内存可见性问题
    //创建启动对象
    public static ChromeDriver chromeDriver(){
        if (driver == null){//判断对象是否需要被加锁
            synchronized (TestUtils.class){
                if(driver == null){//判断是否要被实例化
                    driver = new ChromeDriver();
                    //隐式等待，确保页面渲染出来
                    driver.manage().timeouts().implicitlyWait(3, TimeUnit.MINUTES);
                }
            }
        }
        //判断启动对象是否被创建
        return driver;
    }

    // 为了区分是哪个用例返回的，可以加上用例的名称进行保存,每个截图的路径都不同
    public static void getScreenShot(String str) throws IOException {
        // 文件保存路径：dirName+fileName
        // ./指的是当前路径，这里来说就是autoBlogTest目录下
        // 注意图片保存的路径以及名称！
        SimpleDateFormat sim = new SimpleDateFormat("yyyyMMdd-HHmmssSSS");
        String fileName = "./tmp/screen/"  +  sim.format(System.currentTimeMillis()) +"/" +  UUID.randomUUID().toString() + str   + ".png"; // 保存的路径+名称
        File scrFile = driver.getScreenshotAs(OutputType.FILE); // 获取到的屏幕截图
        // 把屏幕截图生成的文件放到指定路径下
        FileUtils.copyFile(scrFile,new File(fileName));
    }
}
