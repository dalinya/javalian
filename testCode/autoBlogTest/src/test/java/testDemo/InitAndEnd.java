package testDemo;

import common.TestUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class InitAndEnd {
    protected static WebDriver webDriver;
    @BeforeAll
    static void SetUp(){
        webDriver = TestUtils.chromeDriver();
    }
    @AfterAll
    static void TearDown(){
        webDriver.quit();
    }
}