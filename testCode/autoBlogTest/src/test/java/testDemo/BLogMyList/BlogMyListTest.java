package testDemo.BLogMyList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import testDemo.InitAndEnd;

import java.util.concurrent.TimeUnit;

public class BlogMyListTest extends InitAndEnd {
    @BeforeAll
    static void init(){
        //打开博客登录页
        webDriver.get("http://82.157.236.116:8890/myblog_list.html");
    }
    @Test
    void blogListTest(){
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        //获取个人列表页中的数据
        String numStr =  webDriver.findElement(By.xpath("//*[@id=\"artCount\"]")).getText();
        //获取当前列表页中的数据
        int num =  webDriver.findElements(By.cssSelector("div.blog")).size();
        Assertions.assertEquals(num,Integer.valueOf(num));
    }
}
