package testDemo.BLogMyList;

import common.LoginAuxTest;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

//测试套件一次执行多个测试类,测试在登录情况下的博客主页
@Suite
@SelectClasses({LoginAuxTest.class, BlogMyListTest.class})
public class runSuiteLogin {
}
