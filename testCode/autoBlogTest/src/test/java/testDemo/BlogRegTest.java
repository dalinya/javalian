package testDemo;


import common.TestUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;

import java.io.IOException;

import static java.lang.Thread.sleep;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BlogRegTest extends InitAndEnd {
    @Test      // @Test说明方法 是测试方法，执行当前这个类时，会自动的执行该类下的所有带@Test注解的用例
    @BeforeAll // 带有BeforeAll注解的方法会在当前类下的所有测试用例之前（方法）执行一次，注意只是执行一次
    public static void init() {
        // 既然是对注册界面的测试，自然要先跳转到该界面
        webDriver.get("http://82.157.236.116:8890/reg.html");
    }


    //注册操作，注册已经有的账号会注册失败
    @Order(1)
    @ParameterizedTest
    @CsvFileSource(resources = "../RegFailTest.csv")
    void RegFailTest(String username, String password, String password2) throws InterruptedException, IOException {
        // 每次输入信息前， 先要清除输入框的原有内容
        webDriver.findElement(By.cssSelector("#username")).clear();
        webDriver.findElement(By.cssSelector("#password")).clear();
        webDriver.findElement(By.cssSelector("#password2")).clear();
        // 往输入框中输入数据
        webDriver.findElement(By.cssSelector("#username")).sendKeys(username);
        webDriver.findElement(By.cssSelector("#password")).sendKeys(password);
        webDriver.findElement(By.cssSelector("#password2")).sendKeys(password2);
        webDriver.findElement(By.cssSelector("#submit")).click();
        sleep(1000);
        String alter_info = webDriver.switchTo().alert().getText();//获取弹窗信息
        if (username.equals("")) {
            Assertions.assertEquals("请先输入用户名！", alter_info);
        } else if (password.equals("")) {
            Assertions.assertEquals("请先输入密码！", alter_info);
        } else if (password2.equals("")) {
            Assertions.assertEquals("请先输入确认密码！", alter_info);
        } else if (!password.equals(password2)) {
            Assertions.assertEquals("两次密码输入的不一致，请先检查！", alter_info);
        } else {
            Assertions.assertEquals("用户名已存在请重新输入用户名", alter_info);
        }
        //alert弹窗的取消
        webDriver.switchTo().alert().dismiss();
    }

    //注册没有的账号注册成功
    @Order(2)
    @ParameterizedTest
    @CsvSource({"adminlili,123456,123456,http://82.157.236.116:8890/login.html"})
    void RegSuccessTest(String username, String password, String password2,String login_url) throws InterruptedException, IOException {
        // 每次输入信息前， 先要清除输入框的原有内容
        webDriver.findElement(By.cssSelector("#username")).clear();
        webDriver.findElement(By.cssSelector("#password")).clear();
        webDriver.findElement(By.cssSelector("#password2")).clear();
        // 往输入框中输入数据
        webDriver.findElement(By.cssSelector("#username")).sendKeys(username);
        webDriver.findElement(By.cssSelector("#password")).sendKeys(password);
        webDriver.findElement(By.cssSelector("#password2")).sendKeys(password2);
        webDriver.findElement(By.cssSelector("#submit")).click();
        sleep(1000);
        webDriver.switchTo().alert().accept();//点击弹窗确认按钮
        sleep(1000);
        //跳转到博客登录页
        //获取当前页面的url,
        String cur_url = webDriver.getCurrentUrl();
        //如果url=http://82.157.236.116:8890/login.html测试通过，反之不通过
        Assertions.assertEquals(login_url,cur_url);
        //进行截图
        TestUtils.getScreenShot("RegSuccessTest");
    }
}
