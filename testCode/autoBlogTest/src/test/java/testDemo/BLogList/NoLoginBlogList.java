package testDemo.BLogList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import testDemo.InitAndEnd;

import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class NoLoginBlogList extends InitAndEnd {
    @BeforeAll
    static void init(){
        //打开博客登录页
        webDriver.get("http://82.157.236.116:8890/blog_list.html");
    }
    @Test
    void blogListTest() throws InterruptedException {
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S

        int num =  webDriver.findElements(By.cssSelector("div.blog")).size();//博客数量
        Assertions.assertNotEquals(num,0);
        //点击上一页提示  /html/body/div[2]/div/div[2]/button[1]
        //点击上一页
        webDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/button[1]")).click();
        String alter_info = webDriver.switchTo().alert().getText();//获取弹窗信息
        Assertions.assertEquals(alter_info,"当前已经在首页了");
        webDriver.switchTo().alert().dismiss();//取消弹窗
        //点击个人列表页是否会跳转到主页 body > div.nav > a:nth-child(4)
        webDriver.findElement(By.cssSelector("body > div.nav > a:nth-child(4)")).click();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        // sleep(500);
        String cur_url = webDriver.getCurrentUrl();//获取当前页面的url
        Assertions.assertEquals(cur_url,"http://82.157.236.116:8890/login.html");
    }

}
