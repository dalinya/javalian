package testDemo.BLogList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import testDemo.InitAndEnd;

import java.util.concurrent.TimeUnit;

public class LoginBlogList extends InitAndEnd {
    @BeforeAll
    static void init(){
        //打开博客主页
        webDriver.get("http://82.157.236.116:8890/blog_list.html");
    }
    @Test
    void blogListTest() throws InterruptedException {
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S


        //点击个人列表页是否会成功跳转到个人博客列表页
        webDriver.findElement(By.cssSelector("body > div.nav > a:nth-child(4)")).click();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        // sleep(500);
        String cur_url = webDriver.getCurrentUrl();//获取当前页面的url
        Assertions.assertEquals(cur_url,"http://82.157.236.116:8890/myblog_list.html");
        webDriver.navigate().back();//回退到博客主页
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        //点击查看全文页，是不是会跳转到文章详情页
        webDriver.findElement(By.cssSelector("#artListDiv > div:nth-child(1) > a")).click();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        cur_url = webDriver.getCurrentUrl();//获取当前页面的url
        Assertions.assertEquals(cur_url.substring(0,cur_url.indexOf('?')),"http://82.157.236.116:8890/blog_content.html");
    }
}
