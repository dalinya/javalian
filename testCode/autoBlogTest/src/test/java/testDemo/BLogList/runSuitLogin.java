package testDemo.BLogList;
import common.LoginAuxTest;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;
import testDemo.BLogMyList.BlogMyListTest;

//测试套件一次执行多个测试类
@Suite
@SelectClasses({LoginAuxTest.class, LoginBlogList.class})
public class runSuitLogin {
}
