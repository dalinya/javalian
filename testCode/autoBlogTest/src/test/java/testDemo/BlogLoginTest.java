package testDemo;

import common.TestUtils;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BlogLoginTest extends InitAndEnd{
    @BeforeAll
    static void init(){
        //打开博客登录页
        webDriver.get("http://82.157.236.116:8890/login.html");
    }
    //成功登录登录的情况
    @Order(1)
    @ParameterizedTest
    @CsvFileSource(resources = "../LoginSuccessTest.csv")
    void LoginSuccessTest(String username,String password,String blog_list_url) throws InterruptedException, IOException {
        // 在每次登录之后都要进行清空，然后才能重新输入
        webDriver.findElement(By.cssSelector("#username")).clear();
        webDriver.findElement(By.cssSelector("#password")).clear();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        //输入账号
        webDriver.findElement(By.cssSelector("#username")).sendKeys(username);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        //输入密码
        webDriver.findElement(By.cssSelector("#password")).sendKeys(password);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        //点击提交按钮
        webDriver.findElement(By.cssSelector("#submit")).click();
        //登录成功后进行检查
        sleep(1000);
        //跳转到列表页
        //获取当前页面的url,
        String cur_url = webDriver.getCurrentUrl();
        //如果url=http://82.157.236.116:8890/myblog_list.html测试通过，反之不通过
        Assertions.assertEquals(blog_list_url,cur_url);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        //找到个人信息
        String user_text = webDriver.findElement(By.cssSelector("#username")).getText();
        //进行检查
        Assertions.assertEquals(username,user_text);
        sleep(1000);
        //进行截图
        TestUtils.getScreenShot(username.substring(4));
        // 但是因为要多参数，所以在执行完一遍执行下一遍的时候需要进行页面的回退，否则根本找不到登录框
        webDriver.navigate().back();
    }
    //登录失败测试
    @Order(2)
    @ParameterizedTest
    @CsvSource({"adminzhangsan,1235", "adminwu,123456"})//前者账号错误，后者密码错误
    void LoginFailTest(String username,String password) throws InterruptedException {
        // 在每次登录之后都要进行清空，然后才能重新输入
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        webDriver.findElement(By.cssSelector("#username")).clear();
        webDriver.findElement(By.cssSelector("#password")).clear();
        //输入账号
        webDriver.findElement(By.cssSelector("#username")).sendKeys(username);
        //输入密码
        webDriver.findElement(By.cssSelector("#password")).sendKeys(password);
        //点击提交按钮
        webDriver.findElement(By.cssSelector("#submit")).click();
        sleep(1000);
        String alter_info = webDriver.switchTo().alert().getText();//获取弹窗信息
        Assertions.assertEquals("抱歉登录失败，用户名或密码输入错误，请重试！",alter_info);
        //alert弹窗的取消
        webDriver.switchTo().alert().dismiss();
    }
}
