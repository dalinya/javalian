package testDemo.blogAdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import testDemo.InitAndEnd;

import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class BLogAddTest extends InitAndEnd {
    @BeforeAll
    static void init(){
        //打开博客主页
        webDriver.get("http://82.157.236.116:8890/blog_add.html");
    }
    @Test
    void blogAddTest() throws InterruptedException {
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        String set_title = "自动化测试标题";
        webDriver.findElement(By.xpath("//*[@id='title']")).sendKeys(set_title);
        //点击发布按钮
        webDriver.findElement(By.xpath("/html/body/div[2]/div[1]/button")).click();
        sleep(1000);//强制等待
        webDriver.switchTo().alert().accept();//同意发布文章
        sleep(1000);//强制等待
        //取消同意继续发布文章
        webDriver.switchTo().alert().dismiss();
        //进行验证是否成功跳转到个人博客页页面中
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        // sleep(500);
        //进行发布成功的验证
        String cur_url = webDriver.getCurrentUrl();//获取当前页面的url
        Assertions.assertEquals(cur_url,"http://82.157.236.116:8890/myblog_list.html");
        // 最新文章的标题是否是刚刚发布文章的标题
        String cur_title = webDriver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div[1]")).getText();
        Assertions.assertEquals(cur_title,set_title);

    }
}
