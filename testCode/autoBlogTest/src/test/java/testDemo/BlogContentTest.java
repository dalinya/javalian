package testDemo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class BlogContentTest extends InitAndEnd{
    @BeforeAll
    static void init(){
        //打开博客主页
        webDriver.get("http://82.157.236.116:8890/blog_content.html?id=3");
    }
    @Test
    void blogAddTest() throws InterruptedException {
        // 创建一个ChromeOptions对象，用于配置Chrome浏览器选项
        ChromeOptions options = new ChromeOptions();

        // 添加无痕模式选项
        options.addArguments("--incognito");
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        //查看左侧个人信息是否显示成功
        //文章作者和文章数量不为null
        String username = webDriver.findElement(By.xpath("//*[@id=\"username\"]")).getText();
        String blogCount =  webDriver.findElement(By.cssSelector("#artCount")).getText();
        Assertions.assertNotEquals(username,"");
        Assertions.assertNotEquals(blogCount,"");
        //获取当前文章的阅读数
        int curReadCount = Integer.valueOf(webDriver.findElement(By.xpath("//*[@id=\"rcount\"]")).getText());
        //获取刷线后文章的阅读数量
        webDriver.navigate().refresh();
        sleep(1000);
        int refreshReadCount = Integer.valueOf(webDriver.findElement(By.xpath("//*[@id=\"rcount\"]")).getText());
        Assertions.assertEquals(curReadCount + 1,refreshReadCount);
    }
}
