package testDemo.blogedit;

import common.LoginAuxTest;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

//测试套件一次执行多个测试类
@Suite
@SelectClasses({LoginAuxTest.class, BLogEditTest.class})
public class runSuitLogin {
}