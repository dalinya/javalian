package testDemo.blogedit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import testDemo.InitAndEnd;

import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class BLogEditTest extends InitAndEnd {
    @BeforeAll
    static void init(){
        //打开博客编辑页
        webDriver.get("http://82.157.236.116:8890/blog_edit.html?id=11");
    }
    @Test
    void bLogEditTest() throws InterruptedException {
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        WebElement webElement = webDriver.findElement(By.xpath("//*[@id=\"title\"]"));//找到标题输入栏
        String cur_title  = webElement.getAttribute("value");//获取标题信息
        cur_title += "1";
        webElement.clear();//把之前的输入清除
        webElement.sendKeys(cur_title);//修改标题
        //点击发布按钮
        webDriver.findElement(By.xpath("/html/body/div[2]/div[1]/button")).click();
        sleep(1000);//强制等待
        webDriver.switchTo().alert().accept();//确认修改成功提示框
        //进行验证是否成功跳转到个人博客页页面中
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);//隐式等待3S
        //进行发布成功的验证
        String cur_url = webDriver.getCurrentUrl();//获取当前页面的url
        Assertions.assertEquals(cur_url,"http://82.157.236.116:8890/myblog_list.html");
        // 最新文章的标题是否是刚刚发布文章的标题
        String new_title = webDriver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div[1]")).getText();
        Assertions.assertEquals(cur_title,new_title);

    }
}
