package org.example.seleniumAPI;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import static java.lang.Thread.sleep;

public class Main6 {
    public static void main(String[] args) throws InterruptedException, IOException {
        //创建驱动
        WebDriver webDriver = new ChromeDriver();
        //打开网页
        webDriver.get("https://www.baidu.com/");
        //test1(webDriver);//关闭操作
        //test2(webDriver);//切换窗口
        test3(webDriver);//截图
    }

    private static void test3(WebDriver webDriver) throws InterruptedException, IOException {
        webDriver.findElement(By.cssSelector("#kw")).sendKeys("软件测试");
        webDriver.findElement(By.cssSelector("#su")).click();
        sleep(3000);
        File file = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("D://20231109jietu.png"));
    }

    private static void test2(WebDriver webDriver) throws InterruptedException {
        webDriver.findElement(By.cssSelector("#s-top-left > a:nth-child(1)")).click();
        //getWindowHandles()获取所有的窗口句柄
        //getWindowHandle()获取的get打开的页面窗口句柄
        Set<String> handles = webDriver.getWindowHandles();
        String target_handle = "";//找到最后一个页面
        for (String handle:handles) {
            target_handle = handle;
        }
        sleep(4000);
        webDriver.switchTo().window(target_handle);
        webDriver.findElement(By.cssSelector("#ww")).sendKeys("新闻联播");
        webDriver.findElement(By.xpath("//*[@id=\"s_btn_wr\"]")).click();
    }

    private static void test1(WebDriver webDriver ) throws InterruptedException {
        webDriver.findElement(By.cssSelector("#s-top-left > a:nth-child(1)")).click();
        sleep(4000);
        //webDriver.quit();//退出浏览器
        webDriver.close();//关闭改网页
    }

}
