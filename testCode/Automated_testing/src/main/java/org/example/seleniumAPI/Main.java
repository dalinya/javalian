package org.example.seleniumAPI;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Thread.sleep;
//元素定位
public class Main {
    public static void main(String[] args) throws InterruptedException {
        ChromeOptions options = new ChromeOptions();
        WebDriver webDriver = new ChromeDriver(options);
        //打开百度首页
        webDriver.get("https://www.baidu.com/");
        //找到百度搜索输入框
        WebElement element = webDriver.findElement(By.cssSelector(".s_ipt"));
        //在输入框中输入软件测试
        element.sendKeys("软件测试");
        //找到百度一下按钮并且点击
        webDriver.findElement(By.xpath("//*[@id=\"su\"]")).click();
        sleep(3000);//需要进行等待来加载出所有的页面元素

        //校验我们的测试结果 验证所有的a em标签下的文字是否等于软件测试
        List<WebElement> elements = webDriver.findElements(By.cssSelector("a em"));
        int flg = 0;
        for (int i = 0; i < elements.size(); i++) {
            //System.out.println(elements.get(i).getText());//获取文本并打印
            //如果返回的结果又软件测试，则证明测试通过，否则测试不通过
            if(elements.get(i).getText().contains("软件测试")){
                System.out.println("测试通过");
                flg = 1;
                break;
            }
        }
        if (flg == 0){
            System.out.println("测试不通过");
        }
    }

}