package org.example.seleniumAPI;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

//添加等待和打印信息
public class Main3 {
    public static void main(String[] args) {
        //创建驱动
        WebDriver webDriver = new ChromeDriver();
        //打开百度首页
        webDriver.get("https://www.baidu.com/");
        //test1(webDriver);//隐式等待
        //test2(webDriver);//显示等待
        test3(webDriver);//获取页面信息
    }

    private static void test3(WebDriver webDriver) {
        String url = webDriver.getCurrentUrl();//获取url
        String title = webDriver.getTitle();//获取网页标题
        if (url.equals("https://www.baidu.com/") && title.equals("百度一下，你就知道")){
            System.out.println("当前的页面URL：" + url + "，当前页面title：" + title);
            System.out.println("测试通过");
        }else {
            System.out.println("当前的页面URL：" + url + "，当前页面title：" + title);
            System.out.println("测试不通过");
        }
    }

    private static void test2(WebDriver webDriver) {
        //显示等待
        WebDriverWait wait = new WebDriverWait(webDriver,3000);
        //判定元素是否可以被点击
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#s-top-left > div > a")));
        //判定元素网页标题是不是:百度一下，你就知道
        wait.until(ExpectedConditions.titleIs("百度一下，你就知道"));
    }

    private static void test1(WebDriver webDriver) {
        //找到百度的搜索框
        WebElement webElement = webDriver.findElement(By.cssSelector("#kw"));
        //输入软件测试
        webElement.sendKeys("软件测试");
        //点击百度一下按钮
        webDriver.findElement(By.xpath("//*[@id=\"su\"]")).click();
        //隐式等待三天
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);
        //清空百度搜索输入框中的数据
        webDriver.findElement(By.cssSelector("#kw")).clear();
    }
}
