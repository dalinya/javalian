package org.example.seleniumAPI;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static java.lang.Thread.sleep;

//操作测试对象
public class Main2 {
    public static void main(String[] args) throws InterruptedException {
        ChromeOptions options = new ChromeOptions();
        WebDriver webDriver = new ChromeDriver(options);
        //打开百度首页
        webDriver.get("https://www.baidu.com/");
        //输入 点击 清除
        //test1(webDriver);
        //submit 提交 --> 只能点击from标签
        //test2(webDriver);
        //getAttribute：获取元素属性信息
        test3(webDriver);

    }

    private static void test3(WebDriver webDriver) {
        //找到搜索按钮，获取元素信息
        String su = webDriver.findElement(By.cssSelector("#su")).getAttribute("value");
        if (su.equals("百度一下")){
            System.out.println("测试通过");
        }else {
            System.out.println("测试不通过");
        }
        //getText获取标签中的文本内容
        System.out.println(webDriver.findElement(By.cssSelector("#hotsearch-content-wrapper > li:nth-child(1) > a > span.title-content-title")).getText());
    }

    private static void test2(WebDriver webDriver) {
/*        //找到百度搜索输入框
        WebElement element = webDriver.findElement(By.cssSelector("#kw"));
        //输入软件测试
        element.sendKeys("软件测试");//
        //找到百度一下按钮
        //点击百度一下按钮
        webDriver.findElement(By.cssSelector("#su")).submit();*/
        //点击新闻按钮会失败
        webDriver.findElement(By.xpath("//*[@id=\"s-top-left\"]/a[1]")).submit();
    }

    private static void test1(WebDriver webDriver) throws InterruptedException {
        //找到百度搜索输入框
        WebElement element = webDriver.findElement(By.cssSelector("#kw"));
        //输入软件测试
        element.sendKeys("软件测试");//
        //找到百度一下按钮
        //点击百度一下按钮
        webDriver.findElement(By.cssSelector("#su")).click();
        sleep(3000);//休息3秒
        //清空输入框中的数据
        webDriver.findElement(By.cssSelector("#kw")).clear();
    }
}
