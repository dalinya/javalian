package org.example.seleniumAPI;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

//定位元素
public class Main5 {
    public static void main(String[] args) throws InterruptedException {
        //创建驱动
        WebDriver webDriver = new ChromeDriver();
//        test1(webDriver);//全选复选框
        //test2(webDriver);//多层框架
        //test3(webDriver);//下拉框处理
        //alert、confirm、prompt 的处理
        //test4(webDriver);//弹窗处理
        //test5(webDriver);//上传文件
    }

    private static void test5(WebDriver webDriver) {
        webDriver.get("http://localhost:8080/test05.html");
        WebElement webElement = webDriver.findElement(By.cssSelector("input"));
        webDriver.findElement(By.xpath("/html/body/input")).sendKeys("D:\\20230911jietu.png");
    }

    private static void test4(WebDriver webDriver) throws InterruptedException {
        webDriver.get("http://localhost:8080/test04.html");
        WebElement webElement = webDriver.findElement(By.xpath("/html/body/button"));
        webElement.click();
        sleep(3000);
        //alert弹窗取消
        webDriver.switchTo().alert().dismiss();
        sleep(3000);
        //点击按钮
        webElement.click();
        //在弹窗中输入张三
        webDriver.switchTo().alert().sendKeys("张三");
        sleep(3000);
        //alter弹窗确认
        webDriver.switchTo().alert().accept();
    }

    private static void test3(WebDriver webDriver) throws InterruptedException {
        webDriver.get("http://localhost:8080/test03.html");
        WebElement webElement = webDriver.findElement(By.xpath("//*[@id=\"ShippingMethod\"]"));
        Select select = new Select(webElement);
        select.selectByIndex(2);//通过下标选择
        sleep(3000);
        select.selectByValue("12.51");//通过value进行选择
    }

    private static void test2(WebDriver webDriver) {
        webDriver.get("http://localhost:8080/test02.html");
        //定位的主体切换到frame中
        webDriver.switchTo().frame("f1");
        //点击click链接
        webDriver.findElement(By.xpath("/html/body/div[1]/div/a")).click();
    }

    private static void test1(WebDriver webDriver) {
        //打开网页
        webDriver.get("http://localhost:8080/test01.html");
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.DAYS);//隐式等待
        List<WebElement> webElements = webDriver.findElements(By.cssSelector("input"));
        //获取所有的input元素
        for (int i = 0; i < webElements.size(); i++) {
            //如果每个元素type值等于checkbox进行点击
            //getAttribute获取页面上元素属性值，里面的type是当前元素属性
            if (webElements.get(i).getAttribute("type").equals("checkbox")){
                webElements.get(i).click();
            }else {
                //什么也不操作
            }
        }
    }
}
