package org.example.seleniumAPI;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import static java.lang.Thread.sleep;

//浏览器的操作 和  鼠标键盘事件
public class Main4 {
    public static void main(String[] args) throws InterruptedException {
        //创建驱动
        WebDriver webDriver = new ChromeDriver();
        //打开百度首页
        webDriver.get("https://www.baidu.com/");
        //test1(webDriver);//浏览器操作
        //test2(webDriver);//键盘事件
        test3(webDriver);//鼠标事件
    }

    private static void test3(WebDriver webDriver) throws InterruptedException {
        //搜索立冬
        webDriver.findElement(By.xpath("//*[@id=\"kw\"]")).sendKeys("立冬");
        webDriver.findElement(By.cssSelector("#su")).click();//点击百度一下按钮
        sleep(3000);
        //找到图片按钮
        WebElement webElement = webDriver.findElement(By.cssSelector("#s_tab > div > a.s-tab-item.s-tab-item_1CwH-.s-tab-pic_p4Uej.s-tab-pic"));//鼠标右击
        Actions actions = new Actions(webDriver);
        sleep(3000);
        actions.moveToElement(webElement).contextClick().perform();
    }

    private static void test2(WebDriver webDriver) throws InterruptedException {
        //搜索立冬
        webDriver.findElement(By.xpath("//*[@id=\"kw\"]")).sendKeys("立冬");
        sleep(3000);
        //control + A
        webDriver.findElement(By.cssSelector("#kw")).sendKeys(Keys.CONTROL,"A");
        sleep(3000);
        //control + X
        webDriver.findElement(By.cssSelector("#kw")).sendKeys(Keys.CONTROL,"X");
        sleep(3000);
        //control + V
        webDriver.findElement(By.cssSelector("#kw")).sendKeys(Keys.CONTROL,"V");
        sleep(3000);
    }

    private static void test1(WebDriver webDriver) throws InterruptedException {
        webDriver.findElement(By.xpath("//*[@id=\"kw\"]")).sendKeys("立冬");
        //点击搜索
        webDriver.findElement(By.cssSelector("#su")).submit();
        sleep(3000);
        //浏览器后退
        webDriver.navigate().back();
        sleep(3000);
        //浏览器刷新
        webDriver.navigate().refresh();
        sleep(3000);
        //浏览器前进
        webDriver.navigate().forward();
        sleep(3000);
        //浏览器滑动到最下面
        ((JavascriptExecutor)webDriver).executeScript("document.documentElement.scrollTop=10000");
        sleep(3000);
        //浏览器最大化
        webDriver.manage().window().maximize();
        sleep(3000);
        //浏览器全屏
        webDriver.manage().window().fullscreen();
        sleep(3000);
        //设置浏览器的高度
        webDriver.manage().window().setSize(new Dimension(600,100));

    }
}
