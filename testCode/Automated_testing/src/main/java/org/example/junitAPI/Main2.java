package org.example.junitAPI;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.stream.Stream;

public class Main2 {
    @BeforeEach
    void BeforeEachTest() {
        System.out.println("这是BeforeEach里面的语句");
    }

    @AfterEach
    void AfterEachTest() {
        System.out.println("这是AfterEach里面的语句");
    }
    @ParameterizedTest
    @ValueSource(ints = {1,2,3})//类型的限制~
    void test4(int num){
        System.out.println(num);
    }
    @ParameterizedTest
    @ValueSource(strings = {"1","2","3"})//类型的限制~
    void test5(String num){
        System.out.println(num);
    }
    @ParameterizedTest
    @CsvSource({"1,2,3"})//类型的限制~
    void test6(String num1,String num2,String num3){
        System.out.println(num1);
        System.out.println(num2);
        System.out.println(num3);
    }
    @ParameterizedTest
    @CsvSource({"1,2,3,''"})//类型的限制~
    void test7(String num1,String num2,String num3,String q){
        System.out.println(num1);
        System.out.println(num2);
        System.out.println(num3);
        System.out.println(q + "csv");
    }
    public static Stream<Arguments> Generator(){
        return Stream.of(Arguments.arguments(1,"张三"),
                Arguments.arguments(2,"李四"),
                Arguments.arguments(3,"王五"));
    }
    @ParameterizedTest
    @MethodSource("Generator")
    void test8(int id,String name){
        System.out.println(id + ":" + name);
    }
}
