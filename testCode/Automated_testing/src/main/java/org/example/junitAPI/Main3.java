package org.example.junitAPI;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)//按顺序
@TestMethodOrder(MethodOrderer.Random.class)//随机顺序
public class Main3 {

    @Order(2)
    @Test
    void Test2(){
        System.out.println("这是Test2测试用例");
    }
    @Order(3)
    @Test
    void A(){
        System.out.println("这是TestA测试用例");
    }
    @Order(4)
    @Test
    void B(){
        System.out.println("这是TestB测试用例");
    }
    @Order(1)
    @Test
    void Test1(){
        System.out.println("这是Test1测试用例");
    }
}