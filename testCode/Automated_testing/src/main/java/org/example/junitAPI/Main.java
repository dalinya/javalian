package org.example.junitAPI;

import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

//基础注解
public class Main {
    @Test
    void Test1(){
        System.out.println("这是JunitTest中的test1");
    }
    @Test
    void Test2(){
        System.out.println("这是JunitTest中的test2");
    }
    @Disabled
    @Test
    void Test3(){
        //创建驱动
        WebDriver webDriver = new ChromeDriver();
        //打开网页
        webDriver.get("https://www.baidu.com/");
        webDriver.findElement(By.cssSelector("#s-top-left > a:nth-child(6)")).click();
    }
    @BeforeAll
    static void SetUp(){
        System.out.println("这是BeforeAll中的语句");
    }
    @AfterAll
    static void TearDown(){
        System.out.println("这是AfterAll中的语句");
    }
    @BeforeEach
    void BeforEachTest(){
        System.out.println("这是BeforeEach中的语句");
    }
    @AfterEach
    void AfterEachTest(){
        System.out.println("这是AfterEach中的语句");
    }
}
