package org.example.junitAPI;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;

@Suite
//@SelectClasses({Main.class,Main2.class, Main3.class})
@SelectPackages(value = {"org.example.junitAPI.Package1","org.example.junitAPI.Package2"})
public class RunSuit {

}
