import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

public class JunitTest {
    @BeforeEach
    void BeforeEachTest() {
        System.out.println("这是BeforeEach里面的语句");
    }

    @AfterEach
    void AfterEachTest() {
        System.out.println("这是AfterEach里面的语句");
    }
    @Disabled
    @ParameterizedTest
    @CsvFileSource(resources = "test01.csv")
     void test5(String name){
        System.out.println(name);
    }
    @ParameterizedTest
    @CsvFileSource(resources = "test02.csv")
    void test6(String id,String name){
        System.out.println("学号" + id + "姓名：" + name);
    }
}