class Solution {
    public boolean lemonadeChange(int[] bills) {
        int five = 0;
        int ten  = 0;
        for(int i = 0;i < bills.length;i++){
            if(bills[i] == 5){
                five++;
            }else if(bills[i] == 10){
                if(five >= 1){
                    five--;
                    ten++;
                }else{
                    return false;
                }
            }else{
                if(ten >= 1 && five >= 1){
                    ten--;
                    five--;
                }else if(five >= 3){
                    five -= 3;
                }else{
                    return false;
                }

            }
        }
        return true;
    }


    public static void main(String[] args) {
        Solution solution = new Solution();
        //testcase1
        int[] nums = {5,5,5,10,20};
        boolean result = solution.lemonadeChange(nums);
        if (result){
            System.out.println("testcase1 ok");
        }else {
            System.out.println("testcase1 failed");
        }
        //testcase2
        int[] nums2 = {5,5,10,10,20};
        boolean result2 = solution.lemonadeChange(nums2);
        if (result2 == false){
            System.out.println("testcase2 ok");
        }else {
            System.out.println("testcase2 failed");
        }
    }
}
