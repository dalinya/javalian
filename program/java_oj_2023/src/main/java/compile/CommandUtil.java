package compile;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CommandUtil {
    // 1. 通过 Runtime 类得到 Runtime 实例, 执行 exec 方法
    // 2. 获取到标准输出, 并写入到指定文件中.
    // 3. 获取到标准错误, 并写入到指定文件中.
    // 4. 等待子进程结束, 拿到子进程的状态码, 并返回.
    public static int run(String cmd,String stdoutFile,String stderrFile) throws IOException, InterruptedException {
        // 1. 通过 Runtime 类得到 Runtime 实例, 执行 exec 方法
        Process process = Runtime.getRuntime().exec(cmd);
        // 2. 获取到标准输出, 并写入到指定文件中.
        if (stdoutFile != null){
            try (InputStream stdoutFrom = process.getInputStream();
                 OutputStream stdoutTo = new FileOutputStream(stdoutFile)){
                while (true){
                    int ch = stdoutFrom.read();
                    if (ch == -1){
                        break;
                    }
                    stdoutTo.write(ch);
                }
            }
        }
        // 3. 获取到标准错误, 并写入到指定文件中.
        if (stderrFile != null){
            try (InputStream stderrFrom = process.getErrorStream();
                 OutputStream stderrTo = new FileOutputStream(stderrFile)){
                while (true){
                    int ch = stderrFrom.read();
                    if (ch == -1){
                        break;
                    }
                    stderrTo.write(ch);
                }
            }
        }
        // 4. 等待子进程结束, 拿到子进程的状态码, 并返回.
        int exitCode = process.waitFor();
        return exitCode;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        CommandUtil.run("javac","stdout.txt","stderr.txt");
    }
}
