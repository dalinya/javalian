package dao;

import common.DBUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

// 通过这个类封装了针对 Problem 的增删改查.
// 1. 新增题目
// 2. 删除题目
// 3. 查询题目列表
// 4. 查询题目详情
public class ProblemDAO{

    //新增题目
    public void insert(Problem problem){
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //建立数据库连接
            connection = DBUtil.getConnection();
            //构造sql语句
            String sql = "insert into oj_table values(null,?,?,?,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1,problem.getTitle());
            statement.setString(2,problem.getLevel());
            statement.setString(3,problem.getDescription());
            statement.setString(4,problem.getTemplateCode());
            statement.setString(5,problem.getTestCode());
            //执行sql
            int ret = statement.executeUpdate();
            if (ret == -1){
                System.out.println("新增题目失败");
            }else {
                System.out.println("新增题目");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
    //删除题目
    public void delete(int id){
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //建立数据库连接
            connection = DBUtil.getConnection();
            //构造sql语句
            String sql = "delete from oj_table where id = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,id);
            //执行sql
            int ret = statement.executeUpdate();
            if (ret == -1){
                System.out.println("删除题目失败！");
            }else {
                System.out.println("删除题目成功");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
    // 这个操作是把当前题目列表中的所有题都查出来了
    // 万一数据库中的题目特别多, 咋办? 只要实现 "分页查询" 即可. 后台实现分页查询, 非常容易.
    // 前端传过来一个当前的 "页码" , 根据页码算一下, 依据 sql limit offset 语句, 要算出来 offset 是 几
    // 但是前端这里实现一个分页器稍微麻烦一些(比后端要麻烦很多). 此处暂时不考虑分页功能.
    public List<Problem> selectAll(){
        List<Problem> problems = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //建立数据库连接
            connection = DBUtil.getConnection();
            //构造sql语句
            String sql = "select id,title,level from oj_table";
            statement = connection.prepareStatement(sql);
            //执行sql
            resultSet =  statement.executeQuery();
            //遍历resultSet
            while (resultSet.next()){
                //每一行都是一个Problem对象
                Problem problem = new Problem();
                problem.setId(resultSet.getInt("id"));
                problem.setTitle(resultSet.getString("title"));
                problem.setLevel(resultSet.getString("level"));
                problems.add(problem);
            }
            return problems;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }
    //查看题目详情
    public Problem selectOne(int id){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //建立数据库连接
            connection = DBUtil.getConnection();
            //构造sql语句
            String sql = "select * from oj_table where id = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,id);
            //执行sql
            resultSet =  statement.executeQuery();
            //遍历resultSet
            while (resultSet.next()){
                //每一行都是一个Problem对象
                Problem problem = new Problem();
                problem.setId(resultSet.getInt("id"));
                problem.setTitle(resultSet.getString("title"));
                problem.setLevel(resultSet.getString("level"));
                problem.setDescription(resultSet.getString("description"));
                problem.setTemplateCode(resultSet.getString("templateCode"));
                problem.setTestCode(resultSet.getString("testCode"));
                return problem;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,resultSet);
        }
        System.out.println("查询题目详情失败");
        return null;
    }

    private static void testInsert(){
        ProblemDAO problemDAO = new ProblemDAO();
        Problem problem = new Problem();
        problem.setTitle("两数之和");
        problem.setLevel("简单");
        problem.setDescription("给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target  的那 两个 整数，并返回它们的数组下标。\n" +
                "\n" +
                "你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。\n" +
                "\n" +
                "你可以按任意顺序返回答案。\n" +
                "\n" +
                " \n" +
                "\n" +
                "示例 1：\n" +
                "\n" +
                "输入：nums = [2,7,11,15], target = 9\n" +
                "输出：[0,1]\n" +
                "解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。\n" +
                "示例 2：\n" +
                "\n" +
                "输入：nums = [3,2,4], target = 6\n" +
                "输出：[1,2]\n" +
                "示例 3：\n" +
                "\n" +
                "输入：nums = [3,3], target = 6\n" +
                "输出：[0,1]\n" +
                " \n" +
                "\n" +
                "提示：\n" +
                "\n" +
                "2 <= nums.length <= 104\n" +
                "-109 <= nums[i] <= 109\n" +
                "-109 <= target <= 109\n" +
                "只会存在一个有效答案\n" +
                " \n" +
                "\n" +
                "进阶：你可以想出一个时间复杂度小于 O(n2) 的算法吗？");
        problem.setTemplateCode("class Solution {\n" +
                "    public int[] twoSum(int[] nums, int target) {\n" +
                "\n" +
                "    }\n" +
                "}");
        problem.setTestCode("   public static void main(String[] args) {\n" +
                "        Solution solution = new Solution();\n" +
                "        //testcase1\n" +
                "        int[] nums = {2,7,11,15};\n" +
                "        int target = 9;\n" +
                "        int[] result = solution.twoSum(nums,target);\n" +
                "        if (result.length == 2 && result[0] == 0 && result[1] == 1){\n" +
                "            System.out.println(\"testcase1 ok\");\n" +
                "        }else {\n" +
                "            System.out.println(\"testcase1 failed\");\n" +
                "        }\n" +
                "        //testcase2\n" +
                "        int[] nums2 = {3,2,4};\n" +
                "        int target2 = 6;\n" +
                "        int[] result2 = solution.twoSum(nums2,target2);\n" +
                "        if (result.length == 2 && result[0] == 1 && result[1] == 2){\n" +
                "            System.out.println(\"testcase2 ok\");\n" +
                "        }else {\n" +
                "            System.out.println(\"testcase2 failed\");\n" +
                "        }\n" +
                "    }");
        problemDAO.insert(problem);
        System.out.println("插入成功！");
    }
    private static void testInsert2(){
        ProblemDAO problemDAO = new ProblemDAO();
        Problem problem = new Problem();
        problem.setTitle("翻转对");
        problem.setLevel("困难");
        problem.setDescription("给定一个数组 nums ，如果 i < j 且 nums[i] > 2*nums[j] 我们就将 (i, j) 称作一个重要翻转对。\n" +
                "\n" +
                "你需要返回给定数组中的重要翻转对的数量。\n" +
                "\n" +
                "示例 1:\n" +
                "\n" +
                "输入: [1,3,2,3,1]\n" +
                "输出: 2\n" +
                "示例 2:\n" +
                "\n" +
                "输入: [2,4,3,5,1]\n" +
                "输出: 3\n" +
                "注意:\n" +
                "\n" +
                "给定数组的长度不会超过50000。\n" +
                "输入数组中的所有数字都在32位整数的表示范围内。");
        problem.setTemplateCode("class Solution {\n" +
                "    public int reversePairs(int[] nums) {\n" +
                "\n" +
                "    }\n" +
                "}");
        problem.setTestCode("   public static void main(String[] args) {\n" +
                "        Solution solution = new Solution();\n" +
                "        //testcase1\n" +
                "        int[] nums = {1,3,2,3,1};\n" +
                "        int result = solution.reversePairs(nums);\n" +
                "        if (result == 2){\n" +
                "            System.out.println(\"testcase1 ok\");\n" +
                "        }else {\n" +
                "            System.out.println(\"testcase1 failed\");\n" +
                "        }\n" +
                "        //testcase2\n" +
                "        int[] nums2 = {2,4,3,5,1};\n" +
                "        int result2 = solution.reversePairs(nums2);\n" +
                "        if (result2 == 3){\n" +
                "            System.out.println(\"testcase2 ok\");\n" +
                "        }else {\n" +
                "            System.out.println(\"testcase2 failed\");\n" +
                "        }\n" +
                "    }");
        problemDAO.insert(problem);
        System.out.println("插入成功！");
    }
    private static void testInsert3(){
        ProblemDAO problemDAO = new ProblemDAO();
        Problem problem = new Problem();
        problem.setTitle("柠檬水找零");
        problem.setLevel("简单");
        problem.setDescription("在柠檬水摊上，每一杯柠檬水的售价为 5 美元。顾客排队购买你的产品，（按账单 bills 支付的顺序）一次购买一杯。\n" +
                "\n" +
                "每位顾客只买一杯柠檬水，然后向你付 5 美元、10 美元或 20 美元。你必须给每个顾客正确找零，也就是说净交易是每位顾客向你支付 5 美元。\n" +
                "\n" +
                "注意，一开始你手头没有任何零钱。\n" +
                "\n" +
                "给你一个整数数组 bills ，其中 bills[i] 是第 i 位顾客付的账。如果你能给每位顾客正确找零，返回 true ，否则返回 false 。\n" +
                "\n" +
                " \n" +
                "\n" +
                "示例 1：\n" +
                "\n" +
                "输入：bills = [5,5,5,10,20]\n" +
                "输出：true\n" +
                "解释：\n" +
                "前 3 位顾客那里，我们按顺序收取 3 张 5 美元的钞票。\n" +
                "第 4 位顾客那里，我们收取一张 10 美元的钞票，并返还 5 美元。\n" +
                "第 5 位顾客那里，我们找还一张 10 美元的钞票和一张 5 美元的钞票。\n" +
                "由于所有客户都得到了正确的找零，所以我们输出 true。\n" +
                "示例 2：\n" +
                "\n" +
                "输入：bills = [5,5,10,10,20]\n" +
                "输出：false\n" +
                "解释：\n" +
                "前 2 位顾客那里，我们按顺序收取 2 张 5 美元的钞票。\n" +
                "对于接下来的 2 位顾客，我们收取一张 10 美元的钞票，然后返还 5 美元。\n" +
                "对于最后一位顾客，我们无法退回 15 美元，因为我们现在只有两张 10 美元的钞票。\n" +
                "由于不是每位顾客都得到了正确的找零，所以答案是 false。\n" +
                " \n" +
                "\n" +
                "提示：\n" +
                "\n" +
                "1 <= bills.length <= 105\n" +
                "bills[i] 不是 5 就是 10 或是 20 ");
        problem.setTemplateCode("class Solution {\n" +
                "    public boolean lemonadeChange(int[] bills) {\n" +
                "\n" +
                "    }\n" +
                "}");
        problem.setTestCode("       public static void main(String[] args) {\n" +
                "        Solution solution = new Solution();\n" +
                "        //testcase1\n" +
                "        int[] nums = {5,5,5,10,20};\n" +
                "        boolean result = solution.lemonadeChange(nums);\n" +
                "        if (result){\n" +
                "            System.out.println(\"testcase1 ok\");\n" +
                "        }else {\n" +
                "            System.out.println(\"testcase1 failed\");\n" +
                "        }\n" +
                "        //testcase2\n" +
                "        int[] nums2 = {5,5,10,10,20};\n" +
                "        boolean result2 = solution.lemonadeChange(nums2);\n" +
                "        if (result2 == false){\n" +
                "            System.out.println(\"testcase2 ok\");\n" +
                "        }else {\n" +
                "            System.out.println(\"testcase2 failed\");\n" +
                "        }\n" +
                "    }");
        problemDAO.insert(problem);
        System.out.println("插入成功！");
    }
    private static void testSelectAll(){
        ProblemDAO problemDAO = new ProblemDAO();
        List<Problem> problems = problemDAO.selectAll();
        System.out.println(problems);
    }
    private static void testSelectOne(){
        ProblemDAO problemDAO = new ProblemDAO();
        Problem problem = problemDAO.selectOne(1);
        System.out.println(problem);
    }
    private static void testDelete(){
        ProblemDAO problemDAO = new ProblemDAO();
        problemDAO.delete(1);
    }
    public static void main(String[] args) {
        //testInsert2();
        //testInsert3();
        //testSelectAll();
        //testSelectOne();
        //testDelete();

    }
}
