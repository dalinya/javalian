package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import common.CodeInValidException;
import common.ProblemNotFoundException;
import compile.Answer;
import compile.Question;
import compile.Task;
import dao.Problem;
import dao.ProblemDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.PortUnreachableException;

@WebServlet("/compile")
public class CompileServlet extends HttpServlet {
    static class CompileRequest{
        public int id;
        public String code;
    }
    static class CompileResponse{
        //0 表示编译运行 ok,1 表示编译出错,2 表示运行出错(抛异常)，3 表示其他错误
        public int error;
        //出错的详细原因
        public String reason;
        //测试用例的输出情况，包含了通过几个用例这样的信息
        public String stdout;
    }
    ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //获取到smart tomcat的工作目录
        System.out.println("用户的当前工作目录: "+System.getProperty("user.dir"));

        CompileRequest compileRequest = null;
        CompileResponse compileResponse = new CompileResponse();
        try {
            resp.setStatus(200);
            resp.setContentType("application/json;charset=utf8");
            //1. 先读取请求的正文. 先按照 JSON 格式进行解析
            String body = readBody(req);
            compileRequest = objectMapper.readValue(body, CompileRequest.class);
            //2.根据 id 从数据库中查找到题目的详情 => 得到测试用例代码
            ProblemDAO problemDAO = new ProblemDAO();
            Problem problem = problemDAO.selectOne(compileRequest.id);
            if (problem == null){
                //为了同意处理错误，在这个地方抛出异常
                throw new ProblemNotFoundException();
            }
            // 3. 把用户提交的代码和测试用例代码, 给拼接成一个完整的代码.
            //testCode是测试用例代码
            String testCode = problem.getTestCode();
            //finalCode是用户提交代码
            String requestCode =  compileRequest.code;
            //finalCode是最终代码
            String finalCode = mergeCode(testCode,requestCode);
            if (finalCode == null){
                //code不合法异常
                throw new CodeInValidException();
            }
            //System.out.println(finalCode);
            // 4. 创建一个 Task 实例, 调用里面的 compileAndRun 来进行编译运行.
            Task task = new Task();
            Question question = new Question();
            question.setCode(finalCode);
            Answer answer = task.compileAndRun(question);

            // 5. 根据 Task 运行的结果, 包装成一个 HTTP 响应

            compileResponse = new CompileResponse();
            compileResponse.error = answer.getError();
            compileResponse.reason = answer.getReason();
            compileResponse.stdout = answer.getStdout();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ProblemNotFoundException e) {
            //处理题目未找到异常
            compileResponse.error = 3;
            compileResponse.reason = "没有找打指定题目！id = " + compileRequest.id;
        } catch (CodeInValidException e) {
            //处理code不合法异常
            compileResponse.error = 3;
            compileResponse.reason = "提交的代码不符合要求！";
        }finally {
            String respString = objectMapper.writeValueAsString(compileResponse);
            resp.getWriter().write(respString);
        }
    }

    private String mergeCode(String testCode, String requestCode) {
        //1.查找requestCode中的最后一个}
        int pos = requestCode.lastIndexOf("}");
        if (pos == -1){
            //说明这个代码没有},是个非法代码
            return null;
        }
        //2.根据这个位置进行字符串截取
        String subStr = requestCode.substring(0,pos);
        //3.进行拼接
        return subStr + testCode + "\n}";
    }

    private String readBody(HttpServletRequest req) throws UnsupportedEncodingException {
        //1.获取body的长度
        int contentLength = req.getContentLength();
        //2.准备一个byte[]
        byte[] buffer = new byte[contentLength];
        //3.获取body中的流对象
        try (InputStream inputStream = req.getInputStream()){
            //4.将流对象的内容放到byte[]中
            inputStream.read(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //将byte[]中的内容构造成字符串,从二进制变成文本文件，我们应该注意的一个问题是：几个字节算一个字符集
        //为了解决这个问题我们指定UTF8为指定字符集
        return new String(buffer,"UTF8");
    }
}
