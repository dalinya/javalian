package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import dao.Problem;
import dao.ProblemDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/problem")
public class ProblemServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setStatus(200);
        resp.setContentType("application/json;charset=utf8");
        ProblemDAO problemDAO = new ProblemDAO();
        //获取id字段，如果能获取到->题目详情页。不能获取到->题目列表页
        String idString = req.getParameter("id");
        if (idString == null || idString.equals("")){
            //没有获取到id字段，查询题目列表
            List<Problem> problems = problemDAO.selectAll();
            String respString = objectMapper.writeValueAsString(problems);
            resp.getWriter().write(respString);
        }else {
            //题目列表页
            Problem problem = problemDAO.selectOne(Integer.valueOf(idString));
            String respString  = objectMapper.writeValueAsString(problem);
            resp.getWriter().write(respString);
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setStatus(200);
        resp.setContentType("application/json;charset=utf8");
        ProblemDAO problemDAO = new ProblemDAO();
        Problem problem = objectMapper.readValue(req.getInputStream(),Problem.class);
        System.out.println(problem);
        problemDAO.insert(problem);
        System.out.println("插入成功！");
        String respString = objectMapper.writeValueAsString(problem);
        resp.getWriter().write(respString);
    }
}
