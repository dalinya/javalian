create database  if not exists oj_database charset utf8;
use oj_database;
drop table if exists oj_table;
create table oj_table(
    id int primary key auto_increment,
    title varchar(50),
    level varchar(50),
    description varchar(4098), -- 题干
    templateCode varchar(4098), -- 代码模板
    testCode text -- 测试用例
);
insert  into oj_table values(
null,
"你好",
"中等",
"哈哈哈",
"public class Solution {\n                                                                 public static void main(String[] args) {\n             System.out.println(\"hello world\");\n}\n}",
"public class Solution {\n                                                                 public static void main(String[] args) {\n             System.out.println(\"hello world\");\n}\n}"
                                                             );