package com.example.mq.mqserver.datacenter;

import com.example.mq.common.BinaryTool;
import com.example.mq.common.MqException;
import com.example.mq.mqserver.core.MSGQueue;
import com.example.mq.mqserver.core.Message;

import java.io.*;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * 对硬盘中的消息进行管理
 */
public class MessageFileManager {
    /**
     * 使用静态内部类来表示该队列的统计信息 --> 消息总数，有效消息数量
     * 只有两个属性，直接调用即可。
     */
    public static class Stat{
        public Integer totalCount;//消息总数
        public Integer validCount;//有效消息数量
    }
    public void init() {
        // 暂时不需要做啥额外的初始化工作, 以备后续扩展
    }
    //用来获取到指定队列对应的消息文件所在路径
    private String getQueueDir(String queueName){
        return  "./data/" + queueName;
    }
    //这个方法用来获取该队列的消息数据文件路径
    // 注意, 二进制文件, 使用 txt 作为后缀, 不太合适. txt 一般表示文本. 此处咱们也就不改.
    // .bin / .dat
    private String getQueueDataPath(String queueName){
        return getQueueDir(queueName) + "/queue_data.txt";
    }
    //这个方法用来获取该消息队列的消息统计文件路径
    private String getQueueStatPath(String queueName){
        return getQueueDir(queueName) + "/queue_stat.txt";
    }

    private Stat readStat(String queueName){
        // 由于当前的消息统计文件是文本文件, 可以直接使用 Scanner 来读取文件内容
        Stat stat = new Stat();
        try (InputStream inputStream = new FileInputStream(getQueueStatPath(queueName))){
            Scanner scanner = new Scanner(inputStream);
            stat.totalCount = scanner.nextInt();
            stat.validCount = scanner.nextInt();
            return stat;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    private void writStat(String queueName,Stat stat){
        // 使用 PrintWrite 来写文件.
        // OutputStream 打开文件, 默认情况下, 会直接把原文件清空. 此时相当于新的数据覆盖了旧的.
        try (OutputStream outputStream = new FileOutputStream(getQueueStatPath(queueName))){
            PrintWriter printWriter = new PrintWriter(outputStream);
            printWriter.write(stat.totalCount + "\t" + stat.validCount);
            printWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //创建文件对应的文件和目录
    public void createQueueFiles(String queueName) throws IOException {
        //1.创建消息队列对应的目录
        File baseDir = new File(getQueueDir(queueName));
        if (!baseDir.exists()){
            boolean ok = baseDir.mkdirs();
            if (!ok){
                throw new IOException("创建文件失败 ， baseDir:" + baseDir.getAbsolutePath());
            }
        }
        //2.创建消息队列数据文件
        File queueDataFile = new File(getQueueDataPath(queueName));
        if (!queueDataFile.exists()){
            boolean ok = queueDataFile.createNewFile();
            if (!ok){
                throw new IOException("创建文件失败，queueDataFile：" + queueDataFile.getAbsolutePath());
            }
        }
        //3.创建消息队列统计文件
        File queueStatFile = new File(getQueueStatPath(queueName));
        if (!queueStatFile.exists()){
            boolean ok = queueStatFile.createNewFile();
            if (!ok){
                throw new IOException("创建文件失败，queueStatFile：" + queueStatFile.getAbsolutePath());
            }
        }
        //4.初始化统计文件
        Stat stat = new Stat();
        stat.validCount = 0;
        stat.totalCount = 0;
        writStat(queueName,stat);
    }
    //删除队列对应文件和目录
    public void destroyQueueFiles(String queueName) throws IOException {
        //先删除文件再删除目录
        File queueDataFile = new File(getQueueDataPath(queueName));
        boolean ok1 = queueDataFile.delete();
        File queueStatFile = new File(getQueueStatPath(queueName));
        boolean ok2 = queueStatFile.delete();
        File baseDir = new File(getQueueDir(queueName));
        boolean ok3 = baseDir.delete();
        if (!ok1 || !ok2 || !ok3){
            throw new IOException("删除队列文件和目录失败！ baseDir:" + baseDir.getAbsolutePath());
        }
    }
    // 检查消息队列中目录和文件是否存在
    // 比如后续有生产者给 broker server 生产消息了, 这个消息就可能需要记录到文件上(取决于消息是否要持久化)
    public boolean checkFilesExists(String queueName){
        //判断消息队列的数据文件和统计文件是否存在
        File queueDataFile = new File(getQueueDataPath(queueName));
        if (!queueDataFile.exists()){
            return false;
        }
        File queueStatFile = new File(getQueueStatPath(queueName));
        if (!queueStatFile.exists()){
            return false;
        }
        return true;
    }
    // 这个方法用来把一个新的消息, 放到队列对应的文件中.
    // queue 表示要把消息写入的队列. message 则是要写的消息.
    public void sendMessage(MSGQueue queue, Message message) throws MqException, IOException {
        //1.检查要写入的消息队列文件是否存在
        if (!checkFilesExists(queue.getName())){
            throw new MqException("[MessageFileManger] 队列对应的文件不存在! queueName=" + queue.getName());
        }
        //2.把Message对象转化为字节数组形式
        byte[] messageBinary = BinaryTool.toBytes(message);
        synchronized (queue){
            //3.获取队列数据文件的大小，计算offsetBeg和offsetEnd
            File queueDataFile = new File(getQueueDataPath(queue.getName()));
            message.setOffsetBeg(queueDataFile.length() + 4);
            message.setOffsetEnd(queueDataFile.length() + 4 + messageBinary.length);
            //4.将字节数组写入到数据文件中,注意是追加到文件结尾
            try (OutputStream outputStream = new FileOutputStream(queueDataFile,true)){
                //我们存储的格式是有四个字节表示数据长度的，一次性存储四个字节使用DataOutputStream
                try (DataOutputStream dataOutputStream = new DataOutputStream(outputStream)){
                    //当前消息的长度
                    dataOutputStream.writeInt(messageBinary.length);
                    //消息本体
                    dataOutputStream.write(messageBinary);
                }
            }
            //5.更新统计文件的内容
            Stat stat = readStat(queue.getName());
            stat.totalCount += 1;
            stat.validCount += 1;
            writStat(queue.getName(),stat);
        }
    }
    // 这个是删除消息的方法.
    // 这里的删除是逻辑删除, 也就是把硬盘上存储的这个数据里面的那个 isValid 属性, 设置成 0
    public void deleteMessage(MSGQueue queue,Message message) throws IOException, ClassNotFoundException {
        synchronized (queue){
            try (RandomAccessFile randomAccessFile = new RandomAccessFile(getQueueDataPath(queue.getName()),"rw")){
                //1.将消息从文件中读取出来
                byte[] bufferSrc = new byte[(int) (message.getOffsetEnd() - message.getOffsetBeg())];
                randomAccessFile.seek(message.getOffsetBeg());
                randomAccessFile.read(bufferSrc);
                //2.将读取出来的二进制文件转化为对象
                Message diskMessage = (Message) BinaryTool.fromBytes(bufferSrc);
                //3.将isValid设置为0x0
                diskMessage.setIsValid((byte) 0x0);
                //此处不需要给参数的这个 message 的 isValid 设为 0, 因为这个参数代表的是内存中管理的 Message 对象
                // 而这个对象马上也要被从内存中销毁了.
                //4.读取的消息重新写入文件
                byte[] bufferDest = BinaryTool.toBytes(diskMessage);
                // 虽然上面已经 seek 过了, 但是上面 seek 完了之后, 进行了读操作, 这一读, 就导致, 文件光标往后移动, 移动到
                // 下一个消息的位置了. 因此要想让接下来的写入, 能够刚好写回到之前的位置, 就需要重新调整文件光标.
                randomAccessFile.seek(message.getOffsetBeg());
                randomAccessFile.write(bufferDest);
            }
            //5.更新统计文件内容
            Stat stat = readStat(queue.getName());
            if (stat.validCount > 0){
                stat.validCount -= 1;
            }
            writStat(queue.getName(),stat);
        }
    }
    // 使用这个方法, 从文件中, 读取出所有的消息内容, 加载到内存中(具体来说是放到一个链表里)
    // 这个方法, 准备在程序启动的时候, 进行调用.
    // 这里使用一个 LinkedList, 主要目的是为了后续进行头删操作.
    // 这个方法的参数, 只是一个 queueName 而不是 MSGQueue 对象. 因为这个方法不需要加锁, 只使用 queueName 就够了.
    // 由于该方法是在程序启动时调用, 此时服务器还不能处理请求呢~~ 不涉及多线程操作文件.
    public LinkedList<Message> loadAllMessageFromQueue(String queueName) throws IOException, MqException, ClassNotFoundException {
        LinkedList<Message> messages = new LinkedList<>();
        long currentOffset = 0;
        try (InputStream inputStream = new FileInputStream(getQueueDataPath(queueName))){
            try (DataInputStream dataInputStream = new DataInputStream(inputStream)){
                while(true){
                    //1.读取当前消息的长度
                    int messageSize = dataInputStream.readInt();
                    //2.根据长度读取消息
                    byte[] buffer = new byte[messageSize];
                    int actualSize = dataInputStream.read(buffer);
                    if (actualSize != messageSize){
                        // 如果不匹配, 说明文件有问题, 格式错乱了!!
                        throw new MqException("[MessageFileManager] 文件格式错误! queueName=" + queueName);
                    }
                    //3.将读取出来的字节数组，转化为对象。
                    Message message = (Message) BinaryTool.fromBytes(buffer);
                    //4.判断是否有效
                    if (message.getIsValid() != 0x1){//消息无效
                        // 无效数据, 直接跳过.
                        // 虽然消息是无效数据, 但是 offset 不要忘记更新.
                        currentOffset += (4 + messageSize);
                        continue;
                    }
                    //5.计算offsetBeg和offsetEnd
                    message.setOffsetBeg(currentOffset + 4);
                    message.setOffsetEnd(currentOffset + 4 + messageSize);
                    currentOffset += (4 + messageSize);
                    //6.将message放入messages中
                    messages.add(message);
                }
            }catch (EOFException e) {
                // 这个 catch 并非真是处理 "异常", 而是处理 "正常" 的业务逻辑. 文件读到末尾, 会被 readInt 抛出该异常.
                // 这个 catch 语句中也不需要做啥特殊的事情
                System.out.println("[MessageFileManager] 恢复 Message 数据完成!");
            }
        }
        return messages;
    }
    // 检查当前是否要针对该队列的消息数据文件进行 GC
    public boolean checkGC(String queueName){
        // 判定是否要 GC, 是根据总消息数和有效消息数. 这两个值都是在 消息统计文件 中的.
        Stat stat = readStat(queueName);
        if (stat.totalCount > 2000 && (double)stat.validCount/(double)stat.totalCount < 0.5){
            return true;
        }
        return false;
    }
    private String getQueueDataNewPath(String queueName){
        return getQueueDir(queueName) + "queue_data_new.txt";
    }
    // 通过这个方法, 真正执行消息数据文件的垃圾回收操作.
    // 使用复制算法来完成.
    // 创建一个新的文件, 名字就是 queue_data_new.txt
    // 把之前消息数据文件中的有效消息都读出来, 写到新的文件中.
    // 删除旧的文件, 再把新的文件改名回 queue_data.txt
    // 同时要记得更新消息统计文件.
    public void gc(MSGQueue queue) throws MqException, IOException, ClassNotFoundException {
        synchronized (queue){
            // 由于 gc 操作可能比较耗时, 此处统计一下执行消耗的时间.
            long gcBeg = System.currentTimeMillis();
            //1.创建一个新的队里数据文件
            File queueNewDataFile = new File(getQueueDataNewPath(queue.getName()));
            if (queueNewDataFile.exists()){
                //如果存在，说明在之前的gc操作了一半中由于特殊原因中断了，
                throw new MqException("[MessageFileManager] gc 时发现该队列的 queue_data_new 已经存在! queueName=" + queue.getName());
            }
            boolean ok = queueNewDataFile.createNewFile();
            if (!ok){
                throw new MqException("[MessageFileManager] 创建文件失败 queueNewDataFile=" + queueNewDataFile.getAbsolutePath());
            }
            //2.读取队列旧数据文件中所有有效的消息
            LinkedList<Message> messages = loadAllMessageFromQueue(queue.getName());
            //3.将获取的有效的消息，全部放到新的队列数据文件中。
            try (OutputStream outputStream = new FileOutputStream(queueNewDataFile,true)){
                try (DataOutputStream dataOutputStream = new DataOutputStream(outputStream)){
                    for (Message message:messages) {
                        byte[] buffer = BinaryTool.toBytes(message);
                        dataOutputStream.writeInt(buffer.length);//四字节的消息长度
                        dataOutputStream.write(buffer);//消息的内容
                    }
                }
            }
            //4.删除旧文件，并将新的队列数据文件重命名
            File queueOldDataFile = new File(getQueueDataPath(queue.getName()));
            ok = queueOldDataFile.delete();
            if (!ok){
                throw new MqException("[MessageFileManager] 删除旧的数据文件失败！ queueOldDataFile = " + queueOldDataFile.getAbsolutePath());
            }
            // 把 queue_data_new.txt => queue_data.txt
            ok = queueNewDataFile.renameTo(queueOldDataFile);
            if (!ok){
                throw new MqException("[MessageFileManager] 新文件重命名失败！ queueNewDataFile = "+ queueNewDataFile.getAbsolutePath()
                 + ", queueOldDataFile = " + queueOldDataFile.getAbsolutePath());
            }
            //5.重新统计数据文件的消息信息。更新统计文件
            Stat stat = new Stat();
            stat.totalCount = messages.size();
            stat.validCount = messages.size();
            writStat(queue.getName(),stat);
            long gcEnd = System.currentTimeMillis();
            System.out.println("[MessageFileManager] gc执行完毕，queueName = " + queue.getName() +
                    ", time = " + (gcEnd - gcBeg) + "ms");
        }
    }
}
