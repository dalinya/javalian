package com.example.mq.mqserver.core;

import lombok.Data;

import java.io.Serializable;

/**
 * 消息的属性
 */
@Data
public class BasicProperties implements Serializable {
    //消息的唯一标识id
    private String messageId;
    //DIRECT -- 队列名  FANOUT -- 无意义  TOPIC -- 相当于锁子,和bindingKey效果配合
    private String routingKey;
    //是否持久化 1 -- 不持久化，2 -- 持久化。参照RabbitMQ。
    private int deliverMode = 1;
    //RabbitMQ还有其他属性，暂不实现。
}
