package com.example.mq.mapper;

import com.example.mq.mqserver.core.Binding;
import com.example.mq.mqserver.core.Exchange;
import com.example.mq.mqserver.core.MSGQueue;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MetaMapper {
    //三个创表方法
    void createExchangeTable();
    void createQueueTable();
    void createBindingTable();
    //对数据库进行增加和删除
    @Insert("insert into exchange values (#{name},#{type},#{durable},#{autoDelete},#{arguments})")
    void insertExchange(Exchange exchange);
    @Delete("delete from exchange where name = #{exchangeName}")
    void deleteExchange(String exchangeName);
    @Select("select * from  exchange")
    List<Exchange> selectAllExchanges();
    @Insert("insert into queue values (#{name},#{durable},#{exclusive},#{autoDelete},#{arguments})")
    void insertQueue(MSGQueue msgQueue);
    @Delete("delete from queue where name = #{queueName}")
    void deleteQueue(String queueName);
    @Select("select * from queue")
    List<MSGQueue> selectAllQueues();
    @Insert("insert into binding values (#{exchangeName},#{queueName},#{bindingKey})")
    void insertBinding(Binding binding);
    @Delete("delete from binding where exchangeName = #{exchangeName} and queueName = #{queueName}")
    void deleteBinding(Binding binding);
    @Select("select * from binding")
    List<Binding> selectAllBindings();
}
