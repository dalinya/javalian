package com.example.mq.common.net.Returns;

import com.example.mq.common.net.BasicArguments;
import com.example.mq.common.net.BasicReturns;
import com.example.mq.mqserver.core.BasicProperties;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 这个不是参数,是返回值.是服务器给消费者推送的订阅消息.
 */
@Data
public class SubScribeReturns extends BasicReturns implements Serializable {
    //consumerTag其实是channelId.
    private String consumerTag;
    //basicProperties和body共同构成了Message.
    private BasicProperties basicProperties;
    private byte[] body;
}
