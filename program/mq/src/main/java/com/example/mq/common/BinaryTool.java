package com.example.mq.common;

import java.io.*;

public class BinaryTool {
    //将一个对象序列化为一个字节数组
    public static byte[] toBytes(Object object) throws IOException {
        //流对象相当于一个变长的字节数组
        //把object序列化数据写到byteArrayOutputStream中，再统一转成byte[]
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()){
            try (ObjectOutputStream outputStream = new ObjectOutputStream(byteArrayOutputStream)){
                outputStream.writeObject(object);
            }
            //将byteArrayOutputStream中的二进制数据提取出来转化为byte[]
            return byteArrayOutputStream.toByteArray();
        }
    }
    //将字节数组反序列化成一个对象
    public static Object fromBytes(byte[] data) throws IOException, ClassNotFoundException {
        Object object = null;
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data)){
            try (ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)){
                object = objectInputStream.readObject();
            }
        }
        return object;
    }
}
