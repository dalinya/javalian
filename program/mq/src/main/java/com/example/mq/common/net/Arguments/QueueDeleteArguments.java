package com.example.mq.common.net.Arguments;

import com.example.mq.common.net.BasicArguments;
import lombok.Data;

import java.io.Serializable;

@Data
public class QueueDeleteArguments extends BasicArguments implements Serializable {
    private String queueName;
}
