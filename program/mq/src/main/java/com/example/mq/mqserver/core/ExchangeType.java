package com.example.mq.mqserver.core;
// 交换机类型, DIRECT, FANOUT, TOPIC
public enum ExchangeType {
    DIRECT(0),//直接交换机
    FANOUT(1),//扇出交换机
    TOPIC(2);//主题交换机
    private final int code;
    private ExchangeType(int code){
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
