package com.example.mq.mqserver.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 代表交换机
 */
@Data
public class Exchange {
    //身份的唯一标识
    private String name;
    //交换机类型
    ExchangeType type = ExchangeType.DIRECT;
    //是否是否持久化
    private boolean durable = false;
    //是否自动删除--暂不实现
    private boolean autoDelete = false;
    //可选参数---暂不实现
    private Map<String,Object> arguments = new HashMap<>();

    public String getArguments(){
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(arguments);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "{}";
    }
    public void setArguments(String argumentsJson){
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            this.arguments = objectMapper.readValue(argumentsJson, new TypeReference<HashMap<String,Object>>() {});
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
    // 在这里针对 arguments, 再提供一组 getter setter , 用来去更方便的获取/设置这里的键值对.
    // 这一组在 java 代码内部使用 (比如测试的时候)
    public Object getArguments(String key) {
        return arguments.get(key);
    }

    public void setArguments(String key, Object value) {
        arguments.put(key, value);
    }
    public void setArguments(Map<String,Object> arguments){
        this.arguments = arguments;
    }
}
