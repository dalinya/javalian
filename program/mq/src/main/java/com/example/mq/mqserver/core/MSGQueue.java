package com.example.mq.mqserver.core;

import com.example.mq.common.ConsumerEnv;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Data
public class MSGQueue {
    //唯一标识
    private String name;
    //是否持久化
    private boolean durable = false;
    //是否自动删除 -- 暂不实现
    private boolean autoDelete = false;
    //是否独占型队列 -- 暂不实现
    private boolean exclusive = false;
    //可选参数 -- 暂不实现
    private Map<String,Object> arguments = new HashMap<>();
    //当前队列都有哪些消费者
    private List<ConsumerEnv> consumerEnvList = new ArrayList<>();
    //记录当前取到了第几个消费者，方便实现轮训策略
    private AtomicInteger consumerSeq = new AtomicInteger(0);
    //添加一个新的订阅者
    public void addConsumerEnv(ConsumerEnv consumerEnv){
        synchronized (this){
            consumerEnvList.add(consumerEnv);
        }
    }
    //删除订阅者暂时不考虑
    //挑选订阅者，用来处理当前的消息。(按照轮询的方式)
    public ConsumerEnv chooseConsumer(){
        if (consumerEnvList.size() == 0){
            //该队列没有人订阅
            return null;
        }
        //计算一下当前要取的元素的下表
        int index = consumerSeq.get() % consumerEnvList.size();
        consumerSeq.getAndDecrement();
        return consumerEnvList.get(index);
    }
    public String getArguments(){
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(arguments);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "{}";
    }
    public void setArguments(String argumentsJson){
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            this.arguments = objectMapper.readValue(argumentsJson,new TypeReference<HashMap<String,Object>>() {});
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
    // 在这里针对 arguments, 再提供一组 getter setter , 用来去更方便的获取/设置这里的键值对.
    // 这一组在 java 代码内部使用 (比如测试的时候)
    public Object getArguments(String key) {
        return arguments.get(key);
    }

    public void setArguments(String key, Object value) {
        arguments.put(key, value);
    }
    public void setArguments(Map<String,Object> arguments) {
        this.arguments = arguments;
    }
}
