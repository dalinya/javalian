package com.example.mq.mqserver.core;

import lombok.Data;
//表示交换机和队列的关系
@Data
public class Binding {
    private String exchangeName;
    private String queueName;
    //主题交换机对应的钥匙
    private String bindingKey;
}
