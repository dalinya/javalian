package com.example.mq.common.net.Arguments;

import com.example.mq.common.net.BasicArguments;
import com.example.mq.mqserver.core.BasicProperties;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
@Data
public class BasicPublishArguments extends BasicArguments implements Serializable {
    private String exchangeName;
    private String routingKey;
    private BasicProperties basicProperties;
    private byte[] body;
}
