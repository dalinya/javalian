package com.example.mq.mqserver.core;

import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

/**
* 表示要传递的消息
 * 要存储到文件中，需要序列化。使用java自带的Serializable序列化
 * 因为json是文本的，文件是二进制的，使用Serializable序列化
**/

@Data
public class Message implements Serializable {
    //消息的属性
    private BasicProperties basicProperties = new BasicProperties();
    //消息的正文
    private byte[] body;
    //消息偏移量[offsetBeg,offsetEnd)
    //这两个参数存在的目的是为了，让内存中的对象找到，硬盘中对象的位置，
    //硬盘文件中的对象位置存储后已经确定了，就不需要这两个属性了。
    private transient long offsetBeg;//开头距离文件起始位置的偏移量
    private transient long offsetEnd;//结尾距离文件起止位置的偏移量
    //消息是否有效(逻辑删除) 0x1 -- 有效  0x0 --无效
    private byte isValid = 0x1;
    //创建一个工厂方法，创建消息
    public static Message createMessageWithId(String routingKey,BasicProperties basicProperties,byte[] body){
        Message message = new Message();
        if (basicProperties != null){
            message.setBasicProperties(basicProperties);
        }
        message.setRoutingKey(routingKey);
        message.setBody(body);
        message.setMessageId("M-" + UUID.randomUUID());
        return message;
    }

    //更方便的获取属性
    public String getMessageId(){
        return basicProperties.getMessageId();
    }
    public void setMessageId(String messageId){
        basicProperties.setMessageId(messageId);
    }
    public String getRoutingKey(){
        return basicProperties.getRoutingKey();
    }
    public void setRoutingKey(String routingKey){
        basicProperties.setRoutingKey(routingKey);
    }
    public int getDeliverMode() {
        return basicProperties.getDeliverMode();
    }

    public void setDeliverMode(int mode) {
        basicProperties.setDeliverMode(mode);
    }
}
