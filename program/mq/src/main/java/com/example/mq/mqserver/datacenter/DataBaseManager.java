package com.example.mq.mqserver.datacenter;

import com.example.mq.MqApplication;
import com.example.mq.mapper.MetaMapper;
import com.example.mq.mqserver.core.Binding;
import com.example.mq.mqserver.core.Exchange;
import com.example.mq.mqserver.core.ExchangeType;
import com.example.mq.mqserver.core.MSGQueue;

import java.io.File;
import java.util.List;

public class DataBaseManager {
    //从spring中获取现成的metaMapper
    private MetaMapper metaMapper;
    public void init(){
        //手动获取metaMapper
        metaMapper = MqApplication.context.getBean(MetaMapper.class);
        //判断数据库是否存在过
        if(!checkDBExists()){
            //创建data目录
            File dataDir = new File("./data");
            dataDir.mkdirs();
            //创建数据库表
            createTable();
            //插入默认数据
            createDefaultData();
            System.out.println("[DataBaseManager] 数据库初始化完成");
        }else {
            System.out.println("[DataBaseManager]数据库已存在");
        }
    }
    // 建库操作并不需要手动执行. (不需要手动创建 meta.db 文件)
    // 首次执行这里的数据库操作的时候, 就会自动的创建出 meta.db 文件来 (MyBatis 帮我们完成的)
    private void createTable() {
        metaMapper.createBindingTable();
        metaMapper.createExchangeTable();
        metaMapper.createQueueTable();
        System.out.println("[DataBaseManager] 创建数据库表完成");
    }

    // RabbitMQ 里有一个这样的设定: 带有一个 匿名 的交换机, 类型是 DIRECT.

    private void createDefaultData() {
        Exchange exchange = new Exchange();
        exchange.setName("");
        exchange.setType(ExchangeType.DIRECT);
        exchange.setDurable(true);
        exchange.setAutoDelete(false);
        metaMapper.insertExchange(exchange);
        System.out.println("[DataBaseManager] 插入初始数据完成");
    }
    private boolean checkDBExists() {
        File file = new File("./data/meta.db");
        return file.exists();
    }
    public void deleteDB(){
        File file = new File("./data/meta.db");
        boolean ret = file.delete();
        if (ret){
            System.out.println("[DataBaseManager]删除数据库成功");
        }else {
            System.out.println("[DataBaseManager]删除数据库失败");
        }

        File dataDir = new File("./data");
        ret = dataDir.delete();//使用delete删除目录的时候，要保证目中内容为空
        if (ret){
            System.out.println("[DataBaseManager]删除数据库目录成功");
        }else {
            System.out.println("[DataBaseManager]删除数据库目录失败");
        }
    }
    //封装其他数据库操作
    public void insertExchange(Exchange exchange){
        metaMapper.insertExchange(exchange);
    }
    public void deleteExchange(String exchangeName){
        metaMapper.deleteExchange(exchangeName);
    }
    public List<Exchange> selectAllExchanges(){
        return metaMapper.selectAllExchanges();
    }
    public void insertQueue(MSGQueue msgQueue){
        metaMapper.insertQueue(msgQueue);
    }
    public void deleteQueue(String queueName){
        metaMapper.deleteQueue(queueName);
    }
    public List<MSGQueue> selectAllQueues(){
        return metaMapper.selectAllQueues();
    }
    public void  insertBinding(Binding binding){
        metaMapper.insertBinding(binding);
    }
    public void deleteBinding(Binding binding){
        metaMapper.deleteBinding(binding);
    }
    public List<Binding> selectAllBindings(){
        return metaMapper.selectAllBindings();
    }

}
