package com.example.mq.mqserver.datacenter;

import com.example.mq.common.MqException;
import com.example.mq.mqserver.core.MSGQueue;
import com.example.mq.mqserver.core.Message;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;


@SpringBootTest
class MessageFileMangerTest {
    private MessageFileManager messageFileManger = new MessageFileManager();
    private static final String queueName1 = "testQueue1";
    private static final String queueName2 = "testQueue2";

    @BeforeEach
    void setUp() throws IOException {
        //准备队列，创建两个队列为以后备用
        messageFileManger.createQueueFiles(queueName1);
        messageFileManger.createQueueFiles(queueName2);
    }

    @AfterEach
    void tearDown() throws IOException {
        // 收尾阶段, 就把刚才的队列给干掉.
        messageFileManger.destroyQueueFiles(queueName1);
        messageFileManger.destroyQueueFiles(queueName2);
    }

    @Test
    void createQueueFiles() {
        // 创建队列文件已经在上面 setUp 阶段执行过了. 此处主要是验证看看文件是否存在.
        File queueDataFile1 = new File("./data/" + queueName1 + "/queue_data.txt");
        Assertions.assertEquals(true, queueDataFile1.isFile());
        File queueStatFile1 = new File("./data/" + queueName1 + "/queue_stat.txt");
        Assertions.assertEquals(true, queueStatFile1.isFile());

        File queueDataFile2 = new File("./data/" + queueName2 + "/queue_data.txt");
        Assertions.assertEquals(true, queueDataFile2.isFile());
        File queueStatFile2 = new File("./data/" + queueName2 + "/queue_stat.txt");
        Assertions.assertEquals(true, queueStatFile2.isFile());
    }
    @Test
    void testReadWriteStat(){
        MessageFileManager.Stat stat = new MessageFileManager.Stat();
        stat.totalCount = 100;
        stat.validCount = 50;
        // 此处就需要使用反射的方式, 来调用 writeStat 和 readStat 了.
        // Java 原生的反射 API 其实非常难用~~
        // 此处使用 Spring 帮我们封装好的 反射 的工具类.
        ReflectionTestUtils.invokeMethod(messageFileManger,"writStat",queueName1,stat);
        //写入之后检验数据是否正确
        MessageFileManager.Stat newStat = ReflectionTestUtils.invokeMethod(messageFileManger,"readStat",queueName1);
        Assertions.assertEquals(100,newStat.totalCount);
        Assertions.assertEquals(50,newStat.validCount);
    }
    private MSGQueue createTestQueue(String queueName){
        MSGQueue queue = new MSGQueue();
        queue.setName(queueName);
        queue.setDurable(true);
        queue.setExclusive(false);
        queue.setAutoDelete(false);
        return queue;
    }
    private Message createTestMessage(String content){
        Message message = Message.createMessageWithId("testRoutingKey",null,content.getBytes());
        return message;
    }


    @Test
    void sendMessage() throws IOException, MqException, ClassNotFoundException {
        //创建一个队列和消息
        MSGQueue msgQueue = createTestQueue(queueName1);
        Message message = createTestMessage("testMessage");
        //发送消息
        messageFileManger.sendMessage(msgQueue,message);
        //检查stat
        MessageFileManager.Stat stat = ReflectionTestUtils.invokeMethod(messageFileManger,"readStat",queueName1);
        Assertions.assertEquals(1,stat.validCount);
        Assertions.assertEquals(1,stat.totalCount);
        //检查发送的消息
        LinkedList<Message>  messages= messageFileManger.loadAllMessageFromQueue(queueName1);
        Assertions.assertEquals(1,messages.size());
        Message newMessage = messages.get(0);
        Assertions.assertEquals(message.getMessageId(),newMessage.getMessageId());
        Assertions.assertEquals(message.getIsValid(),newMessage.getIsValid());
        Assertions.assertEquals(message.getRoutingKey(),newMessage.getRoutingKey());
        Assertions.assertEquals(message.getDeliverMode(),newMessage.getDeliverMode());

        Assertions.assertArrayEquals(message.getBody(),newMessage.getBody());
    }



    @Test
    void loadAllMessageFromQueue() throws IOException, MqException, ClassNotFoundException {
        // 往队列中插入 100 条消息, 然后验证看看这 100 条消息从文件中读取之后, 是否和最初是一致的.
        LinkedList<Message> expectedMessages = new LinkedList<>();
        MSGQueue queue = createTestQueue(queueName1);
        for (int i = 0; i < 100; i++) {
            Message message = createTestMessage("testMessage" + i);
            messageFileManger.sendMessage(queue,message);
            expectedMessages.add(message);
        }
        LinkedList<Message> actualMessages = messageFileManger.loadAllMessageFromQueue(queueName1);
        Assertions.assertEquals(expectedMessages.size(),actualMessages.size());
        for (int i = 0; i < actualMessages.size(); i++) {
            Message expectedMessage = expectedMessages.get(i);
            Message actualMessage = actualMessages.get(i);
            System.out.println("[" +i +"] actualMessage = " + actualMessage);

            Assertions.assertEquals(expectedMessage.getMessageId(),actualMessage.getMessageId());
            Assertions.assertEquals(expectedMessage.getRoutingKey(),actualMessage.getRoutingKey());
            Assertions.assertEquals(expectedMessage.getDeliverMode(),actualMessage.getDeliverMode());
            Assertions.assertArrayEquals(expectedMessage.getBody(),actualMessage.getBody());
            Assertions.assertEquals(0x01,actualMessage.getIsValid());
        }
    }
    @Test
    void deleteMessage() throws IOException, MqException, ClassNotFoundException {
        // 创建队列, 写入 10 个消息. 删除其中的几个消息. 再把所有消息读取出来, 判定是否符合预期.
        MSGQueue queue = createTestQueue(queueName1);
        LinkedList<Message> expectedMessages = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            Message message = createTestMessage("testMessage" + i);
            messageFileManger.sendMessage(queue,message);
            expectedMessages.add(message);
        }
        messageFileManger.deleteMessage(queue,expectedMessages.get(7));
        messageFileManger.deleteMessage(queue,expectedMessages.get(8));
        messageFileManger.deleteMessage(queue,expectedMessages.get(9));
        LinkedList<Message> actualMessages = messageFileManger.loadAllMessageFromQueue(queueName1);
        Assertions.assertEquals(7,actualMessages.size());
        for (int i = 0; i < 7; i++) {
            Message expectedMessage = expectedMessages.get(i);
            Message actualMessage = actualMessages.get(i);
            System.out.println("[" +i +"] actualMessage = " + actualMessage);

            Assertions.assertEquals(expectedMessage.getMessageId(),actualMessage.getMessageId());
            Assertions.assertEquals(expectedMessage.getRoutingKey(),actualMessage.getRoutingKey());
            Assertions.assertEquals(expectedMessage.getDeliverMode(),actualMessage.getDeliverMode());
            Assertions.assertArrayEquals(expectedMessage.getBody(),actualMessage.getBody());
            Assertions.assertEquals(0x01,actualMessage.getIsValid());
        }
    }
    @Test
    void testGC() throws IOException, MqException, ClassNotFoundException {
        // 先往队列中写 100 个消息. 获取到文件大小.
        // 再把 100 个消息中的一半, 都给删除掉(比如把下标为偶数的消息都删除)
        // 再手动调用 gc 方法, 检测得到的新的文件的大小是否比之前缩小了.
        MSGQueue queue = createTestQueue(queueName1);
        LinkedList<Message> expectedMessages = new LinkedList<>();
        for (int i = 0; i < 100; i++) {
            Message message = createTestMessage("testMessage" + i);
            messageFileManger.sendMessage(queue,message);
            expectedMessages.add(message);
        }
        //获取gc前的文件大小
        File beforeGcFile = new File("./data/" + queueName1 + "/queue_data.txt");
        long beforeGcLength = beforeGcFile.length();
        //删除偶数位的消息消息
        for (int i = 0; i < 100; i+=2) {
            messageFileManger.deleteMessage(queue,expectedMessages.get(i));
        }
        //进行gc
        messageFileManger.gc(queue);
        //获取全部有效消息
        LinkedList<Message> actualMessages = messageFileManger.loadAllMessageFromQueue(queueName1);
        for (int i = 0; i < actualMessages.size(); i++) {
            Message expectedMessage = expectedMessages.get(2 * i + 1);
            Message actualMessage = actualMessages.get(i);
            System.out.println("[" + i +"] actualMessage = " + actualMessage);

            Assertions.assertEquals(expectedMessage.getMessageId(),actualMessage.getMessageId());
            Assertions.assertEquals(expectedMessage.getRoutingKey(),actualMessage.getRoutingKey());
            Assertions.assertEquals(expectedMessage.getDeliverMode(),actualMessage.getDeliverMode());
            Assertions.assertArrayEquals(expectedMessage.getBody(),actualMessage.getBody());
            Assertions.assertEquals(0x01,actualMessage.getIsValid());
        }
        //获取gc后的文件大小
        File afterGcFile = new File("./data/" + queueName1 + "/queue_data.txt");
        long afterGcLength = afterGcFile.length();
        System.out.println("before: " + beforeGcLength);
        System.out.println("after: " + afterGcLength);
        Assertions.assertTrue(beforeGcLength > afterGcLength);
    }
}