package com.example.mq.mqserver.datacenter;

import com.example.mq.MqApplication;
import com.example.mq.mqserver.core.Binding;
import com.example.mq.mqserver.core.Exchange;
import com.example.mq.mqserver.core.ExchangeType;
import com.example.mq.mqserver.core.MSGQueue;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Queue;


@SpringBootTest
class DataBaseManagerTest {
    private DataBaseManager dataBaseManager = new DataBaseManager();
    @BeforeEach
    void setUp() {
        MqApplication.context = SpringApplication.run(MqApplication.class);
        dataBaseManager.init();
    }

    @AfterEach
    void tearDown() {
        // 此处的 context 对象, 持有了 MetaMapper 的实例, MetaMapper 实例又打开了 meta.db 数据库文件.
        // 如果 meta.db 被别人打开了, 此时的删除文件操作是不会成功的 (Windows 系统的限制, Linux 则没这个问题).
        // 另一方面, 获取 context 操作, 会占用 8080 端口. 此处的 close 也是释放 8080.
        MqApplication.context.close();
        dataBaseManager.deleteDB();
    }

    @Test
    void init() {
        //对数据库进行查询操作
        List<Exchange>  exchangeList = dataBaseManager.selectAllExchanges();
        List<MSGQueue> queueList = dataBaseManager.selectAllQueues();
        List<Binding> bindingList = dataBaseManager.selectAllBindings();
        //查询完之后进行检验
        Assertions.assertEquals(1,exchangeList.size());
        Assertions.assertEquals("",exchangeList.get(0).getName());
        Assertions.assertEquals(ExchangeType.DIRECT,exchangeList.get(0).getType());

        Assertions.assertEquals(0,queueList.size());
        Assertions.assertEquals(0,bindingList.size());
    }
    private Exchange createTestExchange(String exchangeName){
        Exchange exchange = new Exchange();
        exchange.setName(exchangeName);
        exchange.setType(ExchangeType.DIRECT);
        exchange.setAutoDelete(false);
        exchange.setDurable(true);
        exchange.setArguments("aaa",1);
        exchange.setArguments("bbb",2);
        return exchange;
    }
    @Test
    void insertExchange() {
        //构造一个对象。然后插入，在查询出来产看结果是否符合预期
        Exchange exchange = createTestExchange("testExchange");
        dataBaseManager.insertExchange(exchange);
        //验证结果是否正确
        List<Exchange> exchangeList = dataBaseManager.selectAllExchanges();
        Assertions.assertEquals(2,exchangeList.size());
        Exchange newExchange = exchangeList.get(1);
        Assertions.assertEquals("testExchange",newExchange.getName());
        Assertions.assertEquals(ExchangeType.DIRECT,newExchange.getType());
        Assertions.assertEquals(false,newExchange.isAutoDelete());
        Assertions.assertEquals(1,newExchange.getArguments("aaa"));
        Assertions.assertEquals(2,newExchange.getArguments("bbb"));
    }

    @Test
    void deleteExchange() {
        //先插入一个交换机，然后再删除
        Exchange exchange = createTestExchange("testExchange");
        dataBaseManager.insertExchange(exchange);
        //查询,检验是否插入成功，检验数量和插入交换机的名称
        List<Exchange> exchangeList = dataBaseManager.selectAllExchanges();
        Assertions.assertEquals(2,exchangeList.size());
        Assertions.assertEquals("testExchange",exchangeList.get(1).getName());
        //进行删除交换机
        dataBaseManager.deleteExchange("testExchange");
        //再次查询进行检验
        exchangeList = dataBaseManager.selectAllExchanges();
        Assertions.assertEquals(1,exchangeList.size());
        Assertions.assertEquals("",exchangeList.get(0).getName());
    }

    private MSGQueue createTestQueue(){
        MSGQueue msgQueue = new MSGQueue();
        msgQueue.setName("testQueue");
        msgQueue.setDurable(true);
        msgQueue.setExclusive(false);
        msgQueue.setAutoDelete(false);
        msgQueue.setArguments("aaa",1);
        msgQueue.setArguments("bbb",2);
        return msgQueue;
    }

    @Test
    void insertQueue() {
        //进行插入，然后查询检测
        MSGQueue msgQueue = createTestQueue();
        dataBaseManager.insertQueue(msgQueue);
        List<MSGQueue> msgQueueList = dataBaseManager.selectAllQueues();
        Assertions.assertEquals(1,msgQueueList.size());
        MSGQueue newMsgQueue = msgQueueList.get(0);
        Assertions.assertEquals("testQueue",newMsgQueue.getName());
        Assertions.assertEquals(true,newMsgQueue.isDurable());
        Assertions.assertEquals(false,newMsgQueue.isAutoDelete());
        Assertions.assertEquals(false,newMsgQueue.isExclusive());
        Assertions.assertEquals(1,newMsgQueue.getArguments("aaa"));
        Assertions.assertEquals(2,newMsgQueue.getArguments("bbb"));
    }

    @Test
    void deleteQueue() {
        MSGQueue msgQueue = createTestQueue();
        dataBaseManager.insertQueue(msgQueue);
        List<MSGQueue> msgQueueList = dataBaseManager.selectAllQueues();
        Assertions.assertEquals(1,msgQueueList.size());

        dataBaseManager.deleteQueue("testQueue");
        msgQueueList = dataBaseManager.selectAllQueues();
        Assertions.assertEquals(0,msgQueueList.size());
    }
    private Binding createTestBinding(){
        Binding binding = new Binding();
        binding.setExchangeName("testExchange");
        binding.setQueueName("testQueue");
        binding.setBindingKey("testBindingKey");
        return binding;
    }
    @Test
    void insertBinding() {
        Binding binding = createTestBinding();
        dataBaseManager.insertBinding(binding);
        List<Binding> bindingList = dataBaseManager.selectAllBindings();
        Assertions.assertEquals(1,bindingList.size());
        Binding newBinding = bindingList.get(0);
        Assertions.assertEquals("testExchange",newBinding.getExchangeName());
        Assertions.assertEquals("testQueue",newBinding.getQueueName());
        Assertions.assertEquals("testBindingKey",newBinding.getBindingKey());
    }

    @Test
    void deleteBinding() {
        Binding binding = createTestBinding();
        dataBaseManager.insertBinding(binding);
        List<Binding> bindingList = dataBaseManager.selectAllBindings();
        Assertions.assertEquals(1,bindingList.size());

        dataBaseManager.deleteBinding(binding);
        bindingList = dataBaseManager.selectAllBindings();
        Assertions.assertEquals(0,bindingList.size());
    }
}