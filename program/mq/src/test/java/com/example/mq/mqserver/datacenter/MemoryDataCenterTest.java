package com.example.mq.mqserver.datacenter;

import ch.qos.logback.core.util.FileUtil;
import com.example.mq.MqApplication;
import com.example.mq.common.MqException;
import com.example.mq.mqserver.core.*;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class MemoryDataCenterTest {
    MemoryDataCenter memoryDataCenter = null;
    @BeforeEach
    void setUp() {
        memoryDataCenter = new MemoryDataCenter();
    }

    @AfterEach
    void tearDown() {
        memoryDataCenter = null;
    }
    //创建一个交换机
    private Exchange createTestExchange(String exchangeName){
        Exchange exchange = new Exchange();
        exchange.setName(exchangeName);
        exchange.setDurable(true);
        exchange.setType(ExchangeType.DIRECT);
        exchange.setAutoDelete(false);
        return exchange;
    }
    //创建一个队列
    private MSGQueue createTestQueue(String queueName){
        MSGQueue queue = new MSGQueue();
        queue.setDurable(true);
        queue.setName(queueName);
        queue.setExclusive(false);
        queue.setAutoDelete(false);
        return queue;
    }
    //测试交换机操作
    @Test
    public void testExchange(){
        //1.创建一个交换机,并插入
        Exchange expectedExchange = createTestExchange("testExchange");
        memoryDataCenter.insertExchange(expectedExchange);
        //2.获取交换机
        Exchange actualExchange = memoryDataCenter.getExchange("testExchange");
        Assertions.assertEquals(expectedExchange,actualExchange);
        //3.删除交换机
        memoryDataCenter.deleteExchange("testExchange");
        //4,再查一次，检验交换机是否找不到
        actualExchange = memoryDataCenter.getExchange("testExchange");
        Assertions.assertNull(actualExchange);
    }
    //测试队列操作
    @Test
    public void testQueue(){
        MSGQueue expectedQueue = createTestQueue("testQueue");
        memoryDataCenter.insertQueue(expectedQueue);
        MSGQueue actualQueue = memoryDataCenter.getQueue("testQueue");
        Assertions.assertEquals(expectedQueue,actualQueue);
        memoryDataCenter.deleteQueue("testQueue");
        actualQueue = memoryDataCenter.getQueue("testQueue");
        Assertions.assertNull(actualQueue);
    }
    //针对绑定进行测试
    @Test
    public void testBinding() throws MqException {
        Binding expectedBinding = new Binding();
        expectedBinding.setBindingKey("testBindingKey");
        expectedBinding.setQueueName("testQueueName");
        expectedBinding.setExchangeName("testExchangeName");
        memoryDataCenter.insertBinding(expectedBinding);
        Binding actualBinding = memoryDataCenter.getBinding("testExchangeName","testQueueName");
        Assertions.assertEquals(expectedBinding,actualBinding);
        ConcurrentHashMap<String,Binding> bindingMap = memoryDataCenter.getBindings("testExchangeName");
        Assertions.assertEquals(1,bindingMap.size());
        Assertions.assertEquals(expectedBinding,bindingMap.get("testQueueName"));
        memoryDataCenter.deleteBinding(expectedBinding);
        actualBinding = memoryDataCenter.getBinding("testExchangeName","testQueueName");
        Assertions.assertNull(actualBinding);
    }
    //创建消息
    private Message createTestMessage(String content){
        Message message = Message.createMessageWithId("testRoutingKey",null,content.getBytes());
        return message;
    }
    @Test
    public void testMessage(){
        Message expectedMessage = createTestMessage("testMessage");
        memoryDataCenter.addMessage(expectedMessage);

        Message actualMessage = memoryDataCenter.getMessage(expectedMessage.getMessageId());
        Assertions.assertEquals(expectedMessage,actualMessage);

        memoryDataCenter.deleteMessage(expectedMessage.getMessageId());
        actualMessage = memoryDataCenter.getMessage(expectedMessage.getMessageId());
        Assertions.assertNull(actualMessage);
    }
    @Test
    public void testSendMessage(){
        //1.创建一个队列，创建10条消息发送到队列中
        MSGQueue queue = createTestQueue("testQueue");
        LinkedList<Message> expectedMessages = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            Message message = createTestMessage("testMessage" + i);
            expectedMessages.add(message);
            memoryDataCenter.sendMessage(queue,message);
        }
        //2.获取队列中的消息
        LinkedList<Message> actualMessages = new LinkedList<>();
        while (true){
            Message message = memoryDataCenter.pollMessage("testQueue");
            if (message == null){
                break;
            }
            actualMessages.add(message);
        }
        //3.对比消息是否一致
        Assertions.assertEquals(10,actualMessages.size());
        for (int i = 0; i < expectedMessages.size(); i++) {
            Assertions.assertEquals(expectedMessages.get(i),actualMessages.get(i));
        }
    }
    @Test
    public void testMessageWaitAck(){
        Message expectedMessage = createTestMessage("testMessage");
        memoryDataCenter.addMessageWaitAck("testQueue",expectedMessage);

        Message actualMessage = memoryDataCenter.getMessageWaitAck("testQueue",expectedMessage.getMessageId());
        Assertions.assertEquals(expectedMessage,actualMessage);

        memoryDataCenter.removeMessageWaitAck("testQueue",expectedMessage.getMessageId());
        actualMessage = memoryDataCenter.getMessageWaitAck("testQueue",expectedMessage.getMessageId());
        Assertions.assertNull(actualMessage);
    }
    @Test
    public void testRecovery() throws IOException, MqException, ClassNotFoundException {
        // 由于后续需要进行数据库操作, 依赖 MyBatis. 就需要先启动 SpringApplication, 这样才能进行后续的数据库操作.
        MqApplication.context = SpringApplication.run(MqApplication.class);
        //1.在硬盘中构造好数据
        DiskDataCenter diskDataCenter = new DiskDataCenter();
        diskDataCenter.init();

        //构造交换机
        Exchange expectedExchange = createTestExchange("testExchange");
        diskDataCenter.insertExchange(expectedExchange);
        //构造队列
        MSGQueue expectedQueue = createTestQueue("testQueue");
        diskDataCenter.insertQueue(expectedQueue);
        //构造绑定
        Binding expectedBinding = new Binding();
        expectedBinding.setExchangeName("testExchangeName");
        expectedBinding.setQueueName("testQueueName");
        expectedBinding.setBindingKey("testBindingKey");
        diskDataCenter.insertBinding(expectedBinding);
        //构造消息
        Message expectedMessage = createTestMessage("testContent");
        diskDataCenter.sendMessage(expectedQueue,expectedMessage);//如果DiskDataCenter中 insertQueue 操作没有创建目录和文件，在此处机会报错
        //2.执行恢复操作
        memoryDataCenter.recovery(diskDataCenter);
        //3.对比结果
        Exchange actualExchange = memoryDataCenter.getExchange("testExchange");
        Assertions.assertEquals(expectedExchange.getName(),actualExchange.getName());
        Assertions.assertEquals(expectedExchange.getType(),actualExchange.getType());
        Assertions.assertEquals(expectedExchange.getArguments(),expectedExchange.getArguments());
        Assertions.assertEquals(expectedExchange.isDurable(),actualExchange.isDurable());
        Assertions.assertEquals(expectedExchange.isAutoDelete(),expectedExchange.isAutoDelete());

        MSGQueue actualQueue = memoryDataCenter.getQueue("testQueue");
        Assertions.assertEquals(expectedQueue.getName(),actualQueue.getName());
        Assertions.assertEquals(expectedQueue.isDurable(),actualQueue.isDurable());
        Assertions.assertEquals(expectedQueue.isAutoDelete(),actualQueue.isAutoDelete());
        Assertions.assertEquals(expectedQueue.isExclusive(),actualQueue.isExclusive());

        Binding actualBinding = memoryDataCenter.getBinding("testExchangeName","testQueueName");
        Assertions.assertEquals(expectedBinding.getExchangeName(),actualBinding.getExchangeName());
        Assertions.assertEquals(expectedBinding.getBindingKey(),actualBinding.getBindingKey());
        Assertions.assertEquals(expectedBinding.getQueueName(),actualBinding.getQueueName());

        Message actualMessage = memoryDataCenter.pollMessage("testQueue");
        Assertions.assertEquals(expectedMessage.getMessageId(),actualMessage.getMessageId());
        Assertions.assertEquals(expectedMessage.getDeliverMode(),actualMessage.getDeliverMode());
        Assertions.assertEquals(expectedMessage.getRoutingKey(),actualMessage.getRoutingKey());
        Assertions.assertArrayEquals(expectedMessage.getBody(),actualMessage.getBody());
        // 4. 清理硬盘的数据, 把整个 data 目录里的内容都删掉(包含了 meta.db 和 队列的目录).
        MqApplication.context.close();//先关闭数据库连接，才能进行下面的删除文件操作
        File dataDir = new File("./data");
        FileUtils.deleteDirectory(dataDir);
    }
}