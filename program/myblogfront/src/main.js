import './assets/main.scss'

import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'
import router from '@/router'
import { createPinia } from 'pinia'
//导入持久化插件
import {createPersistedState} from'pinia-persistedstate-plugin'
//添加中文语言包
import locale from 'element-plus/dist/locale/zh-cn.js'

import Window from '@/components/Window.vue'
// import {bulma} from 'bulma'
import 'bulma/css/bulma.css'
import 'github-markdown-css/github-markdown.css'


import UndrawUi from 'undraw-ui'
import 'undraw-ui/dist/style.css'

const app = createApp(App)
const pinia = createPinia()
const persist = createPersistedState()
//pinia使用持久化插件
pinia.use(persist)
app.use(pinia)
app.use(router)
app.use(ElementPlus,{locale})
app.use(UndrawUi)
// app.use(bulma)
app.component("Window", Window);

app.mount('#app')




