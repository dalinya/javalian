//导入vue-router
import { createRouter, createWebHistory } from 'vue-router'
//导入组件
import LoginVue from '@/views/Login.vue'
import LayoutVue from '@/views/Layout.vue'
//导入子路由组件
import ArticleCategoryVue from '@/views/article/ArticleCategory.vue'
import ArticleManageVue from '@/views/article/ArticleManage.vue'
import ArticleaddVue from '@/views/article/ArticleAdd.vue'
//ArticleEdit.vue
import ArticleEditVue from '@/views/article/ArticleEdit.vue'

import UserAvatarVue from '@/views/user/UserAvatar.vue'
import UserInfoVue from '@/views/user/UserInfo.vue'

import UserResetPasswordVue from '@/views/user/UserResetPassword.vue'

// ArticleIndex.vue
import ArticleIndexVue from '@/views/web/ArticleIndex.vue'
//ArticleDetail.vue
import ArticleDetailVue from '@/views/web/ArticleDetail.vue'
//定义路由关系
const routes = [
    { path: '/login', component: LoginVue },
    { path: '/', 
        component: LayoutVue, 
        //重定向
        redirect: '/article/manage',//相当于默认路由
        //子路由
        children: [
            { path: '/article/category', component: ArticleCategoryVue },
            { path: '/article/manage', component: ArticleManageVue },
            
            { path: '/user/info', component: UserInfoVue },
            { path: '/user/avatar', component: UserAvatarVue },
            { path: '/user/password', component: UserResetPasswordVue },
        ]
    },
    //ArticleEditVue
    { path: '/article/add', component: ArticleaddVue },
    { path: '/article/index', component: ArticleIndexVue },
    { path: '/article/detail', name: 'ArticleDetail',component: ArticleDetailVue },
    { path: '/article/edit', name: 'ArticleEditVue',component: ArticleEditVue }

]

//创建路由器
const router = createRouter({
    history: createWebHistory(),
    routes: routes
});
//导出路由
export default router