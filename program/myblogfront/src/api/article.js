//导入请求工具类
import request from '@/utils/request.js'

//文章分类列表查询
export const articleCategoryListService = ()=>{
    return request.get('/category/list')
}

//添加文章分类
export const articleCategoryAddService = (categoryModel) => {
    return request.post('/category/add', categoryModel)
}

//修改分类
export const articleCategoryUpdateService = (categoryModel)=>{
    return request.put('/category/update',categoryModel)
}
//删除分类
export const articleCategoryDeleteService = (cid) => {
    return request.delete('/category/del?cid='+cid)
}
//文章列表查询
export const myArticleListService = (params) => {
    return request.get('/art/getlistbyuid', { params: params })
}
//添加文章
export const articleAddService = (articleModel)=>{
    return request.post('/art/add',articleModel)
}
//全部 文章列表查询
export const articleListService = (params) => {
    return request.get('/art/getlistbypage', { params: params })
}
//查询文章分类id
export const articleDetailService = (aid) => {
    return request.get('/art/getbyid?aid='+aid.value)
}
//删除根据 aid 文章
export const articleDeleteService = (aid) => {
    return request.delete('/art/delbyaid?aid=' + aid)
}
//根据 aid 修改 文章
export const articleEdieService = (articleModel) => {
    return request.post('/art/update', articleModel)
}
