//导入请求工具类
import request from '@/utils/request.js'

//文章分类列表查询
export const commentListService = (aid)=>{
    return request.get('/comment/getlistbyaid?aid='+aid)
}

//添加文章分类
export const commentAddService = (comment) => {
    return request.post('/comment/add', comment)
}
//删除评论
export const commentDeleteService = (cid) => {
    return request.delete('/comment/del?cid='+cid)
}
