package com.example.demo.service;

import com.example.demo.entity.Problem;
import com.example.demo.mapper.ProblemMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProblemService {
    @Resource
    private ProblemMapper problemMapper;
    public List<Problem> getList(){
        return problemMapper.getList();
    }
    public Problem getDetail(Integer id){
        return problemMapper.getDetail(id);
    }
    public int insert(Problem problem){
        return problemMapper.insert(problem);
    }
}
