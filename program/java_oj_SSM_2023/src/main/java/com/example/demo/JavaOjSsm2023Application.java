package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaOjSsm2023Application {

    public static void main(String[] args) {
        SpringApplication.run(JavaOjSsm2023Application.class, args);
    }

}
