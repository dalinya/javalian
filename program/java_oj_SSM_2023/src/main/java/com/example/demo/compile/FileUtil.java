package com.example.demo.compile;

import java.io.*;

public class FileUtil {
    // 负责把 filePath 对应的文件的内容读取出来, 放到返回值中.
    public static String readFile(String filePath){
        StringBuilder result = new StringBuilder();
        try (Reader reader = new FileReader(filePath)){
            while (true){
                int ch = reader.read();
                if (ch == -1){
                    break;
                }
                result.append((char) ch);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }
    // 负责把 content 写入到 filePath 对应的文件中
    public static void writFile(String filePath,String content){
        try (Writer writer = new FileWriter(filePath)){
            writer.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        FileUtil.writFile("./test.txt","hello world");
        String content = FileUtil.readFile("./test.txt");
        System.out.println(content);
    }
}
