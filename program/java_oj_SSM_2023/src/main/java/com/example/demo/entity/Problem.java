package com.example.demo.entity;

import lombok.Data;

@Data
public class Problem {
    public int id;
    public String title;
    public String level;
    public String description; //题目描述
    public String templateCode; // 代码模版
    public String testCode;//测试用例
}