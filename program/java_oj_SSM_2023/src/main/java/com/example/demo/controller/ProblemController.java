package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.compile.Answer;
import com.example.demo.compile.Question;
import com.example.demo.compile.Task;
import com.example.demo.entity.Problem;
import com.example.demo.service.ProblemService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

@RestController
@RequestMapping("/problem")
public class ProblemController {
    @Data
    static class CompileRequest{//代表传输过来的数据
        public int id;
        public String code;
    }
    @Autowired
    private ProblemService problemService;
    @RequestMapping("/list")
    public AjaxResult getMyList(HttpServletRequest request) {
        List<Problem> problems = problemService.getList();
        if (problems == null) {
            return AjaxResult.fail(-1, "非法请求");
        }
        return AjaxResult.success(problems);
    }
    @RequestMapping("/detail")
    public AjaxResult getDetail(Integer id){
        if(id == null){
            return AjaxResult.fail(-1, "非法请求");
        }
        Problem problem =  problemService.getDetail(id);
        return AjaxResult.success(problem);
    }
    @Autowired
    private ObjectMapper objectMapper;
    @RequestMapping("/compile")//0 表示编译运行 ok,1 表示编译出错,2 表示运行出错(抛异常)，3 表示其他错误
    public AjaxResult Compile(HttpServletRequest req) throws IOException {
       //1. 先读取请求的正文. 先按照 JSON 格式进行解析
       CompileRequest compileRequest =  objectMapper.readValue(req.getInputStream(),  CompileRequest.class);
       //2.根据 id 从数据库中查找到题目的详情 => 得到测试用例代码
       Problem problem = problemService.getDetail(compileRequest.id);
       if (problem == null){
           //为了同意处理错误，在这个地方抛出异常
           return AjaxResult.fail(3,"没有找打指定题目！id = " + compileRequest.id);
       }
       // 3. 把用户提交的代码和测试用例代码, 给拼接成一个完整的代码.
       //testCode是测试用例代码
       String testCode = problem.getTestCode();
       //finalCode是用户提交代码
       String requestCode =  compileRequest.code;
       //finalCode是最终代码
       String finalCode = mergeCode(testCode,requestCode);//对代码进行拼接
       if (finalCode == null){
           //code不合法异常
           return AjaxResult.fail(3,"提交的代码不符合要求！");
       }
       System.out.println(problem.title);
        // 4. 创建一个 Task 实例, 调用里面的 compileAndRun 来进行编译运行.
        Task task = new Task();
        Question question = new Question();
        question.setCode(finalCode);
        Answer answer = null;
        try {
            answer = task.compileAndRun(question);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        // 5. 根据 Task 运行的结果, 包装成一个 HTTP 响应
        return AjaxResult.success(answer.getError(),answer.getReason(),answer.getStdout());
    }
    private String mergeCode(String testCode, String requestCode) {
        //1.查找requestCode中的最后一个}
        int pos = requestCode.lastIndexOf("}");
        if (pos == -1){
            //说明这个代码没有},是个非法代码
            return null;
        }
        //2.根据这个位置进行字符串截取
        String subStr = requestCode.substring(0,pos);
        //3.进行拼接
        return subStr + testCode + "\n}";
    }
    @RequestMapping("/add")
    public AjaxResult addProblem(HttpServletRequest req) throws IOException {
        Problem problem = objectMapper.readValue(req.getInputStream(), Problem.class);
        System.out.println(problem);
        int result = problemService.insert(problem);
        System.out.println("插入成功");
        return AjaxResult.success(problem);
    }
}
