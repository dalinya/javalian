package com.example.demo.mapper;

import com.example.demo.entity.Problem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ProblemMapper {
    public int insert(Problem problem);
    List<Problem> getList();
    Problem getDetail(@Param("id") Integer id);
}
