package com.example.demo.entity;

import lombok.Data;

import java.time.LocalDateTime;
import java.io.Serializable;
@Data
public class Userinfo implements Serializable{
    private Integer id;
    private String username;
    private String password;
    private String photo;
    private LocalDateTime createtime;
    private LocalDateTime updatetime;
    private Integer state;
}
