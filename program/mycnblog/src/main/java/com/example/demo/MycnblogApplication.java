package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
@EnableCaching // 开启全局注解缓存
public class MycnblogApplication {

    public static void main(String[] args) {
        SpringApplication.run(MycnblogApplication.class, args);
    }

}
