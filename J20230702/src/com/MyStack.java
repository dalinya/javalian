package com;

import java.util.LinkedList;
import java.util.Queue;

class MyStack {
    Queue<Integer> qu1;
    Queue<Integer> qu2;
    public MyStack() {
        qu1 = new LinkedList<>();
        qu2 = new LinkedList<>();
    }

    public void push(int x) {
        if(!qu2.isEmpty()){
            qu2.offer(x);
        }else {//要么是qu1不为空，要么两个队列都为空
            qu1.offer(x);
        }
    }

    public int pop() {
        if(qu1.isEmpty() && qu2.isEmpty()){
            return -1;
        }
        if(!qu1.isEmpty()){
            int size = qu1.size();
            for (int i = 0; i < size - 1; i++) {
                qu2.offer(qu1.poll());
            }
            return qu1.poll();
        }
        //qu2不为空
        int size = qu2.size();
        for (int i = 0; i < size - 1; i++) {
            qu1.offer(qu2.poll());
        }
        return qu2.poll();
    }

    public int top() {
        if(qu1.isEmpty() && qu2.isEmpty()){
            return -1;
        }
        if(!qu1.isEmpty()){
            int size = qu1.size();
            for (int i = 0; i < size - 1; i++) {
                qu2.offer(qu1.poll());
            }
            int val = qu1.poll();
            qu2.offer(val);
            return val;
        }
        //qu2不为空
        int size = qu2.size();
        for (int i = 0; i < size - 1; i++) {
            qu1.offer(qu2.poll());
        }
        int val = qu2.poll();
        qu1.offer(val);
        return val;
    }

    public boolean empty() {
        return qu1.isEmpty() && qu2.isEmpty();
    }
}