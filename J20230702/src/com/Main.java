package com;

import java.util.Arrays;
import java.util.Comparator;

class Student{
    public int age;
    public String name;

    public Student(int age, String name) {
        this.age = age;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
public class Main {
    public static void main(String[] args) {
        Student[] students = {new Student(18,"张三"),new Student(21,"李四"),new Student(15,"王五")};
        Arrays.sort(students, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o1.age- o2.age;
            }
        });
        System.out.println(Arrays.toString(students));
    }
    public int countSegments(String ss) {
        int result = 0;
        char[] s = ss.toCharArray();
        for (int i = 0; i < s.length;) {
            if (s[i] != ' '){
                while (s[i] != ' '){//找到下一个是空格的字符
                    i++;
                }
                result++;
            }else {
                while (s[i] == ' '){//找到下一个不是空格的字符
                    i++;
                }
            }
        }

        return result;
    }
    public String toLowerCase(String ss) {
        char[] s = ss.toCharArray();
        int swap = 'a' - 'A';
        for (int i = 0; i < s.length; i++) {
            if (s[i] >= 'A' && s[i] <= 'Z'){
                s[i] += swap;
            }
        }
        return new String(s);
    }
    public int firstUniqChar(String ss) {
        int[] hash = new int[26];
        char[] s = ss.toCharArray();
        for (int i = 0; i < s.length; i++) {
            hash[s[i] - 'a']++;
        }
        for (int i = 0; i < s.length; i++) {
            if (hash[s[i] - 'a'] == 1){
                return i;
            }
        }
        return -1;
    }


    public boolean isPalindrome(String ss) {
        char[] s = ss.toLowerCase().toCharArray();
        int left = 0,right = s.length;
        while (left <= right){
            while (left < right && !isLow(s[left])){
                left++;
            }
            while (left < right && !isLow(s[right])){
                right--;
            }
            if (s[left] != s[right]){
                return false;
            }
            left++;
            right--;
        }

        return true;
    }

    private boolean isLow(char c) {
        if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z')){
            return true;
        }
        return false;
    }
}
