package demoSort;


import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class Test {
    public static void testInsertSort(int[] array) {
        array = Arrays.copyOf(array,array.length);
        long startTime = System.currentTimeMillis();
        Sort.insertSort(array);
        long endTime = System.currentTimeMillis();
        System.out.println("直接插入排序耗费时间："+ (endTime - startTime));
    }
    public static void testShellSort(int[] array) {
        array = Arrays.copyOf(array,array.length);
        long startTime = System.currentTimeMillis();
        Sort.shellSort(array);
        long endTime = System.currentTimeMillis();
        System.out.println("希尔排序耗费时间："+ (endTime - startTime));
    }
    public static void testSelectSort(int[] array) {
        array = Arrays.copyOf(array,array.length);
        long startTime = System.currentTimeMillis();
        Sort.selectSort(array);
        long endTime = System.currentTimeMillis();
        System.out.println("选择排序耗费时间："+ (endTime - startTime));
    }

    public static void testHeapSort(int[] array) {
        array = Arrays.copyOf(array,array.length);
        long startTime = System.currentTimeMillis();
        Sort.heapSort(array);
        long endTime = System.currentTimeMillis();
        System.out.println("堆排序耗费时间："+ (endTime - startTime));
    }

    public static void testQuickSort(int[] array) {
        array = Arrays.copyOf(array,array.length);
        long startTime = System.currentTimeMillis();
        Sort.quickSort1(array);
        long endTime = System.currentTimeMillis();
        System.out.println("快速排序耗费时间："+ (endTime - startTime));
    }
    public static void main(String[] args) {
        int[] array = new int[10_0000];
        notOrderArray(array);

        testInsertSort(array);
        testShellSort(array);
        testSelectSort(array);
        testQuickSort(array);
//        Sort.shellSort(array);

    }
    //数组中的值 是乱序的
    public static void notOrderArray(int[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100_0000);
        }
    }
    //数组中的值 是有序的
    public static void orderArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
            //array[i] = array.length-i;
        }
    }
    public static void main1(String[] args) {
        Student student1 = new Student();
        Student student2 = new Student();
        student1.age = 10;
        student1.name = "wangwu";
        student2.age = 11;
        student2.name = "zhangsan";
        AgeComparator ageComparator = new AgeComparator();
        System.out.println(ageComparator.compare(student1,student2));
        NameComparator nameComparator = new NameComparator();
        System.out.println(nameComparator.compare(student1,student2));
    }
}
class AgeComparator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        return o2.age - o1.age;
    }
}
class NameComparator implements Comparator<Student>{
    @Override
    public int compare(Student o1, Student o2) {
        return o1.name.compareTo(o2.name);
    }
}
class Student implements Comparable<Student>{
    public int age;
    public String name;
    //只是比较对象的大小关系 返回值为正数 、 负数 、 0
    @Override
    public int compareTo(Student o) {
        return this.age - o.age;
    }
}