package demoSort;

import java.util.Stack;

public class Sort {
    /**
     * 时间复杂度：
     *  最好情况下：O(n)  ->  数据有序的情况下  1 2 3 4 5
     *  最坏情况下：O(n^2) -> 数据逆序的情况下  5 4 3 2 1
     * 空间复杂度：O(1)
     * 稳定性：稳定的排序
     *    一个本身就稳定的排序 可以实现为不稳定
     *    但是一个本身就不稳定的排序 不可能实现为稳定的排序
     *
     * 当数据越有序的时候  直接插入排序的效率越高
     * @param array
     */
    public static void insertSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int tmp = array[i];
            int j = i - 1;
            for (; j >= 0; j--) {
                if(array[j] > tmp){
                    array[j + 1] = array[j];
                }else {
                    break;
                }
            }
            array[j + 1] = tmp;
        }
    }

    /**
     * 希尔排序
     * @param array
     */
    public static void shellSort(int[] array) {
        int gap = array.length;//7
        while (gap > 1) {
            gap /= 2;//1
            shell(array,gap);
        }
        //shell(array,1);
    }

    private static void shell(int[] array, int gap) {
        for (int i = gap; i < array.length; i++) {
            int tmp = array[i];
            int j = i - gap;
            for (; j >= 0; j-= gap) {
                if(array[j] > tmp){
                    array[j + gap] = array[j];
                }else {
                    break;
                }
            }
            array[j + gap] = tmp;
        }
    }
    /**
     * 选择排序
     * 时间复杂度：O(n^2) 不管你本身的数据 是有序还是无序  都是这个复杂度
     * 空间复杂度：O(1)
     * 稳定性：不稳定的排序
     * @param array
     */
    public static void selectSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if(array[j] < array[minIndex]){
                    minIndex = j;
                }
            }
            swap(array,i,minIndex);
        }
    }
    public static void selectSort2(int[] array) {
        int left = 0;
        int right = array.length - 1;
        while (left < right){
            int minIndex = left;
            int maxIndex = left;
            for (int i = left + 1; i <= right; i++) {
                if(array[i] > array[maxIndex]){
                    maxIndex = i;
                }
                if(array[i] < array[minIndex]){
                    minIndex = i;
                }
            }
            swap(array,left,minIndex);
            //假如maxIndex == left，left下标的值已经被换到了minIndex上去了
            if(maxIndex == left){
                maxIndex = minIndex;
            }
            swap(array,right,maxIndex);
            left++;
            right--;
        }

    }
    private static void swap(int[] array,int i,int j) {
        int tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }
    /**
     * 堆排序
     * 时间复杂度：O(n*logn)  对数据不敏感  不管有序无序都是这个表达式
     * 空间复杂度：O(1)
     * 稳定性：不稳定
     * @param array
     */
    public static void heapSort(int[] array) {
        createBigHeap(array);
        int end = array.length - 1;
        while (end > 0){
            swap(array,0,end);
            siftDown(array,0,end);
            end--;
        }
    }
    private static void createBigHeap(int[] array) {
        for (int i = (array.length - 1 - 1) / 2; i >= 0; i--) {
            siftDown(array,i,array.length);
        }
    }
    private static void siftDown(int[] array,int parent,int len) {
        int child = 2 * parent + 1;
        while (child < len){
            if(child + 1 < len && array[child + 1] > array[child]){
                child++;
            }
            if(array[child] > array[parent]){
                swap(array,child,parent);
                parent = child;
                child = 2 * parent + 1;
            }else {
                break;
            }
        }
    }
    /**
     * 冒泡排序
     * 时间复杂度：O(N^2) 对数据不敏感  有序 无序都是这个复杂度！
     * 空间负责度：O(1)
     * 稳定性：稳定的排序
     *     插入排序   冒泡排序
     *
     * 加了优化之后，时间复杂度可能会变成O(n)
     * @param array
     */
    public static void bubbleSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            boolean flg = false;
            for (int j = 0; j < array.length - 1 - i; j++) {
                if(array[j] > array[j + 1]){
                    swap(array,j,j+1);
                    flg = true;
                }
            }
            if (flg == false){
                break;
            }
        }
    }
    /**
     * hoare法
     * @param array
     * @param left
     * @param right
     * @return
     */
    private static int parttion1(int[] array,int left,int right) {
        int index = left;
        int tmp = array[left];
        while (left < right){
            while(left < right && array[right] >= tmp){//找比tmp小的
                right--;
            }
            //todo: 先检查前面的会不会有问题
            while (left < right && array[left] <= tmp) {//找比tmp大的
                left++;
            }
            swap(array,left,right);
        }
        swap(array,index,left);
        return left;
    }
    /**
     * 挖坑法
     * @param array
     * @param left
     * @param right
     * @return
     */
    private static int parttion2(int[] array,int left,int right) {
        int tmp = array[left];
        while (left < right){
            while(left < right && array[right] >= tmp){//找比tmp小的
                right--;
            }
            array[left] = array[right];
            //todo: 先检查前面的会不会有问题
            while (left < right && array[left] <= tmp) {//找比tmp大的
                left++;
            }
            array[right] = array[left];
        }
        array[left] = tmp;
        return left;
    }

    /**
     * 前后指针法
     * @param array
     * @param left
     * @param right
     */
    private static int parttion(int[] array,int left,int right) {
        int prev = left;
        int cur = left+1;
        while (cur <= right){
            if (array[cur] < array[left] && array[++prev] != array[cur]){
                swap(array,prev,cur);
            }
            cur++;
        }
        swap(array,left,prev);
        return prev;
    }
    /**
     * 三数取中法
     * @param array
     * @param left
     * @param right
     * @return 返回中间数字的下标
     */
    private static int threeNum(int[] array,int left,int right) {
        int mid = left + (right - left) / 2;
        if(array[left] < array[right]){
            if(array[mid] < array[left]){
                return left;
            }else if (array[right] < array[mid]){
                return right;
            }else {
                return mid;
            }
        }else {
            if(array[right] > array[mid]){
                return right;
            }else if (array[mid] > array[left]){
                return left;
            }else {
                return mid;
            }
        }
    }
    public static void insertSort2(int[] array,int left,int right) {
        for (int i = left + 1; i <= right; i++) {
            int tmp = array[i];
            int j = i - 1;
            for (; j >= left; j--) {
                if(array[j] > tmp){
                    array[j + 1] = array[j];
                }else {
                    break;
                }
            }
            array[j + 1] = tmp;
        }
    }
    private static void quick(int[] array,int start,int end) {
        if(start >= end){
            return;
        }
//        System.out.println("start: "+start);
//        System.out.println("end: "+end);
/*        if(end - start +1 <= 20) {//数据很多的时候，最后几层占据的数据非常多，最后几层用插入排序会减少递归
            //直接插入排序
            insertSort2(array,start,end);
            return;
        }*/
        //三数取中
        int mid = threeNum(array,start,end);
        //交换
        swap(array,mid,start);
        int pivot = parttion(array,start,end);
        quick(array,start,pivot - 1);//左树
        quick(array,pivot + 1,end);//右数
    }
    /**
     * 时间复杂度：
     *     O(n*logN)[最好情况了]   O(N^2)[数据是有序的 或者是逆序的 ]
     *
     * 空间复杂度：O(logN)[好的情况]  O(n) [不好的情况]
     * 稳定性：不稳定排序
     * @param array
     */
    public static void quickSort(int[] array) {
        quick(array,0,array.length - 1);
    }
    /**
     * 非递归实现快速排序
     * @param array
     */
    public static void quickSort1(int[] array) {
        Stack<Integer> stack = new Stack<>();

        int start = 0;
        int end = array.length-1;
        if(end - start +1 <= 20) {
            //直接插入排序
            insertSort2(array,start,end);
            return;
        }
        //三数取中
        int mid = threeNum(array,start,end);
        //交换
        swap(array,mid,start);
        //找基准
        int pivot = parttion(array,start,end);

        if(start + 1< pivot){
           stack.push(start);
           stack.push(pivot - 1);
        }
        if(pivot < end - 1){
            stack.push(pivot + 1);
            stack.push(end);
        }
        while (!stack.isEmpty()){
            if(end - start +1 <= 20) {//此优化仅仅是减少了循环的次数，
                //直接插入排序
                insertSort2(array,start,end);
                //return; 这个地方不能return
            }else {
                end = stack.pop();
                start = stack.pop();
                //三数取中
                mid = threeNum(array,start,end);
                //交换
                swap(array,mid,start);
                //找基准
                pivot = parttion(array,start,end);

                if(start + 1< pivot){
                    stack.push(start);
                    stack.push(pivot - 1);
                }
                if(pivot < end - 1){
                    stack.push(pivot + 1);
                    stack.push(end);
                }
            }
        }
    }

    //归并排序 --> 递归 + 合并
    public static void mergeSort(int[] array){
        mergeSortFunc(array,0,array.length - 1);

    }
    private static void mergeSortFunc(int[] array,int left,int right) {
        if(left >= right){
            return;
        }
        int mid = left + (right - left)/2;
        mergeSortFunc(array,left,mid);
        mergeSortFunc(array,mid + 1,right);

        merge(array,left,mid,right);
    }
    private static void merge(int[] array,int left,int mid,int right){
        int[] tmp = new int[right - left + 1];
        int start1 = left;
        int end1 = mid;
        int start2 = mid + 1;
        int end2 = right;
        int index = 0;//临时数组的下标
        while (start1 <= end1 && start2 <= end2){
            if(array[start1] <= array[start2]){
                tmp[index++] = array[start1++];
            }else{
                tmp[index++] = array[start2++];
            }
        }
        while (start1 <= end1){
            tmp[index++] = array[start1++];
        }
        while (start2 <= end2){
            tmp[index++] = array[start2++];
        }
        for (int i = 0; i < tmp.length; i++) {
            array[left + i] = tmp[i];
        }
    }

    //非递归 的 归并排序
    public static void mergeSort2(int[] array) {
        int gap = 1;
        while (gap <= array.length){
            for (int i = 0; i < array.length; i = i + 2 * gap) {
                int left = i;
                int mid = i + gap - 1;
                int right = mid + gap;
                //防止mid和right越界
                if(mid >= array.length){
                    mid = array.length - 1;
                }
                if(right >= array.length){
                    right = array.length - 1;
                }
                merge(array,left,mid, right);
            }
            gap *= 2;
        }
    }
    /**
     * 计数排序
     * 时间复杂度：O(范围+n)   范围越小复杂度越快--》计数排序适合于 给定范围且范围不大
     * 空间复杂度：O(范围)
     * 稳定性：TODO
     * @param array
     */
    public static void countArray(int[] array) {
        //1、找到数组当中的最大值和最小值 O(N)
        int minVal = array[0],maxVal = array[0];
        for (int i = 1; i < array.length; i++) {
            if(array[i] > maxVal){
                maxVal = array[i];
            }
            if(array[i] < minVal){
                minVal = array[i];
            }
        }
        //2、可以确定计数数组的大小  O(N)
        int range = maxVal - minVal + 1;
        int[] count = new int[range];
        //3、再次遍历原来的数组 把原来的数据 和 计数数组的下标进行对应，来计数
        for (int i = 0; i < array.length; i++) {
            count[array[i] - minVal]++;
        }
        //4、上述循环走完 计数数组已经存好了对应关系 ，遍历计数数组  O(范围+n)
        int index = 0;//array数组下标
        for (int i = 0; i < count.length; i++) {
            int val = count[i];
            while (val-- > 0){
                array[index++] = i + minVal;
            }
        }
    }

    public static void main(String[] args) {
        int[] array = {1,5,2,9,3,6,1,1};
//        testInsertSort(array);
//        Sort.shellSort(array);
//        selectSort2(array);
//        heapSort(array);
        countArray(array);
    }
}
