package demoList1;

import java.util.Arrays;

public class SeqList {
    private int[] elem;
    private int usedSize;//记录当前顺序表当中 有多少个有效的数据
    private static final int DEFAULT_CAPACITY = 5;

    public SeqList() {
        this.elem = new int[DEFAULT_CAPACITY];
    }
    // 打印顺序表，注意：该方法并不是顺序表中的方法，为了方便看测试结果给出的
    public void display() {
        for (int i = 0; i < this.usedSize; i++) {
            System.out.print(this.elem[i] + " ");
        }
        System.out.println();
    }
    // 新增元素,默认在数据 最后新增
    public void add(int data) {
        if (isFull()){
            resize();
        }
        this.elem[this.usedSize] = data;
        this.usedSize++;
    }

    private boolean isFull() {
        return this.usedSize == elem.length;
    }
    // 判定是否包含某个元素
    public boolean contains(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if (toFind == elem[i]){
                return true;
            }
        }
        return false;
    }
    // 查找某个元素对应的位置
    public int indexOf(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if (toFind == elem[i]){
                return i;
            }
        }
        return -1;
    }

    // 获取 pos 位置的元素
    public int get(int pos)  {
        if (!checkPos(pos)){
            throw new PosOutBoundsException("get 获取数据时 位置不合法");
        }
        return elem[pos];
    }
    // 获取顺序表长度
    public int size() {
        return this.usedSize;
    }
    // 给 pos 位置的元素设为 value【更新的意思 】
    public void set(int pos, int value) {
        if (!checkPos(pos)){
            throw new PosOutBoundsException("set 获取数据时 位置不合法");
        }
        this.elem[pos] = value;
    }
    //检查pos下表是否 合法
    private boolean checkPos(int pos){
        if (pos < 0||pos >= this.usedSize){
            return false;
        }
        return true;
    }
    // 在 pos 位置新增元素
    public void add(int pos, int data) {
        if (pos < 0||pos > this.usedSize){
            throw new PosOutBoundsException("add数据时 位置不合法");
        }
        if (isFull()){
            resize();
        }
        int index = this.usedSize;
        while (index > pos){//挪数据
            this.elem[index] = this.elem[index-1];
            index--;
        }
        this.elem[pos] = data;//存数据
        this.usedSize++;
    }
    private void resize() {
        elem = Arrays.copyOf(elem,2*elem.length);
    }
    //删除第一次出现的关键字key
    public void remove(int toRemove) {
        if (isEmpty()){
            return;
        }
        int index = indexOf(toRemove);//确定数组所在的下标
        if (index == -1){
            return;//没有你要删除的数字
        }
        for (int i = index; i < this.usedSize - 1; i++) {
            this.elem[index] = this.elem[index + 1];
        }
        this.usedSize--;
    }
    public Boolean isEmpty(){
        return usedSize == 0;
    }
    // 清空顺序表
    public void clear() {
        this.usedSize = 0;
    }
}

