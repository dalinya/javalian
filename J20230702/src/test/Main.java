package test;

import java.util.List;
import java.util.Scanner; public class Main {    //用回溯算法求出所有组合
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        char[] ss = sc.next().toCharArray();
        backtracking(ss,0,"");
    }
    int midNum(int[] a,int s1,int t1,int b[],int s2,int t2){
        if(s1 == t1 && s2 == t2){//两个数返回小的那个
            return a[s1] < b[s2] ? a[s1] : b[s2];
        }
        int mid1 = (s1 + t1)/2;
        int mid2 = (s2 + t2)/2;
        if(a[mid1] == b[mid2]){
            return a[mid1];//两个中间值相等，返回其中一个
        }
        if(a[mid1] < b[mid2]){//a的后半部分，b的前半部分
            if((t1 - s1 + 1) % 2 == 0){//前者为偶数
                mid1++;
            }
            return midNum(a,mid1,t1,b,s2,mid2);
        }
        //a的前半部分，b的后半部分
        if((t2 - s2 + 1) % 2 == 0){//为偶数个数
            mid2++;
        }
        return midNum(a,s1,mid1,b,mid2,t2);
    }
    private static void backtracking(char[] ss,int index,String path) {
        if (index == ss.length){
            System.out.print("(" + path + ")");
            return;
        }
        backtracking(ss,index +1, path);
        backtracking(ss,index+1,path + ss[index]);
    }
    //斐波那契数列n为个数
    public int fib(int n) {
        if (n <= 1) return n;
        int[] dp = new int[n + 1];
        dp[0] = 0;
        dp[1] = 1;
        for (int index = 2; index <= n; index++){
            dp[index] = dp[index - 1] + dp[index - 2];
        }
        return dp[n];
    }
    //最大连续子序列和(动规版本)
    public static int maxSubArray1(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }

        int res = nums[0];
        int[] dp = new int[nums.length];
        dp[0] = nums[0];
        for (int i = 1; i < nums.length; i++) {
            dp[i] = Math.max(dp[i - 1] + nums[i], nums[i]);
            res = res > dp[i] ? res : dp[i];
        }
        return res;
    }
    //最大连续子序列和(贪心版本)
    public int maxSubArray2(int[] nums) {
        if (nums.length == 1){
            return nums[0];
        }
        int sum = Integer.MIN_VALUE;
        int count = 0;
        for (int i = 0; i < nums.length; i++){
            count += nums[i];
            sum = Math.max(sum, count); // 取区间累计的最大值（相当于不断确定最大子序终止位置）
            if (count <= 0){
                count = 0; // 相当于重置最大子序起始位置，因为遇到负数一定是拉低总和
            }
        }
        return sum;
    }

    //自顶向下
    public int minimumTotal1(int[][] triangle) {
        for(int i = 1;i < triangle.length;i++){
            for(int j = 0;j <= i;j++){
                if(j == 0){
                    triangle[i][j] += triangle[i-1][j];
                }else if (j == i){
                    triangle[i][j] += triangle[i-1][j-1];
                }else {
                    triangle[i][j] += Math.min(triangle[i - 1][j],triangle[i - 1][j - 1]);
                }
            }
        }
        int result = triangle[triangle.length - 1][0];
        for(int i = 1;i < triangle.length;i++){
            result = Math.min(result,triangle[triangle.length - 1][i]);
        }
        return result;
    }
    //自底向上
    public int minimumTotal2(int[][] triangle) {
        for(int i = triangle.length - 2;i >= 0;i--){
            for(int j = 0;j <= i;j++){
                triangle[i][j] = Math.min(triangle[i+1][j],triangle[i+1][j+1]);
            }
        }
        return triangle[0][0];
    }
}
