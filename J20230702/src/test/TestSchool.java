package test;


import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class TestSchool {
    /**
     * hoare法
     * @param array
     * @param left
     * @param right
     * @return
     */
    private static int parttion1(int[] array,int left,int right) {
        int index = left;
        int tmp = array[left];
        while (left < right){
            while(left < right && array[right] >= tmp){//找比tmp小的
                right--;
            }
            //todo: 先检查前面的会不会有问题
            while (left < right && array[left] <= tmp) {//找比tmp大的
                left++;
            }
            swap(array,left,right);
        }
        swap(array,index,left);
        count1++;
        return left;
    }
    private static void quick1(int[] array,int start,int end) {
        if (start >= end) {
            return;
        }
        //找基准
        int pivot = parttion1(array,start,end);
        quick1(array,start,pivot - 1);//左树
        quick1(array,pivot + 1,end);//右数
    }
    /**
     * 挖坑法
     * @param array
     * @param left
     * @param right
     * @return
     */
    private static int parttion2(int[] array,int left,int right) {
        int tmp = array[left];
        while (left < right){
            while(left < right && array[right] >= tmp){//找比tmp小的
                right--;
            }
            array[left] = array[right];
            //todo: 先检查前面的会不会有问题
            while (left < right && array[left] <= tmp) {//找比tmp大的
                left++;
            }
            array[right] = array[left];
        }
        array[left] = tmp;
        count2++;
        return left;
    }
    private static void quick2(int[] array,int start,int end) {
        if (start >= end) {
            return;
        }
        //找基准
        int pivot = parttion2(array,start,end);
        quick2(array,start,pivot - 1);//左树
        quick2(array,pivot + 1,end);//右数
    }
    /**
     * 前后指针法
     * @param array
     * @param left
     * @param right
     */
    private static int parttion3(int[] array,int left,int right) {
        int prev = left - 1;
        int cur = left;
        while (cur < right){
            if (array[cur] < array[right] && array[++prev] != array[cur]){
                swap(array,prev,cur);
            }
            cur++;
        }
        swap(array,right,prev + 1);
        count3++;
        return prev + 1;
    }
    private static void quick3(int[] array,int start,int end) {
        if (start >= end) {
            return;
        }
        //找基准
        int pivot = parttion3(array,start,end);
        quick3(array,start,pivot - 1);//左树
        quick3(array,pivot + 1,end);//右数
    }
    private static void swap(int[] array,int i,int j) {
        int tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }
    public static int count1 = 0;
    public static int count2 = 0;
    public static int count3 = 0;

    public static void main(String[] args) {
        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            int[] arr = new int[i + 10];
            for (int j = 0;j < i + 10;j++){
                arr[j] = random.nextInt(10000) + 10;
            }
            int[] arr2 = new int[arr.length];
            int[] arr3 = new int[arr.length];
            copyOfArray(arr2,arr);
            copyOfArray(arr3,arr);
            count1 = 0;
            quick1(arr,0,arr.length-1);
            count2 = 0;
            quick2(arr2,0,arr.length-1);
            count3 = 0;
            quick3(arr3,0,arr.length-1);
            System.out.println(i + 10 + " " + count1 + " " + count2 + " " + count3);
        }
        Map<Integer,Integer> map = new HashMap<>();
        map.getOrDefault(0,0);

    }

    private static void copyOfArray(int[] arr2, int[] arr) {
        for (int i = 0;i < arr.length;i++){
            arr2[i] = arr[i];
        }
    }

}
