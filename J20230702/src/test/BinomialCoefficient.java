package test;

import java.util.Scanner;

public class BinomialCoefficient {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        if (n < 0 || n > 20 || m < 0 || m > n) {
            System.out.println("error");
        } else {
            System.out.println(binomialCoefficient(n, m));
        }
    }

    public static int binomialCoefficient(int n, int m) {
        int result = 1;
        for (int i = 0; i < m; i++) {
            result *= (n - i);
            result /= (i + 1);
        }
        return result;
    }
}
