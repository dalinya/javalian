package test.demoLinkedList;

public class Main {
     public class ListNode {
         int val;
         ListNode next;
         ListNode() {}
         ListNode(int val) { this.val = val; }
         ListNode(int val, ListNode next) { this.val = val; this.next = next; }
     }

    public ListNode removeElements(ListNode head, int val) {
        ListNode cur = head;
        ListNode prev = head;
        while (cur != null){
            if(cur.val == val){
                prev.next = cur.next;
            }else {
                prev = cur;
            }
            cur = cur.next;
        }
        if (head.val == val){
            head = head.next;
        }
        return head;
    }

    public ListNode reverseList(ListNode head) {
        if (head == null){
            return null;
        }
        ListNode cur = head.next;
        head.next = null;
        while (cur != null){
            ListNode curNext  = cur.next;
            cur.next = head;
            head = cur;
            cur = curNext;
        }
        return head;
    }
    public ListNode middleNode(ListNode head) {
        return null;
    }
}
