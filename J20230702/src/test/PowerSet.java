package test;

import java.util.ArrayList;
import java.util.Scanner;

public class PowerSet {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String s = scanner.next();
        scanner.close();

        ArrayList<String> powerSet = generatePowerSet(s, n);
        System.out.println(powerSet);
    }

    public static ArrayList<String> generatePowerSet(String s, int n) {
        ArrayList<String> powerSet = new ArrayList<>();
        generatePowerSetHelper(s, "", 0, n, powerSet);
        return powerSet;
    }

    private static void generatePowerSetHelper(String s, String current, int index, int n, ArrayList<String> powerSet) {
        if (index == n) {
            powerSet.add(current);
            return;
        }

        generatePowerSetHelper(s, current, index + 1, n, powerSet);
        generatePowerSetHelper(s, current + s.charAt(index), index + 1, n, powerSet);
    }
}
