package LiCoTest1;

import javafx.util.Pair;

import java.util.*;

class Solution51 {
    boolean[] col,dig1,dig2;
    List<List<String>> result;
    char[][] path;
    public List<List<String>> solveNQueens(int n) {
        col = new boolean[n];//是否在同一列
        dig1 = new boolean[2 * n];//是否主对角线
        dig2 = new boolean[2 * n];//是否次对角线
        result = new ArrayList<>();
        path = new char[n][n];
        for (int i = 0; i < n; i++) {
            Arrays.fill(path,'.');
        }
        dfs(0,n);
        return result;
    }

    private void dfs(int deep, int n) {
        if (deep == n){
            List<String> tmp = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                tmp.add(new String(path[i]));
            }
            result.add(tmp);
            return;
        }
        for (int i = 0; i < n; i++) {
            if (col[i] == false && dig1[i + deep] == false && dig2[i - deep + n] == false){
                col[i] = dig1[i + deep] = dig2[i - deep + n] = true;
                path[deep][i] = 'Q';
                dfs(deep + 1,n);
                //恢复现场
                path[deep][i] = '.';
                col[i] = dig1[i + deep] = dig2[i - deep + n] = false;
            }
        }
    }

}
class Solution36 {
    public boolean isValidSudoku(char[][] board) {
        boolean[][] row = new boolean[9][10];//判断第i行是否有某个数字
        boolean[][] col = new boolean[9][10];//判断第j列是否有某个数字
        boolean[][][] grad = new boolean[3][3][10];//判断3*3方格中是否有某个数字
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] != '.'){
                    int num = board[i][j] - '0';
                    if (row[i][num] == false && col[j][num] == false && grad[i/3][j/3][num] == false){
                        row[i][num] =  col[j][num] =  grad[i/3][j/3][num] = true;
                    }else {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
class Solution37 {
    boolean[][] row = new boolean[9][10];//判断第i行是否有某个数字
    boolean[][] col = new boolean[9][10];//判断第j列是否有某个数字
    boolean[][][] grad = new boolean[3][3][10];//判断3*3方格中是否有某个数字
    public void solveSudoku(char[][] board) {
        //初始化哈希数组
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] != '.'){
                    int num = board[i][j] - '0';
                    row[i][num] =  col[j][num] =  grad[i/3][j/3][num] = true;
                }
            }
        }
        dfs(board);
    }

    private boolean dfs(char[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == '.'){
                    //选数
                    for (int k = 1; k < 10; k++) {
                        if (row[i][k] == false && col[j][k] == false && grad[i/3][j/3][k] == false){
                            board[i][j] = (char) (k + '0');
                            row[i][k] = col[j][k] =  grad[i/3][j/3][k] = true;
                            if (dfs(board)){
                                return true;
                            }
                            board[i][j] = '.';
                            row[i][k] = col[j][k] =  grad[i/3][j/3][k] = false;
                        }

                    }
                    return false;//重点理解，代表上一个数选错了
                }
            }
        }
        return true;//重点理解，代表已经全部是数字了，已经填完了。
    }
}

class Solution79 {
    boolean[][] visits;
    int[][] nextP = {{0,1},{0,-1},{-1,0},{1,0}};//上下左右的方向数组
    public boolean exist(char[][] board, String word) {
        visits = new boolean[board.length][board[0].length];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if(board[i][j] == word.charAt(0)){
                    visits[i][j] = true;
                    if (dfs(board,word.toCharArray(),i,j,1)){
                        return true;
                    }
                    visits[i][j] = false;
                }
            }
        }
        return false;
    }

    private boolean dfs(char[][] board, char[] word, int x, int y, int index) {
        if (index == word.length){
            return true;
        }
        for (int k = 0; k < 4; k++) {
            int nextX = x + nextP[k][0];
            int nextY = y + nextP[k][1];
            if (nextX >= 0 && nextX < board.length
                && nextY >= 0 && nextY < board[0].length
                && visits[nextX][nextY] == false
                && board[nextX][nextY] == word[index]){
                visits[nextX][nextY] = true;
                if (dfs(board,word,nextX,nextY,index + 1)){
                    return true;
                }
                visits[nextX][nextY] = false;
            }
        }
        return false;
    }
}

class Solution1219 {
    int result = 0;
    int row,col;
    boolean[][] visits;
    int[][] nextP = {{0,1},{0,-1},{-1,0},{1,0}};//
    public int getMaximumGold(int[][] grid) {
        row = grid.length;col = grid[0].length;
        visits = new boolean[grid.length][grid[0].length];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] != 0){
                    visits[i][j] = true;
                    dfs(grid,i,j,grid[i][j]);
                    visits[i][j] = false;
                }
            }
        }
        return result;
    }

    private void dfs(int[][] grid, int x, int y, int sum) {
        if (sum > result){
            result = sum;
        }
        for (int i = 0; i < 4; i++) {
            int nextX = x + nextP[i][0];
            int nextY = y + nextP[i][1];
            if (nextX >= 0 && nextX < row
                && nextY >= 0 && nextY < col
                && !visits[nextX][nextY]
                && grid[nextX][nextY] != 0){
                visits[nextX][nextY] = true;
                dfs(grid,nextX,nextY,sum + grid[nextX][nextY]);
                visits[nextX][nextY] = false;
            }
        }
    }
}

class Solution980 {
    int count,result,row,col;
    int[][] nextP = {{0,1},{0,-1},{-1,0},{1,0}};
    boolean[][] visits;
    public int uniquePathsIII(int[][] grid) {
        row = grid.length;col = grid[0].length;visits = new boolean[row][col];
        for (int i = 0; i < row; i++) {//统计当前的可走位置的个数
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 0){
                    count++;
                }
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 1){
                    visits[i][j] = true;
                    dfs(grid,i,j,1);
                    visits[i][j] = false;
                }
            }
        }
        return result;
    }

    private void dfs(int[][] grid, int x, int y, int step) {
        if (grid[x][y] == 2){
            if (step == count + 2){
                result++;
            }
            return;
        }
        for (int i = 0; i < 4; i++) {
            int nextX = x + nextP[i][0];
            int nextY = y + nextP[i][1];
            if (nextX >= 0 && nextX < row
                && nextY >= 0 && nextY < col
                && visits[nextX][nextY] == false
                && (grid[nextX][nextY] == 0 || grid[nextX][nextY] == 2)){
                visits[nextX][nextY] = true;
                dfs(grid,nextX,nextY,step + 1);
                visits[nextX][nextY] = false;
            }
        }
    }

}
class Solution733 {
    int row,col,oldColor;
    int[][] nextP = {{0,1},{0,-1},{-1,0},{1,0}};
    public int[][] floodFill(int[][] image, int sr, int sc, int color) {
        row = image.length;col = image[0].length;oldColor = image[sr][sc];
        if (color == oldColor){
            return image;
        }
        image[sr][sc] = color;
        dfs(image,sr,sc,color);
        return image;
    }

    private void dfs(int[][] image, int sr, int sc, int color) {
        for (int i = 0; i < 4; i++) {
            int nextX = sr + nextP[i][0];
            int nextY = sc + nextP[i][1];
            if (nextX >= 0 && nextX < row
                && nextY >= 0 && nextY < col
                && image[nextX][nextY] == oldColor){
                image[nextX][nextY] = color;
                dfs(image,nextX,nextY,color);
            }
        }
    }
}

class Solution200 {
    boolean[][] visits;
    int row,col,result;
    int[][] nextP = {{0,1},{0,-1},{-1,0},{1,0}};
    public int numIslands(char[][] grid) {
        row = grid.length;col = grid[0].length;visits = new boolean[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 1 && !visits[i][j]){
                    result++;
                    dfs(grid,i,j);
                }
            }
        }
        return result;
    }

    private void dfs(char[][] grid, int x, int y) {
        for (int i = 0; i < 4; i++) {
            int nextX = x + nextP[i][0];
            int nextY = y + nextP[i][1];
            if (nextX >= 0 && nextX < row
                && nextY >= 0 && nextY < col
                && grid[nextX][nextY] == 1 && !visits[nextX][nextY]){
                visits[nextX][nextY] = true;
                dfs(grid,nextX,nextY);
            }
        }
    }
}
class Solution695 {
    int result,path,row,col;
    boolean[][] visits;
    int[][] nextP = {{0,1},{0,-1},{-1,0},{1,0}};
    public int maxAreaOfIsland(int[][] grid) {
        row = grid.length;col = grid[0].length;visits = new boolean[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 1 && !visits[i][j]){
                    path++;
                    dfs(grid,i,j);
                    path--;
                }
            }
        }
        return result;
    }

    private void dfs(int[][] grid, int x, int y) {
        if (result < path){
            result = path;
        }
        for (int i = 0; i < 4; i++) {
            int nextX = x + nextP[i][0];
            int nextY = y + nextP[i][1];
            if (nextX >= 0 && nextX < row
                && nextY >= 0 && nextY < col
                && grid[nextX][nextY] == 1
                && !visits[nextX][nextY]){
                path++;
                visits[nextX][nextY] = true;
                dfs(grid,nextX,nextY);
                path--;
            }
        }
    }
}

class Solution417 {
    int row,col;
    int[][] nextP = {{0,1},{0,-1},{-1,0},{1,0}};
    public List<List<Integer>> pacificAtlantic(int[][] heights) {
        row = heights.length;col = heights[0].length;
        boolean[][] pac = new boolean[row][col];
        boolean[][] atl = new boolean[row][col];
        for (int i = 0; i < row; i++) {//遍历第一列
            if (!pac[i][0]){
                dfs(heights,i,0,pac);
            }
        }
        for (int j = 1; j < col; j++) {//遍历第一行
            if (!pac[0][j]){
                dfs(heights,0,j,pac);
            }
        }
        for (int i = 0; i < row; i++) {//遍历最后一列
            if (!atl[i][col - 1] ){
                dfs(heights,i,col - 1,atl);
            }
        }
        for (int j = 1; j < col; j++) {//遍历最后一行
            if (!atl[row - 1][j]){
                dfs(heights,row - 1,j,atl);
            }
        }
        List<List<Integer>> result = new ArrayList<>();

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (pac[i][j] && atl[i][j]){
                    List<Integer> path = new ArrayList<>();
                    path.add(i);path.add(j);
                    result.add(path);
                }
            }
        }
        return result;
    }

    private void dfs(int[][] heights,int x, int y,boolean[][] visits) {
        visits[x][y] = true;
        for (int i = 0; i < 4; i++) {
            int nextX = x + nextP[i][0];
            int nextY = y + nextP[i][1];
            if (nextX >= 0 && nextX < row
                && nextY >= 0 && nextY < col
                && heights[nextX][nextY] >= heights[x][y]
                && !visits[nextX][nextY]){
                dfs(heights,nextX,nextY,visits);
            }
        }
    }
}
class Solution529 {
    int row,col;
    int[][] nextP = {{0,1},{0,-1},{-1,0},{1,0},{-1,-1},{-1,1},{1,-1},{1,1}};
    boolean[][] visits;
    public char[][] updateBoard(char[][] board, int[] click) {
        row = board.length;col = board[0].length;visits = new boolean[row][col];
        if (board[click[0]][click[1]] == 'M'){
            board[click[0]][click[1]] = 'X';
        }else {
            dfs(board,click[0],click[1]);
        }
        return board;
    }

    private void dfs(char[][] board, int i, int j) {
        visits[i][j] = true;
        int count = 0;
        for (int k = 0; k < 8; k++) {
            int nextX = i + nextP[k][0];
            int nextY = j + nextP[k][1];
            if (nextX >= 0 && nextY < row
                && nextY >= 0 && nextY < col
                && board[nextX][nextY] == 'M'){
                count++;
            }
        }
        if (count > 0){
            board[i][j] = (char) (count + '0');
        }else {
            board[i][j] = 'B';
            for (int k = 0; k < 8; k++) {
                int nextX = i + nextP[k][0];
                int nextY = j + nextP[k][1];
                if (nextX >= 0 && nextY < row
                    && nextY >= 0 && nextY < col
                    && !visits[nextX][nextY]){
                    dfs(board,nextX,nextY);
                }
            }
        }
    }
}

class SolutionLCR130 {
    int result,row,col,cnt;
    boolean[][] visits;
    int[][] nextP = {{0,1},{0,-1},{-1,0},{1,0}};
    public int wardrobeFinishing(int m, int n, int _cnt) {
        row = m;col=n;visits = new boolean[m][n];cnt = _cnt;
        visits[0][0] = true;
        dfs(0,0);
        return result;
    }

    private void dfs(int i, int j) {
        result++;
        System.out.println(i +" " + j + " " + (i + j));
        for (int k = 0; k < 4; k++) {
            int nextX = i + nextP[i][0];
            int nextY = j + nextP[i][1];
            if (nextX >= 0 && nextX < row
                && nextY >= 0 && nextY < col
                && !visits[nextX][nextY]){
                System.out.println(sumDigit(nextX) + sumDigit(nextY));
                if (sumDigit(nextX) + sumDigit(nextY) <= cnt){
                    visits[nextX][nextY] = true;
                    dfs(nextX,nextY);
                }

            }
        }
    }

    private int sumDigit(int a) {
        int sum = 0;
        while (a > 0){
            sum += a % 10;
            a /= 10;
        }
        return sum;
    }
}
class Solution509 {
    int[] memo;
    public int fib(int n) {
        memo = new int[n + 1];
        Arrays.fill(memo,-1);//初始化
        return dfs(n);
    }

    private int dfs(int n) {
        //每次查找的时候，往备忘录中找一下
        if (memo[n] != -1){//剪枝
            return memo[n];
        }
        if (n == 0 || n == 1){
            memo[n] = n;//返回之前放在备忘录里面
            return n;
        }
        memo[n] = dfs(n-1 ) + dfs(n-2);//返回之前放在备忘录里面
        return memo[n];
    }
}
class Solution62 {
    int[][] memo;
    public int uniquePaths(int m, int n) {
        memo = new int[m+1][n+1];
        for (int i = 0; i < m; i++) {
            Arrays.fill(memo[i],-1);
        }
        dfs(m,n);
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                System.out.print(memo[i][j] + " ");
            }
        }
        return memo[m][n];
    }

    private int dfs(int m, int n) {
        if (m == 1 || n == 1){
            memo[m][n] = 1;
        }
        if (memo[m][n]!=-1){
            return memo[m][n];
        }
        memo[m][n] = dfs(m-1,n) + dfs(m,n-1);
        return memo[m][n];
    }
}
class Solution300 {
    int[] memo,nums;
    public int lengthOfLIS(int[] _nums) {
        nums = _nums;memo = new int[nums.length];
        System.out.println(Arrays.toString(nums));
        Arrays.fill(memo,-1);
        int result = 0;
        for (int i = 0; i < nums.length; i++) {
            result = Math.max(result,dfs(i));
        }

        return result;
    }

    private int dfs(int pos) {
        if (memo[pos] != -1){
            return memo[pos];
        }
        int max = 1;
        for (int i = pos + 1; i < nums.length; i++) {
            if (nums[i] > nums[pos]){
                max = Math.max(max,dfs(i) + 1);
            }
        }
        memo[pos] = max;
        return memo[pos];
    }
}

class Solution375 {
    int[][] memo;//定义备忘录数组
    public int getMoneyAmount(int n) {
        memo = new int[n + 1][n + 1];
        for (int i = 0; i <= n; i++) {
            Arrays.fill(memo[i],-1);
        }
        int result = dfs(1,n);
        return result;
    }

    private int dfs(int left, int right) {
        if (left >= right){
            return 0;
        }
        if (memo[left][right] != -1){//每次查找的时候，往备忘录中找一下
            return memo[left][right];
        }
        int ret = Integer.MAX_VALUE;
        for (int i = left; i < right; i++) {
            //选择i做根节点
            int count = i + Math.max(dfs(left,i-1),dfs(i + 1,right));
            ret = Math.min(count,ret);
        }
        memo[left][right] = ret;//返回之前放在备忘录里面
        return memo[left][right];
    }
}

class Solution329 {
    int[][] meno,matrix;
    int row,col;
    boolean[][] visits;
    int[][] nextP = {{0,1},{0,-1},{-1,0},{1,0}};
    public int longestIncreasingPath(int[][] _matrix) {
        matrix = _matrix;row = matrix.length;col = matrix[0].length;
        meno = new int[row][col];visits = new boolean[row][col];
        int result = 0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                result = Math.max(dfs(i,j),result);
            }
        }
        return result;
    }

    private Integer dfs(int i, int j) {
        if (meno[i][j] != 0){//每次查找的时候，往备忘录中找一下
            return meno[i][j];
        }
        int result = 1;
        for (int k = 0; k < 4; k++) {
            int nextX = i + nextP[k][0];
            int nextY = j + nextP[k][1];
            if (nextX >= 0 && nextX < row
                && nextY >= 0 && nextY < col
                && matrix[nextX][nextY] > matrix[i][j]){
                if (!visits[nextX][nextY]){
                    visits[nextX][nextY] = true;
                    result = Math.max(result,1 + dfs(nextX,nextY));
                }else {
                    result = Math.max(result,1+meno[nextX][nextY]);
                }
            }
        }
        meno[i][j] = result;
        return meno[i][j];
    }
}
class Solution1137 {
    public int tribonacci(int n) {
        if (n == 0 || n == 1){
            return n;
        }
        int[] dp = new int[n + 1];
        dp[0] = 0;dp[1] = dp[2] = 1;
        for (int i = 3; i <= n; i++) {
            dp[i] = dp[i-1] + dp[i-2] + dp[i-3];
        }
        return dp[n];
    }
}
class Solution08_01 {
    public int waysToStep(int n) {
        if(n == 1 || n== 2){
            return n;
        }
        if(n == 3){
            return 4;
        }
        int MOD = (int)1e9 + 7;
        int[] dp = new int[n + 1];
        //初始化
        dp[1] = 1;dp[2] = 2;dp[3] = 4;
        for(int i = 4;i <= n;i++){
            dp[i] = ((dp[i-1] + dp[i-2]) % MOD + dp[i-3]) % MOD;
        }
        return dp[n];
    }
}
class Solution91 {
    public int numDecodings(String ss) {
        char[] s = ss.toCharArray();
        if(s[0] == '0'){
            return 0;
        }
        int n = s.length;
        int[] dp = new int[n];
        //初始化
        if(s[0] >= '1' && s[0] <= '9'){
            dp[0] = 1;
        }
        if(n == 1){
            return dp[0];
        }
        if(s[1] >= '1' && s[1] <= '9'){
            dp[1] = 1;
            dp[1] += dp[0];
        }
        for(int i = 2;i < n;i++){
            if(s[i] >= '1' && s[i] <= '9'){//s[i] 可以解码
                dp[i] += dp[i-1];
            }
            int num = Integer.parseInt("" + s[i] + s[i-1]);
            if (s[i-1] != 0 && num > 0 && num <= 26){
                dp[i] += dp[i-2];
            }
        }
        System.out.println(Arrays.toString(dp));
        return dp[n-1];
    }
}

class Solution6222 {
    public int uniquePaths(int m, int n) {
        //引入虚节点，简化初始化。并保证后面填表的结果是正确的
        int[][] dp = new int[m+1][n+1];
        dp[0][1] = 1;
        for(int i = 1;i <= m;i++){
            for(int j = 1;j <= n;j++){
                dp[i][j] = dp[i-1][j] + dp[i][j-1];
            }
        }
        return  dp[m][n];
    }
}
class Solution166 {
    public int jewelleryValue(int[][] frame) {
        int m = frame.length;int n = frame[0].length;
        int[][] dp = new int[m + 1][n + 1];
        for(int i = 1;i <= m; i++){
            for(int j = 1;j <=n; j++){
                dp[i][j] = Math.max(dp[i-1][j],dp[i][j-1]) + frame[i-1][j-1];
            }
        }
        return dp[m][n];
    }
}
class Solution931 {
    public int minFallingPathSum(int[][] matrix) {
        int m = matrix.length,n = matrix[0].length;
        int[][] dp = new int[m+1][n+2];
        for(int i = 1;i <= m;i++){
            dp[i][0] = dp[i][n+1] = Integer.MIN_VALUE;
        }
        //状态转移
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                dp[i][j] = Math.max(Math.max(dp[i-1][j-1],dp[i-1][j]),dp[i-1][j+1]) + matrix[i-1][j-1];
            }
        }
        int result = Integer.MAX_VALUE;
        for (int j = 1; j <= n; j++) {
            result = Math.min(dp[m][j],result);
        }
        return result;
    }
}
class Solution64 {
    public int minPathSum(int[][] grid) {
        int m = grid.length,n = grid[0].length;
        int[][] dp = new int[m + 1][n + 1];
        for(int i = 0;i <= m;i++){
            dp[i][0] = Integer.MAX_VALUE;
        }
        for(int j = 0;j <= n;j++){
            dp[0][j] = Integer.MAX_VALUE;
        }
        dp[0][1] = 0;dp[1][0] = 1;
        for(int i = 1;i <= m;i++){
            for(int j = 1;j <= n;j++){
                dp[i][j] = Math.min(dp[i-1][j],dp[i][j-1]) + grid[i-1][j-1];
            }
        }
        return dp[m][n];
    }
}
class Solution174 {
    public int calculateMinimumHP(int[][] dungeon) {
        int m = dungeon.length,n = dungeon[0].length;
        int[][] dp = new int[m+1][n+1];
        //初始化
        for (int i = 0; i < m; i++) {
            dp[i][n] = Integer.MAX_VALUE;
        }
        Arrays.fill(dp[m],Integer.MAX_VALUE);
        dp[m-1][n] = dp[m][n-1] = 1;
        for (int i = m - 1; i >= 0; i--) {
            for (int j = n - 1; j >= 0; j--) {
                dp[i][j] = Math.min(dp[i+1][j],dp[i][j+1]) - dungeon[i][j];
                dp[i][j] = Math.max(1,dp[i][j]);//避免出现某个位置获得血量太多，导致dp[i][j]为负数
            }
        }
        return dp[0][0];
    }
}
class Solution17_16 {
    public int massage(int[] nums) {
        int n = nums.length;
        if(n==0){
            return 0;
        }
        int[] f = new int[n];//选择i位置的最大预约时长
        int[] g = new int[n];//不选择i位置的最大预约时长
        f[0] = nums[0];g[0] = 0;
        for(int i = 1;i < n;i++){
            f[i] = g[i-1] + nums[i];
            g[i] = Math.max(f[i-1],g[i-1]);
        }

        return Math.max(f[n-1],g[n-1]);
    }
}

//f[i] = g[i-1] + nums[i] 选择
//g[i] = min(g[i-1],f[i-1])不选择
class Solution740 {
    public int deleteAndEarn(int[] nums) {
        //预处理数组到arr中,然后进行打家劫舍
        int n = (int)1e4 + 1;
        int[] arr = new int[n];
        for(int num:nums){
            arr[num] += num;
        }
        int[] f = new int[n];//偷第i个
        int[] g = new int[n];//不偷第i个
        f[0] = arr[0];
        for(int i = 1;i < n;i++){
            f[i] = g[i-1] + arr[i];
            g[i] = Math.max(g[i-1],f[i-1]);
        }
        return Math.max(f[n-1],g[n-1]);
    }
}

class SolutionLCR091 {
    public int minCost(int[][] costs) {
        int n = costs.length;
        int[][] dp = new int[n + 1][3];
        for (int i = 1; i <= n; i++) {
            dp[i][0] = Math.max(dp[i-1][1],dp[i-1][2]) + costs[i-1][0];
            dp[i][1] = Math.max(dp[i-1][0],dp[i-1][2]) + costs[i-1][1];
            dp[i][2] = Math.max(dp[i-1][0],dp[i-1][1]) + costs[i-1][2];
        }
        return Math.max(dp[n][0],Math.max(dp[n][1],dp[n][2]));
    }
}
class Solution309 {
    public int maxProfit(int[] prices) {
        int n = prices.length;
        int[][] dp = new int[n][3];
        dp[0][0] = -prices[0];//表示第0天结束处于买入状态
        dp[0][1] = 0;//表示第0天结束处于可交易状态 --> 啥也不干
        dp[0][2] = 0;//表示第0天结束处于冷冻期 --> 买了之后，又卖出
        for (int i = 1; i < n; i++) {
            dp[i][0] = Math.max(dp[i-1][0],dp[i-1][1] - prices[i]);
            dp[i][1] = Math.max(dp[i-1][1],dp[i-1][2]);
            dp[i][2] = dp[i-1][0] + prices[i];
        }
        return Math.max(dp[n-1][0],Math.max(dp[n-1][1],dp[n-1][2]));
    }
}

class Solution714 {
    public int maxProfit(int[] prices, int fee) {
        int n = prices.length;
        int[] f = new int[n];//持有股票
        int[] g = new int[n];//不持有股票
        f[0] = -prices[0];//初始化
        for(int i = 1; i < n;i++){
            f[i]  = Math.max(f[i-1],g[i-1]-prices[i]);
            g[i] = Math.max(g[i-1],f[i-1] + prices[i] - fee);
        }
        return g[n-1];//最后一天一定是不持有股票大于持有股票
    }
}

class Solution188 {
    public int maxProfit(int k, int[] prices) {
        int n = prices.length;
        k = Math.min(k,n/2);
        int[][] f = new int[n][k + 1];//持有股票
        int[][] g = new int[n][k + 1];//不持有股票
        //初始化
        int INF = (int)-0x3f3f3f3f;
        for(int j = 0;j <= k;j++){
            f[0][j] = g[0][j] = INF;
        }
        f[0][0] = -prices[0];g[0][0] = 0;
        for(int i = 1;i < n;i++){
            for(int j = 0;j <= k;j++){
                f[i][j] = Math.max(f[i-1][j],f[i-1][j] - prices[i]);
                g[i][j] = g[i-1][j];
                if(j - 1 >= 0){
                    g[i][j] = Math.max(g[i][j],f[i-1][j-1] + prices[i]);
                }
                System.out.print(g[i][j] + " ");
            }
            System.out.println();
        }
        int result = 0;
        for(int j = 0;j <= k;j++){
            result = Math.max(result,g[n-1][j]);
        }
        return result;
    }
}
class Solution53 {
    public int maxSubArray(int[] nums) {
        int n = nums.length;
        int[] dp = new int[n + 1];
        for (int i = 1; i <= n; i++) {
            dp[i] = Math.max(nums[i-1],dp[i-1] + nums[i-1]);
        }
        int result = Integer.MIN_VALUE;
        for (int i = 1; i <= n; i++) {
            result = Math.max(result,dp[i]);
        }
        return result;
    }
}
class Solution928 {
    public int maxSubarraySumCircular(int[] nums) {
        int n = nums.length,sum = 0;
        int[] f = new int[n + 1];//以i结尾的最大连续子数组和
        int[] g = new int[n + 1];//以i结尾的最小连续子数组和
        //初始化
        f[0] = 0;g[0] = 0;
        for (int i = 1; i <= n; i++) {
            f[i] = Math.max(nums[i-1],nums[i-1] + f[i-1]);
            g[i] = Math.min(nums[i-1],nums[i-1] + g[i-1]);
            sum += nums[i-1];
        }
        int min = 0,max = 0;
        for (int i = 1; i <= n; i++) {
            max = Math.max(f[i],max);
            min = Math.min(g[i],min);
        }
        return Math.max(max,sum - min);
    }
}
class Solution152 {
    public int maxProduct(int[] nums) {
        int n = nums.length;
        int[] f = new int[n + 1];//以i结尾最大乘积
        int[] g = new int[n + 1];//以i结尾最小乘积
        //初始化
        f[0] = g[0] = 1;
        for (int i = 1; i <= n; i++) {
            f[i] = Math.max(nums[i-1],Math.max(nums[i-1] * f[i-1],nums[i-1]*g[i-1]));
            g[i] = Math.min(nums[i-1],Math.min(nums[i-1] * f[i-1],nums[i-1]*g[i-1]));
        }
        int result = Integer.MIN_VALUE;
        for (int i = 1; i <= n; i++) {
            result = Math.max(result,f[i]);
        }
        return result;
    }
}
class Solution413 {
    public int numberOfArithmeticSlices(int[] nums) {
        int n = nums.length;
        if (n <= 2){
            return 0;
        }
        int[] dp = new int[n];//以 i 位置元素为结尾的所有子数组中有多少个等差数列
        //初始化
        dp[0] = dp[1] = 0;
        int result = 0;
        for (int i = 2; i < n; i++) {
            dp[i] = (nums[i] - nums[i-1]) == (nums[i-1] - nums[i-2]) ? dp[i-1] + 1 : 0;
            result += dp[i];
        }
        return result;
    }
}

class Solution978 {
    public int maxTurbulenceSize(int[] arr) {
        int n = arr.length;
        int[] f = new int[n];
        int[] g = new int[n];
        //初始化
        Arrays.fill(f,1);
        Arrays.fill(g,1);
        for (int i = 1; i < n; i++) {
            if (arr[i] - arr[i- 1] > 0){
                f[i] = g[i - 1] + 1;
            }else if (arr[i] - arr[i-1] < 0){
                g[i] = f[i - 1] + 1;
            }
        }
        int result = 1;
        for (int i = 0; i < n; i++) {
            result = Math.max(result,f[i]);
            result = Math.max(result,g[i]);
        }
        return result;
    }
}
class Solution139 {
    public boolean wordBreak(String s, List<String> wordDict) {
        //将字典字符放到哈希表中
        HashSet<String> hashSet = new HashSet<>(wordDict);
        int n = s.length();
        boolean[] dp = new boolean[n + 1];//[0,i]区间内的字符串,能否被字典中的单词拼接而成
        dp[0] = true;//初始化
        s = " " + s;//预处理，不用考虑下表间的映射关系
        for (int i = 1; i <= n; i++) {
            for (int j = i; j >= 1; j--) {//设j为最后一个单词的起始位置的下标
                if (hashSet.contains(s.substring(j,i+1)) && dp[j-1] == true){
                    dp[i] = true;
                }
            }
        }
        return dp[n];
    }
}
class Solution467 {
    public int findSubstringInWraproundString(String ss) {
        char[] s = (" "+ss).toCharArray();//不用计算下标映射的操作
        int[] hash = new int[26];//去重：相同字符结尾的dp值,我们取最大的即可
        int n = s.length;
        int[] dp = new int[n];//以i位置的元素为结尾的所有的子串里面,有多少个在base中出现过
        Arrays.fill(dp,1);
        for (int i = 1; i < n; i++) {
            if (s[i-1] + 1 == s[i] || s[i-1] == 'z' && s[i] == 'a'){
                dp[i] += dp[i-1];
            }
            int index = s[i] - 'a';
            hash[index] = Math.max(hash[index],dp[i]);
        }
        int result = 0;
        for (int i = 0; i < hash.length; i++) {
            result += hash[i];
        }
        return result;
    }
}
class Solution300_2 {
    public int lengthOfLIS(int[] nums) {
        int n = nums.length;
        int[] dp = new int[n];
        Arrays.fill(dp,1);
        int result = 1;
        for (int i = 1; i < n; i++) {
            for (int j = 0; j < i; j++) {
                if(nums[j] < nums[i]){
                    dp[i] = Math.max(dp[i],dp[j] + 1);
                }
            }
            result = Math.max(result,dp[i]);
        }
        return result;
    }
}

class Solution376 {
    public int wiggleMaxLength(int[] nums) {
        int n = nums.length;
        int[] f = new int[n];
        int[] g = new int[n];
        Arrays.fill(f,1);
        Arrays.fill(g,1);
        for (int i = 1; i < n; i++) {
            for (int j = 0; j < i; j++) {
                if (nums[j] < nums[i]){
                    f[i] = Math.max(f[i],g[j] + 1);
                }else if (nums[j] > nums[i]){
                    g[i] = Math.max(g[i],f[j] + 1);
                }
            }
        }
        int ret = 1;
        for (int i = 1; i < n; i++) {
            ret = Math.max(ret,f[i]);
            ret = Math.max(ret,g[i]);
        }
        return ret;
    }
}

class Solution673 {
    public int findNumberOfLIS(int[] nums) {
        int n = nums.length;
        int[] len = new int[n];
        int[] count = new int[n];
        //初始化
        Arrays.fill(len,1);
        Arrays.fill(count,1);
        for (int i = 1; i < n; i++) {
            for (int j = 0; j < i; j++) {
                if (nums[j] < nums[i]){
                    if (len[j] + 1 == len[i]){
                        count[i] += count[j];
                    }else if (len[j] + 1 > len[i]){
                        len[i] = len[j] + 1;
                        count[i] = count[j];
                    }
                }
            }
        }
        //用小贪心法求返回值，最终结果
        int retLen = 1;
        int retCount = 1;
        for (int i = 1; i < n; i++) {
            if (len[i] == retLen){
                retCount += count[i];
            }else if (len[i] > retLen){
                retLen = len[i];
                retCount = count[i];
            }
        }
        return retCount;
    }
}

class Solution646 {
    public int findLongestChain(int[][] pairs) {
        Arrays.sort(pairs,(arr1,arr2)-> arr1[0] - arr2[0]);
        int n = pairs.length;
        int[] dp = new int[n];
        Arrays.fill(dp,1);
        for (int i = 1; i < n; i++) {
            for (int j = 0; j < i; j++) {
                if (pairs[j][1] < pairs[i][0]){
                    dp[i] = Math.max(dp[i],dp[j] + 1);
                }
            }
        }
        int result = 1;
        for (int i = 0; i < n; i++) {
            result = Math.max(result,dp[i]);
        }
        return result;
    }
}

class Solution1218 {
    public int longestSubsequence(int[] arr, int difference) {
        Map<Integer,Integer> hash = new HashMap<>();
        //<元素,dp[j]>
        //dp[i] --> 以i结尾所有所有子序列中，最长的定长子序列
        int ret = 0;
        for (int i = 0; i < arr.length; i++) {
            hash.put(arr[i],hash.getOrDefault(arr[i] - difference,0) + 1);
            ret = Math.max(ret,hash.get(arr[i]));
        }
        return ret;
    }
}

class Solution873 {
    public int lenLongestFibSubseq(int[] arr) {
        int n = arr.length;
        int[][] dp = new int[n][n];
        //<元素,下标>
        Map<Integer,Integer> hash = new HashMap<>();
        for (int i = 0; i < n; i++) {
            hash.put(arr[i],i);
        }
        for (int i = 0; i < n; i++) {
            Arrays.fill(dp[i],2);
        }
        int result = 2;
        for (int i = 1; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                int index = hash.getOrDefault(arr[j] - arr[i],-1);
                if (index > -1 && index < i){
                    dp[i][j] = Math.max(dp[i][j],dp[index][i] + 1);
                    result = Math.max(result,dp[i][j]);
                }
            }
        }
        return result < 3 ? 2 : result;
    }
}

class Solution283 {
    public void moveZeroes(int[] nums) {
        //双指针算法
        //[0,dest] --> 非0 [dest + 1, cur] --> 0.[cur + 1,n-1] 未处理
        int n = nums.length,dest = -1;
        for (int cur = 0; cur < n; cur++) {
            if (nums[cur] != 0){
                int tmp = nums[cur];
                nums[cur] = nums[++dest];
                nums[dest] = tmp;
            }
        }
    }
}
class Solution1809 {
    public void duplicateZeros(int[] arr) {
        int n = arr.length;
        //先找到最后一个复写的位置
        //dest : 最后一个要复写位置的下标
        int dest = -1,cur = 0;
        while (cur < n){
            if (arr[cur] == 0){
                dest += 2;
            }
            if (dest >= n - 1){
                break;
            }
            cur++;
        }
        //特殊处理：要复写的嘴壶一个位置是否越界
        if (dest == n){
            //已经越界了，一次走两个说明最后一个为0
            dest-=2;
            cur--;
            arr[n-1] = 0;
        }
        //进行复写操作
        while (cur >= 0){
            if (arr[cur] == 0){
                cur--;
                arr[dest--] = 0;
                arr[dest--] = 0;
            }else {
                arr[dest--] = arr[cur--];
            }
        }
    }
}

class Solution202 {
    public boolean isHappy(int n) {
        //快慢指针
        int slow = bitSum(n),fast = bitSum(bitSum(n));
        while (slow != fast){
            slow = bitSum(n);
            fast = bitSum(bitSum(n));
        }
        return slow == 1;
    }
    private int bitSum(int num){
        int ret = 0;
        while (num !=  0){
            ret = (int) Math.pow(num%10,2);
            num /= 10;
        }
        return ret;
    }
}
class Solution1027 {
    public int longestArithSeqLength(int[] nums) {
        int n = nums.length;
        int[][] dp = new int[n][n];
        for (int i = 0; i < n; i++) {
            Arrays.fill(dp[i],2);
        }
        //<元素,下标>
        Map<Integer,Integer> hash = new HashMap<>();
        hash.put(nums[0], 0);
        int ret = 2;
        for (int j = 2; j < n; j++) {
            for (int i = 1; i < j; i++) {
                Integer prev = 2 * nums[i] - nums[j];
                int index = hash.getOrDefault(prev,-1);
                if (index > -1 && index < i){
                    dp[i][j] = dp[index][i] + 1;
                    ret = Math.max(ret,dp[i][j]);
                }
                hash.put(nums[i],i);
            }
//            for (int j = i + 1; j < n; j++) {
//                Integer prev = 2 * nums[i] - nums[j];
//                if (hash.containsKey(prev)){
//                    dp[i][j] = dp[hash.get(prev)][i] + 1;
//                    ret = Math.max(ret,dp[i][j]);
//                }
//            }
//            hash.put(nums[i], i);
        }
        return ret;
    }
}
class Solution446 {
    public int numberOfArithmeticSlices(int[] nums) {
        int n = nums.length;
        Map<Long,List<Integer>> hash = new HashMap<>();
        for (int i = 0; i < n; i++) {
            long num = nums[i];
            if (!hash.containsKey(num)){
                hash.put(num,new ArrayList<>());
            }
            hash.get(num).add(i);
        }
        int[][] dp = new int[n][n];//以i,j结尾的等差子序列的长度
        int ret = 0;
        for (int i = 1; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                List<Integer> indexArr = hash.get(2 * nums[i] - nums[j]);
                if (indexArr != null){
                    for (int k = 0; k < indexArr.size(); k++) {
                        if (indexArr.get(k) >= i){
                            break;
                        }
                        int index = indexArr.get(k);
                        dp[i][j] += (dp[index][i] + 1);
                    }
                    ret += dp[i][j];
                }
            }
        }
        return ret;
    }
}

class Solution647 {
    public int countSubstrings(String ss) {
        char[] s = ss.toCharArray();
        int n = s.length;
        boolean[][] dp = new boolean[n][n];
        int ret = 0;
        for (int i = n - 1; i >= 0; i--) {
            for (int j = i; j < n; j++) {
                if ( s[i] == s[j] && (i == j || i + 1 == j ||dp[i+1][j-1] == true)){
                    dp[i][j] = true;
                    ret++;
                }
            }
        }
        return ret;
    }
}

class Solution5 {
    public String longestPalindrome(String ss) {
        char[] s = ss.toCharArray();
        int n = s.length;
        boolean[][] dp = new boolean[n][n];
        for (int i = n - 1; i >= 0; i--) {
            for (int j = i; j < n; j++) {
                if (s[i] != s[j]){
                    dp[i][j] = false;
                }else if (i == j || i + 1 == j){
                    dp[i][j] = true;
                }else {
                    dp[i][j] = dp[i + 1][j-1];
                }
            }
        }
        String ret = "";
        int len = 0;
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                if (dp[i][j] && len < (j - i + 1)){
                    ret = ss.substring(i,j + 1);//因为substring区间范围[i,j+1)
                }
            }
        }
        return ret;
    }
}
class Solution1745 {
    public boolean checkPartitioning(String ss) {
        char[] s = ss.toCharArray();
        int n = s.length;
        boolean[][] dp = new boolean[n][n];
        for (int i = n - 1; i >= 0; i--) {
            for (int j = i; j < n; j++) {
                if (s[i] != s[j]){
                    dp[i][j] = false;
                }else if (i == j || i + 1 == j){
                    dp[i][j] = true;
                }else {
                    dp[i][j] = dp[i + 1][j-1];
                }
            }
        }
        for (int i = 1; i < n; i++) {
            for (int j = i; j < n; j++) {
                if (dp[0][i-1] && dp[i][j] && dp[j + 1][n-1]){
                    return true;
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(dp[i][j] + " ");
            }
            System.out.println();
        }
        return false;
    }
}
class Solution132 {
    public int minCut(String ss) {
        char[] s = ss.toCharArray();
        int n = s.length;
        boolean[][] isPal = new boolean[n][n];//i --> j是否是回文串
        for (int i = n - 1; i >= 0; i--) {
            for (int j = i; j < n; j++) {
                if (s[i] != s[j]){
                    continue;
                }else if (i == j || i + 1 == j){
                    isPal[i][j] = true;
                }else{
                    isPal[i][j] = isPal[i + 1][j - 1];
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(isPal[i][j] + " ");
            }
            System.out.println();
        }
        int[] dp = new int[n];//s[0,i]区间上的最长的子串,最少分割次数
        Arrays.fill(dp,Integer.MAX_VALUE);
        dp[0] = 0;
        for (int i = 1; i < n; i++) {
            for (int j = 0; j <= i; j++) {
                if (isPal[j][i]){
                    if (dp[j] != Integer.MAX_VALUE){
                        dp[i] = Math.min(dp[i],dp[j] + 1);
                    }
                }
            }
        }
        return dp[n-1] == Integer.MAX_VALUE ? n : dp[n-1];
    }
}
class Solution516 {
    public int longestPalindromeSubseq(String ss) {
        char[] s = ss.toCharArray();
        int n = s.length;
        int[][] dp = new int[n][n];
        for (int i = n; i >= 0; i--) {
            for (int j = i; j < n; j++) {//两侧字符相等
                if (s[i]==s[j]){
                    if (i == j){
                        dp[i][j] = 1;
                    }else if (i + 1 == j){
                        dp[i][j] = 2;
                    }else {
                        dp[i][j] = dp[i+1][j-1] + 2;
                    }
                }else {//两侧字符不相等
                    dp[i][j] = Math.max(dp[i+1][j],dp[i][j-1]);
                }
            }
        }
        return dp[0][n-1];
    }
}
class Solution1312 {
    public int minInsertions(String ss) {
        char[] s = ss.toCharArray();
        int n = s.length;
        int[][] dp = new int[n][n];
        for (int i = n - 1; i >= 0; i--) {
            for (int j = i; j < n; j++) {
                if (s[i] == s[j]){
                    if (i + 1 < j){
                        dp[i][j] = dp[i+1][j-1];
                    }
                }else {
                    dp[i][j] = Math.min(dp[i+1][j],dp[i][j-1]) + 1;
                }
            }
        }
        return dp[0][n-1];
    }
}
class Solution1143 {
    public int longestCommonSubsequence(String text1, String text2) {
        int m = text1.length();
        int n = text2.length();
        char[] s1 = (" " + text1).toCharArray();
        char[] s2 = (" " + text2).toCharArray();
        int[][] dp = new int[m + 1][n + 1];
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (s1[i] == s2[j]){
                    dp[i][j] = dp[i-1][j-1]+1;
                }else {
                    dp[i][j] = Math.max(dp[i-1][j],dp[i][j-1]) + 1;
                }
            }
        }
        return dp[m][n];
    }
}
class Solution1035 {
    public int maxUncrossedLines(int[] nums1, int[] nums2) {
        int m = nums1.length, n = nums2.length;
        int[][] dp = new int[m + 1][n + 1];
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (nums1[i - 1] == nums2[j-1]) {
                    dp[i][j] = Math.max(dp[i - 1][j - 1] + 1, dp[i][j]);
                } else {
                    dp[i][j] = Math.max(dp[i][j - 1], dp[i - 1][j]);
                }
            }
        }
        return dp[m][n];
    }
}

class Solution115 {
    public int numDistinct(String ss, String tt) {
        int m = ss.length(),n = tt.length();
        char[] s = (" " + ss).toCharArray();
        char[] t = (" " + tt).toCharArray();
        //s字符串[0,j]区间内所有的子序列中,有多少个t字符串[0,i]区间内的子串(i-1为结尾，连续)
        int[][] dp = new int[m + 1][n + 1];
        //初始化dp
        Arrays.fill(dp[0],1);//t=""时，s[0,j]有一个字串
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (s[i] == t[j]){//包括i位置
                    dp[i][j] += dp[i-1][j-1];
                }
                dp[i][j] += dp[i-1][j];//不包括i位置
            }
        }
        return dp[m][n];
    }
}
class Solution44 {
    public boolean isMatch(String ss, String pp) {
        int m = ss.length();
        int n = pp.length();
        //1.定义动态规划方程 s[0,i-1]对应p[0,j-1]是否匹配
        boolean[][] dp = new boolean[m + 1][n + 1];
        char[] s = (" " + ss).toCharArray();
        char[] p = (" " + pp).toCharArray();
        //2.初始化
        dp[0][0] = true;//第一列除了(0,0)全为false
        for (int j = 1; j <= n; j++) {//第一行如果连续为*则为true
            if(p[j] == '*'){
                dp[j] = dp[j-1];
            }
        }
        //进行递推
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (s[i] == p[j] || p[j] == '?'){
                    dp[i][j] = dp[i-1][j-1];
                } else if (p[j] == '*'){
                    //代表匹配“” dp[i][j-1],匹配了一个但是不舍去匹配字符串，
                    dp[i][j] = dp[i][j-1] || dp[i-1][j];
                }
            }
        }
        return dp[m][n];
    }
}
class Solution10 {
    public boolean isMatch(String ss, String pp) {
        int m = ss.length(),n=pp.length();
        boolean[][] dp = new boolean[m + 1][n + 1];
        char[] s = (" " + ss).toCharArray(),p = (" " + pp).toCharArray();
        //第一列进行初始化。p为“”的时候，只有s为“”的时候才能匹配
        dp[0][0] = true;
        for (int j = 2; j <= n; j+=2) {//第一行进行初始化
            //当整个p子串中全是连续的.*时候，为true
            if (p[j] == '*'){
                dp[0][j] = dp[0][j-2];
            }
        }
        //进行方程的递归
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (s[i] == p[j] || p[j] == '.'){
                    dp[i][j] = dp[i-1][j-1];
                }else if (p[j] == '*'){
                    //dp[i][j] =  (s[i] == p[j - 1]||p[j - 1] == '.') && dp[i-1][j] || dp[i][j - 2];
                    if (dp[i][j - 2]){
                        //表示匹配“” dp[i][j - 2]
                        dp[i][j] = dp[i][j - 2];
                    }else if(s[i] == p[j - 1]||p[j - 1] == '.'){
                        //表示匹配了一个但不舍去匹配的字符串dp[i-1][j]
                        dp[i][j] =  dp[i-1][j];
                    }
                }
            }
        }
        return dp[m][n];
    }
}
class Solution97 {
    public boolean isInterleave(String ss1, String ss2, String ss3) {
        int m = ss1.length(), n = ss2.length();
        // 预处理：假如两个字符串长度相加不等于ss3的长度那么就肯定不能组成直接返回false;
        if (m + n != ss3.length()) {
            return false;
        }
        boolean[][] dp = new boolean[m + 1][n + 1];
        ss1 = " " + ss1;
        ss2 = " " + ss2;
        ss3 = " " + ss3;// 前面加个虚位置，不仅仅是初始化方方便，而且方便找到在s3中对应的值
        char[] s1 = ss1.toCharArray(), s2 = ss2.toCharArray(), s3 = ss3.toCharArray();
        // 初始化
        dp[0][0] = true;// 两个串合成的串也是空串
        for (int i = 1; i <= m; i++) {// 第一行ss1[0,i]必须和ss3[0,i]完全对应
            if (s1[i] == s3[i]) {
                dp[i][0] = dp[i-1][0];
            }
        }
        for (int j = 1; j <= n; j++) {// 第一列ss2[0,j]必须和ss3[0,j]完全对应
            if (s2[j] == s3[j]) {
                dp[0][j] = dp[0][j-1];
            }
        }
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                dp[i][j] = s1[i] == s3[i + j] && dp[i - 1][j] || s2[j] == s3[i + j] && dp[i][j - 1];
            }
        }

        return dp[m][n];
    }
}
class Solution712 {
    public int minimumDeleteSum(String ss1, String ss2) {
        //思路正难则反 --> 求两个字符串最大的连续子序列的ascii码值的和
        int m = ss1.length(),n = ss2.length();
        ss1 = " " + ss1;ss2 = " " + ss2;
        char[] s1 = ss1.toCharArray(),s2 = ss2.toCharArray();
        int[][] dp = new int[m + 1][n + 1];
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (s1[i] == s2[j]){//最大的ascii码和包括两个子字符串的最后一个字符
                    dp[i][j] = Math.max(dp[i][j],dp[i-1][j-1] + s1[i]);
                }else {
                    dp[i][j] = Math.max(dp[i-1][j],dp[i][j]);
                    dp[i][j] = Math.max(dp[i][j-1],dp[i][j]);
                }
            }
        }
        int sum = 0;
        for (int i = 1; i <= m; i++) {
            sum += s1[i];
        }
        for (int j = 1; j <= n; j++) {
            sum += s1[j];
        }
        return sum - 2 * dp[m][n];
    }
}
class Solution718 {
    public int findLength(int[] nums1, int[] nums2) {
        int m = nums1.length,n = nums2.length;
        int[][] dp = new int[m + 1][n + 1];
        int ret = 0;
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (nums1[i - 1] == nums2[j-1]){
                    dp[i][j] = dp[i-1][j-1] + 1;
                    ret = Math.max(ret,dp[i][j]);
                }

            }
        }
        return ret;
    }
}
class Solution416 {
    public boolean canPartition(int[] nums) {
        int sum = 0,n = nums.length;
        for (int i = 0; i < n; i++) {
            sum += nums[i];
        }
        if (sum % 2 == 1){//是奇数肯定不能分割为2份
            return false;
        }
        int aim = sum / 2;
        //是否恰好装满这个背包
        boolean[] dp = new boolean[aim + 1];
        dp[0] = true;
        for (int i = 1; i <= n; i++) {
            for (int j = aim; j >= nums[i-1]; j--) {
                //dp[j]:不选择第i个元素
                //dp[j - nums[i-1]]：选择第i个元素
                dp[j] = dp[j] || dp[j - nums[i-1]];
            }
        }
        return dp[aim];
    }
}
class Solution494 {
    public int findTargetSumWays(int[] nums, int target) {
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
        }
        if ((sum + target) % 2 != 0 || sum <= target){
            return 0;
        }
        int aim = (sum + target) / 2;
        int n = nums.length;
        int[] dp = new int[aim + 1];
        dp[0] = 1;
        for (int i = 0; i < n; i++) {
            for (int j = aim; j >= nums[i]; j--) {
                dp[j] += dp[j - nums[i]];
            }
        }
        return dp[aim];
    }
}
class Solution1049 {
    public int lastStoneWeightII(int[] stones) {
        int n = stones.length;
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += stones[i];
        }
        int aim = sum/2;
        int[] dp = new int[aim + 1];
        for (int i = 0; i < n; i++) {
            for (int j = aim; j >= stones[i]; j--) {
                dp[j] = Math.max(dp[j - stones[i]] + stones[i],dp[j]);
            }
        }
        return sum - 2 * dp[aim];
    }
}

class Solution322 {
    public int coinChange(int[] coins, int amount) {
        int len = coins.length;
        int[] dp = new int[amount + 1];
        Arrays.fill(dp,0x3f3f3f3f);
        dp[0] = 0;
        for (int i = 0; i < len; i++) {
            for (int j = coins[i]; j <= amount; j++) {
                dp[j] = Math.min(dp[j-coins[i]],dp[j]) + 1;
            }
        }
        return dp[amount] >= 0x3f3f3f3f ? -1 : dp[amount];
    }
}

class Solution518 {
    public int change(int amount, int[] coins) {
        int[] dp = new int[amount + 1];
        dp[0] = 1;
        for (int i = 0; i < coins.length; i++) {
            for (int j = coins[i]; j <= amount; j++) {
                dp[j] += dp[j - dp[i-coins[i]]];
            }
        }
        return dp[amount];
    }
}
class Solution279 {
    public int numSquares(int n) {
        int[] dp = new int[n + 1];
        Arrays.fill(dp,0x3f3f3f3f);
        dp[0] = 0;
        for (int i = 0; i*i <= n; i++) {
            for (int j = i * i; j <= n; j++) {
                dp[j] = Math.min(dp[j],dp[j - i * i] + 1);
            }
        }
        return dp[n] >= 0x3f3f3f3f ? 0 : dp[n];
    }

}
class Solution474 {
    public int findMaxForm(String[] strs, int m, int n) {
        int[][] dp = new int[m + 1][n + 1];
        int len = strs.length;
        for (int i = 1; i <= len; i++) {
            // 统计0和1的个数
            int zero = 0, one = 0;
            for (char ch : strs[i - 1].toCharArray()) {
                if (ch == '0') {
                    zero++;
                } else {
                    one++;
                }
            }
            for (int j = m; j >= zero; j--) {
                for (int k = n; k >= one; k--) {
                    dp[j][k] = Math.max(dp[j][k], dp[j - zero][k - one] + 1);
                }
            }

        }
        return dp[m][n];
    }
}
class Solution879 {
    public int profitableSchemes(int n, int minProfit, int[] group, int[] profit) {
        int INF = (int) (1e9 + 7);
        int[][] dp = new int[n + 1][minProfit + 1];

        int len = group.length;
        //初始化
        for (int i = 0; i <= n; i++) {
            dp[i][0] = 1;
        }
        for (int i = 0; i < len; i++) {
            for (int j = n; j >= group[i]; j--) {//遍历人数
                for (int k = minProfit; k >= 0; k--) {
                    dp[n][k] += dp[n-group[i]][Math.max(0,k - profit[i])];
                }
            }
        }
        return dp[n][minProfit];
    }
}
class Solution377 {
    public int combinationSum4(int[] nums, int target) {
        int[] dp = new int[target + 1];// 凑成总和为i，一共有多少种排列数
        dp[0] = 1;
        for (int i = 1; i <= target; i++) {
            for (int j = 0; j < nums.length; j++) {
                if (nums[j] >= i){
                    dp[i] += dp[i - nums[j]];
                }
            }
        }
        return dp[target];
    }
}
class Solution11 {
    public int maxArea(int[] height) {
        int left = 0,right = height.length - 1,result = 0;

        while (left < right){
            int w = right - left;//容量的底部
            int h = Math.min(height[left],height[right]);//容量的高
            result = Math.max(result,w * h);
            if (height[left] < height[right]){
                left++;
            }else {
                right--;
            }
        }
        return result;
    }
}
class Solution96 {
    public int numTrees(int n) {
        int[] dp = new int[n + 1];//结点个数为i的时候,一共有多少种二叉搜索树
        dp[0] = 1;//初始化
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j < i ; j++) {
                dp[i] += dp[j - 1] * dp[i - j];
            }
        }
        return dp[n];
    }
}
class Solution179 {
    public int[] twoSum(int[] price, int target) {
        int left = 0,right = price.length - 1;
        int[] result = new int[2];
        while (left < right){
            if (price[left] + price[right] > target){
                right--;
            }else if (price[left] + price[right] < target){
                left++;
            }else {
                result[0] = price[left];
                result[1] = price[right];
            }
        }
        return result;
    }
}
class Solution15 {
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        //对数组进行排序
        Arrays.sort(nums);
        //先固定一个最小
        int n = nums.length;
        for (int i = 0; i < n - 2; i++) {
            if (nums[i] > 0){//最小的一个数都大于0了，后面的数都是大于0的，后面的数无法构成一个负数
                //直接退出循环小优化
                break;
            }
            int left = i + 1,right = n - 1,target = - nums[i];
            while (left < right){
                int sum = nums[left] + nums[right];
                if (sum > target){
                    right--;
                }else if (sum < target){
                    left++;
                }else {//满足三数之和，直接放入
                    result.add(new ArrayList<>(Arrays.asList(nums[left],nums[right],nums[i])));
                    //进行移动
                    left++;right--;
                    //进行去重操作
                    while (left < right && nums[left] == nums[left-1]){
                        left++;
                    }
                    while (left <right && nums[right] == nums[right + 1]){
                        right--;
                    }
                }
            }
            while (i < n - 2 && nums[i] == nums[i+1]){//进行去重操作
                i++;
            }
        }
        return result;
    }
}

class Solution18 {
    public List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> result = new ArrayList<>();
        Arrays.sort(nums);
        //先固定两个数
        int len = nums.length;
        for (int i = 0; i < len; i++) {//固定第一个数
            for (int j = i + 1; j < len; j++) {//固定第二个数
                long aim =(long) target - nums[i] - nums[j];
                int left = j + 1,right = len - 1;
                while (left < right){
                    long sum = (long) nums[left] + nums[right];
                    if (sum > aim){
                        right--;
                    }else if (sum < aim){
                        left++;
                    }else {
                        result.add(new ArrayList<>(Arrays.asList(nums[i],nums[j],nums[left],nums[right])));
                        //进行去重操作
                        left++;
                        right--;
                        while (left < right && nums[left] == nums[left - 1]){
                            left++;
                        }
                        while (left < right && nums[right] == nums[right + 1]){
                            right--;
                        }
                    }
                }
                while (j < len - 1&& nums[j] == nums[j + 1]){
                    j++;
                }
            }
            while (i < len - 1 && nums[i] == nums[i+1]){//去重，避免四个数字中有一样的。
                i++;
            }
        }
        return result;
    }
}

class Solution209 {
    public int minSubArrayLen(int target, int[] nums) {
        int result = Integer.MAX_VALUE,sum = 0;
        int left = 0,right = 0,len = nums.length;
        while (right < len){
            sum += nums[right];//进窗口
            while (sum > target){//出窗口
                result = Math.min(result,right - left + 1);
                sum -= nums[left];
                left++;
            }
            right++;
        }
        return result;
    }
}

class Solution1004 {
    public int longestOnes(int[] nums, int k) {
        int result = 0;
        int left = 0,right = 0,len = nums.length,zeroNum = 0;
        while (right < len){
            if (nums[right] == 0){//进窗口
                zeroNum++;
            }
            while (zeroNum > k){//判断条件
                if (nums[left++] == 0){
                    zeroNum--;//出窗口
                }
            }
            result = Math.max(result,right - left + 1);
            right++;
        }
        return result;
    }
}
class Solution293 {
    public void moveZeroes(int[] nums) {
        //[0,dest] 非0，[dest+1,cur]0，[0,len-1]未处理
        int len = nums.length,dest = -1,cur = 0;
        while (cur < len){
            if (cur != 0){
                int tmp = nums[cur];
                nums[cur] = nums[dest];
                nums[dest++] = tmp;
            }
            cur++;
        }
    }
}

class Solution1089 {
    public void duplicateZeros(int[] arr) {
        //先进行移动确定最后一个要复写的位置
        int len = arr.length;
        int cur = 0,dest = -1;
        //dest 需要复写的最后一个数的位置
        //cur 复写后移动的位置
        while (cur < len){
            if (arr[cur] == 0){
                dest += 2;
            }else {
                dest++;
            }
            if (dest >= len - 1){
                break;
            }
            cur++;
        }
        if (dest == len){//说明最后一个数字是0，但是复写两个位置后越界了，我们需要回调
            dest -= 2;
            cur--;
            arr[len - 1] = 0;
        }
        //进行复写操作
        while (cur >= 0){
            if (arr[cur] == 0){
                arr[dest--] = 0;
                arr[dest--] = 0;
                cur--;
            }else {
                arr[dest--] = arr[cur--];
            }
        }
    }
}
class Solution1658 {
    public int minOperations(int[] nums, int x) {
        //思路转化 --> sum - x 的最长连续子数组
        int sum = 0,len = nums.length;
        for (int i = 0; i < len; i++) {
            sum += nums[i];
        }
        int aim = sum - x,result = 0;
        int left = 0,right = 0;
        sum = 0;
        while (right < len){
            sum += nums[right];//进窗口
            while (sum > aim){
                sum -= nums[left++];
            }
            if (sum == aim){
                result = Math.max(result,right - left + 1);
            }
            right++;
        }
        return result;
    }
}
class Solution904 {
    public int totalFruit(int[] fruits) {
        int len = fruits.length;
        int[] hash = new int[len];//表示第i个种类的个数
        int count = 0;//表示种类的个数
        int left = 0,right = 0;
        int result = 0;
        while (right < len){
            //进窗口
            int input = fruits[right];
            hash[input]++;
            if (hash[input] == 1){
                count++;
            }
            //判断条件
            while (count > 2){
                int output = fruits[left++];
                hash[output]--;
                if (hash[output] == 0){//出窗口
                    count--;
                }
            }
            if (count <= 2){//更新结果
                result = Math.max(result,result - left + 1);
            }
            right++;
        }
        return result;
    }
}

class Solution438 {
    public List<Integer> findAnagrams(String ss, String pp) {
        char[] s = ss.toCharArray(),p = pp.toCharArray();
        int[] hash1 = new int[26];
        int[] hash2 = new int[26];
        for (int i = 0; i < p.length; i++) {//用hashP和countP统计p字符串的字符出现情况
            hash1[p[i] - 'a']++;
        }
        List<Integer> result = new ArrayList<>();
        //count:有效字符的长度
        int len = s.length,left = 0,right = 0,count = 0;
        while (right < len){
            int in = s[right] - 'a';
            hash2[in]++;
            if (hash2[in] <= hash1[in]){//进窗口
                count++;
            }
            while (right - left + 1 > p.length){
                int out = s[left++] - 'a';
                hash2[out]--;
                if (hash2[out] < hash1[out]){
                    count--;
                }
            }
            if (count == p.length){
                result.add(left);
            }
            right++;
        }
        return result;
    }
}
class Solution974 {
    public int subarraysDivByK(int[] nums, int k) {
        Map<Integer,Integer> hash = new HashMap<>();
        int result = 0;//最终结果
        int sum = 0;//前缀和
        hash.put(0,1);//初始化，余数为0的情况，
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            int num =  hash.getOrDefault((sum % k + k) % k , 0);
            result += num;
            hash.put((sum % k + k) % k ,num + 1);
        }
        return result;
    }
}
class Solution733_2 {
    int[][] nextP = {{0,1},{0,-1},{-1,0},{1,0}};
    public int[][] floodFill(int[][] image, int sr, int sc, int color) {
        int olderColor = image[sr][sc],row = image.length,col = image[0].length;
        if (olderColor == color){
            return image;
        }
        image[sr][sc] = color;
        //设置队列
        Queue<Pair<Integer,Integer>> qu = new LinkedList<>();
        qu.offer(new Pair<>(sr,sc));
        while (!qu.isEmpty()){
            Pair<Integer,Integer> pair = qu.poll();
            int x = pair.getKey(), y = pair.getValue();//获取下标

            for (int i = 0; i < 4; i++) {
                int nextX = x + nextP[i][0];
                int nextY = y + nextP[i][1];
                if (nextX >= 0 && nextX < row
                    && nextY >= 0 && nextY < col
                    && image[nextX][nextY] != color){
                    image[nextX][nextY] = color;
                    qu.offer(new Pair<>(nextX,nextY));
                }
            }
        }
        return image;
    }
}
class Solution {
    int[][] nextP = {{0,1},{0,-1},{-1,0},{1,0}};
    public int numIslands(char[][] grid) {
        int row = grid.length,col = grid[0].length,result = 0;
        boolean[][] visits = new boolean[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 1 && !visits[i][j]){
                    //进行bfs
                    Queue<int[]> qu = new LinkedList<>();
                    qu.offer(new int[]{i,j});
                    visits[i][j] = true;
                    while (!qu.isEmpty()){
                        int[] pair = qu.poll();
                        int x = pair[0],y = pair[1];
                        for (int k = 0; k < 4; k++) {
                            int nextX = x + nextP[k][0];
                            int nextY = y + nextP[k][1];
                            if (nextX >= 0 && nextX < row
                                && nextY >= 0 && nextY < col
                                && !visits[nextX][nextY]){
                                visits[nextX][nextY] = true;
                                qu.offer(new int[]{nextX,nextY});
                            }
                        }
                    }
                }
            }
        }
        return result;
    }
}
class Solution695_2 {
    public int maxAreaOfIsland(int[][] grid) {
        int result = 0,row = grid.length,col = grid[0].length;
        int[][] nextP = {{0,1},{0,-1},{-1,0},{1,0}};
        boolean[][] visits = new boolean[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 1 && !visits[i][j]){
                    Queue<int[]> qu = new LinkedList<>();
                    qu.offer(new int[]{i,j});
                    visits[i][j] = true;
                    int count = 1;
                    while (qu.isEmpty()){
                        int[] pair = qu.poll();
                        int x  = pair[0],y = pair[1];
                        for (int k = 0; k < 4; k++) {
                            int nextX = x + nextP[i][0],nextY = y + nextP[i][1];
                            if (nextX >= 0 && nextX < row
                                && nextY >= 0 && nextY < col
                                && grid[nextX][nextY] == 1 && !visits[nextX][nextY]){
                                count++;
                                visits[nextX][nextY] = true;
                                qu.offer(new int[]{nextX,nextY});
                            }
                        }
                    }
                    result = Math.max(result,count);
                }
            }
        }
        return result;
    }
}
class Solution_130 {
    int[][] nextP = {{0,1},{0,-1},{1,0},{-1,0}};
    int row ,col;
    public void solve(char[][] board) {
        row = board.length;col = board[0].length;
        for (int i = 0; i < board.length; i++) {
            //进行bfs
            if (board[i][0] == 'O'){
                bfs(board,i,0);
            }
            //进行bfs
            if (board[i][col-1] == 'O'){
                bfs(board,i,col-1);
            }
        }
        for (int j = 1; j < col - 1; j++) {
            //进行bfs
            if (board[0][j] == 'O'){
                bfs(board,0,j);
            }
            //进行bfs
            if (board[row - 1][j] == 'O'){
                bfs(board,row - 1,j);
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (board[i][j] == '0'){
                    board[i][j] = 'X';
                }
                if (board[i][j] == '*'){
                    board[i][j] = 'O';
                }
            }
        }
    }

    private void bfs(char[][] board, int i, int j) {
        Queue<int[]> qu = new LinkedList<>();
        qu.offer(new int[]{i,j});
        board[i][j] = '*';
        while (!qu.isEmpty()){
            int[] pair = qu.poll();
            int x = pair[0];
            int y = pair[1];
            for (int k = 0; k < 4; k++) {
                int nextX = x + nextP[i][0],nextY = y + nextP[i][1];
                if (nextX >= 0 && nextX < row
                    && nextY >= 0 && nextY < col
                    && board[nextX][nextY] == 'O'){
                    board[nextX][nextY] = '*';
                    qu.offer(new int[]{nextX,nextY});
                }
            }
        }
    }
}
class Solution525 {
    public int findMaxLength(int[] nums) {
        int result = 0;
        //数组数据的转化
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0){
                nums[i] = -1;
            }
        }
        //存放前缀和,<前缀和，下标>
        Map<Integer,Integer> map = new HashMap<>();
        int sum = 0;
        map.put(0,-1);
        for (int i = 0; i < nums.length; i++) {
            sum+= nums[i];
            if (map.containsKey(sum)){
                result = Math.max(result,i -  map.get(sum));
            }
            if (!map.containsKey(sum)){
                map.put(sum,i);
            }
        }
        return result;
    }
}
class Solution1926 {
    int[][] nextP = {{0,1},{0,-1},{1,0},{-1,0}};
    public int nearestExit(char[][] maze, int[] entrance) {
        Queue<int[]> qu = new LinkedList<>();
        qu.offer(entrance);
        int row = maze.length,col = maze[0].length;
        boolean[][] visits = new boolean[row][col];
        visits[entrance[0]][entrance[1]] = true;
        int step = 0;
        while (!qu.isEmpty()){
            int count = qu.size();
            while (count-- > 0){
                int[] pair = qu.poll();
                int x = pair[0],y = pair[1];
                for (int i = 0; i < 4; i++) {
                    int nextX = x + nextP[i][0],nextY = y + nextP[i][1];
                    if(nextX >= 0 && nextX < row
                        && nextY >= 0 && nextY < col
                        && maze[nextX][nextY] == '.'&&
                            !visits[nextX][nextY]){
                        if (nextX == 0 || nextX == row-1 ||
                                nextY==0 || nextY==col-1){
                            return step+1;
                        }
                        visits[nextX][nextY] = true;
                        qu.offer(new int[]{nextX,nextY});
                    }
                }
            }
            step++;
        }
        return -1;
    }
}

class Solution433 {
    public int minMutation(String startGene, String endGene, String[] bank) {
        Set<String> bankHash = new HashSet<>();
        for (int i = 0; i < bank.length; i++) {
            bankHash.add(bank[i]);
        }
        int step = 0;
        Queue<String> qu = new LinkedList<>();
        qu.offer(startGene);
        if (!bankHash.contains(endGene)){
            return -1;
        }
        Set<String> vis = new HashSet<>();
        vis.add(startGene);
        char[] chs = {'A', 'C' , 'G' , 'T'};
        while (!qu.isEmpty()){
            int size = qu.size();
            while (size-- > 0){
                String ss = qu.poll();
                if (ss.equals(endGene)){
                    return step;
                }
                for (int i = 0; i < ss.length(); i++) {
                    char[] s = ss.toCharArray();
                    for (int j = 0; j < chs.length; j++) {
                        s[i] = chs[j];
                        String tmp = new String(s);
                        if(bankHash.contains(tmp) && !vis.contains(tmp)){
                            vis.add(tmp);
                            qu.offer(tmp);
                        }
                    }
                }
            }
            step++;
        }
        return -1;
    }
}
class Solution127 {
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Set<String> wordListHash = new HashSet<>();
        for (String s:wordList) {//存储到字典哈希
            wordListHash.add(s);
        }
        if (!wordListHash.contains(endWord)){
            return 0;
        }
        Queue<String> qu = new LinkedList<>();
        Set<String> vis = new HashSet<>();//已经遍历过的单词存储在哈希中
        vis.add(beginWord);
        qu.offer(beginWord);
        int step = 1;
        while (!qu.isEmpty()){
            int size = qu.size();
            while (size-- > 0){
                String ss = qu.poll();
                if (ss.equals(endWord)){
                    return step;
                }
                for (int i = 0; i < ss.length(); i++) {
                    char[] s = ss.toCharArray();
                    for (char ch = 'a'; ch <= 'z'; ch++) {
                        s[i] = ch;
                        String str = new String(s);
                        if (wordListHash.contains(str) && !vis.contains(str)){
                            vis.add(str);
                            qu.offer(str);

                        }
                    }
                }
            }
            step++;
        }
        return 0;
    }
}
class Solution1314 {
    public int[][] matrixBlockSum(int[][] mat, int k) {
        int row = mat.length,col = mat[0].length;
        int[][] dp = new int[row + 1][col + 1];
        for (int i = 1; i <= row; i++) {
            for (int j = 1; j <= col; j++) {
                dp[i][j] = mat[i-1][j-1] + dp[i][j-1] + dp[i-1][j] - dp[i-1][j-1];
            }
        }

        int[][] answer = new int[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                int x1 = Math.max(i-k,0),y1 = Math.max(j-k,0);
                int x2 = Math.min(i+k+1,row),y2 = Math.max(j+k+1,col);
                answer[i][j] = dp[x2][y2] - dp[x1][y1];
            }
        }
        return answer;
    }
}

class Solution675 {
    int row = 0,col = 0;
    public int cutOffTree(List<List<Integer>> forest) {
        //将遍历之后的结果，放在优先级队列中。pair<树高度,树下标>
        //以树高度为升序进行排序。
        PriorityQueue<Pair<Integer,Integer[]>> pq = new PriorityQueue<>(
            (a,b) -> {
                return a.getKey() - b.getKey();
            }
        );
        row = forest.size();col = forest.get(0).size();
        for (int i = 0; i < forest.size(); i++) {
            List<Integer> list = forest.get(i);
            for (int j = 0; j < list.size(); j++) {
                int height = list.get(j);
                if (height != 0 && height != 1){
                    pq.offer(new Pair<>(height,new Integer[]{i,j}));
                }
            }
        }
        for (Pair<Integer,Integer[]> pair: pq) {
            System.out.println(pair.getKey() + " : " + pair.getValue()[0] + "+" + pair.getValue()[1]);
        }
        int result = 0;
        int count = pq.size();
        if (count == 0){
            return 0;
        }
        Integer[] prev = {0,0};//前一个点
        Integer[] next = pq.poll().getValue();//下一个点

        while (count-- > 0){


            int step = bfs(prev,next,forest);
            if (step == -1){
                return -1;
            }
            result += step;
            prev = next;
            next = pq.poll().getValue();

        }
        return result;
    }
    int[][] nextP = {{0,1},{0,-1},{-1,0},{1,0}};
    private int bfs(Integer[] prev, Integer[] next, List<List<Integer>> forest) {
        Queue<Integer[]> qu = new LinkedList<>();
        boolean[][] vis = new boolean[row][col];
        int step = 0;

        qu.offer(prev);
        while (!qu.isEmpty()){
            int size = qu.size();
            while (size-- > 0){
                Integer[] pair = qu.poll();
                int x = pair[0],y = pair[1];
                if (x == next[0] && y == next[1]){
                    return step;
                }
                for (int i = 0; i < 4; i++) {
                    int nextX = x + nextP[i][0],nextY = y + nextP[i][1];
                    if (nextX >= 0 && nextX < row
                            && nextY >= 0 && nextY < col
                            && forest.get(nextX).get(nextY) != 0
                            && !vis[nextX][nextY]){
                        qu.offer(new Integer[]{nextX,nextY});
                        vis[nextX][nextY] = true;
                    }
                }
            }
            step++;
        }
        return -1;
    }
}

class Solution542 {
    int row = 0,col = 0;
    public int[][] updateMatrix(int[][] mat) {
        row = mat.length;col = mat[0].length;
        int[][] dist = new int[row][col];
        for (int i = 0; i < dist.length; i++) {
            Arrays.fill(dist[i],-1);
        }
        bfs(mat,dist);

        return dist;
    }
    int[][] nextP = {{0,1},{0,-1},{1,0},{-1,0}};
    private void bfs(int[][] mat, int[][] dist) {
        Queue<int[]> qu = new LinkedList<>();
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (mat[i][j] == 0){
                    qu.offer(new int[]{i,j});
                }
            }
        }
        int step = 0;
        while (!qu.isEmpty()){
            int size = qu.size();
            while (size-- > 0){
                int[] pair = qu.poll();
                int x = pair[0],y = pair[1];
                dist[x][y] = step;
                for (int i = 0; i < 4; i++) {
                    int nextX = x + nextP[i][0],nextY = y + nextP[i][1];
                    if (nextX >= 0 && nextX < row
                        && nextY >= 0 && nextY < col
                        && mat[nextX][nextY] == 1
                        && dist[nextX][nextY] != -1){
                        qu.offer(new int[]{nextX,nextY});
                        dist[nextX][nextY] = step;
                    }
                }
            }
            size++;
        }
    }
}

class Solution01_01 {
    public boolean isUnique(String astr) {
        char[] s = astr.toCharArray();
        if (s.length > 26){
            return false;
        }
        boolean[] hash = new boolean[26];
        for (int i = 0; i < s.length; i++) {
            int index = s[i] - 'a';
            if (!hash[index]){
                hash[index] = true;
            }else {
                return false;
            }
        }
        return true;
    }
}
class Solution268 {
    public int missingNumber(int[] nums) {
        int result = nums.length;
        for(int i = 0;i < nums.length;i++){
            result ^= i;
            result ^= nums[i];
        }
        return result;
    }
}
class Solution1020 {
    int row = 0,col = 0;
    public int numEnclaves(int[][] grid) {
        row = grid.length;col = grid[0].length;
        boolean[][] vis = new boolean[row][col];
        bfs(grid,vis);
        int result = 0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (!vis[i][j] && grid[i][j] == 1){
                    result++;
                }
            }
        }
        return result;
    }
    int[][] nextP = {{1,0},{-1,0},{0,1},{0,-1}};
    private void bfs(int[][] grid, boolean[][] vis) {
        Queue<int[]> qu = new LinkedList<>();
        for (int i = 0; i < row; i++) {
            if (grid[i][0] == 1){
                qu.offer(new int[]{i,0});
                vis[i][0] = true;
            }
            if (grid[i][col - 1] == 1){
                qu.offer(new int[]{i,col - 1});
                vis[i][col - 1] = true;
            }
        }
        for (int j = 1; j < col - 1; j++) {
            if (grid[0][j] == 1){
                qu.offer(new int[]{0,j});
                vis[0][j] = true;
            }
            if (grid[row - 1][j] == 1){
                qu.offer(new int[]{row-1,j});
                vis[row - 1][j] = true;
            }
        }
        while (!qu.isEmpty()){
            int size = qu.size();
            while (size-- > 0){
                int[] pair = qu.poll();
                int x = pair[0],y = pair[1];
                for (int i = 0; i < 4; i++) {
                    int nextX = x + nextP[i][0],nextY = y + nextP[i][1];
                    if (nextX >= 0 && nextX < row
                        && nextY >= 0 && nextY < col
                        && grid[nextX][nextY] == 1
                        && !vis[nextX][nextY]){
                        vis[nextX][nextY] = true;
                        qu.offer(new int[]{nextX,nextY});
                    }
                }
            }
        }
    }
}
class Solution1765 {
    int row = 0,col = 0;
    public int[][] highestPeak(int[][] isWater) {
        row = isWater.length;col = isWater[0].length;
        int[][] dist = new int[row][col];
        for (int i = 0; i < row; i++) {
            Arrays.fill(dist[i],-1);
        }
        dfs(isWater,dist);
        return dist;
    }
    int[][] nextP = {{1,0},{-1,0},{0,1},{0,-1}};
    private void dfs(int[][] isWater, int[][] dist) {
        Queue<int[]> qu = new LinkedList<>();
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (isWater[i][j] == 1){
                    dist[i][j] = 0;
                    qu.offer(new int[]{i,j});
                }
            }
        }
        int height = 0;
        while (!qu.isEmpty()){
            int size = qu.size();
            height++;
            while (size-- > 0){
                int[] pair = qu.poll();
                int x = pair[0],y = pair[1];
                for (int i = 0; i < 4; i++) {
                    int nextX = x + nextP[i][0],nextY = y + nextP[i][1];
                    if (nextX >= 0 && nextX < row
                        && nextY >= 0 && nextY < col
                        && isWater[nextX][nextY] == 0
                        && dist[nextX][nextY] == -1){
                        dist[nextX][nextY] = height;
                        qu.offer(new int[]{nextX,nextY});
                    }

                }

            }
        }
    }
}
class Solution1662 {
    int row = 0,col = 0;
    public int maxDistance(int[][] grid) {
        row = grid.length;col = grid[0].length;
        int[][] dist = new int[row][col];
        for (int i = 0; i < row; i++) {
            Arrays.fill(dist[i],-1);
        }
        int result = dfs(grid,dist);
        return result;
    }
    int[][] nextP = {{1,0},{-1,0},{0,1},{0,-1}};
    private int dfs(int[][] grid, int[][] dist) {
        Queue<int[]> qu = new LinkedList<>();
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 1){//把陆地放到队列中
                    qu.offer(new int[]{i,j});
                    dist[i][j] = 0;
                    System.out.println("--------------");
                }
            }
        }
        int count = 0;
        while (!qu.isEmpty()){
            int size = qu.size();
            while (size-- > 0){
                int[] pair = qu.poll();
                int x = pair[0],y = pair[1];
                for (int i = 0; i < 4; i++) {
                    int nextX = x + nextP[i][0],nextY = y + nextP[i][1];
                    if (nextX >= 0 && nextX < row
                        && nextY >= 0 && nextY < col
                        && grid[nextX][nextY] == 0
                        && dist[nextX][nextY] == -1){
                        dist[nextX][nextY] = dist[x][y] + 1;
                        count = dist[nextX][nextY];
                        qu.offer(new int[]{nextX,nextY});
                    }

                }
            }
        }
        return count;
    }
}
class Solution371 {
    public int getSum(int a, int b) {
        while (b != 0){
            int tmpA = a ^ b;//^ --> 无进位相加
            int tmpB = (a & b) << 1;//移动左进位
            a = tmpA;
            b = tmpB;
        }
        return a;
    }
}
class Solution137 {
    public int singleNumber(int[] nums) {
        int result = 0;
        for (int i = 0; i < 32; i++) {
            int count = 0;
            for (int j = 0; j < nums.length; j++) {
                if ((nums[i] >> i & 1) == 1){
                    count++;
                }
            }
            if (count % 3 == 1){//把第i位设置为1
                result |= 1 << i;
            }
        }

        return result;
    }
}
class Solution17_19 {
    public int[] missingTwo(int[] nums) {
        int bitSum = 0;
        int len = nums.length;
        for (int i = 0; i < len; i++) {
            bitSum ^= nums[i];
        }
        for (int i = 1;i <= len + 2;i++){
            bitSum ^= i;
        }
        //获取最右侧的一个1
        int rightBitOne = bitSum & (- bitSum);
        int[] result = new int[2];
        for (int i = 0; i < nums.length; i++) {
            if ((nums[i] & rightBitOne) == 1){
                result[0] ^= nums[i];
            }else {
                result[1] ^= nums[i];
            }
        }
        for (int i = 1;i <= len + 2;i++){
            if ((i & rightBitOne) == 1){
                result[0] ^= i;
            }else {
                result[1] ^= i;
            }
        }
        return result;
    }
}
class Solution207 {
    public boolean canFinish(int n, int[][] p) {
        int[] in = new int[n];//记录各个节点的入度
        HashMap<Integer,List<Integer>> map = new HashMap<>();//存储邻接表
        for (int i = 0; i < p.length; i++) {
            int a = p[i][0],b = p[i][1];
            if (!map.containsKey(b)){
                map.put(b,new ArrayList<>());
            }
            map.get(b).add(a);
            in[a]++;
        }
        Queue<Integer> queue = new LinkedList<>();//存放入度为0的结点
        for (int i = 0; i < n; i++) {//把所有入度为0的点，放入队列中
            if (in[i] == 0){
                queue.offer(i);
            }
        }
        while (!queue.isEmpty()){
            int size = queue.size();
            while (size-- >  0){
                int point = queue.poll();
                for (int next : map.getOrDefault(point,new ArrayList<>())) {
                    in[next]--;
                    if (in[next] == 0){
                        queue.offer(next);
                    }
                }
            }
        }
        for (int i = 0; i < n; i++) {
            if (in[n]!=0){
                return false;
            }
        }
        return true;
    }
}
class Solution210 {
    public int[] findOrder(int n, int[][] p) {
        int len = p.length;
        int[] result = new int[len],in = new int[len];
        Map<Integer,ArrayList> map = new HashMap<>();
        for (int i = 0; i < len; i++) {//构造邻接表，统计各个节点的入度
            in[p[i][0]]++;
            if (!map.containsKey(p[i][1])){
                map.put(p[i][1],new ArrayList<>());
            }
            map.get(p[i][1]).add(p[i][0]);
        }
        Queue<Integer> qu = new LinkedList<>();
        int index = 0;
        for (int i = 0; i < len; i++) {//存放所有入度0的点
            result[index++] = i;
            if (in[i] == 0){
                qu.offer(i);
            }
        }
        while (!qu.isEmpty()){
            int from = qu.poll();
            List<Integer> list = map.getOrDefault(from,new ArrayList<>());
            for (int to : list) {
                in[to]--;
                if (in[to] == 0){
                    result[index++] = to;
                    qu.offer(to);
                }
            }
        }
        for (int i = 0; i < len; i++) {
            if (in[i] != 0){//存在入度非0的点，说明存在环
                return new int[]{};
            }
        }
        return result;
    }
}
class Solution210_2 {
    public int[] findOrder(int n, int[][] p) {
        List<List<Integer>> edges = new ArrayList<>();//邻接表存储
        for (int i = 0; i < n; i++) {//初始化邻接表
            edges.add(new ArrayList<>());
        }
        int[] in = new int[n];//统计各个节点的入度
        for (int i = 0; i < p.length; i++) {//构建邻接表
            edges.get(p[i][1]).add(p[i][0]);
            in[p[i][0]]++;
        }
        int[] result = new int[n];
        int index = 0;//下标
        Queue<Integer> qu = new LinkedList<>();
        for (int i = 0; i < n; i++) {
            if (in[i] == 0){
                qu.offer(i);
                result[index++] = i;
            }
        }
        while (!qu.isEmpty()){
            int from = qu.poll();
            for (int to:edges.get(from)) {
                in[to]--;
                if (in[to] == 0){
                    result[index++] = to;
                    qu.offer(to);
                }
            }
        }
        return index == n ? result : new int[]{};
    }
}
class Solution114 {
    //构造邻接表
    Map<Character,List<Character>> edges = new HashMap<>();
    //记录所有节点的入度信息。
    Map<Character,Integer> in = new HashMap<>();
    //标记顺序是否合法 --> "abc"  "ab"
    boolean check;

    public String alienOrder(String[] words) {
        int len = words.length;
        //1.初始化入度哈希表
        for (String str:words) {
            for (char ch: str.toCharArray()) {
                in.put(ch,0);
            }
        }
        //2.进行构造邻接表
        for (int i = 0; i < len; i++) {
            for (int j = i + 1; j < len; j++) {
                //搜集两个字符串的顺序信息
                add(words[i].toCharArray(),words[j].toCharArray());
                if (check){//字符串位置不合法
                    return "";
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        //3.进行队列的初始化
        Queue<Character> qu = new LinkedList<>();
        for (Map.Entry<Character,Integer> entry:in.entrySet()) {
            if (entry.getValue() == 0){
                qu.offer(entry.getKey());
                sb.append(entry.getKey());
            }
        }
        //4.进行拓扑排序
        while (!qu.isEmpty()){
            char from = qu.poll();
            for (char to:edges.getOrDefault(from,new ArrayList<>())) {
                in.put(to,in.get(to) - 1);
                if (in.get(to) == 0){
                    sb.append(to);
                    qu.offer(to);
                }
            }
        }
        //5.进行判断是否存在入度不为0的点
        for (Map.Entry<Character,Integer> entry:in.entrySet()) {
            if (entry.getValue() != 0){//存在说明有环，返回“”
                return "";
            }
        }
        return sb.toString();
    }

    private void add(char[] prev, char[] next) {
        int len = Math.min(prev.length,next.length);
        int index = 0;
        while (index < len){
            if (prev[index] != next[index]){
                if (!edges.containsKey(prev[index])){
                    edges.put(prev[index],new ArrayList<>());
                }
                //不包含的时候，才进行构建邻接表
                if (!edges.get(prev[index]).contains(next[index])){
                    edges.get(prev[index]).add(next[index]);
                    in.put(next[index],in.get(next[index]) + 1);
                }
                break;
            }
            index++;
        }
        if (index == len && prev.length > next.length){
            check = true;
        }
    }
}

class Solution300_3 {
    public int lengthOfLIS(int[] nums) {
        int n = nums.length;
        List<Integer> result = new ArrayList<>();
        //进行贪心策略
        for (int i = 0; i < n; i++) {
            //预先判断一下是否需要添加最长子序列
            if (result.isEmpty() || result.get(result.size() - 1) < nums[i]){
                result.add(nums[i]);
                continue;
            }
            int left = 0,right = result.size() - 1;
            //二分查找
            //查找区间：[0,nums[i]) [nums[i],right]
            while (left < right){
                int mid = left + (right - left)/2;
                if (result.get(mid) >= nums[i]){
                    right = mid;
                }else {
                    left = mid + 1;
                }
            }
            result.set(left,nums[i]);
        }
        return result.size();
    }
}
class Solution334 {
    public boolean increasingTriplet(int[] nums) {
        int a = Integer.MAX_VALUE,b = Integer.MAX_VALUE;
        for (int i = 0; i < nums.length; i++) {
            if (a >= nums[i]){
                a = nums[i];
            }else if (b >= nums[i]){
                b = nums[i];
            }else {//b < nums[i]
                return true;
            }
        }
        return false;
    }
}


class Solution674 {
    public int findLengthOfLCIS(int[] nums) {
        int left = 0,result = 0;
        for (int right = 1; right < nums.length; right++) {
            if (nums[right] >= nums[right - 1]){
                result = Math.max(right - left + 1,result);
            }else {
                left = right;
            }
        }
        return result;
    }
}
class Solution121_2 {
    public int maxProfit(int[] prices) {
        int prevMin = Integer.MAX_VALUE;
        int result = 0;
        for (int i = 0; i < prices.length; i++) {
            if (prevMin < prices[i]){
                result = Math.max(result,prices[i] - prevMin);
            }else {
                prevMin = prices[i];
            }
        }
        return result;
    }
}
class Solution122 {
    public int maxProfit(int[] prices) {
        //双指针算法
        int left = 0,right = 0,result = 0,len = prices.length;
        while (left < len){
            right = left;
            while (right + 1 < len && prices[right] < prices[right + 1]){//向后寻找上升元素的末端
                right++;
            }
            result += prices[right] - prices[left];
            left = right + 1;//left移动到right后面，因为到right 及其之前都是上升的。
        }
        return result;
    }
}
class Solution1005 {
    public int largestSumAfterKNegations(int[] nums, int k) {
        int m = 0,min = 0;
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i++) {
            if(nums[i] < 0){//求负数元素的个数
                m++;
            }
            min = Math.min(min,Math.abs(nums[i]));//求绝对值最小的元素
        }
        int result = 0;
        if (m >= k){//将负数从小到大转化为正数
            for (int i = 0; i < nums.length; i++) {
                if(i < k){
                    result += -nums[i];
                }else {
                    result += nums[i];
                }
            }
        }else {
            for (int i = 0; i < nums.length; i++) {
                result += Math.abs(nums[i]);
            }
            if((k-m) % 2 == 1){//如果剩余翻转次数为奇数，就将绝对值最小的数字进行翻转
                result -=  2 * min;
            }
        }
        return result;
    }
}
class Solution2418 {
    public String[] sortPeople(String[] names, int[] heights) {
        //对身高的下标进行排序
        int len = heights.length;
        Integer[] index = new Integer[len];
        for (int i = 0; i < len; i++) {
            index[i] = i;
        }
        Arrays.sort(index,(a,b)->{return heights[b] - heights[a];});
        System.out.println(Arrays.toString(index));
        String[] result = new String[len];
        for (int i = 0; i < len; i++) {
            result[i] = names[index[i]];
        }
        return result;
    }
}
class Solution870 {
    public int[] advantageCount(int[] nums1, int[] nums2) {
        int len = nums1.length;
        Arrays.sort(nums1);//对nums1进行升序排序
        Integer[] index2 = new Integer[len];
        for (int i = 0; i < len; i++) {
            index2[i] = i;
        }
        Arrays.sort(index2,(a,b)->{return nums2[a] - nums2[b];});//对nums2[2]进行下标排序
        int left = 0,right = len - 1;
        int[] result = new int[len];
        for (int i = 0; i < len; i++) {
            if (nums1[i] <= nums2[index2[left]]){
                result[index2[left++]] = nums1[i];
            }else {
                result[index2[right--]] = nums1[i];
            }
        }
        return result;
    }
}
class Solution409 {
    public int longestPalindrome(String ss) {
        char[] s = ss.toCharArray();
        int result = 0;
        int[] hash = new int['z' - 'A' + 1];
        for (int i = 0; i < s.length; i++) {
            hash[s[i] - 'A']++;
        }
        for (int i = 0; i < hash.length; i++) {
            result += hash[i]/2;
        }
        if (s.length > result){
            result++;
        }
        return result;
    }
}
class Solution942 {
    public int[] diStringMatch(String ss) {
        char[] s = ss.toCharArray();
        int len = s.length;
        int left = 0,right = len - 1;
        int index = 0;
        int[] result = new int[len];
        while (left < right){
            if (s[index] == 'I'){
                result[index++] = left++;
            }else {
                result[index++] = right--;
            }
        }
        result[index] = left;
        return result;
    }
}
class Solution455 {
    public int findContentChildren(int[] g, int[] s) {//g:胃口值，s:饼干尺寸
        int result = 0;
        Arrays.sort(g);Arrays.sort(s);
        int index = 0;
        for (int i = 0; i < s.length; i++) {//固定孩子
            if (index < g.length && g[index] <= s[i]){
                result++;
                index++;
            }
        }
        return result;
    }
}
class Solution553 {
    public String optimalDivision(int[] nums) {
        if (nums.length == 1){
            return "" + nums[0];
        }
        if (nums.length == 2){
            return nums[0] + "(" + nums[1] + ")";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(nums[0]);
        if(1 < nums.length){
            sb.append("/(" + nums[1]);
        }
        for (int i = 2; i < nums.length; i++) {
            sb.append("/" + nums[i]);
        }
        if(1 < nums.length){
            sb.append(")");
        }
        return sb.toString();
    }
}
class Solution45 {
    public int jump(int[] nums) {
        int result = 0,left = 0,right = 0,maxPos = 0,n = nums.length;
        while (left <= right){//以防跳不到 n - 1的位置
            if (maxPos >= n - 1){//判断是否已经跳到最后一个位置了
                return maxPos;
            }
            for (int i = left; i <= right; i++) {
                maxPos = Math.max(maxPos,nums[i] + i);
            }
            left = right+1;
            right = maxPos;
            result++;
        }

        return result;
    }
}
class Solution55 {
    public boolean canJump(int[] nums) {
        int left = 0,right = 0,maxPos = 0;
        while (left <= right){
            if (maxPos >= nums.length - 1){
                return true;
            }
            for (int i = left; i < right; i++) {
                maxPos = Math.max(maxPos,i + nums[i]);
            }
            left = right + 1;
            right  = maxPos;
        }
        return false;
    }
}
class Solution134 {
    public int canCompleteCircuit(int[] gas, int[] cost) {
        int len = gas.length;
        int[] diff = new int[len];
        for (int i = 0; i < len; i++) {
            diff[i] = gas[i] - cost[i];
        }
        int path = 0;
        int index = 0;
        for (int i = 0; i < cost.length; i++) {
            path += diff[i];
            if (path < 0){
                index = i + 1;
            }
        }
        if (path >= 0){
            return index;
        }
        return -1;
    }
}
class Solution738 {
    public int monotoneIncreasingDigits(int n) {
        char[] s = (n + "").toCharArray();
        int index = n - 1;
        for (int i = 0; i < s.length; i++) {
            if (i < n - 1 && s[i] > s[i + 1]){
                index = i;
            }
        }
        if (index != n - 1){
            while (index > 0 && s[index] == s[index - 1]){
                index--;
            }
            s[index]--;
            while (++index < s.length){
                s[index] = '9';
            }
        }

        return Integer.parseInt(new String(s));
    }
}
class Solution991 {
    public int brokenCalc(int startValue, int target) {
        int step = 0;
        while (startValue != target){
            if (startValue < target){
                target = target + 1;
            }else {
                if (target % 2 == 0){
                    target /= 2;
                }else {
                    target = target + 1;
                }
            }
            step++;
        }
        return step;
    }
}
class Solution56 {
    public int[][] merge(int[][] intervals) {
        Arrays.sort(intervals,(a,b)->a[0]-b[0]);
        //System.out.println(Arrays.deepToString(intervals));
        List<int[]> res = new ArrayList<>();
        int left = intervals[0][0],right = intervals[0][1];
        for (int i = 1; i < intervals.length; i++) {
            if (right >= intervals[i][0]){
                right = Math.max(right,intervals[i][1]);
            }else {
                res.add(new int[]{left,right});
                left = intervals[i][0];
                right = intervals[i][1];
            }
        }
        res.add(new int[]{left,right});
        int[][] result = new int[res.size()][2];
        for (int i = 0; i < res.size(); i++) {
            result[i][0] = res.get(i)[0];
            result[i][1] = res.get(i)[1];
        }
        return result;
    }
}
class Solution435 {
    public int eraseOverlapIntervals(int[][] intervals) {
        Arrays.sort(intervals,(a,b)->a[0]-b[0]);//按照起始位置进行升序
        int result = 1;
        int right = intervals[0][0];
        for (int i = 1; i < intervals.length; i++) {
            if (intervals[i][0] < right){
                right = Math.min(intervals[i][1],right);
            }else {
                result++;
                right = intervals[i][1];
            }
        }
        return intervals.length - result;
    }
}
class Solution452 {
    public int findMinArrowShots(int[][] points) {
        Arrays.sort(points,(a,b)->a[0]> b[0] ? 1 : -1);//按着左端点进行升序排序
        int result = 1;
        int right = points[0][1];
        for (int i = 1; i < points.length; i++) {
            if (points[i][0] <= right){
                right = Math.min(right,points[i][1]);
            }else {
                right = points[i][1];
                result++;
            }
        }
        return result;
    }
}
class Solution397 {
    //
    Map<Long,Integer> hash = new HashMap<>();
    public int integerReplacement(int n) {
        return dfs(n * 1L);
    }
    public int dfs(long n){
        if (hash.containsKey(n)){
            return hash.get(n);
        }
        if(n == 1){//终结情况
            hash.put(n,0);
            return 0;
        }else if(n % 2 == 0){//偶数情况
            hash.put(n,1 + dfs(n / 2));
            return hash.get(n);
        }else{//奇数情况
            hash.put(n,1 + Math.min(dfs(n + 1),dfs(n - 1)));
            return hash.get(n);
        }
    }
}
class Solution379_2 {
    public int integerReplacement(int n) {
        int step = 0;
        while (n > 1){
            if (n % 2 == 0){
                n /= 2;
            }else {
                 if (n % 4 == 1){
                     n--;
                 }else {//n % 4 == 3
                     n++;
                 }
            }
            step++;
        }
        return step;
    }
}
class Solution354 {
    public int maxEnvelopes(int[][] envelopes) {
        //贪心+二分
        //按照左端点升序，右端点降序进行排序
        //右端点降序是为了防止出现左端点相同的情况，我们降序，就不会出现左端点相同情况
        Arrays.sort(envelopes,(a,b)->a[0] == b[0] ? b[1] - a[1] : a[0] - b[0]);
        List<Integer> list = new ArrayList<>();
        list.add(envelopes[0][1]);
        for (int i = 0; i < envelopes.length; i++) {
            int b = envelopes[i][1];
            if (list.get(list.size() - 1) < b){
                list.add(b);
                continue;
            }
            int left = 0,right = list.size() - 1;
            while (left < right){
                int mid = left + (right - left) / 2;
                if (list.get(mid) < b){
                    left = mid + 1;
                }else {
                    right = mid;
                }
            }
            list.set(left,b);
        }
        return list.size();
    }
}
class Solution1262 {
    public int maxSumDivThree(int[] nums) {
        int sum = 0;
        int INF = 0x3f3f3f3f;
        int x1 = INF,x2 = INF,y1 = INF,y2= INF;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            if (nums[i] % 3 == 1){
                if (nums[i] < x1){
                    x2 = x1;
                    x1 = nums[i];
                }else if (nums[i] < x2){
                    x2 = nums[i];
                }
            }else if (nums[i] % 3 == 2){
                if (nums[i] < y1){
                    y2 = y1;
                    y1 = nums[i];
                }else if (nums[i] < x2){
                    y2 = nums[i];
                }
            }
        }
        
        if (sum % 3 == 0){
            return sum;
        }else if (sum % 3 == 1){
            int ret = Math.min(x1,y1+y2);
            if (ret >= INF){
                return 0;
            }
            return sum - ret;
        }else {
            int ret = Math.min(y1,x1+x2);
            if (ret >= INF){
                return 0;
            }
            return sum - ret;
        }
    }
}
class Solution1054 {
    public int[] rearrangeBarcodes(int[] barcodes) {
        Map<Integer,Integer> map = new HashMap<>();//统计出现次数
        int n = barcodes.length;
        int maxVal = 0,maxCount = 0;
        for (int i = 0; i < n; i++) {
            map.put(barcodes[i],map.getOrDefault(barcodes[i],0) + 1);
            if (maxCount < map.get(barcodes[i])){
                maxVal = barcodes[i];
                maxCount = map.get(barcodes[i]);
            }
        }
        //先排列最大的出现次数
        int index = 0;
        int[] result = new int[n];
        for (int i = 0; i < maxCount; i++) {
            result[index] = maxVal;
            index+=2;
        }
        map.remove(maxVal);
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            for (int i = 0; i < entry.getValue(); i++) {
                if (index >= n){
                    index = 1;
                }
                result[index] = entry.getValue();
                index += 2;
            }
        }
        return result;
    }
}
class Solution767 {
    public String reorganizeString(String ss) {
        char[] s = ss.toCharArray();
        int[] hash = new int[26];
        char maxChar = ' ';
        int maxVal = 0,n = s.length;
        char[] result = new char[n];
        //哈希表
        for (int i = 0; i < n; i++) {
            int in = s[i] - 'a';
            hash[in]++;
            if (maxVal < hash[in]){
                maxChar = s[i];
                maxVal = hash[in];
            }
        }
        //先处理最大的
        //奇数的时候，大于 1/2 + 1
        //偶数的时候，大于 1/2
        if (maxVal > (n + 1) / 2){
            return "";
        }
        int index = 0;
        //先处理最大的
        for (int i = 0; i < maxVal; i++) {
            result[index] = maxChar;
            index+= 2;
        }
        hash[maxChar - 'a'] = 0;
        for (int i = 0; i < 26; i++) {
            for (int j = 0; j < hash[i]; j++) {
                if (index >= n){
                    index = 1;
                }
                result[index] = (char)(i + 'a');
                index += 2;
            }
        }
        return new String(result);
    }
}
class Main {
    public static void main(String[] args) {
        PriorityQueue<Pair<Integer,Integer[]>> pq = new PriorityQueue<>(
            (a,b) -> {
                return a.getKey() - b.getKey();
            }
        );
        pq.offer(new Pair<Integer,Integer[]>(10,new Integer[]{1,2}));
        pq.offer(new Pair<Integer,Integer[]>(20,new Integer[]{3,4}));
        pq.offer(new Pair<Integer,Integer[]>(50,new Integer[]{3,4}));
        pq.offer(new Pair<Integer,Integer[]>(30,new Integer[]{3,4}));
        pq.offer(new Pair<Integer,Integer[]>(70,new Integer[]{3,4}));
        pq.offer(new Pair<Integer,Integer[]>(40,new Integer[]{3,4}));
        System.out.println(pq.poll().getKey());
        System.out.println(pq.poll().getKey());
        System.out.println(pq.poll().getKey());
        System.out.println(pq.poll().getKey());
//        int maxSum = 0;
//        for (int N = 1; N <= 500; N++) {
//            maxSum = Math.max(maxSum, f(1000 - N, N));
//        }
//        System.out.println(maxSum);
//        System.out.println(f(4,2));
//        System.out.println(7 % 9);
        System.out.println('z' - 'A' + 1);
    }

    // 计算 f(n, k)
    public static int f(int n, int k) {
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += Math.max(2,i);
        }
        return sum;
    }
}

