package mapANDset;

import java.util.*;
class Person {
    public String id;

    public Person(String id) {
        this.id = id;
    }

    /**
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(id, person.id);
    }

    /**
     * 找位置
     *
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id='" + id + '\'' +
                '}';
    }
}
public class Test {
    public static void main(String[] args) {

        Person person1 = new Person("1234");
        Person person2 = new Person("1234");

        //System.out.println(person1.hashCode());

        //System.out.println(person2.hashCode());

        HashBuck<Person,String> hashBuck = new HashBuck<>();
        hashBuck.put(person1,"高博");

        String name = hashBuck.get(person1);

        System.out.println(name);

    }
    public static void main3(String[] args) {
        HashBucket hashBucket = new HashBucket();
        hashBucket.put(1,11);
        hashBucket.put(2,22);
        hashBucket.put(12,122);
        hashBucket.put(3,33);
        hashBucket.put(4,44);
        hashBucket.put(14,144);
        hashBucket.put(5,55);
        hashBucket.put(16,166);//添加此元素开始扩容
        hashBucket.put(36,155);
        System.out.println(hashBucket.get(5));
        System.out.println(hashBucket.get(16));
    }
    //set当中的元素 是不可以重复的
    public static void main2(String[] args) {
        Set<String> strings = new TreeSet<>();
        strings.add("hello");
        strings.add("abcd");
        strings.add("def");
        strings.add("def");//这里TreeSet底层是TreeMap，每次存储元素放到map的key上
        // System.out.println("fdsaa");
        //strings.clear();

        Iterator<String> it = strings.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }

    }
    public static void main1(String[] args) {
        Map<String,Integer> map = new TreeMap<>();
        //相当于往搜索树当中 插入数据  他是和key比较的
        map.put("hello",2);
        map.put("abcd",3);
        map.put("abcde",4);
        map.put("abcdf",5);
        map.put("abcdfg",6);
        map.put("abcdfg",61);//相同的key 会更新val值
        Set<String> set = map.keySet();
        //Map.Entry 相当于 map中的节点。
        Set<Map.Entry<String,Integer>> entrySet = map.entrySet();
        for(Map.Entry<String,Integer> entry : entrySet) {
            System.out.println("key: "+entry.getKey()+" val: "+entry.getValue());
        }
    }
}
