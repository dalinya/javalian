package mapANDset;

public class BinarySearchTree {
    static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    public TreeNode root;//根节点
    /**
     * 找到val值  返回节点的地址
     * O(logn)
     * @param val
     * @return
     */
    public TreeNode search(int val) {
        TreeNode cur = root;
        while (cur != null){
            if(val > cur.val){
                cur = cur.right;
            }else if (val < cur.val){
                cur = cur.left;
            }else {
                return cur;
            }
        }
        return null;
    }
    public boolean insert(int key) {
        TreeNode node = new TreeNode(key);
        if(root == null){
            root = node;
            return true;
        }
        TreeNode cur = root;
        TreeNode prev = root;
        while (cur != null){
            if(cur.val > key){
                prev = cur;
                cur = cur.left;
            }else if (cur.val < key){
                prev = cur;
                cur = cur.right;
            }else {
                return false;
            }
        }
        if (prev.val > key){
            prev.left = node;
        }else {
            prev.right = node;
        }
        return true;
    }
    /**
     * 删除节点key
     * @param key
     */
    public void removeNode(int key) {
        TreeNode cur = root;
        TreeNode parent = null;
        while (cur != null){
            if(cur.val > key){
                parent = cur;
                cur = cur.left;
            }else if (cur.val < key){
                parent = cur;
                cur = cur.right;
            }else {
                remove(cur,parent);
                return;
            }
        }
    }
    /**
     * 删除cur这个节点
     * @param cur 要删除的节点
     * @param parent 要删除的节点的父节点
     */
    private void remove(TreeNode cur, TreeNode parent) {
        if(cur.left == null){
            if (cur == root){
                root = root.right;
            }else if (parent.left == cur){
                parent.left = cur.right;
            }else {
                parent.right = cur.right;
            }
        }else if (cur.right == null){
            if(cur == root){
                root = root.left;
            }else if (parent.left == cur){
                parent.left = cur.left;
            }else {
                parent.right = cur.left;
            }
        }else {
            //cur的左右两边 都不为空 ！！
            //找右树的最左边 --> 替罪羊删除法
            TreeNode target = cur.right;
            TreeNode targetParent = cur;
            while (target.left != null){
                targetParent = target;
                target = target.left;
            }
            cur.val = target.val;
            if(targetParent.left == target){
                targetParent.left = target.right;
            }else {
                targetParent.right = target.right;
            }
        }
    }

    public static void main(String[] args) {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        int[] array = {12,4,5,10,8};
        for (int x :
                array ) {
            binarySearchTree.insert(x);
        }

        binarySearchTree.insert(3);

        binarySearchTree.removeNode(4);

    }
}
