package mapANDset;

public class HashBuck<K,V>{
    static class Node<K,V> {
        private K key;
        private V val;
        private Node<K,V> next;

        public Node(K key, V val) {
            this.key = key;
            this.val = val;
        }
    }
    public Node<K,V>[] array = (Node<K,V>[])new Node[10];
    public int usedSize;
    public HashBuck() {
    }
    public void put(K key,V val) {
        Node<K,V> node = new Node<>(key,val);
        int hash = key.hashCode();
        int index = hash % array.length;//控制成合理的位置
        Node<K,V> cur = array[index];
        while (cur != null) {
            if(cur.key.equals(key)) {//自定义类型用equals比较
                cur.val = val;
                return;
            }
            cur = cur.next;
        }

        node.next = array[index];
        array[index] = node;

        usedSize++;
        //计算负载因子
    }
    /**
     * 通过key值 返回value
     * @param key
     * @return
     */
    public V get(K key) {
        int hash = key.hashCode();
        int index = hash % array.length;//控制成合理的位置
        //遍历index位置下方的链表
        Node<K,V> cur = array[index];
        while (cur != null){
            if(cur.key.equals(key)){
                return cur.val;
            }
            cur = cur.next;
        }
        return null;
    }
}
