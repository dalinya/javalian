package mapANDset;

public class HashBucket {
    static class Node {//相当于 Map.Entry
        private int key;
        private int value;
        private Node next;

        public Node(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }
    public Node[] array;
    public int usedSize;
    //默认的负载因子为0.75
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;

    public HashBucket() {
        this.array = new Node[10];
    }

    //java1.8之前采用头插法，1.8及其之后采用尾插法
    public void put(int key,int val) {
        //查找其中是否又相同key
        Node prev = null;
        int index = key % array.length;
        //遍历index位置下方的链表
        Node cur = array[index];
        while (cur != null){
            if(cur.key == key){
                cur.value = val;
                return;
            }
            prev = cur;
            cur = cur.next;
        }
        //cur == null 没有这个key  那么 进行尾插法
        Node node = new Node(key,val);
        //该链表为null
        if(prev == null){
            array[index] = node;
        }else {
            prev.next = node;
        }

/*      //尾插法
        node.next = array[index];
        array[index] = node;*/

        //扩容
        usedSize++;
        //计算负载因子
        if(loadFactor() >= DEFAULT_LOAD_FACTOR) {
            //扩容！！
            resize();
        }
    }

    //重新哈希原来的数据
    private void resize() {
        Node[] tmpArray = new Node[2 * array.length];
        for (int i = 0; i < array.length; i++) {
            Node cur = array[i];
            while (cur != null){
                Node curNext = cur.next;//需要记录下来 原来链表的下一个节点的位置
                int index = cur.key % tmpArray.length;//新数组的位置
                //采用尾插法
                Node tmp = tmpArray[index];
                if(tmp == null){
                    tmpArray[index] = cur;
                    cur.next = null;//这里修改之后 cur的next已经变了
                    cur = curNext;
                    continue;
                }
                while (tmp.next != null){//找到最后一个节点
                    tmp = tmp.next;
                }
                tmp.next = cur;
                cur = curNext;
                /*
                //采用头插法 放到新数组的index位置
                cur.next  = tmpArray[index];//这里修改之后 cur的next已经变了
                tmpArray[index] = cur;
                cur = curNext;
                */
            }
        }
        array = tmpArray;
    }

    //计算负载因子
    private float loadFactor() {
        return usedSize * 1.0f / array.length;
    }

    /**
     * 通过key值 返回value
     * @param key
     * @return
     */
    public int get(int key) {
        int index = key % array.length;
        //遍历index位置下方的链表
        Node cur = array[index];
        while (cur != null){
            if(cur.key == key){
                return cur.value;
            }
            cur = cur.next;
        }
        return -1;
    }
}
