package demoList2;

import java.util.Arrays;

public class SeqList {
    private int[] elem;
    private int usedSize;//记录当前顺序表当中 有多少个有效的数据
    private static final int DEFAULT_CAPACITY = 5;

    public SeqList() {
        this.elem = new int[DEFAULT_CAPACITY];
    }
    // 打印顺序表，注意：该方法并不是顺序表中的方法，为了方便看测试结果给出的
    public void display() {
        for (int i = 0; i < this.usedSize; i++) {
            System.out.print(this.elem[i] + " ");
        }
        System.out.println();
    }
    // 新增元素,默认在数据 最后新增
    public void add(int data) {
        if (isFull()){
            resize();//由于不仅仅有一个需要扩容的方法，所有将扩容单独封装成一个方法
        }
        this.elem[usedSize++] = data;
    }
    //判断顺序表是否满了
    private boolean isFull() {
      return this.usedSize == this.elem.length;
    }
    //扩容
    private void resize() {
        this.elem = Arrays.copyOf(this.elem,this.elem.length * 2);//扩容增加2倍的空间
    }
    // 判定是否包含某个元素
    public boolean contains(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if (toFind == this.elem[i]){
                return true;
            }
        }
        return false;
    }
    // 查找某个元素对应的位置
    public int indexOf(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if (toFind == this.elem[i]){
                return i;
            }
        }
        return -1;
    }
    // 获取 pos 位置的元素
    public int get(int pos)  {
        if (!checkPos(pos)){
            throw new RuntimeException("get 元素位置错误");
        }
        return this.elem[pos];
    }
    // 获取顺序表长度
    public int size() {
        return this.usedSize;
    }
    // 给 pos 位置的元素设为 value【更新的意思 】
    public void set(int pos, int value) {
        if (!checkPos(pos)){
            throw new RuntimeException("set 元素位置错误");
        }
        this.elem[pos] = value;
    }
    //检查pos下表是否 合法
    private boolean checkPos(int pos){
        if (pos < 0 || pos >= this.usedSize) return false;
        return true;
    }

    // 在 pos 位置新增元素
    public void add(int pos, int data) {
        if (!checkPos(pos)){//判断位置受否合理
            throw new RuntimeException("add 元素位置错误");
        }
        if (isFull()){//判断是否满了，满了的话进行扩容
            resize();
        }
        //把后面的元素pos及其之后的元素往后挪一位
        for (int i = this.usedSize - 1; i >= pos; i--) {
            this.elem[i + 1] = this.elem[i];
        }
        this.elem[pos] = data;
        this.usedSize++;
    }
    //删除第一次出现的关键字key
    public void remove(int toRemove) {
        if (isEmpty()){//判断顺序表是否为空
            return;//顺序表为空就不能进行删除
        }
        int pos = indexOf(toRemove);//找到该元素的下标
        if (pos == -1){//没有找到你要删除的数字
            return;
        }
        for (int i = pos; i < this.usedSize - 1; i++) {//将pos后的元素向前挪一位
            this.elem[i] = this.elem[i + 1];
        }
        this.usedSize--;
    }
    //判断顺序表是否为空
    public Boolean isEmpty(){
        return this.usedSize == 0;
    }
    // 清空顺序表
    public void clear() {
        this.usedSize = 0;
        //我们模拟实现的是基本数据类型。如果是引用数据类型的话，我们得遍历每个位置然后再进行赋值为null
    }
}
