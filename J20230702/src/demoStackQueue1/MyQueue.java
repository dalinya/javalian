package demoStackQueue1;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;

//单链表实现队列，尾插头删
public class MyQueue {
    static class ListNode{
        public int val;
        public ListNode next;
        public ListNode(int val){
            this.val = val;
        }
    }
    public ListNode head;
    public ListNode last;
    public int usedSize;
    public void offer(int val){
        ListNode node = new ListNode(val);
        if(head == null){
            head = node;
            last = node;
        }else {
            last.next = node;
            last = last.next;
        }
        usedSize++;
    }
    public int getUsedSize(){
        return usedSize;
    }
    public int poll(){
        if(head == null){
            return -1;
        }
        int val = -1;
        if (head.next == null){
            val = head.val;
            head = null;
            last = null;
            return val;
        }
        val = head.val;
        head = head.next;
        usedSize--;
        return val;
    }
    public int peek(){
        if(head == null){
            return -1;
        }
        return head.val;
    }

    public static void main(String[] args) {
        Deque<Integer> deque = new LinkedList<>();
        deque.offer(1);
        Deque<Integer> stack = new LinkedList<>();//链式栈
        stack.push(10);
        Deque<Integer> stack2 = new ArrayDeque<>();//底层是数组 顺序栈
    }

    public static void main1(String[] args) {
        MyQueue myQueue = new MyQueue();
        myQueue.offer(1);
        myQueue.offer(2);
        int val = myQueue.poll();
        System.out.println(val);
        int val2 = myQueue.peek();
        System.out.println(val2);
    }
}
