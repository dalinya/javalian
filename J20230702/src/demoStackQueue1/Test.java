package demoStackQueue1;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class Test {

    public static void main(String[] args) {


    }

    public static void main1(String[] args) {
        MyStack myStack = new MyStack();
        myStack.push(1);
        myStack.push(2);
        myStack.push(3);
        myStack.push(4);
        System.out.println(myStack.peek());
        System.out.println(myStack.peek());
        System.out.println(myStack.pop());
        System.out.println(myStack.pop());

    }
    public int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();
        for (String str:tokens) {
            if (str.equals("+")){
                int a = stack.pop();
                int b = stack.pop();
                stack.push(a + b);
            }else if (str.equals("-")){
                int a = stack.pop();
                int b = stack.pop();
                stack.push(a - b);
            }else if (str.equals("*")){
                int a = stack.pop();
                int b = stack.pop();
                stack.push(a * b);
            }else if (str.equals("/")){
                int a = stack.pop();
                int b = stack.pop();
                stack.push(a / b);
            }else {
                stack.push(Integer.parseInt(str));
            }
        }
        return stack.pop();
    }
    public boolean isValid(String ss) {
        char[] s = ss.toCharArray();
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length; i++) {
            if (s[i] == '['){
                stack.push(']');
            }else if (s[i] == '{'){
                stack.push('}');
            }else if (s[i] == '('){
                stack.push(')');
            }else {
                if (stack.isEmpty()){//括号不匹配
                    return false;
                }
                if(stack.pop() != s[i]){
                    return false;//左括号多
                }
            }
        }
        return stack.isEmpty();
    }

    public boolean validateStackSequences(int[] pushed, int[] popped) {
        int  index = 0;
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < pushed.length; i++) {
            stack.push(pushed[i]);
            while (index < popped.length && !stack.isEmpty() && stack.peek() == popped[index]){
                index++;
                stack.pop();
            }
        }
        return stack.isEmpty();
    }

}
