package demoStackQueue1;

import java.util.Arrays;

public class MyStack {
    private int[] elem;
    private int useSize;
    public MyStack(){
        elem = new int[5];
    }
    public void push(int val){
        if (isFull()){
            elem = Arrays.copyOf(elem,2 * elem.length);
        }
        elem[useSize] = val;
        useSize++;
    }
    public boolean isFull(){
        return useSize == elem.length;
    }
    //出栈
    public int pop() {
        //判断栈不为空
        if (empty()){
            throw new StackEmptyException("栈为空异常");
        }
        //开始删除
        return elem[--useSize];
    }

    public boolean empty(){
        return this.useSize == 0;
    }
    //获取栈顶元素
    public int peek() {
        //判断栈不为空
        if (empty()){
            throw new StackEmptyException("栈为空异常");
        }
        return elem[useSize];
    }

}
