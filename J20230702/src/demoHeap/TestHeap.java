package demoHeap;

import java.util.Arrays;

public class TestHeap {
    private int[] elem;
    public int usedSize;
    public TestHeap() {
        this.elem = new int[10];
    }
    //创建一个大根堆
    //时间复杂度O(n)
    public void createHeap(int[] array){
        for (int i = 0; i < array.length; i++) {
            this.elem[i] = array[i];
            usedSize++;
        }
        //向下调整
        //len - 1  --》 最后一个孩子节点
        for (int parent = (usedSize - 1 - 1)/2; parent >= 0 ; parent--) {
            shiftDown(parent,usedSize);
        }
    }

    /**
     * @param parent 每棵子树的根节点
     * @param len 代表每棵子树的结束位置
     */
    private void shiftDown(int parent,int len){
        int child = 2 * parent + 1;//左孩子 节点
        while(child < len){//左孩子节点存在
            if(child + 1 < len && elem[child] < elem[child + 1]){//确保右孩子存在，并且判断左右孩子那个大
                child = child + 1;
            }
            if(elem[child] > elem[parent]){
                int tmp = elem[child];
                elem[child] = elem[parent];
                elem[parent] = tmp;
                parent = child;
                child = 2 * parent + 1;
            }else {
                break;
            }
        }
    }
    //插入之后，要保证当前堆还是一个大根堆
    public void push(int val) {
        //检查满
        if (isFull()){
            elem = Arrays.copyOf(elem,2 * elem.length);
        }
        //存数据
        elem[usedSize] = val;
        usedSize++;
        shiftUp(usedSize - 1);
    }
    public boolean isFull() {
        return usedSize == elem.length;
    }

    public void shiftUp(int child) {
        int parent = (child - 1)/2;
        while (child > 0){
            if(elem[child] > elem[parent]){
                int tmp = elem[child];
                elem[child] = elem[parent];
                elem[parent] = tmp;
                child = parent;
                parent = (child - 1)/2;
            }else{
                break;
            }
        }
    }

    public void poll(){
        if(empty()){
            throw new HeapEmptyException("优先级队列是空的！");
        }
        int tmp = elem[0];
        elem[0] = elem[usedSize - 1];
        elem[usedSize - 1] = tmp;
        usedSize--;
        shiftDown(0,usedSize);
    }
    private boolean empty(){
        return usedSize == 0;
    }
    public int peek(){
        if(empty()){
            throw new HeapEmptyException("优先级队列是空的！");
        }
        return elem[0];
    }

    public void heapSort(){
        int end = usedSize - 1;
        while (end > 0){
            int tmp = elem[0];
            elem[0] = elem[end];
            elem[end] = tmp;
            shiftDown(0,end);
            end--;
        }
    }
}
