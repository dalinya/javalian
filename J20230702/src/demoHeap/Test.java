package demoHeap;

import java.util.PriorityQueue;

public class Test {


    public static void main2(String[] args) {
        PriorityQueue<Integer> pq  = new PriorityQueue<>();//线程不安全的队列
        //默认是小根堆

    }
    public static void main(String[] args) {
        TestHeap testHeap = new TestHeap();
        int[] array = { 27,15,19,18,28,34,65,49,25,37 };
        testHeap.createHeap(array);
        testHeap.push(80);
        testHeap.poll();
        testHeap.poll();
        testHeap.heapSort();
    }
    public int[] getLeastNumbers(int[] arr, int k) {
        PriorityQueue<Integer> pq = new PriorityQueue<>((o1, o2) -> o2 - o1);//建立大根堆
        for (int i = 0; i < k; i++) {//放入前i个元素
            pq.offer(arr[i]);
        }
        for (int i = k; i < arr.length; i++) {
            //获取堆顶元素值
            int top = pq.peek();
            if(top > arr[i]){
                pq.poll();
                pq.offer(arr[i]);
            }
        }
        int[] result = new int[k];
        for (int i = 0; i < k; i++) {
            result[i] = pq.poll();
        }
        return result;
    }

}
