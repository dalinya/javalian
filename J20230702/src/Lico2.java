import java.util.*;

class ListNode {
     int val;
     ListNode next;
     ListNode() {}
     ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}

public class Lico2 {
    Random random = new Random();
    public int[] sortArray(int[] nums) {
        qSort(nums,0,nums.length);
        return nums;
    }

    private void qSort(int[] nums, int start, int end) {
        if(start >= end){//递归终止条件
            return;
        }
        int key = nums[new Random().nextInt(end - start + 1) + start];//生成start 到 end的随机主元
        int left = start - 1,right = end + 1,index = start;
        while(index < right){
            if(nums[index] < key){
                swap(nums,--left,index);
                index++;
            }else if (nums[index] == key){
                index++;
            }else {
                swap(nums,index,--right);
            }
        }
        qSort(nums,start,left);
        qSort(nums,right,end);
    }
    public int findKthLargest(int[] nums, int k) {
        return qSort2(nums,0,nums.length-1,k);
    }
    private int qSort2(int[] nums, int start, int end,int k) {
        if(start >= end){//递归终止条件
            return start;
        }
        int key = nums[new Random().nextInt(end - start + 1) + start];//生成start 到 end的随机主元
        int left = start - 1,right = end + 1,index = start;
        while(index < right){
            if(nums[index] < key){
                swap(nums,++left,index);
                index++;
            }else if (nums[index] == key){
                index++;
            }else {
                swap(nums,index,--right);
            }
        }
        int a = left,b = right - left - 1,c = nums.length - right + 1;
        //c>=k去[right,r],找第k大
        //b+c>= k -> 返回k值
        //上面两条都不满足去[start,left]找第k-b-c大
        if(c >= k){
            return qSort2(nums,right,end,k);
        }else if(k <= b + c){
            return key;
        }
        return qSort2(nums,start,left,k - c - b);
    }
    public int[] inventoryManagement(int[] stock, int cnt) {
         qSort3(stock,0,stock.length-1,cnt);
         int[] result = new int[cnt];
        for (int i = 0; i < cnt; i++) {
            result[i] = stock[i];
        }
         return result;
    }

    private void qSort3(int[] nums,int start, int end, int k) {
        //终止条件没什么必要进行判断。
        //终止条件start >= end 时候，下面的循环根本进不去
        int key = nums[new Random().nextInt(end - start + 1) + start];//生成start 到 end的随机主元
        int left = start - 1,right = end + 1,index = start;
        //划分三块区间
        while(index < right){
            if(nums[index] < key){
                swap(nums,++left,index);
                index++;
            }else if (nums[index] == key){
                index++;
            }else {
                swap(nums,index,--right);
            }
        }
        int a = left - start + 1,b = right - left - 1;
        //a > k去[start,left],找前k个小的元素
        //a+b >= k -> 直接终止
        //上面两条都不满足去[right,end]找前k-a-b小
        if(a >= k){//如果包括条件是 a>=k,会产生
            qSort3(nums,start,left,k);
        }else if(k <= b + a){

        }else {
            qSort3(nums,right,nums.length-1,k - a - b);
        }

    }

    private void swap(int[] nums, int i, int index) {
        int tmp = nums[i];
        nums[i] = nums[index];
        nums[index] = tmp;
    }
    public int[] sortArray2(int[] nums) {
        mergeSort(nums,0,nums.length - 1);
        return nums;
    }

    private void mergeSort(int[] nums, int left, int right) {
        if(left >= right){
            return;
        }
        //划分两块
        int mid  = left + (right - left)/2;
        //递归
        mergeSort(nums,left,mid);
        mergeSort(nums,mid+1,right);
        //合并
        int[] tmp = new int[right - left + 1];
        int index= 0,start1 = left,end1 = mid,start2 = mid + 1,end2 = right;
        while (start1 <= end1 && start2 <= end2){
            if (nums[start1] < nums[start2]){
                tmp[index++] = nums[start1++];
            }else {
                tmp[index++] = nums[start2++];
            }
        }
        while (start1 <= end1){
            tmp[index++] = nums[start1++];
        }
        while (start2 <= end2){
            tmp[index++] = nums[start2++];
        }
        for (int i = 0; i < tmp.length; i++) {
            nums[i + left] = tmp[i];
        }
    }
/*    public int reversePairs(int[] nums) {
        int result = 0;
        return merge(nums,0,nums.length-1);
    }
    private int merge(int[] nums, int left, int right) {
        if(left >= right){
            return 0;
        }
        //划分两个区间
        //[left,mid] [mid+1,right]
        int mid = left + (right - left)/2;
        // 2. 左半部分的逆序对个数 + 排序 + 右半部分逆序对的个数 + 排序
        int result = merge(nums,left,mid);
        result += merge(nums,mid + 1,right);
        int[] tmp = new int[right - left + 1];
        int index = 0,start1 = left,end1 = mid,start2 = mid + 1,end2 = right;
        //合并，一左一右逆序对的个数
        while (start1 <= end1 && start2 <= end2){
            if(nums[start1] <= nums[start2]){
                tmp[index++] = nums[start2++];
            }else {
                //加上左侧比他大的数字个数
                result += end2 - start2 + 1;
                tmp[index++] = nums[start1++];
            }
        }
        while (start1 <= end1){
            tmp[index++] = nums[start1++];
        }
        while (start2 <= end2){
            tmp[index++] = nums[start2++];
        }
        for (int i = 0; i < tmp.length; i++) {
            nums[left + i] = tmp[i];
        }
        return result;
    }*/
/*    int[] indexS;//记录当前下标的元素对应的初始下标
    int[] tmpIndexS;//分治时临时数组下标元素对应的初始下标
    int[] tmpNumS;//合并时用到的临时数组
    int[] result;//记录结果
    public List<Integer> countSmaller(int[] nums) {
        int n = nums.length;
        indexS = new int[n];
        tmpIndexS = new int[n];
        tmpNumS = new int[n];
        result = new int[n];
        for (int i = 0; i < n; i++) {//初始化indexS数组
            indexS[i] = i;
        }
        merge(nums,0,n-1);
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            list.add(result[i]);
        }
        return list;
    }
    private void merge(int[] nums, int left, int right) {
        if (left >= right){
            return;
        }
        int mid = left + (right - left)/2;
        merge(nums,left,mid);
        merge(nums,mid+1,right);
        int index = left,start1 = left,end1 = mid,start2 = mid + 1,end2 = right;
        while (start1 <= end1 && start2 <= end2){
            if (nums[start1] <= nums[start2]){
                tmpIndexS[index] = indexS[start1];
                tmpNumS[index++] = nums[start1++];
            }else {
                int i = indexS[start2];
                result[i] += end2 - start2 + 1;
                tmpIndexS[index] = indexS[start2];
                tmpNumS[index++] = nums[start2++];
            }
        }
        while (start1 <= end1){
            tmpIndexS[index] = indexS[start1];
            tmpNumS[index++] = nums[start1++];
        }
        while (start2 <= end2){
            tmpIndexS[index] = indexS[start2];
            tmpNumS[index++] = nums[start2++];
        }
        for (int i = left; i <= right; i++) {
            indexS[i] = tmpIndexS[i];
            nums[i] = tmpNumS[i];
        }
    }*/

    public int reversePairs(int[] nums) {
        int result = merge(nums,0,nums.length-1);
        return result;
    }

    private int merge(int[] nums, int left, int right) {
        if (left >= right){
            return 0;
        }
        int mid = left + (right - left)/2;
        int result = merge(nums,left,mid);
        result += merge(nums,mid + 1,right);
        int[] tmp = new int[right - left + 1];
        int index = 0,start1 = left,end1 = mid,start2 = mid + 1,end2 = right;
        //计算翻转对
        while (start1 <= end1){
            while (start2 <= end2 && nums[start1]/2.0 <= nums[start2]){
                start2++;
            }
            if (start2 > end2){
                break;
            }
            result += end1 - start1 + 1;
            start1++;
        }
        //合并
        start1 = left;start2=mid+1;
        while (start1 <= end1 && start2 <= end2){
            if (nums[start1] <= nums[start2]){
                tmp[index++] = nums[start1++];
            }else {
                tmp[index++] = nums[start2++];
            }
        }
        while (start1 <= end1){
            tmp[index++] = nums[start1++];
        }
        while (start2 <= end2){
            tmp[index++] = nums[start2++];
        }
        for (int i = left; i <= right; i++) {
            nums[i] = tmp[i-left];
        }
        return result;
    }

    public int lengthOfLongestSubstring(String ss) {
        char[] s = ss.toCharArray();
        int[] hash = new int[128];
        int result = 0;
        int prev = 0;
        for (int i = 0; i < s.length; i++) {
            hash[s[i]]++;//进窗口
            while (hash[s[i]] > 1){
                hash[s[prev++]]--;//出窗口
            }
            result = Math.max(result,i - prev + 1);
        }
        return result;
    }
    public int minOperations(int[] nums, int x) {
        int result = -1;
        int sumNum = 0;
        for (int i = 0; i < nums.length; i++) {
            sumNum += nums[i];
        }
        int target = sumNum - x;
        int sum = 0;
        int left = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            while (sum > target){
                sum -= nums[left];
                left++;
            }
            if (sum == target){
                result = Math.max(result,i - left + 1);
            }
        }

        return result == -1 ? -1 : nums.length - result;
    }
    public ListNode mergeKLists(ListNode[] lists) {
        //优先级队列默认情况下是小根堆,
        // 但是这个链表节点无定法直接比较，我们使用表达式给其进行定义比较规则
        PriorityQueue<ListNode> pq = new PriorityQueue<>((o1, o2) -> o1.val - o2.val);
        ListNode newHead = new ListNode();
        ListNode cur = newHead;
        for (int i = 0; i < lists.length; i++) {
            pq.offer(lists[i]);
        }
        while (!pq.isEmpty()){
            cur.next = pq.poll();
            cur = cur.next;
            if (cur.next!=null){
                pq.offer(cur.next);
            }
        }
        return newHead.next;
    }
    public static void main(String[] args) {
        System.out.println(new Random().nextInt(1));
    }


}
