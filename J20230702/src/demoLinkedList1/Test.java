package demoLinkedList1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Test {
    public static void main3(String[] args) {
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        for (Integer s:
             list) {
            System.out.print(s  + " ");
        }
        System.out.println();
        ListIterator<Integer> lit = list.listIterator();
        while (lit.hasNext()) {
            System.out.print(lit.next()+" ");
            //lit.remove();
        }//???? 思考：这个迭代器的工作原理
        // 作业2：能不能 一边遍历 一边删除？？ 可以删除会发生什么问题？ 不可以删除会发生什么问题？
        System.out.println();
        ListIterator<Integer> lit2 = list.listIterator(list.size());
        while (lit2.hasPrevious()) {
            System.out.print(lit2.previous()+" ");
        }
    }
    public static void main2(String[] args) {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.addLast(30);
        myLinkedList.addLast(30);
        myLinkedList.addLast(30);
        myLinkedList.display();

//        myLinkedList.addIndex(0,11);
//        myLinkedList.addIndex(2,22);
//        myLinkedList.display();
        myLinkedList.removeAllKey(30);
        myLinkedList.display();
    }
    public static void main(String[] args) {
        MySingleList mySingleList = new MySingleList();
        mySingleList.addLast(12);
        mySingleList.addLast(12);
        mySingleList.addLast(12);
        mySingleList.addLast(23);
        mySingleList.addLast(34);
        mySingleList.addLast(45);
        mySingleList.addLast(56);
        mySingleList.addLast(12);
        mySingleList.show4();
        mySingleList.removeAllKey(12);
        mySingleList.show();
        mySingleList.clear();
        mySingleList.show();
        /*        mySingleList.addIndex(0,100);
        mySingleList.show();
        mySingleList.addIndex(3,200);
        mySingleList.show();
        mySingleList.addIndex(7,300);
        mySingleList.show();
        mySingleList.remove(100);
        mySingleList.show();
        mySingleList.remove(200);
        mySingleList.show();
        mySingleList.remove(300);
        mySingleList.show();
        mySingleList.remove(500);*/

        /*
        try {
            mySingleList.addIndex(17,300);
        }catch (PosOutBoundsException e){
            e.printStackTrace();
        }*/
    }
}
