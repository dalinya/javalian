package demoLinkedList1;

public class PosOutBoundsException extends RuntimeException{
    public PosOutBoundsException() {
    }

    public PosOutBoundsException(String message) {
        super(message);
    }
}
