package demoLinkedList1;

public class MyLinkedList {
    static class ListNode {
        public int val;
        public ListNode prev;
        public ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }
    public ListNode head;
    public ListNode last;
    public int size(){
        ListNode cur = this.head;
        int count = 0;
        while (cur != null){
            cur = cur.next;
            count++;
        }
        return count;
    }

    public void display(){
        ListNode cur = this.head;
        while (cur != null){
            System.out.print(cur.val + " ");
            cur = cur.next;
        }
        System.out.println();
    }

    //查找是否包含关键字key是否在链表当中
    public boolean contains(int key){
        ListNode cur = this.head;
        while (cur != null){
            if (cur.val == key){
                return true;
            }
            cur = cur.next;
        }
        return false;
    }
    //头插法
    public void addFirst(int data){
        ListNode node = new ListNode(data);
        if(this.head == null){
            this.head = node;
            this.last = node;
            return;
        }
        node.next = head;
        head.prev = node;
        head = node;
    }
    //尾插法
    public void addLast(int data){
        ListNode node = new ListNode(data);
        if(this.head == null){
            this.head = node;
            this.last = node;
            return;
        }
        last.next = node;
        node.prev = last;
        last = node;
    }
    //任意位置插入,第一个数据节点为0号下标
    public void addIndex(int index,int data){
        if (index < 0 || index > size()){
            throw new PosOutBoundsException("add 位置错误");
        }
        if(index == 0){
            addFirst(data);
            return;
        }
        if (index == size()){
            addLast(data);
            return;
        }
        ListNode node = new ListNode(data);
        ListNode cur = head;
        for (int i = 0; i < index; i++) {
            cur =  cur.next;
        }
        node.next = cur;
        node.prev = cur.prev;
        cur.prev.next = node;
        cur.prev = node;
    }

    //删除第一次出现关键字为key的节点
    public void remove(int key){
        ListNode cur = head;
        while (cur != null){
            if (cur.val == key){
                if (cur.prev == null){//头节点
                    head = head.next;
                    if(head.next != null){//不是单个节点
                        head.prev = null;
                    }else {
                        last = null;
                    }
                }else {
                    if (cur.next == null){//尾节点
                        cur = cur.prev;
                        cur.next = null;
                    }else {//普通节点
                        cur.prev.next = cur.next;
                        cur.next.prev = cur.prev;
                    }
                }
                return;
            }
            cur = cur.next;
        }
    }




    //删除所有值为key的节点
    public void removeAllKey(int key){
        ListNode cur = head;
        while (cur != null){
            if (cur.val == key){
                if (cur == head){//头节点
                    head = head.next;
                    if(head != null){//不是单个节点
                        head.prev = null;
                    }else {
                        last = null;
                    }
                }else {
                    if (cur == last){//尾节点
                        cur = cur.prev;
                        cur.next = null;
                    }else {//普通节点
                        cur.prev.next = cur.next;
                        cur.next.prev = cur.prev;
                    }
                }
            }
            cur = cur.next;
        }
    }
    public void clear(){
        ListNode cur = head;
        while (cur != null){
            ListNode curTmp = cur.next;
            cur.prev = null;
            cur.next = null;
            cur = curTmp;
        }
        last = null;
    }
}
