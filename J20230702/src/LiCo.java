import java.util.*;

public class LiCo {

    public void moveZeroes(int[] nums) {
        for (int cur = 0,dest = 0; cur < nums.length; cur++) {
            if (nums[cur] != 0){
                int tmp = nums[cur];
                nums[cur] = nums[dest];
                nums[dest++] = tmp;
            }
        }
    }
    public void duplicateZeros(int[] arr) {
        //预处理找到最后复写的位置下标
        int slow = 0,fast = 0,n = arr.length;
        while (fast < n - 1){
            if (arr[slow++] == 0){
                fast+=2;
            }else {
                fast++;
            }
        }
        slow--;
        if (fast == n){
            arr[n-1] = 0;
            fast-=2;
            slow--;
        }
        while (slow >= 0){
            if (arr[slow--] != 0){
                arr[fast] = arr[slow + 1];
            }else {
                arr[fast--] = 0;
                arr[fast--] = 0;
            }
        }
    }
    public boolean isHappy(int n) {
        int slow = n,fast = bitSum(slow);
        while (slow != fast){
            slow = bitSum(slow);
            fast = bitSum(bitSum(fast));
        }
        System.out.println(slow);
        return slow == 1;
    }

    private int bitSum(int num) {
        int sum = 0;
        while (num != 0){
            int tmp = num % 10;
            sum +=  tmp * tmp;
            num /= 10;
        }
        return sum;
    }
    public void rotate(int[] nums, int k) {
        int len = nums.length;
        k %= len;
        for (int i = 0; i < k; i++) {
            int tmp = nums[len - 1];
            for (int j = len - 1; j >  0; j--) {
                nums[j] = nums[j - 1];
            }
            nums[0] = tmp;
        }
    }
    public void rotate2(int[] nums, int k) {
        int len = nums.length;
        k %= len;
        rotateChild(nums,0,len - k - 1);
        rotateChild(nums,len - k,len - 1);
        rotateChild(nums,0,len);
    }

    private void rotateChild(int[] nums, int left, int right) {
        while (left < right){
            int tmp = nums[left];
            nums[left] = nums[right];
            nums[right] = tmp;
            left++;
            right--;
        }
    }

    public int removeElement(int[] nums, int val) {
        int slow = 0;
        for (int fast = 0; fast < nums.length; fast++) {
            if (nums[fast] != val){
                nums[slow++] = nums[fast];
            }
        }
        return slow;
    }
    public int removeDuplicates(int[] nums) {
        int slow = 0;
        for (int fast = 1; fast < nums.length; fast++) {
            if (nums[slow] != nums[fast]){
                nums[++slow] = nums[fast];
            }
        }
        return slow;
    }

    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int index1 = m - 1;
        int index2 = n - 1;
        for (int i = m + n - 1; i >= 0; i--) {
            if (nums1[index1] > nums2[index2]){
                nums1[i] = nums1[index1];
                index1--;
            }else {
                nums1[i] = nums2[index2];
                index2--;
            }
            if (index1 < 0 || index2 < 0){
                break;
            }
        }
        System.out.println(Arrays.toString(nums1));
        if (index2 >= 0){
            nums1[index2] = nums2[index2];
            index2--;
        }
    }

    public static void func(String str1,String str2){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str1.length(); i++) {
            if (!str2.contains(str1.charAt(i) + "")){
                sb.append(str1.charAt(i));
            }
        }
        System.out.println(sb.toString());
    }
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        path.add(1);
        result.add(path);
        for (int i = 1; i < numRows; i++) {
            List<Integer> tmp = new ArrayList<>();
            tmp.add(1);
            for (int j = 1; j < i; j++) {
                tmp.add(result.get(i-1).get(j) + result.get(i-1).get(j-1));
            }
            tmp.add(1);
            result.add(tmp);
        }
        return result;
    }

    public int[] sortArray(int[] nums) {
        qSort(nums,0,nums.length-1);
        return nums;
    }

    private void qSort(int[] nums, int start, int end) {
        if(start >= end){//递归终止条件
            return;
        }
        int key = nums[new Random().nextInt(end - start + 1) + start];
        int left = start - 1,right = end + 1,index = start;
        while (index < right){
            if (nums[index] < key){
                swap(nums,index++,++left);
            }else if (nums[index] == key){
                index++;
            }else {
                swap(nums,index,--right);
            }
        }
        qSort(nums,start,left);
        qSort(nums,right,end);
    }


    public int findKthLargest(int[] nums, int k) {
        return qSort(nums,0,nums.length - 1,k);
    }

    private int qSort(int[] nums, int start, int end, int k) {
        if(start == end){
            return nums[start];
        }
        int left = start - 1;
        int right = end + 1;
        int index = 0;
        int key = nums[(new Random().nextInt(end - start + 1) + start)];
        while (index < right){
            if(nums[index] < key){
                swap(nums,++left,index++);
            }else if (nums[index] == key){
                index++;
            }else {
                swap(nums,--right,index);
            }
        }
        if(end - right + 1 >= k){
            return qSort(nums,right,end,k);
        }else if (end - left >= k){
            return nums[left + 1];
        }else {
            return qSort(nums,0,left,k);
        }
    }
    public int[] getLeastNumbers(int[] arr, int k) {
        qsort(arr,0,arr.length - 1,k);
        int[] result = new int[k];
        for (int i = 0; i < k; i++) {
            result[i] = arr[i];
        }
        return result;
    }

    private void qsort(int[] arr, int start, int end, int k) {
        if(start == end){
            return;
        }
        int left = start - 1,right = end + 1,index = start;
        int key = arr[new Random().nextInt(end - start + 1) + start];
        while (index < right){
            if(arr[index] < key){
                swap(arr,++left,index++);
            }else if (arr[index] == key){
                index++;
            }else {
                swap(arr,--right,index);
            }
        }
        if (left - start >= k){
            qsort(arr,left,start,k);
        }else if (right - start >= k){
            return;
        }else {
            qsort(arr,right,end,k - right + start);
        }
    }
    private void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }
    private void mergeSort(int[] array,int left,int right){
        if(left >= right){
            return;
        }
        int mid = left + (right - left)/2;
        //递归
        mergeSort(array,left,mid);
        mergeSort(array,mid + 1,right);
        //合并
        int[] tmp = new int[right - left + 1];
        int index = 0, start1 = left,end1 = mid,start2 = mid + 1,end2 = right;
        while (start1 <= end1 && start2 <= end2){
            if(array[start1] < array[start2]){
                tmp[index++] = array[start1++];
            }else {
                tmp[index++] = array[start2++];
            }
        }
        while (start1 <= end1){
            tmp[index++] = array[start1++];
        }
        while (start2 <= end2){
            tmp[index++] = array[start2++];
        }
        for (int i = 0; i < tmp.length; i++) {
            array[i + left] = tmp[i];
        }
    }
    public int reversePairs1(int[] nums) {
        int result = 0;
        return merge1(nums,0,nums.length-1);

    }

    private int merge1(int[] nums, int left, int right) {
        if(left >= right){
            return 0;
        }
        //递归
        int mid = left + (right - left)/2;
        int result = merge1(nums,left,mid);
        result += merge1(nums,mid + 1,right);
        //合并
        int[] tmp = new int[right - left + 1];
        int start1 = left,end1 = mid,start2 = mid + 1,end2 = right,index = 0;
        while (start1 <= end1 && start2 <= end2){
            if(nums[start1] <= nums[start2]){
                tmp[index++] = nums[start1++];
            }else {
                result += end1 - start1 + 1;
                tmp[index++] = nums[start2++];
            }
        }
        while (start1 <= end1){
            tmp[index++] = nums[start1++];
        }
        while (start2 <= end2){
            tmp[index++] = nums[start2++];
        }
        for (int i = 0; i < tmp.length; i++) {
            nums[left + i] = tmp[i];
        }
        return result;
    }

    int[] indexS;
    int[] tmpIndexS;
    int[] tmpNumS;
    int[] result;
    public List<Integer> countSmaller(int[] nums) {
        int n = nums.length;
        indexS = new int[n];
        tmpIndexS = new int[n];
        tmpNumS = new int[n];
        result = new int[n];
        for (int i = 0; i < n; i++) {//初始化indexS数组
            indexS[i] = i;
        }
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            list.add(result[i]);
        }
        return list;
    }
    private void merge2(int[] nums, int left, int right) {
        if(left >= right){
            return;
        }
        int mid = left + (right - left)/2;
        merge2(nums,left,mid);
        merge2(nums,mid+1,right);
        int cur1 = left,cur2 = mid + 1,index = 0;
        while (cur1 <= mid && cur2 <= right){//降序
            if(nums[cur1] <= nums[cur2]){
                tmpIndexS[index] = indexS[cur2];
                tmpNumS[index++] = nums[cur2++];
            }else {
                int i = indexS[cur1];//原始下标
                result[i] += right - cur2 + 1;
                tmpIndexS[index] = indexS[cur1];
                tmpNumS[index++] = nums[cur1++];
            }
        }
        while (cur1 <= mid){//降序
            tmpNumS[index++] = nums[cur2++];
        }
        while (cur2 <= right){//降序
            tmpNumS[index++] = nums[cur1++];
        }
        for (int i = left; i <= right; i++) {
            nums[i] = tmpNumS[i - left];
            indexS[i] = tmpIndexS[i - left];
        }
    }
    int[] tmp;
    public int reversePairs(int[] nums) {
        tmp = new int[nums.length];
        return merge(nums,0,nums.length-1);
    }
    private int merge(int[] nums, int left, int right){
        if(left >= right){
            return 0;
        }
        int result = 0;
        //递归
        int mid = left + (right - left)/2;
        result += merge(nums,left,mid);
        merge(nums,mid+1,right);
        //进行计算翻转对
        int cur1 = left,cur2 = mid + 1;
        while (cur1 <= mid && cur2 <= right){//升序
            if(nums[cur1] <= 2 * nums[cur2]){
                cur1++;
            }else {
                result += mid - cur1 + 1;
                cur2++;
            }
        }
        //进行数组的排序
        cur1 = left;cur2 = mid + 1;
        int index = left;
        while (cur1 <= mid && cur2 <= right){
            if(nums[cur1] <= nums[cur2]){
                tmp[index++] = nums[cur1++];
            }else {
                tmp[index++] = nums[cur2++];
            }
        }
        while (cur1 <= mid){
            tmp[index++] = nums[cur1++];
        }
        while (cur2 <= right){
            tmp[index++] = nums[cur2++];
        }
        for (int i = left; i <= right; i++) {
            nums[i] = tmp[i - left];
        }
        return result;
    }

    public static void main(String[] args) {
        func("welcome to bit","come");
    }

    //使用位图进行解决
    public boolean isUnique(String astr) {
        if(astr.length() > 26){//优化
            return false;
        }
        char[] chars = astr.toCharArray();
        int hash = 0;//一个整形数据可以存储32个哈希
        for (int i = 0; i < chars.length; i++) {
            int x = (chars[i] - 'a');
            if(((hash >> x) & 1) != 0){//判断x位是否为1
                return false;
            }else {
                //将x位变为1
                hash = hash | (1 << x);
            }
        }
        return true;
    }
    //哈希表解决
    public int missingNumber(int[] nums) {
        int[] hash = new int[nums.length + 1];
        for(int i = 0;i < nums.length;i++){
            hash[nums[i]]++;
        }
        for(int i = 0;i < hash.length;i++){
            if (hash[i] == 0){
                return hash[i];
            }
        }

        return 0;
    }

    public int[] missingTwo(int[] nums) {
        int sum = 0;
        for(int i = 0;i < nums.length;i++){
            sum ^= nums[i];
        }
        for(int i = 1;i <= nums.length + 2;i++){
            sum ^= i;
        }
        //sum是两个缺失数字的异或值
        //现在将他们的最右侧不同的一位找出来
        sum = sum & (0 - sum);
        //将数据分为两组进行异或寻找答案
        int[] result = new int[2];
        for(int i = 0;i < nums.length;i++){
            if((sum & nums[i]) == 0){
                result[0] ^= nums[i];
            }else {
                result[1] ^= nums[i];
            }
        }
        for(int i = 1;i <= nums.length + 2;i++){
            if((sum & i) == 0){
                result[0] ^= i;
            }else {
                result[1] ^= i;
            }
        }
        return result;
    }
    public String modifyString(String ss) {
        char[] s = ss.toCharArray();
        for (int i = 0; i < ss.length(); i++) {
            if(s[i] == '?'){
                for(char ch = 'a';ch <= 'z';ch++){
                    if((i == 0||s[i-1] != ch) && (i == ss.length() - 1||s[i + 1] != ch)){
                        s[i] = ch;
                        break;
                    }
                }
            }
        }
        return new String(s);
    }

    public String convert(String ss, int numRows) {
        //模拟+找规律
        //公差：2 * numRows - 2
        //第一行：0 -> 0 + d -> 0 + 2d …… -> 0 + kd
        //第k行：(k,d-k) -> (k+d,d-k+d) -> (k+2d,d-k+2d) -> ……
        //n-1行：n-1 -> n-1+d-> n-1+2d -> n-1+kd

        StringBuilder result = new StringBuilder();
        char[] s = ss.toCharArray();
        int d = 2 * numRows - 2;
        for (int i = 0; i < s.length; i+= d) {//处理第一行
            result.append(s[i]);
        }
        for(int i = 1;i < numRows - 1;i++){//处理中间行
            for (int j = i; j < s.length; j+=d) {
                result.append(s[j]);
                if(j - 2 * i + d < s.length){
                    result.append(s[j - 2 * i + d]);
                }
            }
        }
        for (int i = numRows - 1; i < s.length; i+=d) {//处理最后一行
            result.append(s[i]);
        }
        return result.toString();
    }

    public int minNumberOfFrogs(String croakOfFrogs) {
        String str = "croak";//青蛙的叫声
        Map<Character,Integer> map = new HashMap<>();
        int n = str.length();
        for (int i = 0; i < n; i++) {
            map.put(str.charAt(i),i);
        }
        int[] hash = new int[n];
        char[] s = croakOfFrogs.toCharArray();
        for (int i = 0; i < n; i++) {
            if (s[i] == str.charAt(0)){
                if (hash[n-1] > 0){
                    hash[n-1]--;
                }
                hash[0]++;
            }else {
                int num = map.get(s[i]);
                if(hash[num - 1] == 0){
                    return -1;

                }
                hash[num-1]--;
                hash[num]++;
           }
        }
        for(int i = 0;i < n -1;i++){
            if(hash[i] != 0){
                return -1;
            }
        }
        return hash[n - 1];
    }
    public void sortColors(int[] nums) {
        //三指针进行划分
        //left:0位置的最右边
        //right:2位置的最左边
        int i = 0,left = -1,right = nums.length;
        while(i < right){
            //[0,left]:全是0.[left + 1,i - 1]:全是1.
            //[i,right-2]:未划分的元素。[right,n]:全是2
            if(nums[i] == 0){
                sortColorsSwap(nums,++left,i);
                i++;
            }else if (nums[i] == 1){
                i++;
            }else {
                sortColorsSwap(nums,i,--right);
                i++;
            }
        }
    }

    private void sortColorsSwap(int[] nums, int left, int right) {
        int tmp = nums[left];
        nums[left] = nums[right];
        nums[right] = tmp;
    }


}
