package demoBinaryTree;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class BinaryTree {
    static class TreeNode{
        public char val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(char val) {
            this.val = val;
        }

        @Override
        public String toString() {
            return "TreeNode{" +
                    "val=" + val +
                    '}';
        }
    }
    public TreeNode createTree(){
        TreeNode A = new TreeNode('A');
        TreeNode B = new TreeNode('B');
        TreeNode C = new TreeNode('C');
        TreeNode D = new TreeNode('D');
        TreeNode E = new TreeNode('E');
        TreeNode F = new TreeNode('F');
        TreeNode G = new TreeNode('G');
        TreeNode H = new TreeNode('H');
        A.left = B;
        A.right = C;
        B.left = D;
        B.right = E;
        C.left = F;
        C.right = G;
        E.right = H;
        return A;
    }
    //对二叉树进行遍历 前序遍历  中序遍历 后序遍历 层次遍历
    //前序： 根左右
    //中序：
    //后续：
    //层次：

    //递归的结束条件：root == null
    // 前序遍历
    public void preOrder(TreeNode root) {
        if(root == null){
            return;
        }
        System.out.print(root.val + " ");
        preOrder(root.left);
        preOrder(root.right);
    }
    // 中序遍历
    public void inOrder(TreeNode root) {
        if(root == null){
            return;
        }
        inOrder(root.left);
        System.out.print(root.val + " ");
        inOrder(root.right);
    }
    // 后序遍历
    public void postOrder(TreeNode root) {
        if(root == null){
            return;
        }
        postOrder(root.left);
        postOrder(root.right);
        System.out.print(root.val + " ");
    }
    public static int usedSize = 0;
    // 获取树中节点的个数
    public int size(TreeNode root) {
        if (root == null){
            return 0;
        }
        size(root.left);
        size(root.right);
        usedSize++;
        return usedSize;
    }
    // 获取树中节点的个数
    public int size2(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return size2(root.left) + size2(root.right) + 1;
    }

    public static int leafSize = 0;
    public int getLeafNodeCount(TreeNode root) {
        if(root == null){
            return 0;
        }
        if (root.left == null && root.right == null){
            leafSize++;
        }
        getLeafNodeCount(root.left);
        getLeafNodeCount(root.right);
        return leafSize;
    }
    public int getLeafNodeCount2(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if(root.left == null && root.right == null){
            return 1;
        }
        return getLeafNodeCount2(root.left) + getLeafNodeCount2(root.right);
    }
    // 获取第K层节点的个数
    public int getKLevelNodeCount(TreeNode root, int k) {
        if(root == null){
            return 0;
        }
        if(k == 1){
            return 1;
        }
        return getKLevelNodeCount(root.left,k-1) + getKLevelNodeCount(root.right,k-1);
    }
    /**
     * 获取二叉树的高度
     * 时间复杂度：O(N)
     * @param root
     * @return
     */
    public  int getHeight(TreeNode root) {
        if(root == null){
            return 0;
        }
        return Math.max(getHeight(root.left),getHeight(root.right)) + 1;
    }
    // 检测值为value的元素是否存在
    public TreeNode find(TreeNode root, int val){
        if (root == null){
            return null;
        }
        if (root.val == val){
            return root;
        }
        TreeNode nodeLeft = find(root.left,val);
        if (nodeLeft != null){
            return nodeLeft;
        }
        TreeNode nodeRight = find(root.right,val);
        if (nodeRight != null){
            return nodeRight;
        }
        return null;
    }
    //层序遍历
    public void levelOrder(TreeNode root){
        Queue<TreeNode> qu = new LinkedList<>();
        if(root != null){
            qu.offer(root);
        }
        while(!qu.isEmpty()){
            int size = qu.size();
            for (int i = 0; i < size; i++) {
                TreeNode top = qu.poll();
                System.out.print(top.val + " ");
                if(top.left != null){
                    qu.offer(top.left);
                }
                if(top.right != null){
                    qu.offer(top.right);
                }
            }
        }
    }
    // 判断一棵树是不是完全二叉树
    public boolean isCompleteTree(TreeNode root){
        Queue<TreeNode> qu = new LinkedList<>();
        qu.offer(root);
        while(!qu.isEmpty()){
            TreeNode cur = qu.poll();
            if(cur == null){
                break;
            }
            qu.offer(cur.left);
            qu.offer(cur.right);
        }
        while(!qu.isEmpty()){
            if(qu.poll() != null){
                return false;
            }
        }
        return true;
    }
    //非递归前序遍历二叉树
    public void preOrderNOr(TreeNode root) {
        if(root == null){
            return;
        }
        Stack<TreeNode> stack = new Stack<>();
        TreeNode cur = root;
        while(cur != null || !stack.isEmpty()){
            while(cur != null){
                System.out.print(cur.val + " ");
                stack.push(cur);
                cur = cur.left;
            }
            //找右子树
            TreeNode tmp = stack.pop();
            cur = tmp.right;
        }
    }
    public static void main(String[] args) {
        BinaryTree binaryTree = new BinaryTree();
        BinaryTree.TreeNode root = binaryTree.createTree();
        binaryTree.preOrder(root);
        System.out.println();
        binaryTree.inOrder(root);
        System.out.println();
        binaryTree.postOrder(root);
        System.out.println();
        System.out.println(binaryTree.size2(root));
        System.out.println(binaryTree.size(root));
        System.out.println(binaryTree.usedSize);
        System.out.println(binaryTree.getLeafNodeCount(root));
        System.out.println(binaryTree.leafSize);
        System.out.println(binaryTree.getLeafNodeCount2(root));
        System.out.println(binaryTree.getKLevelNodeCount(root,3));
        System.out.println("树的高度: " + binaryTree.getHeight(root));
        System.out.println("二叉树查找：" + binaryTree.find(root,'E'));
        binaryTree.levelOrder(root);
        System.out.println("是否是平衡二叉树" + binaryTree.isCompleteTree(root));
        binaryTree.preOrderNOr(root);
        System.out.println();

    }

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        Stack<TreeNode> stack1 = new Stack<>();
        getPath(root,p,stack1);//到p节点的位置
        Stack<TreeNode> stack2 = new Stack<>();
        getPath(root,q,stack2);//到q节点的位置
        //类似于两个链表求交点
        int len1 = stack1.size();
        int len2 = stack2.size();
        if(len1 > len2){
            int len = len1 - len2;
            for(int i = 0;i < len;i++){
                stack1.pop();
            }
        }else{
            int len = len2 - len1;
            for(int i = 0;i < len;i++){
                stack2.pop();
            }
        }
        while(stack1.peek() != stack2.peek()){
            stack1.pop();
            stack2.pop();
        }
        return stack1.peek();
    }
    //在root 这棵树中 到 node节点的位置
    public boolean getPath(TreeNode root, TreeNode node, Stack<TreeNode> stack){
        if(root == null){
            return false;
        }
        stack.push(root);
        //当前节点就是node
        if(root == node){
            return true;
        }
        //左子树右node节点
        if(getPath(root.left,node,stack)){
            return true;
        }
        //右子树右node节点
        if(getPath(root.right,node,stack)){
            return true;
        }
        stack.pop();
        return false;
    }
}
