import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

class Student{
    public int classId;
    public int studentId;

}
public class TestJackson {
    public static void main(String[] args) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
//        String s = "{ \"classId\":10 \"studentId\":20}";
//        Student student = objectMapper.readValue(s,Student.class);
//        System.out.println(student.studentId);
//        System.out.println(student.classId);

        Student student = new Student();
        student.studentId = 10;
        student.classId = 20;
        String s = objectMapper.writeValueAsString(student);
        System.out.println(s);
    }
}

















