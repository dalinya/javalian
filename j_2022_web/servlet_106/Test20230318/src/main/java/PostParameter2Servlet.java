import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

class Student{
    public String studentId;
    public String classId;
}

@WebServlet("/postParameter2")
public class PostParameter2Servlet extends HelloServlet{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        int length = req.getContentLength();
//        byte[] buffer = new byte[length];
//
//        InputStream inputStream = req.getInputStream();
//        inputStream.read(buffer);
//
//        String body = new String(buffer);
//        System.out.println("body = " + body);

        ObjectMapper objectMapper = new ObjectMapper();
        Student student = objectMapper.readValue(req.getInputStream(),Student.class);
        System.out.println(student.studentId+","+student.classId);

    }
}
