import java.util.*;

public class testLanQiao {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //在此输入您的代码...
        int num = scan.nextInt();
        int sum = 0;
        int[] array = new int[num];
        for (int i = 0; i < num; i++) {
            array[i] = scan.nextInt();
            sum += array[i];
        }
        scan.close();
        HashSet<Integer> set = new HashSet<>();
        set.add(0);
        for (int i = 0; i < array.length; i++) {
             List<Integer> list = new ArrayList<>(set);
            for (int k: list) {
                set.add(k + array[i]);
                set.add(Math.abs(k-array[i]));
            }
        }
        set.remove(0);
        System.out.println(set.size());
    }
    public static void main3(String[] args) {
        Scanner scan = new Scanner(System.in);
        //在此输入您的代码...
        int n = scan.nextInt();
        int[][] arr = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= i; j++) {
                arr[i][j] = scan.nextInt();
            }
        }
        for (int i = 1; i < n; i++) {
            for (int j = 0; j <= i; j++) {
                if (j==0){
                    arr[i][j] += arr[i-1][j];
                }else if (j==i){
                     arr[i][j] += arr[i-1][j-1];
                }else {
                    arr[i][j] += Math.max(arr[i-1][j-1],arr[i-1][j]);
                }
            }
            
        }

        scan.close();
        int result = 0;
        if (n%2 == 1){
            result = arr[n-1][n/2];
        }else {
            result = Math.max(arr[n-1][n/2-1],arr[n-1][n/2]);
        }
        System.out.println(result);
    }
    public static void main2(String[] args) {
        Scanner scan = new Scanner(System.in);
        int count = scan.nextInt();
        int[] array = new int[count + 1];
        for (int i = 1; i <= count; i++) {
            array[i] = scan.nextInt();
        }
        Queue<Integer> queue = new LinkedList<>();
        queue.offer(array[1]);
        int level = 1;
        int maxNum = queue.peek();
        int result = 1;
        int i = 2;
        while (!queue.isEmpty()){
            int sum = 0;
            int size = queue.size();
            while (size-- > 0){
                sum += queue.poll();

                for (int j = 0; j < 2; j++) {
                    if (i > count){
                        continue;
                    }
                    queue.offer(array[i]);
                    i++;
                }
            }
            if (sum > maxNum){
                maxNum = sum;
                result = level;
            }
            level++;
        }
        System.out.println(result);
    }

    public static void main1(String[] args) {
        Scanner scan = new Scanner(System.in);
        int count = 0;
        count = scan.nextInt();
        int[] array = new int[count + 1];
        for (int i = 1; i <= count; i++) {
            array[i] = scan.nextInt();
        }
        //1 3 7
        int left = 1;
        int flg = 1;
        ArrayList<Integer> list = new ArrayList<>();
        list.add(0);
        int sum = 0;
        left = (int) (Math.pow(2,flg) - 1);
        for (int i = 1; i < array.length; i++) {
            if (i <= left){
                sum += array[i];
            }else {
                list.add(sum);
                sum = array[i];
                flg ++;
                left = (int) (Math.pow(2,flg) - 1);
            }
        }
        if (left == count){
            list.add(sum);
        }

        int num = Integer.MIN_VALUE;
        int index = 1;
        for (int i = 1; i < list.size(); i++) {
            if (num < list.get(i)){
                index = i;
                num = list.get(i);
            }

        }
        //在此输入您的代码...
        scan.close();
        System.out.println(index);
    }
}
