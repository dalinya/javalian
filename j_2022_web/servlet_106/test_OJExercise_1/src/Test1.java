import java.util.*;
class MyQueue{
    LinkedList<Integer> qu;

    public MyQueue() {
        this.qu = new LinkedList<>();
    }

    public void push(int val){
        while (!qu.isEmpty()&&qu.getLast()<val){
            qu.removeLast();
        }
        qu.addLast(val);
    }
    public void pop(int val){
        if (!qu.isEmpty()&&qu.get(0) == val){
            qu.poll();
        }
    }
    public int getMaxVal(){
        return qu.get(0);
    }
}

class Pair{
    int x;
    int y;

    public Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}

class Employee {
    public int id;
    public int importance;
    public List<Integer> subordinates;
};
public class Test1 {




    String[] hash = {"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};

    int[][]  nextP = {{1,0},{-1,0},{0,1},{0,-1}};

    public int openLock(String[] deadends, String target) {
        Set<String> deadSet = new HashSet<>();
        for (int i = 0; i < deadends.length; i++) {//将
            deadSet.add(deadends[i]);
        }
        if (deadSet.contains(target)){
            return -1;
        }
        int result = 0;
        Set<String> used = new HashSet<>();
        used.add("0000");
        Queue<String> qu = new LinkedList<>();
        qu.offer("0000");
        while (!qu.isEmpty()){
            int size = qu.size();
            while (size-- > 0){
                String str = qu.poll();
                if (str.equals(target)){
                    return result;
                }
                for (int i = 0; i < str.length(); i++) {
                    StringBuilder sb = new StringBuilder(str);
                    char ch1 = str.charAt(i);
                    char ch2 = str.charAt(i);
                    if (ch1 == '9'){
                        ch1 = '0';
                    }else {
                        ch1++;
                    }
                    sb.setCharAt(i,ch1);
                    if (!deadSet.contains(sb.toString()) && !used.contains(sb.toString())) {
                        qu.offer(sb.toString());
                        used.add(sb.toString());
                    }
                    if (ch2 == '0'){
                        ch2 = '9';
                    }else {
                        ch2--;
                    }
                    sb.setCharAt(i,ch2);
                    if (!deadSet.contains(sb.toString()) && !used.contains(sb.toString())) {
                        qu.offer(sb.toString());
                        used.add(sb.toString());
                    }
                }
            }
            if (!qu.isEmpty()){
                result++;
            }
        }
        return -1;
    }
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Set<String> wordSet = new HashSet<>(wordList);
        int result = 1;
        if (!wordSet.contains(endWord)){
            return 0;
        }
        Queue<String> qu = new LinkedList<>();
        Set<String> used = new HashSet<>();
        qu.offer(beginWord);
        used.add(beginWord);
        while (!qu.isEmpty()){
            int size = qu.size();
            while (size-->0){
                String str = qu.poll();
                for (int i = 0; i < str.length(); i++) {
                    StringBuilder sb = new StringBuilder(str);
                    for (char ch = 'a'; ch <= 'z'; ch++) {
                        sb.setCharAt(i,ch);
                        if (!used.contains(sb.toString())&&wordSet.contains(sb.toString())){
                            qu.offer(sb.toString());
                            used.add(sb.toString());
                        }
                        if (sb.toString().equals(endWord)){
                            return result+1;
                        }
                    }
                }
            }
            if (qu.size() > 0){
                result++;
            }
        }
        return 0;
    }
    public int orangesRotting(int[][] grid) {
        int result = 0;
        Queue<Pair> qu = new LinkedList<>();
        int row = grid.length;
        int col = grid[0].length;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 2){
                    qu.offer(new Pair(i,j));
                }
            }
        }
        while (!qu.isEmpty()){
            int size = qu.size();
            boolean flg = false;
            while (size-- > 0){
                Pair pair = qu.poll();
                int x = pair.x;
                int y = pair.y;
                for (int i = 0; i < 4; i++) {
                    int newX = x + nextP[i][0];
                    int newY = y + nextP[i][1];
                    if (newX < 0 || newX >= row
                        ||newY < 0 || newY >= col){
                        continue;
                    }
                    if (grid[newX][newY] == 1){
                        grid[newX][newY] = 2;
                        qu.offer(new Pair(newX,newY));
                        flg = true;
                    }
                }
            }
            if (flg){
                result++;
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 1){
                    return -1;
                }
            }
        }
        return result;
    }
    public int numTilePossibilities(String tiles) {
        String path = "";
        boolean[] used = new boolean[tiles.length()];
        int index = 0;
        Set<String> set = new HashSet<>();
        DFS(set,tiles,path,used);


        return set.size();
    }

    private void DFS(Set<String> set, String tiles, String path, boolean[] used) {
        System.out.println(path);
        if (path.length() > 0){
            set.add(path);
        }
        if (path.length() >= tiles.length()){
            return;
        }
        for (int i = 0; i < tiles.length(); i++) {
            if (used[i] == true){
                continue;
            }
            used[i] = true;
            DFS(set,tiles,path + tiles.charAt(i),used);
            used[i] = false;
        }

    }

    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        int pathSum = 0;
        DFS6(result,path,candidates,pathSum,target,0);
        return result;
    }

    private void DFS6(List<List<Integer>> result, List<Integer> path, int[] candidates, int pathSum, int target, int index) {
        if (pathSum > target){
            return;
        }
        if (pathSum == target){
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = index; i < candidates.length; i++) {
            path.add(candidates[i]);
            DFS6(result,path,candidates,pathSum + candidates[i],target,i);
            path.remove(path.size() - 1);
        }
    }

    public List<String> letterCombinations(String digits) {
        char[] arr = digits.toCharArray();
        List<String> result = new ArrayList<>();
        if (digits.length() == 0){
            return result;
        }
        DFS5(result,arr,"",0);

        return result;
    }

    private void DFS5(List<String> result, char[] arr, String path, int index) {
        if (path.length() == arr.length){
            result.add(path);
            return;
        }
        int num =  Integer.parseInt(arr[index] + "");
        String str = hash[num];
        for (int i = 0; i < str.length(); i++) {
            DFS5(result,arr,path + str.charAt(i),index + 1);
        }
    }

    public int numIslands(char[][] grid) {
        int row = grid.length;
        int col = grid[0].length;
        int result = 0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == '1'){
                    DFS4(grid,i,j,row,col);
                    result++;
                }
            }
        }
        System.out.println(Arrays.deepToString(grid));
        return result;
    }

    private void DFS4(char[][] grid, int x, int y, int row, int col) {
        grid[x][y] = '*';
        for (int i = 0; i < 4; i++) {
            int newX = nextP[i][0];
            int newY = nextP[i][1];
            if (newX < 0 || newX >= row
                ||newY < 0 || newY >= col){//判断是否越界
                continue;
            }
            if (grid[newX][newY] == '1'){//判断是否有陆地
                DFS4(grid,newX,newY,row,col);
            }
        }
    }

    public void solve(char[][] board) {
        int row = board.length;
        int col = board[0].length;
        for (int i = 0; i < row; i++) {//判断第一列和最后一列
            if (board[i][0] == 'O'){
                DFS3(board,i,0,row,col);
            }
            if (board[i][col - 1] == 'O'){
                DFS3(board,i,col-1,row,col);
            }
        }
        for (int j = 0; j < row; j++) {//判断第一行和最后一行
            if (board[0][j] == 'O'){//第一行
                DFS3(board,0,j,row,col);
            }
            if (board[row - 1][j] == 'O'){//最后一列
                DFS3(board,row-1,j,row,col);
            }
        }
        for (int i = 0; i < row; i++) {//对搜索的矩阵进行遍历
            for (int j = 0; j < col; j++) {
                if (board[i][j] == 'O'){
                    board[i][j] = 'X';
                }else if (board[i][j] == '*'){
                    board[i][j] = 'O';
                }
            }
        }
    }

    private void DFS3(char[][] board, int x, int y, int row, int col) {
        board[x][y] = '*';
        for (int i = 0; i < 4; i++) {
            int newX = x + nextP[i][0];
            int newY = y + nextP[i][1];
            if (newX < 0 || newX >= row
                ||newY < 0 || newY >= col){//判断是否越界
                continue;
            }
            if (board[newX][newY] == 'O'){
                DFS3(board,newX,newY,row,col);
            }
        }
    }

    public int[][] floodFill(int[][] image, int sr, int sc, int color) {
        int oldColor = image[sr][sc];
        DFS2(image,sr,sc,color,oldColor);
        return image;
    }

    private void DFS2(int[][] image, int sr, int sc, int color, int oldColor) {
        image[sr][sc] = color;
        for (int i = 0; i < 4; i++) {
            int newX = sr + nextP[i][0];
            int newY = sc + nextP[i][1];
            if (newX < 0 || newX >= image.length
                || newY < 0 || newY >= image[sr].length){
                continue;
            }
            if (image[newX][newY] == color){//这种是为了避免染色和旧色一致
                continue;
            }
            if (image[newX][newY] == oldColor){
                DFS2(image,newX,newY,color,oldColor);
            }
        }
    }

    public int getImportance(List<Employee> employees, int id) {
        int[] result = {0};
        Map<Integer,Employee> map = new TreeMap<>();
        for (Employee employee:employees) {
            map.put(employee.id,employee);
        }
        DFS1(map,id,result);
        return result[0];
    }

    private void DFS1(Map<Integer,Employee> employees, int id, int[] result) {
        result[0] += employees.get(id).importance;
        for (int num: employees.get(id).subordinates) {
            DFS1(employees,num,result);

        }


    }

    public int[] topKFrequent(int[] nums, int k) {
        Map<Integer,Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (!map.containsKey(nums[i])) {
                map.put(nums[i], 1);
            }else {
                int num = map.get(nums[i]);
                map.put(nums[i],num + 1);
            }
        }
        PriorityQueue<Pair> pq = new PriorityQueue<>((n1,n2)->n1.y - n2.y);

        for (int key:map.keySet()) {
            pq.offer(new Pair(key,map.get(key)));
            if (pq.size() > k){
                pq.poll();
            }
        }
        int[] result = new int[k];
        for (int i = k - 1; i >= 0; i--) {
            result[i] = pq.poll().x;
        }
        return result;
    }
    public int[] maxSlidingWindow(int[] nums, int k) {
        int count = nums.length - k + 1;
        int[] result = new int[count];
        MyQueue qu = new MyQueue();
        for (int i = 0; i < nums.length; i++) {
            qu.push(nums[i]);
            if (i >= k){
                result[i-k] = qu.getMaxVal();
                qu.pop(nums[i-k]);
            }
        }
        return result;
    }


    public int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();
        for (String str: tokens) {
            if (str.equals("+")){
                int num1 = stack.pop();
                int num2 = stack.pop();
                stack.add(num1 + num2);
            }else if (str.equals("-")){
                int num1 = stack.pop();
                int num2 = stack.pop();
                stack.add(num1 - num2);
            }else if (str.equals("*")){
                int num1 = stack.pop();
                int num2 = stack.pop();
                stack.add(num1 * num2);
            } else if (str.equals("/")) {
                int num1 = stack.pop();
                int num2 = stack.pop();
                stack.add(num1 / num2);
            }else {
                stack.add(Integer.parseInt(str));
            }

        }
        return stack.pop();
    }

    public String removeDuplicates(String s) {
        String result = "";
        for (char ch:s.toCharArray()) {
            if (result.length() == 0 || !result.endsWith(ch + "")){
                result += ch;
            }else {
                result = result.substring(0,result.length() - 1);
            }
        }

        return result;
    }
    public boolean isValid(String s) {
        char[] arr = s.toCharArray();
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == '('){
                stack.add(')');
            }else if (arr[i] == '['){
                stack.add(']');
            }else if (arr[i] == '{'){
                stack.add('}');
            }else if (stack.isEmpty() ||arr[i] != stack.peek()){
                return false;
            }else{
                stack.pop();
            }
        }
        return !stack.isEmpty();
    }
    public int minPathSum(int[][] grid) {

        int m = grid.length;
        int n = grid[0].length;
        // 记录到每个位置的最小权值和
        for (int i = 1; i < grid.length; i++) {//第一列的初始化
            grid[i][0] += grid[i-1][0];
        }
        for (int j = 1; j < grid[0].length; j++) {
            grid[0][j] += grid[0][j-1];
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                grid[i][j] += Math.min(grid[i][j-1], grid[i-1][j]);
            }
        }
        return grid[m-1][n-1];
    }
    public int findMinArrowShots(int[][] points) {
        int result  = 0;
        int right = 0;
        Arrays.sort(points,(a,b)->{return a[1] - b[1];});//按着结束位置进行升序排序
        for (int i = 0; i < points.length; i++) {
            if (points[i][0] > right){
                right = points[i][1];
                result++;
            }
        }
        return result;
    }
    public int[][] reconstructQueue(int[][] people) {
        Arrays.sort(people,(a,b)->(b[0]-a[0]==0?a[1]-b[1]:b[0]-a[0]));//降序排列
        List<int[]> list = new LinkedList<>();
        for (int i = 0; i < people.length; i++) {
            list.add(people[i][1],people[i]);
        }
        return list.toArray(new int[people.length][]);
    }
    public boolean lemonadeChange(int[] bills) {
        int five = 0;
        int ten = 0;
        for (int i = 0; i < bills.length; i++) {
            if (bills[i] == 5){
                five++;
            }
            if (bills[i] == 10){
                if (five > 1){
                    five--;
                    ten++;
                }else {
                    return false;
                }
            }
            if (bills[i] == 20){
                if (five > 0 && ten > 0){
                    five--;
                    ten--;
                }else if (five > 1){
                    five-=2;
                }else {
                    return false;
                }

            }
        }
        return true;

    }
    public int candy(int[] ratings) {
        int[] candyArr = new int[ratings.length];
        Arrays.fill(candyArr,1);
        for (int i = 1; i < ratings.length; i++) {
            if (ratings[i] > ratings[i-1]){
                candyArr[i] = candyArr[i-1] + 1;
            }
        }
        for (int i = ratings.length - 2; i >= 0; i--) {
            if (ratings[i] > ratings[i + 1]){
                candyArr[i] = Math.max(candyArr[i],candyArr[i+1]+1);
            }
        }

        int result = 0;
        for (int i = 0; i < candyArr.length; i++) {
            result+=candyArr[i];
        }
        return result;
    }
    public int canCompleteCircuit(int[] gas, int[] cost) {
        int total = 0;//判断整体修完车的
        int index = 0;
        int curSum = 0;
        for (int i = 0; i < gas.length; i++) {
            total += (gas[i] - cost[i]);
            curSum += (gas[i] - cost[i]);
            if (curSum < 0){
                index = i + 1;
                curSum = 0;
            }
        }
        if (total < 0){
            return -1;
        }
        return index;
    }

    public int largestSumAfterKNegations(int[] nums, int k) {
        Integer[] array = new Integer[nums.length];
        for (int i = 0; i < nums.length; i++) {
            array[i] = nums[i];
        }

        Arrays.sort(array,(a,b)->Math.abs(b)-Math.abs(a));
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 0){
                array[i] *= -1;
                k--;
            }
            if (k == 0){
                break;
            }
        }
        if (k % 2 == 1){
            array[array.length - 1] *= -1;
        }
        int sum = 0;
        for (int j = 0; j < array.length; j++) {
            sum += array[j];
        }
        return sum;
    }

    public int jump(int[] nums) {
        int result = 0;
        int cur = 0;
        int next = 0;
        if (nums.length == 1){
            return 0;
        }
        for (int i = 0; i < nums.length; i++) {
            next = Math.max(i + nums[i],next);
            if (i == cur){
                cur = next;
                result++;
                if (cur >= nums.length - 1){
                    break;
                }
            }
        }
        return result;
    }

    public boolean canJump(int[] nums) {
        int canCover = 0;
        for (int i = 0; i < nums.length; i++) {
            if (canCover < i){
                return false;
            }
            canCover = Math.max(canCover,i + nums[i]);
        }
        return true;
    }
    public int maxProfit(int[] prices) {
        int[][] dp = new int[prices.length + 1][2];
        //dp[i][0]代表不买股票，dp[i][1]代表买股票最大剩余金额
        dp[0][0] = 0;
        dp[0][1] = -prices[0];
        for (int i = 1; i < prices.length; i++) {
            dp[i][0] = Math.max(dp[i-1][0],dp[i-1][1] + prices[i]);//昨天没有股票和昨天有股票买了最大剩余价值
            dp[i][1] = Math.max(dp[i-1][1],dp[i-1][0]-prices[i]);//昨天有股票和昨天没有股票最大剩余价值
            System.out.println(dp[i][0] + "   " + dp[i][1]);
        }
        return dp[prices.length][0];
    }
    public int maxSubArray(int[] nums) {
        //遇到子序列和为负数就重新开始
        int result = Integer.MIN_VALUE;
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            if (sum >= 0){
                sum += nums[i];
                if (sum > result){
                    result  = sum;
                }
                if (sum < 0){
                    sum = 0;
                }
            }
        }
        return result;
    }
    public int wiggleMaxLength(int[] nums) {
        int curDiff = 0;//当前差值
        int preDiff = 0;//以前前差值
        int result = 1;//前提条件默认为1；
        for (int i = 1; i < nums.length; i++) {
            curDiff = nums[i] - nums[i-1];
            if (curDiff > 0 && preDiff <= 0){
                result++;
                preDiff = curDiff;
            }
            if (curDiff < 0 && preDiff >= 0){
                result++;
                preDiff = curDiff;
            }
        }
        return result;
    }

    public int findContentChildren(int[] g, int[] s) {
        int result = 0;//统计喂饱的同学的人数
        int index = 0;//人的下标
        Arrays.sort(g);
        Arrays.sort(s);
        for (int i = 0; i < s.length; i++) {
            if (g[index] <= s[i]){
                index++;
                result++;
            }
        }
        return result;
    }
    public int findContentChildren1(int[] g, int[] s) {
        //贪心算法
        //只考虑局部最优解，然后逐步推出全局的解(不一定最优)
        //局部最优解--> 先考虑胃口小的孩子，然后再给胃口小的孩子喂小饼干
        //遍历问题：通过遍历孩子找饼干，遍历饼干找孩纸
        int count = 0;//统计喂饱的同学的人数
        int j = 0;//饼干的下标
        Arrays.sort(g);
        Arrays.sort(s);
        for (int i = 0; i < g.length; i++) {
            while (j < s.length && g[i] > s[j]){//g[i] <= s[j]孩子的胃口小于等于饼干大小的时候才会进行吃
                j++;
            }
            if (j < s.length){
                j++;
                count++;
            }else {
                break;
            }
        }
        return count;
    }
    public int[] twoSum(int[] nums, int target) {
        Map<Integer,Integer> map = new HashMap<>();
        int[] arr = new int[2];
        for (int i = 0; i < nums.length; i++) {
            int tmp = target - nums[i];
            if (map.containsKey(tmp)){
                arr[1] = i;
                arr[0] = map.get(tmp);
            }
            map.put(tmp,i);
        }
        return arr;
    }
    public boolean wordBreak(String s, List<String> wordDict) {
        boolean[] dp = new boolean[s.length() + 1];//代表前i个字符是否再字典中
        dp[0] = true;//初始化，辅助条件
        HashSet<String> set = new HashSet<>(wordDict);
        for (int i = 1; i < dp.length; i++) {//先遍历背包
            for (int j = 0; j <= i; j++) {//再遍历物品
                if (dp[j] && set.contains(s.substring(j,i))){
                    dp[i] = true;
                }
            }
        }
        return dp[s.length()];
    }
    public int numSquares(int n) {
        int[] dp = new int[n + 1];//存放最少素数个数
        Arrays.fill(dp,Integer.MAX_VALUE);
        dp[0] = 0;
        for (int i = 1; i <= Math.sqrt(n); i++) {
            for (int j = i*i; j <= n; j++) {
                if (dp[j-i*i] != Integer.MAX_VALUE){//代表此位置没有放任何平方数，并且不为0
                    dp[j] = Math.min(dp[j],dp[j-i*i]+1);
                }
            }
        }
        return dp[n] == Integer.MAX_VALUE ? -1 : dp[n];//判断是否放满背包，没有放满就返回-1
    }
    public int coinChange(int[] coins, int amount) {
        int[] dp = new int[amount + 1];
        Arrays.fill(dp,Integer.MAX_VALUE);
        dp[0] = 0;
        for (int i = 0; i < coins.length; i++) {
            for (int j = coins[i]; j <= amount; j++) {
                if (dp[j-coins[i]] != Integer.MAX_VALUE){
                    dp[j] = Math.min(dp[j],dp[j-coins[i]]+1);//数组的递推
                }
            }
        }
        return  dp[amount] == Integer.MAX_VALUE ? -1 : dp[amount];
    }
    public int combinationSum4(int[] nums, int target) {
        int[] dp = new int[target + 1];
        dp[0] = 1;
        for (int j = 0; j <= target; j++) {//先遍历背包
            for (int i = 0; i < nums.length; i++) {
                if (nums[i] <= dp[j]){
                    dp[j] += dp[j - nums[i]];
                }
            }

        }
        return dp[target];
    }
    public int change(int amount, int[] coins) {
        int[] dp = new int[amount + 1];
        dp[0] = 1;
        for (int i = 0; i < coins.length; i++) {//先遍历背包
            for (int j = coins[i]; j <= amount; j++) {//再去遍历背包
                dp[j] += dp[j - coins[i]];
            }
            for (int j = coins[i]; j <= amount; j++) {//再去遍历背包
                System.out.print(dp[j] + " ");
            }
            System.out.println();
        }
        return dp[amount];
    }


    public String aaa(String str){
        String string = "";
        boolean[] arr = new boolean['z' - 'A'];
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (arr[ch - 'A'] == false){
                arr[ch - 'A'] = true;
                string += (ch + "");
            }
        }
        return string;
    }



    public int findTargetSumWays(int[] nums, int target) {
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
        }
        //left + right = sum  left - right = target
        //left = (sum + target)/2
        if ((sum + target)%2 != 2){//如果正数和不为整数则不存在
            return 0;
        }
        if (Math.abs(target) > sum){
            return 0;
        }
        int bagWeight = (sum + target)/2;//需要的正数和
        int[] dp = new int[bagWeight + 1];
        dp[0] = 1;//当正数和为0时，没有正数和本身也是一种选择
        for (int i = 0; i < nums.length; i++) {//遍历物品
            for (int j = bagWeight; j >= nums[i]; j--) {//遍历背包，也就是数组
                dp[j] += dp[j - nums[i]];
            }
        }
        return dp[bagWeight];
    }
}
