import java.util.Stack;

class MyQueue2 {

    Stack<Integer> stackIn = null;
    Stack<Integer> stackOut = null;
    public MyQueue() {
        stackIn = new Stack<>();
        stackOut = new Stack<>();
    }
    
    public void push(int x) {
        stackIn.add(x);
    }
    
    public int pop() {
        if (stackOut.isEmpty()){
            while (!stackIn.isEmpty()){
                int num = stackIn.pop();
                stackOut.add(num);
            }
        }
        int num = stackOut.pop();
        return num;
    }
    
    public int peek() {
        int num = pop();
        stackOut.add(num);
        return num;
    }
    
    public boolean empty() {
        return stackOut.isEmpty() && stackIn.empty();
    }
}