import java.util.LinkedList;
import java.util.Queue;

class MyStack {

    Queue<Integer> qu;
    public MyStack() {
        qu = new LinkedList<>();
    }
    
    public void push(int x) {
        qu.offer(x);
    }
    
    public int pop() {
        int size = qu.size() - 1;
        while (size-- != 0){
            qu.offer(qu.poll());
        }
        int result = qu.poll();
        return result;
    }
    
    public int top() {
        int result = pop();
        qu.offer(result);
        return result;
    }
    
    public boolean empty() {
        return qu.isEmpty();
    }
}