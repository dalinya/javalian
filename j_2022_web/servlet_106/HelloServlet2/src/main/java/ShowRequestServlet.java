import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

@WebServlet("/showRequest")
public class ShowRequestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doGet(req, resp);
        resp.setContentType("text/html");
        StringBuilder sb = new StringBuilder();
        sb.append(req.getProtocol());
        sb.append("<br>");
        sb.append(req.getMethod());
        sb.append("<br>");
        sb.append(req.getRequestURI());
        sb.append("<br>");
        sb.append(req.getContextPath());
        sb.append("<br>");
        sb.append(req.getQueryString());
        sb.append("<br>");
        sb.append("<br>");
        sb.append("<br>");
        sb.append("<br>");

        Enumeration<String> headerNames = req.getHeaderNames();
        while (headerNames.hasMoreElements()){
            String headerName = headerNames.nextElement();
            sb.append(headerName + ": " + req.getHeader(headerName));
            sb.append("<br>");
        }
        resp.getWriter().write(sb.toString());
    }
}
