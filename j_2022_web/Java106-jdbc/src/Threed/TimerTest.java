package Threed;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

class MyTask extends TimerTask{

    @Override
    public void run() {
        System.out.println("^_^该起床了");
    }
}

public class TimerTest {
    public static void main(String[] args) throws ParseException, InterruptedException {
        Timer timer = new Timer();
        timer.schedule(new MyTask(),1000,2000);
        String datetime = "2022-11-07 18:19:00";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = sdf.parse(datetime);
        timer.schedule(new MyTask(),date);
        Thread.sleep(50000);
        timer.cancel();

    }
}
