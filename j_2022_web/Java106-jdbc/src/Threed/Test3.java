package Threed;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Test3 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //创建MyThread3的实例
        MyThread3 m1 = new MyThread3();
        //使用FutureTask封装Callable接口
        FutureTask<Object> ft1 = new FutureTask<>(m1);
        //使用Thread(Runnable target, String name)创建线程对象
        Thread t1 = new Thread(ft1,"A");
        t1.start();
        //创建MyThread3的实例
        MyThread3 m2 = new MyThread3();
        //使用FutureTask封装Callable接口
        FutureTask<Object> ft2 = new FutureTask<>(m2);
        //使用Thread(Runnable target, String name)创建线程对象
        Thread t2 = new Thread(ft2,"B");
        t2.start();
        System.out.println(ft1.get());
        System.out.println(ft2.get());

    }
}
