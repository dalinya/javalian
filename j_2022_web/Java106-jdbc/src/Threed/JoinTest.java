package Threed;

class T extends Thread{
    Thread t;
    public T(String name,Thread t) {
        setName(name);
        this.t = t;
    }
    @Override
    public void run() {
        try {
            if(t != null){
                t.join();
            }
            System.out.println(getName() + "开始......");
            Thread.sleep(1000);
            System.out.println(getName() + "结束......");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
public class JoinTest {
    public static void main(String[] args) {
        T p3 = new T("C",null);
        T p2  = new T("B",p3);
        T p1  = new T("A",p2);
        p1.start();
        p2.start();
        p3.start();

    }


}

