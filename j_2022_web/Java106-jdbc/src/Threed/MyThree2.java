package Threed;

public class MyThree2 implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println(i + 1 + " === "+Thread.currentThread().getName());
        }
    }

    public static void main(String[] args) {
        MyThree2 d = new MyThree2();
        Thread t1 = new Thread(d,"D");
        t1.start();
    }
}
