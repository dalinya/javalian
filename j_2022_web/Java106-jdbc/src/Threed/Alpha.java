package Threed;

public class Alpha extends Thread{
    @Override
    public void run() {
        char ch = ' ';
        try {
            for (int i = 0; i < 100; i++) {
                ch = (char)('A'+(int)(Math.random()*26));
                //System.out.println("%20c\n", ch);
                System.out.printf("%20c\n",ch);
                sleep(1000);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        Alpha a = new Alpha();
        a.start();
    }
}
class Digit implements Runnable{

    @Override
    public void run() {
        char ch = ' ';
        try {
            for (int i = 0; i < 50; i++) {
                ch = (char)('0'+(int)(Math.random()*10));
                System.out.printf("%-20c\n",ch);
                Thread.sleep(2000);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        Alpha a = new Alpha();
        a.start();
        Digit d = new Digit();
        Thread t = new Thread(d);
        t.start();
    }
}




