package Threed;

import java.util.concurrent.*;

public class CallableThread implements Callable<Integer> {
    private int begin;
    private int end;

    public CallableThread(int begin, int end) {
        this.begin = begin;
        this.end = end;
    }

    @Override
    public Integer call() throws Exception {
        int sum = 0;
        for (int i = begin; i <= end; i++) {
            sum += i;
            System.out.println(Thread.currentThread().getName() + "---" + i);

        }
        return sum;
    }
}
class CallabledandFutureTest{
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        /*CallableThread c1 = new CallableThread(1,100);
        FutureTask<Integer> ft1 = new FutureTask<>(c1);
        Thread t1 = new Thread(ft1,"A");
        t1.start();
        CallableThread c2 = new CallableThread(101,200);
        FutureTask<Integer> ft2 = new FutureTask<>(c2);
        Thread t2 = new Thread(ft2,"B");
        t2.start();
        System.out.println("1~200的整数和：" + (ft1.get() + ft2.get()));*/
/*        int sum = 0;
        for (int i = 0; i < 10; i++) {
            CallableThread c1 = new CallableThread(1 + i * 100,100 + i * 100);
            FutureTask<Integer> ft1 = new FutureTask<>(c1);
            Thread t1 = new Thread(ft1);
            t1.start();
            sum += ft1.get();
        }
        System.out.println(sum);*/
        ExecutorService es = Executors.newFixedThreadPool(2);
        Future<Integer> ft1 = es.submit(new CallableThread(1,100));
        Future<Integer> ft2 = es.submit(new CallableThread(101,200));
        System.out.println("1~200的整数和：" + (ft1.get() + ft2.get()));
        /*ExecutorService es = Executors.newFixedThreadPool(10);

        int sum = 0;
        for (int i = 0; i < 10; i++) {
            Future<Integer> ft1 = es.submit(new CallableThread(1 + i * 100,100 + i * 100));
            sum += ft1.get();
        }
        System.out.println(sum);*/
    }

}