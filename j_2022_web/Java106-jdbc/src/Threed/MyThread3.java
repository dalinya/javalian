package Threed;

import java.util.concurrent.Callable;

public class MyThread3 implements Callable<Object> {
    int i;
    @Override
    public Object call() throws Exception {
        for (i = 1; i <= 5; i++) {
            System.out.println(i + " === "+Thread.currentThread().getName());
        }
        return i;
    }
}
