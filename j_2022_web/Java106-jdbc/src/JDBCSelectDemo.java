import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
public class JDBCSelectDemo {
    public static void main(String[] args) throws SQLException {
        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource)dataSource).setURL("jdbc:mysql://127.0.0.1:13306/java106?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("abc123");

        Connection connection = dataSource.getConnection();

        String sql = "select * from student where id = 1";
        PreparedStatement statement = connection.prepareStatement(sql);

        // 结果集合
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            // next 相当于移动一下光标, 光标指向下一行. 然后移动到结尾, 就返回 false
            // 使用 getXX 方法获取到每一列.
            // 获取 int, 就使用 getInt, 获取 String, 使用 getString.....
            // 这里的参数, 就是数据库表的列名.
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            System.out.println(id + ": " + name);
        }

        // 释放资源
        resultSet.close();
        statement.close();
        connection.close();
    }
}