import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class JDBCInsertDemo {
    public static void main(String[] args) throws SQLException {
        //使用jdbc 往数据库插入数据
        //需要提前准备好数据库(java106) 和 数据表

        //1.创建数据源，描述了数据库服务器在哪里
        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:13306/java106?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("abc123");
        //2.和数据库建立网络连接，(写jdbc代码本质上是实现了一个MySQL客户端，需要通过网络和服务器进行通信)
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入学号");
        int id = scanner.nextInt();
        System.out.println("请输入姓名");
        String name = scanner.next();

        Connection  connection =  dataSource.getConnection();
        //3.构造一个sql语句，来完成插入操作
        //String sql = "insert into student values(" +  id + ",'" + name + "')";
        String sql = "insert into student values(?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1,id);
        statement.setString(2,name);//会对输入的数据进行校验
        System.out.println("sql" + statement);
        //4.执行sql语句，(控制客户端给服务器发送请求)
        int ret = statement.executeUpdate();
        System.out.println("ret = " + ret);
        //5.断开和数据库的连接，并且释放必要的资源
        statement.close();
        connection.close();
    }

}
