package JDBC;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;
import java.sql.Connection;
public class zhuce {
	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		String url="jdbc:mysql://localhost:13306/qq?characterEncoding=utf8&useSSL=false";
		Connection con=DriverManager.getConnection(url, "root", "abc123");
		System.out.println(con);
		String sql = "INSERT INTO USER VALUES(?,?,?,?)";
		PreparedStatement pdmt = con.prepareStatement(sql);
		Scanner sc = new Scanner(System.in);
		pdmt.setString(1, sc.next());
		pdmt.setString(2, sc.next());
		pdmt.setString(3, sc.next());
		pdmt.setString(4, sc.next());

		int ret = pdmt.executeUpdate();
		if (ret >0){
			System.out.println("注册成功");
		}else {
			System.out.println("注册失败");
		}
		pdmt.close();
		con.close();
	}
}
