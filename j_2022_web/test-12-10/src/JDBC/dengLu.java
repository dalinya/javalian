package JDBC;

import java.sql.*;
import java.util.Scanner;

public class dengLu {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String url="jdbc:mysql://localhost:13306/qq?characterEncoding=utf8&useSSL=false";
        Connection con= DriverManager.getConnection(url, "root", "abc123");
        System.out.println(con);
        String sql = "select count(*) from user where user_name = ? and user_pwd = ?";

        PreparedStatement psmt = con.prepareStatement(sql);
        Scanner sc = new Scanner(System.in);

        psmt.setString(1,sc.next());
        psmt.setString(2,sc.next());
        ResultSet rs = psmt.executeQuery();
        int i = 0;
        if (rs.next()){
            i = rs.getInt(1);
        }
        if (i == 1){
            System.out.println("登录成功");
        }else {
            System.out.println("登录失败");
        }
    }
}
