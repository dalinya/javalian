package UDP_TCP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
public class UDPChatReceive {
 public static void main(String[] args) throws IOException{
	 try {
		 DatagramSocket ds = new DatagramSocket(9000);
		  byte[] buf = new byte[1024];
		  DatagramPacket rece_Packet = new DatagramPacket(buf,buf.length);
		  while(true) {
		   ds.receive(rece_Packet);
		   /*InetAddress sendAddr=rece_Packet.getAddress();
		   int sendPort=rece_Packet.getPort();*/
		   String msg = new String(buf,0,rece_Packet.getLength());//byte[]->String
		   System.out.println("从"+rece_Packet.getAddress().getHostAddress()+":"+rece_Packet.getPort()+"接收的数据:"+msg);
		   if("exit".equalsIgnoreCase(msg)) {
		    break;
		   }
		  }
	} catch (SocketException e) {
		e.printStackTrace();
	}
 }
}
