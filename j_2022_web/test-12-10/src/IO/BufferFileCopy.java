package IO;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
public class BufferFileCopy {
	public static void main(String[] args) throws IOException {
		FileReader fr=new FileReader("d:\\sg.txt");
		BufferedReader br=new BufferedReader(fr);
		FileWriter fw=new FileWriter("d:\\sbbak.txt");
		BufferedWriter bw=new BufferedWriter(fw);
		String s;
		while((s=br.readLine())!=null) {
			System.out.println(s);
			bw.write(s);
			bw.newLine();
		}
		bw.flush();
		fr.close();
		fw.close();
	}
}
