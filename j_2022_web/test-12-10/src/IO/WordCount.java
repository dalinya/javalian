package IO;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
//11-07
public class WordCount {
    public static void main(String[] args) throws FileNotFoundException {
        TreeMap<String,Integer> map = new TreeMap<>();
        Scanner sc = new Scanner(new File("src\\Tetris.java"));
        //sc.useDelimiter("\\s|\\.|\\(|\\)|[\\u4e00-\\u9fa5]");
        //sc.useDelimiter("\\W|\\d|_|^\\'");
        sc.useDelimiter("\\s|\\.|\\(|\\)|[\\u4e00-\\u9fa5]|"
                + "\\+|\\-|\\*|\\/|\\%|\\&|\\{|\\}|\\=|[0-9]|"
                + ";|\\<|\\>|\"|!|_|#####|,|\\||\\[|\\]");
        String s = null;
        int count = 0;
        while (sc.hasNext()){
            s = sc.next();
            if(!"".equals(s.trim())){
                count++;
                if(map.containsKey(s)){
                    map.put(s, map.get(s) + 1);
                }else {
                    map.put(s,1);
                }
            }
        }
        sc.close();
        System.out.println(count);
        //map.forEach((k,v)-> System.out.println(k + " => " + v));
        Set<String> set = map.keySet();
        Iterator<String> it = set.iterator();
        while (it.hasNext()){
            String k = it.next();
            Integer value = map.get(k);
            System.out.println(k + " => " + value);
        }
    }
}
