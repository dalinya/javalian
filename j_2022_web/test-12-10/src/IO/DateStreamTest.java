package IO;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
public class DateStreamTest {
 public static void main(String[] args) throws IOException {
  //public DataOutputStream(OutputStream out)
  FileOutputStream fos = new FileOutputStream("d:\\t.txt");
  DataOutputStream dos = new DataOutputStream(fos);
  dos.writeByte((byte)123);
  dos.writeShort((short)11223);
  dos.writeInt(1234567890);
  dos.writeLong(998877665544332211L);
  dos.writeFloat(2.7182f);
  dos.writeDouble(3.1415926);
  dos.writeChar('J');
  dos.writeBoolean(true);
  dos.writeUTF("邯郸");
  //DataInputStream(InputStream in)
  FileInputStream fis = new FileInputStream("d:\\t.txt");
  DataInputStream dis = new DataInputStream(fis);
  System.out.println("Read from file Tree.java:");
  System.out.println(dis.readByte());
  System.out.println(dis.readShort());
  System.out.println(dis.readInt());
  System.out.println(dis.readLong());
  System.out.println(dis.readFloat());
  System.out.println(dis.readDouble());
  System.out.println(dis.readChar());
  System.out.println(dis.readBoolean());
  System.out.println(dis.readUTF());
 }
}
