package Thread;

import java.util.concurrent.CountDownLatch;
public class CoountDownLatchTest {
	public static void main(String[] args) {
		final int N=3;
		CountDownLatch countDownLatch=new CountDownLatch(N);
		Athlete p1=new Athlete(countDownLatch, "A");
		Athlete p2=new Athlete(countDownLatch, "B");
		Athlete p3=new Athlete(countDownLatch, "C");
		p1.start();
		p2.start();
		p3.start();
		for (int i =1; i <=N; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.print(N-i+1+" ");
			countDownLatch.countDown();
		}
		System.out.println("开始跑！");
	}
	
}
class Athlete extends Thread{
	private CountDownLatch countDownLatch;
	public Athlete(CountDownLatch countDownLatch,String name) {
		setName(name);
		this.countDownLatch=countDownLatch;
	}
	public void run() {
		System.out.println(getName()+"等待发令枪响...");
		try {
			countDownLatch.await();
			for (int i = 1; i <=3; i++) {
				System.out.println(getName()+"拼命奔跑中...");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(getName()+"is done.");
	}
}
