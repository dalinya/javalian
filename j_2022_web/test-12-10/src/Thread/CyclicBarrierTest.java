package Thread;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
public class CyclicBarrierTest{
	public static void main(String[] args) {
		CyclicBarrier cb=new CyclicBarrier(3, new Runnable() {
			@Override
			public void run() {
				System.out.println("导游：出发去下一个景点!");
			}
		});
        new Player(cb, "张三").start();
        new Player(cb, "李四").start();
        new Player(cb, "王五").start();

	}
}
class Player extends Thread{
	private CyclicBarrier cb;
	public Player(CyclicBarrier cb,String name){
		setName(name);
		this.cb=cb;
	}
	public void run() {		
		try {
			System.out.println(getName()+"开始游览！");
			sleep((long)Math.random()*10000);//模拟旅游
			System.out.println(getName()+"浏览结束，返回集合地点！");
			cb.await();					
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			e.printStackTrace();
		}
	}
}
