package thread;
//wait和notify
public class ThreadDemo15 {
    // 使用这个锁对象来负责加锁, wait, notify
    private static Object locker = new Object();
    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            while (true){
                synchronized (locker){
                    System.out.println("t1 wait开始");
                    try {
                        locker.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("t1 wait结束");
                }
            }
        });
        t1.start();
        Thread t2 = new Thread(()->{
            while (true){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (locker){
                    System.out.println("t2 notify开始");
                    locker.notify();
                    System.out.println("t2 notify结束");
                }
            }
        });
        t2.start();
    }
}
