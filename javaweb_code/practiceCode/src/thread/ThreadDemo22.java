package thread;

import java.util.PriorityQueue;
import java.util.TimerTask;

// 创建一个类, 用来描述定时器中的一个任务
class MyTimerTask implements Comparable<MyTimerTask>{
    //任务啥时候执行，毫秒级时间戳
    private long time;
    //任务具体是啥
    private Runnable runnable;

    public MyTimerTask(Runnable runnable,long delay) {
        // delay 是一个相对的时间差. 形如 3000 这样的数值.
        // 构造 time 要根据当前系统时间和 delay 进行构造.
        this.time = System.currentTimeMillis() + delay;
        this.runnable = runnable;
    }

    public long getTime() {
        return time;
    }

    public Runnable getRunnable() {
        return runnable;
    }

    @Override
    public int compareTo(MyTimerTask o) {
        return (int)(this.time - o.time);
    }
}
//定时器本体
class MyTimer{
    // 使用优先级队列, 来保存上述的 N 个任务
    private PriorityQueue<MyTimerTask> queue = new PriorityQueue<>();
    //用来加锁的对象
    Object locker = new Object();
    // 定时器的核心方法, 就是把要执行的任务添加到队列中.
    public void schedule(Runnable runnable,long delay){
        synchronized (locker){
            MyTimerTask task = new MyTimerTask(runnable,delay);
            queue.offer(task);
            // 每次来新的任务, 都唤醒一下之前的扫描线程. 好让扫描线程根据最新的任务情况, 重新规划等待时间.
            locker.notify();
        }
    }
    // MyTimer 中还需要构造一个 "扫描线程", 一方面去负责监控队首元素是否到点了, 是否应该执行;
    //  一方面当任务到点之后,就要调用这里的 Runnable 的 Run 方法来完成任务
    public MyTimer(){
        //扫描线程
        Thread t = new Thread(()->{
            while (true){
                synchronized (locker){
                    try {
                        while (queue.isEmpty()){
                            // 注意, 当前如果队列为空, 此时就不应该去取这里的元素.
                            // 此处使用 wait 等待更合适. 如果使用 continue, 就会使这个线程 while 循环运行的飞快,
                            // 也会陷入一个高频占用 cpu 的状态(忙等).
                            locker.wait();
                        }
                        MyTimerTask task = queue.peek();
                        long curTime = System.currentTimeMillis();
                        if (curTime >= task.getTime()){
                            //需要执行任务
                            queue.poll();//出队列
                            task.getRunnable().run();//执行内容
                        }else {
                            // 让当前扫描线程休眠一下, 按照时间差来进行休眠.
                            //Thread.sleep(task.getTime() - curTime);
                            locker.wait(task.getTime() - curTime);
                        }
                    }catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        t.start();
    }
}

public class ThreadDemo22 {
    public static void main(String[] args) {
        MyTimer timer = new MyTimer();
        timer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("定时器任务执行3");
            }
        },3000);
        timer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("定时器任务执行2");
            }
        },2000);
        timer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("定时器任务执行1");
            }
        },1000);
        System.out.println("程序开始运行");
    }
}
