package thread;

//单例模式(饿汉模式)
class Singleton{
    //类对象单例 => 类对象中的属性也是单例的
    private static  Singleton instance = new Singleton();
    public static Singleton getInstance(){
        return instance;
    }
    // 做出一个限制, 禁止别人去 new 这个实例!!
    private Singleton(){

    }
}
public class ThreadDemo16 {
    public static void main(String[] args) {
        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();
        System.out.println(s1 == s2);
    }
}
