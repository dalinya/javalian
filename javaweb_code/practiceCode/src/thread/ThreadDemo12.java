package thread;
class Counter{
    public int count = 0;
    private Object locker = new Object();
    public synchronized void increase(){
         synchronized(locker){
             count++;
         }
    }
    public void increase2(){
            count++;
    }
    //synchronized 修饰静态方法相当于是对类对象加锁。
}

public class ThreadDemo12 {
    public static void main(String[] args) throws InterruptedException {
        Counter counter = new Counter();

        Thread t1 = new Thread(()->{
            for (int i = 0; i < 50000; i++) {
                counter.increase();
            }
        });
        Thread t2 = new Thread(()->{
            for (int i = 0; i < 50000; i++) {
                counter.increase2();
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(counter.count);
    }
}
