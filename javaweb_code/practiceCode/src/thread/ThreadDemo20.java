package thread;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

//实现阻塞队列
class MyBlockingQueue{
    // 使用一个 String 类型的数组来保存元素. 假设这里只存 String.
    private String[] items = new String[1001];
    // 指向队列的头部
    volatile private int head = 0;//考虑内存可见性安全
    // 当 head 和 tail 相等(重合), 相当于空的队列.
    volatile private int tail = 0;

    //入队列
    public void put(String item) throws InterruptedException {
        // 此处的写法就相当于直接把 synchronized 写到方法上了.
        synchronized (this){
            while ((tail + 1) % items.length == head){//为了wait被唤醒之后，再次确认条件。
                //队列满了
                this.wait();
            }
            items[tail] = item;
            tail = (tail + 1) % items.length;
            //用来唤醒阻塞队列为空的情况
            this.notify();
        }
    }

    //出队列
    public String take() throws InterruptedException {
        synchronized (this){
            while (tail == head){
                //队列为空，暂时不能出队列
                this.wait();
            }
            String elem = items[head];
            head = (head + 1) % items.length;
            //使用这个notify唤醒队列满的情况
            this.notify();
            return elem;
        }
    }
}
public class ThreadDemo20 {
    public static void main(String[] args) throws InterruptedException {
        /*MyBlockingQueue queue  = new MyBlockingQueue();
        queue.put("aaa");
        queue.put("bbb");
        queue.put("ccc");
        String elem = queue.take();
        System.out.println("elem = " + elem);
        elem = queue.take();
        System.out.println("elem = " + elem);
        elem = queue.take();
        System.out.println("elem = " + elem);
        elem = queue.take();
        System.out.println("elem = " + elem);*/

        // 搞一个阻塞队列, 作为交易场所
        MyBlockingQueue queue  = new MyBlockingQueue();
        // 负责生产元素
        Thread t1 = new Thread(()->{
            int count = 0;
            while (true){
                try {
                    queue.put(count + "");
                    System.out.println("生产元素: " + count);
                    count++;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        // 负责消费元素
        Thread t2 = new Thread(()->{
            while (true){
                try {
                    String n = queue.take();
                    System.out.println("消费元素：" + n);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t1.start();
        t2.start();
    }
}
