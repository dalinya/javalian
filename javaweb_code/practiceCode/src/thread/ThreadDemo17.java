package thread;
//单例模式(懒汉模式)
class SingletonLazy{
    private static volatile SingletonLazy instance = null;
    public static SingletonLazy getInstance(){
        // instance 如果为 null, 说明是首次调用, 就需要加锁. 如果非 null, 就说明是后续调用, 就不用加锁了.
        if(instance == null){
            synchronized (Singleton.class){
                if(instance == null){
                    instance = new SingletonLazy();
                }
            }
        }
        return instance;
    }
    private SingletonLazy(){
    }
}
public class ThreadDemo17 {
    public static void main(String[] args) {
        SingletonLazy s1 = SingletonLazy.getInstance();
        SingletonLazy s2 = SingletonLazy.getInstance();
        System.out.println(s1 == s2);
    }
}
