package thread;

public class ThreadDemo11 {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(()->{
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        System.out.println("new状态：" + t.getState());
        t.start();
        Thread.sleep(1000);
        System.out.println("TIMED_WAITING状态：" + t.getState());
    }

    public static void main2(String[] args) throws InterruptedException {
        Thread t = new Thread(()->{
            while (true){

            }
        });
        System.out.println("new状态：" + t.getState());
        t.start();
        Thread.sleep(1000);
        System.out.println("RUNNABLE状态：" + t.getState());
    }
    public static void main1(String[] args) throws InterruptedException {
        Thread t = new Thread(()->{
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        System.out.println("new状态：" + t.getState());
        t.start();
        t.join();
        System.out.println("结束状态：" + t.getState());
    }

}
