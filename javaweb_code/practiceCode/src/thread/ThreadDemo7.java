package thread;

// 后台线程 和 前台线程
public class ThreadDemo7 {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(()->{
            while (true){
                System.out.println("hello thread");
            }
        });
        // 设置成后台线程了.
        t.setDaemon(true);
        t.start();
        //后台线程无法影响进程结束
        Thread.sleep(3000);
    }
}
