package thread;
// 先写一个简单的 hello world
// Thread 来自于 java.lang 这个包,
class MyThread extends Thread{
    @Override
    public void run() {
        while (true){
            System.out.println("hello word");
            try {
                // 这里只能 try catch, 不能 throws.
                // 此处是方法重写. 对于父类的 run 方法来说, 就没有 throws xxx 异常这样的设定.
                // 在重写的时候, 也就不能 throws 异常了.
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
public class ThreadDemo1 {
    public static void main(String[] args) throws InterruptedException {
        MyThread myThread = new MyThread();
        myThread.start();
//        myThread.run();
        while (true){
            System.out.println("hello main");
            Thread.sleep(1000);
        }
    }
}
