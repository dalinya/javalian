package thread;
//wait
public class ThreadDemo14 {
    public static void main(String[] args) throws InterruptedException {
        Object object = new Object();
        // 当 wait 引起线程阻塞之后, 可以使用 interrupt 方法, 把线程给唤醒打断当前线程的阻塞状态的
        synchronized (object){
            object.wait();//线程进入阻塞状态，这里的 wait 就会一直阻塞到其他线程进行 notify 了
        }
        System.out.println("wait结束");
    }
}
