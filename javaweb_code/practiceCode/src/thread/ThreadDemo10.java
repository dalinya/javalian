package thread;

public class ThreadDemo10 {
    public static void main(String[] args) {
        Thread b = new Thread(()->{
            for (int i = 0; i < 5; i++) {
                System.out.println("hello b");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("b结束了");
        });
        Thread a = new Thread(()->{
            for (int i = 0; i < 3; i++) {
                System.out.println("hello b");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            try {
                // 如果 b 此时还没执行完毕, b.join 就会产生阻塞的情况
                b.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("a结束了");
        });
        b.start();
        a.start();
    }
}
