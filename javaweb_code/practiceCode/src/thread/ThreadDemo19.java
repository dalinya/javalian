package thread;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

//生产者消费者模型
public class ThreadDemo19 {
    public static void main(String[] args) {
        // 搞一个阻塞队列, 作为交易场所
        BlockingQueue<Integer> queue = new LinkedBlockingDeque<>();
        // 负责生产元素
        Thread t1 = new Thread(()->{
            int count = 0;
            while (true){
                try {
                    queue.put(count);
                    System.out.println("生产元素: " + count);
                    count++;
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        // 负责消费元素
        Thread t2 = new Thread(()->{
            while (true){
                try {
                    Integer n = queue.take();
                    System.out.println("消费元素：" + n);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t1.start();
        t2.start();
    }
}
