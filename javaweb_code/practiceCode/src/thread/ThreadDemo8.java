package thread;

public class ThreadDemo8 {
    // 写作成员变量就不是触发变量捕获的逻辑了. 而是 "内部类访问外部类的成员" , 本身就是 ok 的~~
    public static boolean isQuit = false;
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(()->{
           while (!isQuit){
               System.out.println("hello thread");
               try {
                   Thread.sleep(1000);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
           }
        });
        t.start();
        // 主线程这里执行一些其他逻辑之后, 要让 t 线程结束.
        Thread.sleep(3000);
        // 这个代码就是在修改前面设定的标志位.
        isQuit = true;
        System.out.println("终止t线程");
    }
}
