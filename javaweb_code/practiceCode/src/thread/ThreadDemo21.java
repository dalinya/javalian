package thread;

import java.util.Timer;
import java.util.TimerTask;

//定时器的使用
public class ThreadDemo21 {
    public static void main(String[] args) {
        Timer timer = new Timer();
        // 给 timer 中注册的这个任务, 不是在 调用 schedule 的线程中执行的. 而是通过 Timer 内部的线程, 来负责执行的.
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("定时器任务执行3");
            }
        },3000);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("定时器任务执行2");
            }
        },2000);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("定时器任务执行1");
            }
        },1000);
        System.out.println("程序开始运行");
    }
}
