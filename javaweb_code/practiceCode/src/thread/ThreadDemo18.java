package thread;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

//使用阻塞队列
public class ThreadDemo18 {
    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<String> queue = new LinkedBlockingDeque<>();
        queue.put("hello");
        String elem = queue.take();
        System.out.println(elem);
        elem = queue.take();//此时会阻塞
        System.out.println(elem);
    }
}
