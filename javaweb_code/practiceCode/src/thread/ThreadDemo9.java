package thread;
// 线程终止. 使用 Thread 自带的标志位.
public class ThreadDemo9 {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(()->{
            // Thread.currentThread 就是 t .
            // 但是 lambda 表达式是在构造 t 之前就定义好的. 编译器看到的 lambda 里的 t 就会认为这是一个还没初始化的对象.
            while (!Thread.currentThread().isInterrupted()){
                System.out.println("hello Thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    //e.printStackTrace();
                    break;
                }
            }
        });
        t.start();
        Thread.sleep(3000);
        // 把上述的标志位给设置成 true
        t.interrupt();
    }
}
