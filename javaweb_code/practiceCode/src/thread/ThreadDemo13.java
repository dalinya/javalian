package thread;

import java.util.Scanner;

//内存可见性问题
public class ThreadDemo13 {
    private static int isQuit = 0;
    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            while (isQuit == 0){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("t1执行结束");
        });
        Thread t2 = new Thread(()->{
            Scanner sc = new Scanner(System.in);
            System.out.println("请输入 isQuit 的值：");
            isQuit = sc.nextInt();
        });
        t1.start();
        t2.start();
    }
}
