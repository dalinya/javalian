package file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Demo7 {
    public static void main(String[] args) throws IOException {
        try (InputStream inputStream = new FileInputStream("./test.txt")){
            while (true){
                byte[] buf = new byte[1024];
                int n = inputStream.read(buf);
                if (n == -1){
                    break;
                }
                for (int i = 0; i < n; i++) {
                    System.out.printf("%x ",buf[i]);
                }
                String s = new String(buf,0,n,"utf8");
                System.out.println(s);
            }
        }
    }
}
