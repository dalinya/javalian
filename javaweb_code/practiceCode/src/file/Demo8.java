package file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class Demo8 {
    public static void main(String[] args) throws FileNotFoundException {
        try(InputStream inputStream = new FileInputStream("./test.txt")){
            Scanner sc = new Scanner(inputStream);
            String s = sc.next();
            System.out.println(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
