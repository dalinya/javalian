package file;

import java.io.*;
import java.util.Scanner;

public class Demo12 {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("输入源文件目录");
        String src =  sc.next();
        File srcFile = new File(src);
        if (!srcFile.isFile()){
            System.out.println("输入的的元文件路径错误");
            return;
        }
        System.out.println("输入目标文件目录");
        String dest = sc.next();
        File destFile = new File(dest);
        if (destFile.getParentFile().isFile()){
            System.out.println("输入的目标路径是非法的");
            return;
        }
        try(OutputStream outputStream = new FileOutputStream(destFile);
            InputStream inputStream = new FileInputStream(srcFile)){
            while (true){
                byte[] buf = new byte[20480];
                int n = inputStream.read(buf);
                System.out.println("n=:" + n);
                if (n == -1){
                    System.out.println("读取到 eof，循环结束");
                    break;
                }
                outputStream.write(buf,0,n);
            }
        }

    }
}
