package file;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.WatchService;

public class Demo9 {
    public static void main(String[] args) {
        try (Writer writer = new FileWriter("./test.txt",true)){
            writer.write("hello java");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
