package file;

import java.io.File;
import java.util.Scanner;
//文件的删除
public class Demo10 {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("请输入你要搜素的根目录");
        File rootPath = new File(sc.next());
        System.out.println("请你输入你要删除的关键字 ");
        String word = sc.next();
        if(!rootPath.isDirectory()){
            System.out.println("您输入的路径不合法");
            return;
        }
        scanDir(rootPath,word);
    }

    private static void scanDir(File currentDir, String word) {
        File[] files = currentDir.listFiles();
        if(files == null || files.length == 0){
            //目录不合法或者是空目录
            return;
        }
        for (File file: files) {
            System.out.println(file.getAbsoluteFile());
            if (file.isFile()){
                dealFile(file,word);
            }else{
                scanDir(file,word);
            }
        }
    }

    private static void dealFile(File file, String word) {
        if (!file.isFile()){
            return;
        }
        if (!file.getName().contains(word)){
            return;
        }
        System.out.println("路径是" + file.getAbsoluteFile() + " 是否要删除[y|n]:");
        char ch = sc.next().charAt(0);
        if (ch == 'y' || ch == 'Y'){
            file.delete();
        }
    }
}
