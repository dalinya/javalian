package file;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class Demo6 {
    public static void main(String[] args) throws IOException {
        try (Reader reader = new FileReader("test.txt")){
            while (true){
                char buf[]  = new char[1024];
                int n = reader.read(buf);
                if (n == -1){
                    //读到文件末尾了
                    break;
                }
                for (int i = 0; i < n; i++) {
                    System.out.print(buf[i] + ",");
                }
            }
        }
    }
}
