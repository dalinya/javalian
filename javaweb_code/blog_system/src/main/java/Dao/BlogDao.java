package Dao;

import entity.Blog;
import entity.Html2PlainText;
import entity.Markdown2Html;
import entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BlogDao {
    //新增一个博客
    public  void add(Blog blog){
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "insert into blog values(null,?,?,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1,blog.getTitle());
            statement.setString(2,blog.getContent());
            statement.setTimestamp(3,blog.getPostTimestamp());
            statement.setInt(4,blog.getUserId());

            //3.执行sql
            int n = statement.executeUpdate();
            System.out.println(n);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
    //2. 根据博客id来查询制定博客
    public Blog selectById(int blogId){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "select * from blog where blogId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            //3.执行sql
            resultSet = statement.executeQuery();
            if (resultSet.next()){
                Blog blog = new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                blog.setContent(resultSet.getString("content"));
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setUserId(resultSet.getInt("userId"));
                return blog;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
        return null;
    }
    // 3. 直接查询出数据库中所有的博客列表 (用于博客列表页)
    public List<Blog> selectAll() {
        List<Blog> blogs = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "select * from blog order by postTime desc";
            statement = connection.prepareStatement(sql);
            //3.执行sql
            resultSet = statement.executeQuery();
            //4.遍历集合结果
            while (resultSet.next()){
                Blog blog = new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                //这里的正文对应的是博客列表，不需要把正文全部显示出来
                String content = Html2PlainText.convert(Markdown2Html.convert(resultSet.getString("content")));
                if (content == null){
                    content = "";
                }
                if (content.length() >= 100){
                    content = content.substring(0,100) + "...";
                }
                blog.setContent(content);
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setUserId(resultSet.getInt("userId"));
                blogs.add(blog);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
        return blogs;
    }
    //4.删除指定博客
    public void delete(int blogId){
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "delete from blog where blogId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            //3.执行sql
            int n = statement.executeUpdate();
            System.out.println("delete : " + n);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
    public static void main(String[] args) {
        BlogDao blogDao = new BlogDao();
//        List<Blog> blogs = blogDao.selectAll();
        blogDao.delete(4);
        /*
        Blog blog = new Blog();
        blog.setContent("这是一篇博客");
        blog.setTitle("一篇博客");
        blog.setUserId(1);
        blog.setPostTime(new Timestamp(System.currentTimeMillis()));
        blogDao.add(blog);*/
    }
}
