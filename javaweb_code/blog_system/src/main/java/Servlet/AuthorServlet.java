package Servlet;

import Dao.BlogDao;
import Dao.UserDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.Blog;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/author")
public class AuthorServlet extends HttpServlet {
    ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String blogId = req.getParameter("blogId");
        if (blogId == null){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("非法参数，缺少blogId");
            return;
        }
        //1. 根据blogId查询blog对象
        BlogDao blogDao = new BlogDao();
        Blog blog = blogDao.selectById(Integer.parseInt(blogId));
        if (blog == null) {
            // 博客不存在.
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("没有找到指定博客: blogId = " + blogId);
            return;
        }
        //2.根据blog中的userId找到对应的用户信息
        UserDao userDao = new UserDao();
        User author = userDao.selectById(blog.getUserId());
        String respJson = objectMapper.writeValueAsString(author);
//        System.out.println(respJson);
        resp.setContentType("application/json;charset=utf8");
        resp.getWriter().write(respJson);
    }
}
