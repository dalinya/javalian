package Servlet;

import Dao.BlogDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.Blog;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

@WebServlet("/blog")
public class BlogServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BlogDao blogDao = new BlogDao();
        String blogId = req.getParameter("blogId");
        if (blogId == null){
            //queryString不存在，说明这次请求是获取博客列表，
            List<Blog> blogs = blogDao.selectAll();
            //把blogs转化成符合需求的json字符串
            String respJson = objectMapper.writeValueAsString(blogs);
            System.out.println("获取博客列表");
            resp.setContentType("application/json;charset=utf8");
            resp.getWriter().write(respJson);
        }else {
            //queryString存在，说明本次获取请求时指定id的博客
            Blog blog = blogDao.selectById(Integer.parseInt(blogId));
            if (blog == null){
                System.out.println("当前blogId = " + blogId + " 对应的博客不存在！");
            }
            String respJson = objectMapper.writeValueAsString(blog);
            resp.setContentType("application/json;charset=utf8");
            resp.getWriter().write(respJson);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //发布博客
        //读取请求，构造Blog 对象,插入数据库即可
        HttpSession httpSession = req.getSession(false);
        if (httpSession == null){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前未未登录，无法发布博客");
            return;
        }
        User user = (User) httpSession.getAttribute("user");
        if (user == null){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前未未登录，无法发布博客");
            return;
        }
        //确定登录后，就可以处理博客了
        //获取博客正文和标题
        req.setCharacterEncoding("utf8");
        String title = req.getParameter("title");
        String content = req.getParameter("content");
        if (title == null || "".equals(title)||
            content == null || "".equals(content)){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前数据提交有误！标题或者正文为空" + title + "<br>" + content);
            return;
        }
        //构造Blog对象
        Blog blog = new Blog();
        blog.setTitle(title);
        blog.setContent(content);
        blog.setUserId(user.getUserId());
        //发布时间，在java中/数据库中生成都可以
        blog.setPostTime(new Timestamp(System.currentTimeMillis()));
        //插入数据库
        BlogDao blogDao = new BlogDao();
        blogDao.add(blog);
        //跳转到博客列表页
        resp.sendRedirect("blog_list.html");
    }
}
