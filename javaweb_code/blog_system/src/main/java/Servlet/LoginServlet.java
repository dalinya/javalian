package Servlet;

import Dao.UserDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置请求的编码. 告诉 servlet 按照啥格式来理解请求
        req.setCharacterEncoding("utf8");
        // 设置响应的编码. 告诉 servlet 按照啥格式来构造响应
        resp.setContentType("text/html;charset=utf8");
        // 1. 读取参数中的用户名和密码
        //    注意!! 如果用户名密码包含中文, 此处的读取可能会乱码.
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if (username == null || "".equals(username) || password == null || "".equals(password)){
            //登录失败
            String html = "<h3>登录失败!缺少username或者密码字段</h3>";
            resp.getWriter().write(html);
            return;
        }
        //2.读取数据库，查看用户名是否存在，并且验证密码是否匹配
        UserDao userDao = new UserDao();
        User user = userDao.selectByUsername(username);
        if (user == null || !password.equals(user.getPassword())){
            //密码或者用户名不正确
            String html = "<h3>登录失败！用户名或密码错误</h3>";
            resp.getWriter().write(html);
            return;
        }
        //3.用户名密码验证通过，登陆成功，接下来创建会话，使用该会话保存用户信息
        HttpSession session = req.getSession(true);
        session.setAttribute("user",user);
        //4.进行重定向，转跳到博客列表页
        resp.sendRedirect("blog_list.html");
    }
    private ObjectMapper objectMapper = new ObjectMapper();
    //使用这个方法来获取用户登录的状态
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置响应的编码. 告诉 servlet 按照啥格式来构造响应
        resp.setContentType("text/json;charset=utf8");
        //用户未登录，这里的会话是拿不到的。
        HttpSession session = req.getSession(false);
        if (session == null){
            //未登录，返回一个空的user对象
            User user = new User();
            String respJson = objectMapper.writeValueAsString(user);
            resp.getWriter().write(respJson);
            return;
        }
        User user = (User) session.getAttribute("user");
        if (user == null){
            //用户不存在
            user = new User();
            String respJson = objectMapper.writeValueAsString(user);
            resp.getWriter().write(respJson);
            return;
        }
        //返回取出的user对象
        String respJson = objectMapper.writeValueAsString(user);
        resp.getWriter().write(respJson);
    }
}
