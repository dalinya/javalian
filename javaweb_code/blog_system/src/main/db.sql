drop database servlet_blog_system if  exists;
create database  servlet_blog_system charset utf8;
use servlet_blog_system;
-- 删除旧表，创建新表。防止之前残留的数据对后续程序产生影响
drop table if exists user;
drop table if exists blog;
-- 创建表
create table blog(
    blogId int primary key auto_increment,
    title varchar(128),
    content text not null,
    postTime datetime,
    userId int
);
create table user(
    userId int primary key auto_increment,
    username varchar(20) unique, -- 要求你的用户名和别人不重复
    password varchar(64)
);

-- 构造测试数据
insert into blog values(1, '这是我的第一篇博客', '从今天开始我要认真敲代码', now(), 1);
insert into blog values(2, '这是我的第二篇博客', '从昨天开始我要认真敲代码', now(), 1);
insert into blog values(3, '这是我的第三篇博客', '从前天开始我要认真敲代码', now(), 1);

-- 构造测试数据
insert into user values(1, 'zhangsan', '123');
insert into user values(2, 'lisi', '123');