import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class JDBCDemo3 {
    public static void main(String[] args) throws SQLException {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入要查询的学号：");
        int studentId = sc.nextInt();
        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/my_test2023?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("abc123");
        Connection connection = dataSource.getConnection();
        String sql = "select * from student where id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1,studentId);
        // 执行查询操作, 要使用 executeQuery. 返回值是一个 ResultSet 类型的对象. 表示了一个 "表格"
        ResultSet resultSet = statement.executeQuery();
        //遍历结果集合
        while (resultSet.next()){
            //获取这一行的学号 列
            int id = resultSet.getInt("id");
            //获取这一行的姓名 列
            String name = resultSet.getString("name");
            System.out.println("id: " + id + " name: " + name);
        }
        //释放资源 --> 先建立的后释放
        resultSet.close();
        statement.close();
        connection.close();
    }
}
