import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

//插入操作
public class JDBCDemo1 {
    public static void main(String[] args) throws SQLException {
        // 假定数据库中有一个 student 表 (id, name), 往里面插入一个数据.
        // 让用户通过控制台来输入学号和姓名.
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入学号:");
        int id = sc.nextInt();
        System.out.println("请输入姓名:");
        String name = sc.next();
        //1.创建"数据源"
        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/my_test2023?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("abc123");
        //2.和数据库建立连接
        Connection connection = dataSource.getConnection();
        //3.构造SQL语句
        String sql = "insert into student values(?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1,id);
        statement.setString(2,name);
        //4.执行SQL语句，返回值就是“这次操作影响的几行”
        int n = statement.executeUpdate();
        System.out.println("n : " + n);
        //5.释放必要资源，关闭连接
        statement.close();
        connection.close();
    }
}
