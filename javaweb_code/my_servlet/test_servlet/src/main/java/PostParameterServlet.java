import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/postParameter")
public class PostParameterServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf8");//设置收数据的数据类型
        String studentId = req.getParameter("studentId") ;
        String claseeId = req.getParameter("classId");
        resp.setContentType("text/html;charset=utf8");
        System.out.println("学生id = " + studentId + " 班级id = " + claseeId);
        resp.getWriter().write("学生id = " + studentId + " 班级id = " + claseeId);
    }
}
