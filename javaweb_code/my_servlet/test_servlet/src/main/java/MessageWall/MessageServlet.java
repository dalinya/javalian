package MessageWall;


import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

//描述 请求 body 内容,方便 jackson 进行json 解析
class Message{
    public String from;
    public String to;
    public String message;
}
@WebServlet("/message")
public class MessageServlet extends HttpServlet {
    //像服务器提交数据
    ObjectMapper objectMapper = new ObjectMapper();

    MessageDao messageDao = new MessageDao();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 把 body 中的内容读取出来了, 解析成了个 Message 对象
        Message message = objectMapper.readValue(req.getInputStream(), Message.class);
        messageDao.save(message);
        // 此处的设定状态码可以省略, 不设置默认也是 200 .
        resp.setStatus(200);
    }
    //从服务器获取数据
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 显式告诉浏览器, 数据是 json 格式的, 字符集是 utf8 的
        resp.setContentType("application/json; charset=utf8");
        //把Java对象转化为json字符串
        List<Message> messageList = messageDao.load();
        String jsonResp = objectMapper.writeValueAsString(messageList);
        System.out.println(jsonResp);
        //把这个字符串写回到相应body中
        resp.getWriter().write(jsonResp);
    }

}
