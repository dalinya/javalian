package MessageWall;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageDao {
    //保存数据
    public void save(Message message) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //建立连接
            connection = DBUtil.getConnection();
            //构造sql语句
            String sql = "insert into message values(?,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, message.from);
            statement.setString(2, message.to);
            statement.setString(3, message.message);
            //执行sql
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            //关闭连接
            DBUtil.close(connection,statement,null);
        }
    }

    //加载数据
    public  List<Message> load() {
        List<Message> messageList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.建立连接
            connection = DBUtil.getConnection();
            //2.构造SQL
            String sql = "select * from message";
            statement = connection.prepareStatement(sql);
            //执行sql
            resultSet = statement.executeQuery();
            //遍历结果集合
            while (resultSet.next()){
                Message message = new Message();
                message.from = resultSet.getString("from");
                message.to = resultSet.getString("to");
                message.message = resultSet.getString("message");
                messageList.add(message);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            //关闭连接
            DBUtil.close(connection,statement,null);
        }
        return messageList;
    }

    public static void main(String[] args) {
        Message message = new Message();
        message.from = "2222";
        message.to = "1111";
        message.message = "haha";
        new MessageDao().load();
    }
}
