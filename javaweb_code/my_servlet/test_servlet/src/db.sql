drop database if exists messagewall;
create database messagewall charset utf8;
use messagewall;
create table message(
    `from` varchar(20) not null,
    `to` varchar(20) not null,
    `message` varchar(1024) not null
);
insert into message values('张三','李四','哈哈'),('张三','王五','呵呵');