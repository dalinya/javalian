package login;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        // 验证用户名密码是否正确.
        // 正常情况下, 用户名密码会使用数据库来保存.
        // 此处直接写死.
        // 此处约定, 用户名合法的是 zhangsan 和 lisi
        // 密码合法的都是 123
        // 登陆成功
        if(!username.equals("zhangsan") && !username.equals("lisi")){
            //登录失败
            //重定向到登录页面
            System.out.println("登录失败，用户名错误");
            resp.sendRedirect("login.html");
            return;
        }
        if(!password.equals("123")){
            System.out.println("登录失败，密码错误");
            resp.sendRedirect("login.html");
            return;
        }
        // 1. 创建一个会话.
        HttpSession session = req.getSession(true);
        // 2. 把当前的用户名保存到会话中. 此处 HttpSession 又可以当成一个 map 使用.
        session.setAttribute("username",username);
        // 3. 重定向到主页
        resp.sendRedirect("index");
    }

}
