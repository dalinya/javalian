import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/hello")
public class HelloServlet extends HttpServlet {
    @Override
    public void init() throws ServletException {
        // 可以在这里重写 init 方法
        // 插入一些咱们自己的 "初始化" 相关的逻辑.
        System.out.println("init");
    }

    @Override
    public void destroy() {
        // 经常有同学拼写成 destory (俺以前也经常写错)
        System.out.println("destroy");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("hello world");
        // 要想把 hello world 返回到客户端, 需要使用下面的代码.
        // getWriter 会得到一个 Writer 对象.
/*        String str = null;
        System.out.println(str.length());*/
        resp.getWriter().write("hello java");
    }
}
