package dao;

import Util.DBUtil;
import entity.User;
import entity.Word;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class WordDao {
    //查询所有单词。
    public List<Word> getAll(int id){
        List<Word> wordList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "select * from wordlist where uid = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,id);
            //3.执行sql
            resultSet = statement.executeQuery();
            //4.遍历集合结果
            while (resultSet.next()){
                Word word = new Word();
                word.setId(resultSet.getInt("id"));
                word.setWord(resultSet.getString("word"));
                word.setPronounce(resultSet.getString("pronounce"));
                word.setDescription(resultSet.getString("description"));
                word.setUid(resultSet.getInt("uid"));
                wordList.add(word);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
        return wordList;
    }

    public Integer getCount(int id){
        List<Word> wordList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "select count(*) from wordlist where uid = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,id);
            //3.执行sql
            resultSet = statement.executeQuery();
            //4.遍历集合结果
            while (resultSet.next()){
                return resultSet.getInt("count(*)");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
        return null;
    }
    //新增一个单词
    public  void add(Word word){
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "insert into wordlist values(null,?,?,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1,word.getWord());
            statement.setString(2,word.getPronounce());
            statement.setString(3,word.getPronounce());
            statement.setInt(4,word.getUid());
            //3.执行sql
            int n = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
    public  void del(int id){
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "delete  from wordlist where id = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,id);
            //3.执行sql
            int n = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }

    public  void update(Word wordF){
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "update wordlist set word=?,pronounce=?,description=? where id=?";
            statement = connection.prepareStatement(sql);
            statement.setString(1,wordF.getWord());
            statement.setString(2,wordF.getPronounce());
            statement.setString(3,wordF.getDescription());
            statement.setInt(4,wordF.getId());

            //3.执行sql
            int n = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
    public List<Word> getAllByPage(int id,int psize,int offsize){
        List<Word> wordList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "select * from wordlist where uid = ? limit ? offset ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,id);
            statement.setInt(2,psize);
            statement.setInt(3,offsize);


            //3.执行sql
            resultSet = statement.executeQuery();
            //4.遍历集合结果
            while (resultSet.next()){
                Word word = new Word();
                word.setId(resultSet.getInt("id"));
                word.setWord(resultSet.getString("word"));
                word.setPronounce(resultSet.getString("pronounce"));
                word.setDescription(resultSet.getString("description"));
                word.setUid(resultSet.getInt("uid"));
                wordList.add(word);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }


        return wordList;
    }

    public static void main(String[] args) {
        WordDao wordDao = new WordDao();
        //System.out.println(wordDao.getAllByPage(1, 2, 0));
    }
}
