package servlet;

import Util.Result;
import com.fasterxml.jackson.databind.ObjectMapper;
import service.WordService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/delete")
public class DelWordServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    WordService wordService = new WordService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id = Integer.valueOf(req.getParameter("id"));
        if (id != null){
            wordService.del(id);
            String respJson = objectMapper.writeValueAsString(Result.success("删除成功"));
            resp.setContentType("application/json;charset=utf8");
            resp.getWriter().write(respJson);
            return;
        }
        String respJson = objectMapper.writeValueAsString(Result.error("删除失败"));
        resp.setContentType("application/json;charset=utf8");
        resp.getWriter().write(respJson);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }
}
