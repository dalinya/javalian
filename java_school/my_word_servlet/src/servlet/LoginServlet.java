package servlet;

import Util.Result;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.User;
import service.UserService;
import service.WordService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    private UserService userService = new UserService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //跨域问题
//        resp.setHeader("Access-Control-Allow-Origin","*");
//        resp.setHeader("Access-Control-Allow-Credentials", "true");
//        resp.setHeader("Access-Control-Allow-Methods", "*");
//        resp.setHeader("Access-Control-Max-Age", "3600");
//        resp.setHeader("Access-Control-Allow-Headers", "Authorization,Origin,X-Requested-With,Content-Type,Accept,"
//                + "content-Type,origin,x-requested-with,content-type,accept,authorization,token,id,X-Custom-Header,X-Cookie,Connection,User-Agent,Cookie,*");
//        resp.setHeader("Access-Control-Request-Headers", "Authorization,Origin, X-Requested-With,content-Type,Accept");
//        resp.setHeader("Access-Control-Expose-Headers", "*");
        req.setCharacterEncoding("UTF-8");

        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if (username == null || "".equals(username) || password == null || "".equals(password)){
            //登录失败
            String msg = "登录失败!缺少username或者密码字段";
            String respJson = objectMapper.writeValueAsString(Result.error(msg));
            resp.setContentType("application/json;charset=utf8");
            resp.getWriter().write(respJson);
            return;
        }
        //根据用户名查找用户
        User user = userService.selectByUsername(username);
        //System.out.println(user);
        if (user == null ||!user.getPassword().equals(password)){
            //System.out.println(user.getUsername() + "||" + user.getPassword());
            //登录失败
            String msg = "用户名或密码错误";
            String respJson = objectMapper.writeValueAsString(Result.error(msg));
            resp.setContentType("application/json;charset=utf8");
            resp.getWriter().write(respJson);
            return;
        }
        //3.用户名密码验证通过，登陆成功，接下来创建会话，使用该会话保存用户信息
        HttpSession session = req.getSession(true);
        //System.out.println("login:" + session);
        session.setAttribute("user",user);
        String respJson = objectMapper.writeValueAsString(Result.success("登录成功"));
        resp.setContentType("application/json;charset=utf8");
        resp.getWriter().write(respJson);
        return;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req,resp);
    }
}
