package servlet;

import Util.Result;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.User;
import entity.Word;
import service.WordService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/getall")
public class GetWordListServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    WordService wordService = new WordService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
//        resp.setHeader("Access-Control-Allow-Origin","*");
//        resp.setHeader("Access-Control-Allow-Credentials", "true");
//        resp.setHeader("Access-Control-Allow-Methods", "*");
//        resp.setHeader("Access-Control-Max-Age", "3600");
//        resp.setHeader("Access-Control-Allow-Headers", "Authorization,Origin,X-Requested-With,Content-Type,Accept,"
//                + "content-Type,origin,x-requested-with,content-type,accept,authorization,token,id,X-Custom-Header,X-Cookie,Connection,User-Agent,Cookie,*");
//        resp.setHeader("Access-Control-Request-Headers", "Authorization,Origin, X-Requested-With,content-Type,Accept");
//        resp.setHeader("Access-Control-Expose-Headers", "*");
        HttpSession httpSession = req.getSession(false);
        if (httpSession == null){
            //System.out.println("dddddddddddddddddd");
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前未未登录，无法查看单词");
            return;
        }
        //System.out.println(httpSession);
        User user = (User) httpSession.getAttribute("user");
        //System.out.println("list:" + user);
        List<Word> list = wordService.getAll(user.getId());
        String respJson = objectMapper.writeValueAsString(Result.success(list));
        resp.setContentType("application/json;charset=utf8");
        resp.getWriter().write(respJson);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req,resp);
    }
}
