package servlet;

import Util.Result;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.Word;
import service.WordService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/update")
public class UpdateServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    WordService wordService = new WordService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String word = req.getParameter("word");
        String pronounce = req.getParameter("pronounce");
        String description = req.getParameter("description");
        //System.out.println("id" + req.getParameter("id") + "::" + word + "::" + pronounce);
        Integer id = Integer.parseInt(req.getParameter("id"));

        Word wordF = new Word(id,word,pronounce,description,0);
        System.out.println(id + "update:"  + wordF);

        wordService.update(wordF);
        String respJson = objectMapper.writeValueAsString(Result.success("修改成功"));
        resp.setContentType("application/json;charset=utf8");
        resp.getWriter().write(respJson);
    }
}
