package servlet;

import Util.Result;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.User;
import entity.Word;
import service.WordService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/add")
public class AddwordServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    WordService wordService = new WordService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        HttpSession httpSession = req.getSession(false);
        if (httpSession == null){
            //System.out.println("dddddddddddddddddd");
            String respJson = objectMapper.writeValueAsString(Result.success("当前未未登录，无法添加单词"));
            resp.setContentType("application/json;charset=utf8");
            resp.getWriter().write(respJson);
            return;
        }
        System.out.println(httpSession);
        User user = (User) httpSession.getAttribute("user");
        String word = req.getParameter("word");
        String pronounce = req.getParameter("pronounce");
        String description = req.getParameter("description");
        Word wordF = new Word(0,word,pronounce,description,user.getId());
        wordService.add(wordF);
        String respJson = objectMapper.writeValueAsString(Result.success("插入成功"));
        resp.setContentType("application/json;charset=utf8");
        resp.getWriter().write(respJson);
//        word: newWord.word,
//                pronounce: newWord.pronounce,
//                description: newWord.description
    }
}
