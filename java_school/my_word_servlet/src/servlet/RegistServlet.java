package servlet;

import Util.Result;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.User;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/regist")
public class RegistServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    private UserService userService = new UserService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if (username == null || "".equals(username) || password == null || "".equals(password)){
            //登录失败
            String msg = "注册失败!缺少username或者密码字段";
            String respJson = objectMapper.writeValueAsString(Result.error(msg));
            resp.setContentType("application/json;charset=utf8");
            resp.getWriter().write(respJson);
            return;
        }
        //根据用户名查找用户
        User user = userService.selectByUsername(username);
        if (user != null){
            String respJson = objectMapper.writeValueAsString(Result.error("该用户已存在"));
            resp.setContentType("application/json;charset=utf8");
            resp.getWriter().write(respJson);
            return;
        }
        user = new User(0,username,password);
        userService.add(user);
        String respJson = objectMapper.writeValueAsString(Result.success("注册成功"));
        resp.setContentType("application/json;charset=utf8");
        resp.getWriter().write(respJson);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }
}
