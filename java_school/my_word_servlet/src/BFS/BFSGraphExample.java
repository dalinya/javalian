package BFS;

import java.util.*;

class Graph {
    private int V; // 顶点的数量
    private LinkedList<Integer>[] adjList; // 邻接表

    public Graph(int V) {
        this.V = V;
        adjList = new LinkedList[V];
        for (int i = 0; i < V; ++i)
            adjList[i] = new LinkedList<>();
    }

    // 添加有向边
    public void addEdge(int v, int w) {
        adjList[v].add(w);
    }

    // 从指定节点开始广度优先搜索
    private void BFSUtil(int start, boolean[] visited) {
        Queue<Integer> queue = new LinkedList<>();
        visited[start] = true;
        queue.add(start);

        while (!queue.isEmpty()) {
            int current = queue.poll();
            System.out.print((char) (current + 'A') + " ");

            for (Integer neighbor : adjList[current]) {
                if (!visited[neighbor]) {
                    visited[neighbor] = true;
                    queue.add(neighbor);
                }
            }
        }
    }

    // 对整个图进行广度优先搜索
    public void BFS() {
        for (int K = 0; K < V; K++) {
            boolean[] visited = new boolean[V];
            int count = 0;
            int i = K;
            while (count++ < V){
                if (!visited[i]) {
                    BFSUtil(i, visited);
                }
                i = (i + 1) % V;
            }
            System.out.println();
        }
    }
}

public class BFSGraphExample {
    public static void main(String[] args) {
        // 创建一个有向图
        Graph graph = new Graph(5);
        graph.addEdge(0, 1);
        graph.addEdge(0, 3);
        graph.addEdge(1, 0);
        graph.addEdge(1, 2);
        graph.addEdge(2, 4);
        graph.addEdge(3, 2);
        graph.addEdge(3, 4);

        System.out.println("广度优先搜索结果：");
        graph.BFS();
    }
}