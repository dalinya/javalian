package service;

import dao.UserDao;
import entity.User;

public class UserService {
    UserDao userDao = new UserDao();
    public User selectByUsername(String username){
        return userDao.selectByUsername(username);
    }
    public void add(User user){
        userDao.add(user);
    }
}
