package service;

import dao.WordDao;
import entity.Word;

import java.util.List;

public class WordService {
    WordDao wordDao = new WordDao();
    public List<Word> getAll(int id){
        return wordDao.getAll(id);
    }
    public  void add(Word word){
        wordDao.add(word);
    }
    public  void del(int id){
        wordDao.del(id);

    }
    public  void update(Word wordF){
        wordDao.update(wordF);
    }
    public List<Word> getAllByPage(int id,int psize,int offsize){
        List<Word> wordList = wordDao.getAllByPage(id,psize,offsize);
        return wordList;
    }
    public Integer getCount(int id){
        int count = wordDao.getCount(id);
        return count;
    }

}
