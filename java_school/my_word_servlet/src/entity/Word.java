package entity;

public class Word {
    int id;
    String  word;
    String pronounce;
    String description;
    int uid;

    @Override
    public String toString() {
        return "Word{" +
                "id=" + id +
                ", word='" + word + '\'' +
                ", pronounce='" + pronounce + '\'' +
                ", description='" + description + '\'' +
                ", uid=" + uid +
                '}';
    }

    public Word(int id, String word, String pronounce, String description, int uid) {
        this.id = id;
        this.word = word;
        this.pronounce = pronounce;
        this.description = description;
        this.uid = uid;
    }

    public Word() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getPronounce() {
        return pronounce;
    }

    public void setPronounce(String pronounce) {
        this.pronounce = pronounce;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }
}