package servlet;

import Dao.CakeDao;
import entity.Cake;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/updateCake")
public class UpdateCakeServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        Cake cake = new Cake();
        cake.setId(req.getParameter("id"));
        cake.setName(req.getParameter("name"));
        cake.setPrice(Integer.parseInt(req.getParameter("price")));
        cake.setCount(Integer.parseInt(req.getParameter("count")));
        System.out.println(cake);
        CakeDao cakeDao = new CakeDao();
        cakeDao.upDate(cake);
        resp.sendRedirect("./show");
    }
}
