package servlet;

import Dao.UserDao;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/sign")
public class signServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String confirmPassword = req.getParameter("confirmPassword");
        resp.setContentType("text/html;charset=utf-8");
        System.out.println(username +" " + password +" " + confirmPassword);
        if (!confirmPassword.equals(password)){
            // 两次密码重复
            String html = "<h1>注册失败! 两次输入密码不一致</h1>";
            resp.getWriter().write(html);
            return;
        }
        resp.setContentType("text/html;charset=utf-8");
        // 2. 读数据库, 看看用户名是否存在, 并且密码是否匹配
        UserDao userDao = new UserDao();
        User user = userDao.selectByUsername(username);
        if (user != null){
            // 用户已存在
            String html = "<h1>注册失败! 用户名已存在</h1>";
            resp.getWriter().write(html);
            return;
        }
        boolean flg =  userDao.insert(username,password);
        if (flg == true){//注册成功
            resp.sendRedirect("login.html");
            return;
        }
        String html = "<h1>注册失败! 未知原因</h1>";
        resp.getWriter().write(html);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req,resp);
    }
}
