package servlet;

import Dao.UserDao;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/login")
public class loginServlet extends HttpServlet {
        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            String username = req.getParameter("username");
            String password = req.getParameter("password");
            resp.setContentType("text/html;charset=utf-8");
            // 2. 读数据库, 看看用户名是否存在, 并且密码是否匹配
            UserDao userDao = new UserDao();
            User user = userDao.selectByUsername(username);
            if (user == null || !password.equals(user.getPassword())) {
                // 用户不存在.
                String html = "<h3>登录失败! 用户名或密码错误</h3>";
                resp.getWriter().write(html);
                return;
            }
            //3.用户名密码验证通过，登陆成功，接下来创建会话，使用该会话保存用户信息
            HttpSession session = req.getSession(true);
            session.setAttribute("user",user);
            // 发送自动登录的cookie
            String autoLogin = req.getParameter("autologin");
            if (autoLogin != null) {
                // 注意 cookie 中的密码要加密
                Cookie cookie = new Cookie("autologin", username + "-"
                        + password);
                cookie.setMaxAge(Integer.parseInt(autoLogin));
                cookie.setPath(req.getContextPath());
                resp.addCookie(cookie);
            }
            // 跳转至首页
            resp.sendRedirect(req.getContextPath()+"/index.jsp");
            //resp.sendRedirect("show");
        }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            this.doGet(req,resp);
        }

        public static void main(String[] args) {
            UserDao userDao = new UserDao();
            User user = userDao.selectByUsername("zhangsan");
            if (user == null || !"123".equals(user.getPassword())) {
                System.out.println("登录失败");
                return;
            }
            System.out.println("登录成功");
        }
}
