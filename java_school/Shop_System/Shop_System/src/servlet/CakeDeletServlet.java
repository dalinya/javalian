package servlet;

import Dao.CakeDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/cakeDelet")
public class CakeDeletServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       String id =  req.getParameter("id");
        CakeDao cakeDao = new CakeDao();
        cakeDao.delete(id);
        resp.sendRedirect("./show");
    }
}
