package Dao;

import entity.Cake;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class OrderDao {
    public void insert(Cake cake){

        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "insert into cake values(?,? ,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(2,cake.getName());
            statement.setInt(3,cake.getPrice());
            statement.setInt(4,cake.getCount());
            statement.setString(1,cake.getId());
            //3.执行sql
            int n = statement.executeUpdate();
            System.out.println("update : " + n);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
}
