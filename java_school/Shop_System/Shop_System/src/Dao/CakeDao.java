package Dao;

import entity.Cake;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CakeDao {

    // 3. 直接查询出数据库中所有的用户列表 (用于博客列表页)
    public List<Cake> selectAll() {
        List<Cake> cakes = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "select * from cake";
            statement = connection.prepareStatement(sql);
            //3.执行sql
            resultSet = statement.executeQuery();
            //4.遍历集合结果
            while (resultSet.next()){
                Cake cake = new Cake();
                cake.setId(resultSet.getString("id"));
                cake.setName(resultSet.getString("name"));
                cake.setPrice(resultSet.getInt("price"));
                cake.setCount(resultSet.getInt("count"));
                cake.setCover(resultSet.getString("cover"));

                cakes.add(cake);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }  finally {
            DBUtil.close(connection,statement,null);
        }
        return cakes;
    }
    public void delete(String cakeId){
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "delete from cake where id = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1,cakeId);
            //3.执行sql
            int n = statement.executeUpdate();
            System.out.println("delete : " + n);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
    //2. 根据蛋糕id来查询指定蛋糕
    public Cake selectById(int CakeId){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "select * from cake where id = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,CakeId);
            //3.执行sql
            resultSet = statement.executeQuery();
            if (resultSet.next()){
                Cake cake = new Cake();
                cake.setId(resultSet.getString("id"));
                cake.setName(resultSet.getString("name"));
                cake.setPrice(resultSet.getInt("price"));
                cake.setCount(resultSet.getInt("count"));
                cake.setCover(resultSet.getString("cover"));

                return cake;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
        return null;
    }
    public void upDate(Cake cake){

        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "update cake set name = ? ,price = ?,count = ? where id = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1,cake.getName());
            statement.setInt(2,cake.getPrice());
            statement.setInt(3,cake.getCount());
            statement.setString(4,cake.getId());
            //3.执行sql
            int n = statement.executeUpdate();
            System.out.println("update : " + n);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
    public void insert(Cake cake){

        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "insert into cake values(?,? ,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(2,cake.getName());
            statement.setInt(3,cake.getPrice());
            statement.setInt(4,cake.getCount());
            statement.setString(1,cake.getId());
            //3.执行sql
            int n = statement.executeUpdate();
            System.out.println("update : " + n);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }

    public static void main(String[] args) {
        CakeDao cakeDao = new CakeDao();
        List<Cake> cakes = cakeDao.selectAll();
        for (Cake c : cakes) {
            System.out.println(c.getName());
        }
    }
}
