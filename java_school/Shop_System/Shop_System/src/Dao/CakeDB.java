package Dao;
import entity.Cake;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
public class CakeDB {
    private static Map<String, Cake> cake = new LinkedHashMap<String, Cake>();
    // 获得所有的蛋糕
    public static Collection<Cake> getAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "select * from cake ";
            statement = connection.prepareStatement(sql);
            //3.执行sql
            resultSet = statement.executeQuery();
            //4.遍历集合结果
            int count = 0;
            while (resultSet.next()){
                Cake cakeChild = new Cake();
                cakeChild.setId(resultSet.getInt("id") + "");
                cakeChild.setName(resultSet.getString("name"));
                cake.put(++count + "", cakeChild);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
        return cake.values();
    }
    // 根据指定的id获蛋糕
    public static Cake getCake(String id) {
        return cake.get(id);
    }

    public static void main(String[] args) {
        getAll();
        for (Map.Entry<String,Cake> index:cake.entrySet()) {
            System.out.println(index.getValue().getName());
        }
    }

}
