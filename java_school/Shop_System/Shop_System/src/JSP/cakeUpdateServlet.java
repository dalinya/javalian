package JSP;

import Dao.CakeDao;
import entity.Cake;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/cakeUpdate")
public class cakeUpdateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        CakeDao cakeDao = new CakeDao();
        Cake cake = cakeDao.selectById(Integer.valueOf(id));
        System.out.println(cake);
        //2.将list存入request域
        req.setAttribute("cake",cake);
        req.setAttribute("id",id);
        req.getRequestDispatcher("/cakeUpdate.jsp").forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
