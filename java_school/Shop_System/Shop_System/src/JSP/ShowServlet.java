package JSP;

import Dao.CakeDao;
import entity.Cake;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/show")
public class ShowServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CakeDao cakeDao = new CakeDao();
        List<Cake> cakes = cakeDao.selectAll();
        int len = cakes.size();
        //2.将list存入request域
        req.setAttribute("len",len);
        req.setAttribute("cakes",cakes);
        req.getRequestDispatcher("/show.jsp").forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
