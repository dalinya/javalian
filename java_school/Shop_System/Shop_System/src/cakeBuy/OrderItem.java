package cakeBuy;

import entity.Cake;

public class OrderItem {
    private int id;

    @Override
    public String toString() {
        return "OrderItem{" +
                "id=" + id +
                ", price=" + price +
                ", amount=" + amount +
                ", goodsName='" + goodsName + '\'' +
                ", goods=" + goods +
                ", order=" + order +
                '}';
    }

    private float price;
    private int amount;
    private String goodsName;
    private Cake goods;
    private Order order;// order_id

    public void setName(String name) {
        this.goodsName=name;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public float getPrice() {
        return price;
    }
    public void setPrice(float price) {
        this.price = price;
    }
    public int getAmount() {
        return amount;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }
    public Cake getGoods() {
        return goods;
    }
    public void setGoods(Cake goods) {
        this.goods = goods;
    }
    public Order getOrder() {
        return order;
    }
    public void setOrder(Order order) {
        this.order = order;
    }
    public OrderItem() {
        super();
    }
    public OrderItem(float price, int amount, Cake goods, Order order) {
        super();
        this.price = price;
        this.amount = amount;
        this.goods = goods;
        this.order = order;
    }
}
