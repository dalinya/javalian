drop database if exists test_school;
create database test_school charset utf8;
use test_school;
create table user(username varchar(25),password varchar(25));
insert into user values("zhangsan","123"),("lisi","123"),("wangwu","123");
create table cake( id int,  name varchar(25), price int, count int, `cover` VARCHAR(45));
insert into cake values(1,"A类蛋糕",23,5,'/picture/1-1.jpg'),(2,"B类蛋糕",22,4,'/picture/1-2.jpg'),(3,"C类蛋糕",55,3,'/picture/1-3.jpg'),(4,"D类蛋糕",66,5,'/picture/2-1.jpg'),(5,"E类蛋糕",36,7,'/picture/2-2.jpg')
,(6,"E类蛋糕",36,7,'/picture/2-3.jpg'),(7,"E类蛋糕",36,7,'/picture/3-1.jpg'),(8,"E类蛋糕",36,7,'/picture/3-2.jpg'),(9,"E类蛋糕",36,7,'/picture/3-3.jpg'),(10,"E类蛋糕",36,7,'/picture/4-1.jpg');
-- 保存订单信息
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
         `id` INT(11) NOT NULL PRIMARY KEY,
         `total` FLOAT DEFAULT NULL,
         `amount` INT(6) DEFAULT NULL,
         `status` TINYINT(1) DEFAULT NULL,
         `paytype` TINYINT(1) DEFAULT NULL,
         `name` VARCHAR(45) DEFAULT NULL,
         `phone` VARCHAR(45) DEFAULT NULL,
         `address` VARCHAR(45) DEFAULT NULL,
         `datetime` DATETIME DEFAULT NULL,
         `user_id` INT(11) DEFAULT NULL
);
INSERT INTO `order` VALUES (75,28,1,4,2,'管理员','1333333333','中华人民共和国','2019-10-07 12:31:07',1);
DROP TABLE IF EXISTS `orderitem`;
-- 订单条目信息
CREATE TABLE `orderitem` (
     `id` INT(11) NOT NULL PRIMARY KEY,
     `price` FLOAT DEFAULT NULL,
     `amount` INT(11) DEFAULT NULL,
     `goods_id` INT(11) DEFAULT NULL,
     `order_id` INT(11) DEFAULT NULL
) ;

LOCK TABLES `orderitem` WRITE;
INSERT INTO `orderitem` VALUES (78,28,1,10,75);
UNLOCK TABLES;
