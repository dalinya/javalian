<%--
  Created by IntelliJ IDEA.
  User: YB_Account
  Date: 23-10-12
  Time: 下午 2:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2>增加蛋糕界面</h2>
    <form class="form-inline" action="/addCake" method="post">
        <label for="email2" class="mb-2 mr-sm-2">编号:</label>
        <input type="text" class="form-control mb-2 mr-sm-2" id="email2" placeholder="蛋糕编号" name="id" >
        <label for="email2" class="mb-2 mr-sm-2">名称:</label>
        <input type="text" class="form-control mb-2 mr-sm-2" id="email2" placeholder="蛋糕名称" name="name" >
        <label for="email2" class="mb-2 mr-sm-2">价格:</label>
        <input type="text" class="form-control mb-2 mr-sm-2" id="pwd2" placeholder="蛋糕价格" name="price" >
        <label for="email2" class="mb-2 mr-sm-2">数量:</label>
        <input type="text" class="form-control mb-2 mr-sm-2" id="pwd2" placeholder="蛋糕数量" name="count">
        <button type="submit" class="btn btn-primary mb-2">新增</button>
    </form>
</div>
</body>
</html>
