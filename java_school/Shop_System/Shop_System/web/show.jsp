<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>蛋糕列表</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%--    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">--%>
<%--    <link type="text/css" rel="stylesheet" href="css/style.css">--%>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim 和 Respond.js 是为了让 IE8 支持 HTML5 元素和媒体查询（media queries）功能 -->
    <!-- 警告：通过 file:// 协议（就是直接将 html 页面拖拽到浏览器中）访问页面时 Respond.js 不起作用 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->
    <style>
        html, body {
            height: 100%;
            /*background: url('https://cdn.pixabay.com/photo/2018/08/14/13/23/ocean-3605547_1280.jpg') ;*/
            /*background-image: url(../img/cat.jpg);*/
            /*background-image: linear-gradient(-225deg, #5D9FFF 0%, #B8DCFF 48%, #6BBBFF 100%);		*/
            background-image: linear-gradient(to top, #09203f 0%, #537895 100%);
        }
        img {
            width: 100%;
            height: 300px;
        }

        .btn-block {
            text-align: left;
        }

        .font-color {
            color: red;
            font-size: 25px;
        }

        .green {
            color: green;
        }

        h3 {
            padding-bottom: 5px;
            border-bottom: 2px solid #ccc;

        }
    </style>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <form class="form-inline">
        <a class="btn btn-info" href="http://localhost:8080/goods_cart.jsp" target="_blank">购物车</a>
    </form>
</nav>
<br>

<div>
    <div class="container">
        <div class="row">
            <c:forEach  var="cake" items="${cakes}" varStatus="s">
                <div class=" col-md-4 ">
                    <h3>名称：${cake.name}</h3>
                    <a href="javascript:CakeDetail(${cake.id});"><img src="${pageContext.request.contextPath}${cake.cover}" class="img-responsive"></a>
                    <h6 class="green"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>数量：${cake.count}
                    </h6>
                    <div>
                        <a class="btn btn-block btn-danger" href="javascript:buyCake(${cake.id})" >添加购物车</a>
                    </div>
                    <div class="font-color">一口价：${cake.price}</div>
                </div>
            </c:forEach>
        </div>
        window.open("./goods_buy?goodsid="+id, "_blank")
        // location.href="./goods_buy?goodsid="+id;
        }
    </div>

</div>

<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>

<script>
    function buyCake(id) {
        //用户安全提示
        if (confirm("您确定要添加吗？")) {
            //访问路径
            location.href = "./detail?id=" + id;
        }
    }
        function CakeDetail(id){
            //访问路径
            location.href="./detail?id="+id;
        }
</script>

<!--//account-->
<!--footer-->
<%--	<jsp:include page="/footer.jsp"/>--%>
<!--//footer-->
</body>
</html>