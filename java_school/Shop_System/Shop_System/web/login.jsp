<%--
  Created by IntelliJ IDEA.
  User: YB_Account
  Date: 23-10-30
  Time: 下午 7:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录</title>
    <link rel="stylesheet" type="text/css" href="css/login.css">

    <script type="text/javascript">
        //切换验证码
        function refreshCode(){
            //1.获取验证码图片对象
            var vcode = document.getElementById("vcode");

            //2.设置其src属性，加时间戳
            vcode.src = "${pageContext.request.contextPath}/getCode?time="+new Date().getTime();
        }
    </script>
</head>
<body>
<div class="loginBox">
    <h2>login</h2>
    <form action="/login" method="post">
        <div class="item">
            <input type="text" required name="username">
            <label for="">userName</label>
        </div>
        <div class="item">
            <input type="password" required name="password">
            <label for="">password</label>
        </div>
        <div class="item">
            <div  style="float:left;">
                <input type="text" required name="verifycode">
                <label for="">verifycode</label>
            </div>
            <div>
                <a href="javascript:refreshCode();">
                    <img src="${pageContext.request.contextPath}/getCode" title="看不清点击刷新" id="vcode"/>
                </a>
            </div>
        </div>
        <tr>
            <td height="35" align="center">自动登录时间</td>
            <td><input type="radio" name="autologin"
                       value="${60*60*24*31}"/>一个月
                <input type="radio" name="autologin"
                       value="${60*60*24*31*3}"/>三个月
                <input type="radio" name="autologin"
                       value="${60*60*24*31*6}"/>半年
                <input type="radio" name="autologin"
                       value="${60*60*24*31*12}"/>一年
            </td>
        </tr>
        <button class="btn">submit
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </button>
    </form>

    <form action="/sign.html">
        <button class="btn">&nbsp;&nbsp;sign&nbsp;&nbsp;
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </button>
    </form>
</div>
</body>
</html>
