package demo;


import java.util.*;

class Graph {
    int vertices; // 节点数量
    List<List<Node>> adj; // 邻接表

    // 构造函数
    public Graph(int vertices) {
        this.vertices = vertices;
        adj = new ArrayList<>(vertices);
        for (int i = 0; i < vertices; i++) {
            adj.add(new ArrayList<>());
        }
    }

    // 添加边
    public void addEdge(int source, int destination, int weight) {
        adj.get(source).add(new Node(destination, weight));
    }
}
class Node {
    int vertex;
    int weight;

    // 构造函数
    public Node(int vertex, int weight) {
        this.vertex = vertex;
        this.weight = weight;
    }
}
class ShortestPath {

    static void dijkstra(Graph graph, int source) {
        int[] path = new int[graph.vertices];//存储路径
        for (int i = 0; i < path.length; i++) {
            path[i] = -1;//初始化数组
        }
        int[] dist = new int[graph.vertices]; // 用于存储最短路径
        Arrays.fill(dist, Integer.MAX_VALUE); // 初始化为无穷大
        dist[source] = 0; // 源节点到自身的距离为0

        PriorityQueue<Node> pq = new PriorityQueue<>(graph.vertices, Comparator.comparingInt(a -> a.weight));
        pq.add(new Node(source, 0));
        int index = 0;
        while (!pq.isEmpty()) {
            int u = pq.poll().vertex;

            for (Node neighbor : graph.adj.get(u)) {
                int v = neighbor.vertex;
                int weight = neighbor.weight;

                // 如果找到更短的路径，则更新距离
                if (dist[u] != Integer.MAX_VALUE && dist[u] + weight < dist[v]) {
                    dist[v] = dist[u] + weight;
                    path[v] = u;
                    pq.add(new Node(v, dist[v]));
                }
            }
            if (index < path.length - 1){
                System.out.print(index++ + ":\t");
                for (int i = 0; i < graph.vertices; i++) {
                    System.out.print("(" + (dist[i] == Integer.MAX_VALUE ? "∞" : dist[i]) + "," + path[i] + ")\t");
                }
                System.out.println();
            }
        }

        // 打印最短路径
        System.out.println("从源节点 " + source + " 到各个节点的最短路径：");
        for (int i = 0; i < graph.vertices; i++) {
            System.out.println("节点 " + source + " 到节点 " + i + " 的最短路径距离为：" + dist[i]);
        }
    }
}
public class  Dijkstra {
        public static void main(String[] args) {
            Graph graph = new Graph(6);
            graph.addEdge(0, 1, 50);
            graph.addEdge(0, 2, 10);
            graph.addEdge(0, 4, 70);

            graph.addEdge(1,4,10);
            graph.addEdge(1, 2, 15);

            graph.addEdge(2, 0, 20);
            graph.addEdge(2, 3, 15);

            graph.addEdge(3,1,20);
            graph.addEdge(3, 4, 35);
            graph.addEdge(4, 3, 30);
            graph.addEdge(5, 3, 3);

            ShortestPath.dijkstra(graph, 0);
        }
}

