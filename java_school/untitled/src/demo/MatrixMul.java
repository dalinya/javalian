package demo;

public class MatrixMul {
    public static void main(String[] args) {
        int[] p = {50,10,40,30,5};
        //int[] p = {45,8,40,25,10};
        //int[] p = {30,35,15,5,10,20,25};
        int[][] d = new int[p.length][p.length];
        int[][] dp = matrixChainOrder(p,d);
        for (int i = 1; i < dp.length; i++) {
            for (int j = 1; j < dp[0].length; j++) {
                System.out.print(dp[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println("-------------------");
        for (int i = 1; i < dp.length; i++) {
            for (int j = 1; j < dp[0].length; j++) {
                System.out.print(d[i][j] + "\t");
            }
            System.out.println();
        }
    }
    public static int[][] matrixChainOrder(int[] dimensions,int[][] d) {
        int n = dimensions.length;
        int[][] dp = new int[n][n];

        // 初始化对角线上的值为0，表示单个矩阵相乘的次数为0
        for (int i = 0; i < n; i++) {
            dp[i][i] = 0;
        }

        // 计算链长为2到n的子问题
        for (int chainLength = 2; chainLength < n; chainLength++) {
            for (int i = 1; i < n - chainLength + 1; i++) {
                int j = i + chainLength - 1;
                dp[i][j] = Integer.MAX_VALUE;
                for (int k = i; k < j; k++) {
                    int cost = dp[i][k] + dp[k+1][j] + dimensions[i-1] * dimensions[k] * dimensions[j];
                    System.out.println(dp[i][k] + " + " +dp[k+1][j] + " + " + dimensions[i-1] + "*" + dimensions[k] + "*" + dimensions[j] + "=" + cost);
                    if (cost < dp[i][j]) {
                        dp[i][j] = cost;
                        d[i][j] = k-1;
                    }
                }
                System.out.println("---------------------------------------");
            }
        }

        // 返回最终结果
        return dp;
    }
}
