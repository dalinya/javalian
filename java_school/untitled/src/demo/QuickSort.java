package demo;

import java.util.Arrays;

public class QuickSort {
    //horea方法
    private static int parttion1(int[] array,int left,int right) {
        int index = left;
        int tmp = array[left];
        while (left < right){
            while(left < right && array[right] >= tmp){//找比tmp小的
                 right--;
            }
            //todo: 先检查前面的会不会有问题
            while (left < right && array[left] <= tmp) {//找比tmp大的
                 left++;
            }
            swap(array,left,right);
        }
        swap(array,index,left);
        return left;
    }
    private static void quick1(int[] array,int start,int end) {
        if (start >= end) {
            return;
        }

        //找基准
        int pivot = parttion1(array,start,end);
        System.out.println(Arrays.toString(array));
        quick1(array,start,pivot - 1);//左树
        quick1(array,pivot + 1,end);//右数
    }
    //挖坑法
    private static int parttion2(int[] array,int left,int right) { int tmp = array[left];
        while (left < right){
            while(left < right && array[right] >= tmp){//找比tmp小的
                 right--;
            }
            array[left] = array[right];
            while (left < right && array[left] <= tmp) {//找比tmp大的
                left++;
            }
            array[right] = array[left];
        }
        array[left] = tmp;
        return left;
    }
    private static void quick2(int[] array,int start,int end) {
        if (start >= end) {
            return;
        }
        //找基准
        int pivot = parttion2(array,start,end);
        System.out.println(Arrays.toString(array));
        quick2(array,start,pivot - 1);//左树
        quick2(array,pivot + 1,end);//右数
    }
    private static int parttion3(int[] array,int left,int right) {
        int prev = left - 1;
        int cur = left;
        while (cur < right){
            if (array[cur] < array[right] && array[++prev] != array[cur]){
                swap(array,prev,cur);
            }
            cur++;
        }
        swap(array,right,prev + 1);
        return prev + 1;
    }
    private static void quick3(int[] array,int start,int end) {
        if (start >= end) {
            return;
        }
        //找基准
        int pivot = parttion3(array,start,end);
        System.out.println(Arrays.toString(array));
        quick3(array,start,pivot - 1);//左树
        quick3(array,pivot + 1,end);//右数
    }

    private static void swap(int[] array,int i,int j) {
        int tmp = array[i]; array[i] = array[j]; array[j] = tmp;

    }
    public static void main(String[] args) {
        int[] array = {47, 79, 69, 41, 92, 86, 43, 51, 80};
        //horea方法
        quick1(array,0,array.length-1);
        System.out.println("=========================");
        array = new int[]{47, 79, 69, 41, 92, 86, 43, 51, 80};
        //挖坑法
        quick2(array,0,array.length-1);
        System.out.println("=========================");
        array = new int[]{47, 79, 69, 41, 92, 86, 43, 51, 80};
        //前后指针法
        quick3(array,0,array.length-1);
    }
}
