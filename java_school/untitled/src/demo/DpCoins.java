package demo;

public class DpCoins {
    public static void main(String[] args) {
        int[] coins = {2, 3, 5};
        int amount = 9;
        int n = coins.length, INF = 0x3f3f3f3f;
        int[][] dp = new int[n + 1][amount + 1];//动规⽅程
        for (int j = 1; j <= amount; j++) dp[0][j] = INF;//初始化
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j <= amount; j++) {
                dp[i][j] = dp[i - 1][j];
                if (j >= coins[i - 1]) {
                    dp[i][j] = Math.min(dp[i][j], dp[i][j - coins[i - 1]] + 1);
                }
            }
        }
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= amount; j++) {
                System.out.print((dp[i][j] >= INF ? "∞" : dp[i][j]) + "\t");
            }
            System.out.println();
        }
        if (dp[n][amount] >= INF){//判断最终结果
            System.out.println("硬币不能凑够");
        } else {
            System.out.println(dp[n][amount]);
        }

    }
}
