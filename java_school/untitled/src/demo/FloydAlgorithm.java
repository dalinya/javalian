package demo;

public class FloydAlgorithm {
    public static void floyd(int[][] graph) {
        int n = graph.length;
        int[][] path = new int[n][n];//可达矩阵
        // 初始化距离矩阵
        int[][] distance = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                distance[i][j] = graph[i][j];
                if (graph[i][j] == 0 || graph[i][j] == Integer.MAX_VALUE){
                    path[i][j] = -1;
                }else {
                    path[i][j] = i;
                }
            }
        }
        DsPlay(distance);
        System.out.println("-------------------");
        DsPlay(path);
        System.out.println("===================");
        // 计算最短路径
        for (int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (distance[i][k] != Integer.MAX_VALUE && distance[k][j] != Integer.MAX_VALUE
                            && distance[i][k] + distance[k][j] < distance[i][j]) {
                        distance[i][j] = distance[i][k] + distance[k][j];
                        path[i][j] = path[k][j];
                    }
                }
            }
            DsPlay(distance);
            System.out.println("-------------------");
            DsPlay(path);
            System.out.println("===================");
        }

        // 输出最短路径
        System.out.println("多源最短路径：");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (distance[i][j] == Integer.MAX_VALUE) {
                    System.out.print("INF\t");
                } else {
                    System.out.print(distance[i][j] + "\t");
                }
            }
            System.out.println();
        }
    }
    public static void DsPlay(int[][] array){
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (array[i][j] == Integer.MAX_VALUE){
                    System.out.print("∞\t");
                }else {
                    System.out.print(array[i][j] + "\t");
                }
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {
        int[][] graph = {
            {0, 1, Integer.MAX_VALUE, 4},
            {Integer.MAX_VALUE, 0, 9, 2},
            {3, 5, 0, 8},
            {Integer.MAX_VALUE, Integer.MAX_VALUE, 6, 0}
        };

        floyd(graph);
    }
}