package demo;

import java.util.Arrays;

public class MergeSort {
    //归并排序 --> 递归 + 合并
    public static void mergeSort(int[] array){
        mergeSortFunc(array,0,array.length - 1);

    }
    private static void mergeSortFunc(int[] array,int left,int right) {
        if(left >= right){
            return;
        }

        int mid = left + (right - left)/2;
        mergeSortFunc(array,left,mid);
        mergeSortFunc(array,mid + 1,right);

        merge(array,left,mid,right);
        System.out.println(Arrays.toString(array));
    }
    private static void merge(int[] array,int left,int mid,int right){
        int[] tmp = new int[right - left + 1];
        int start1 = left;
        int end1 = mid;
        int start2 = mid + 1;
        int end2 = right;
        int index = 0;//临时数组的下标
        while (start1 <= end1 && start2 <= end2){
            if(array[start1] <= array[start2]){
                tmp[index++] = array[start1++];
            }else{
                tmp[index++] = array[start2++];
            }
        }
        while (start1 <= end1){
            tmp[index++] = array[start1++];
        }
        while (start2 <= end2){
            tmp[index++] = array[start2++];
        }
        for (int i = 0; i < tmp.length; i++) {
            array[left + i] = tmp[i];
        }
    }
    public static void main(String[] args) {
        int[] array = {96, 76, 14, 96, 80, 67, 38, 37, 99};
        mergeSort(array);
    }
}
