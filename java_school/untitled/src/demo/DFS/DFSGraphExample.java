package demo.DFS;

import java.util.LinkedList;

class Graph {
    private int V; // 顶点的数量
    private LinkedList<Integer>[] adjList; // 邻接表

    public Graph(int V) {
        this.V = V;
        adjList = new LinkedList[V];
        for (int i = 0; i < V; ++i)
            adjList[i] = new LinkedList<>();
    }

    // 添加有向边
    public void addEdge(int v, int w) {
        adjList[v].add(w);
    }

    // 从指定节点开始深度优先搜索
    private void DFSUtil(int v, boolean[] visited) {
        visited[v] = true;
        System.out.print((char) (v + 'A') + " ");

        for (Integer neighbor : adjList[v]) {
            if (!visited[neighbor]) {
                DFSUtil(neighbor, visited);
            }
        }
    }

    // 对整个图进行深度优先搜索
    public void DFS() {
        for (int K = 0; K < V; K++) {
            boolean[] visited = new boolean[V];
            int count = 0;
            int i = K;
            while (count++ < V){
                if (!visited[i]) {
                    DFSUtil(i, visited);
                }
                i = (i + 1) % V;
            }
            System.out.println();
        }
    }
}

public class DFSGraphExample {
    public static void main(String[] args) {
        // 创建一个有向图
        Graph graph = new Graph(5);
        graph.addEdge(0, 1);
        graph.addEdge(0, 3);
        graph.addEdge(1, 0);
        graph.addEdge(1, 2);
        graph.addEdge(2, 4);
        graph.addEdge(3, 2);
        graph.addEdge(3, 4);

        System.out.println("深度优先搜索结果：");
        graph.DFS();
    }
}