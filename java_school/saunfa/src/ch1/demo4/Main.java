package ch1.demo4;


import java.util.Scanner;

public class Main {
    //用回溯算法合
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        char[] ss = sc.next().toCharArray();
        backtracking(ss,0,"");
    }

    private static void backtracking(char[] ss,int index,String path) {
        if (index == ss.length){
            System.out.print("(" + path + ")");
            return;
        }
        backtracking(ss,index +1, path);
        backtracking(ss,index+1,path + ss[index]);
    }
}
