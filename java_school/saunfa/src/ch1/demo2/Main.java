package ch1.demo2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        if(m > n){
            System.out.println("error");
            return;
        }
        if(m == n){
            System.out.println(1);
            return;
        }
        long num1 = 1;
        for (int i = m + 1; i <= n; i++) {
            num1 *= i;
        }
        long num2 = 1;
        for (int i = 1; i <= n-m; i++) {
            num2 *= i;
        }
        System.out.println(num1/num2);
    }
}
