package ch1.demo3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int num = 1 + n;
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0){
                num += i;
                int tmp = n / i;
                if(tmp != i){
                    num += tmp;
                }
            }
        }
        System.out.println(num);
    }
}
