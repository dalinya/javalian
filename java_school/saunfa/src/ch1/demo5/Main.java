package ch1.demo5;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        int[][] array = new int[100][4];
        for (int i = 0; i < 100; i++) {
            int num1 = random.nextInt(10000) + 1;
            int num2 = random.nextInt(10000) + 1;
            //连续递减
            int count1 = test1(num1,num2);
            //辗转相除
            int count2 = test2(num1,num2);
            array[i][0] = num1;
            array[i][1] = num2;
            array[i][2] = count1;
            array[i][3] = count2;
        }
        Arrays.sort(array,(a,b)-> a[0] == b[0] ? a[1]-b[1]:a[0] - b[0]);
        for (int[] arr:array) {
            System.out.println(arr[0] + "   " + arr[1] + "  " + arr[2] + "  " + arr[3]);
        }
    }

    private static int test1(int num1, int num2) {
        int count = 0;
        int num = num1 < num2 ? num1 : num2;
        while (num1 % num != 0 || num2 % num != 0){
            count++;
            num--;
        }
        return count;
    }

    private static int test2(int num1, int num2) {
        int count = 0;
        while (num2 != 0){
            int tmp = num1 % num2;
            num1 = num2;
            num2 = tmp;
            count++;
        }
        return count;
    }
}
