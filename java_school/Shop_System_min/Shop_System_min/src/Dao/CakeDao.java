package Dao;

import entity.Cake;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class CakeDao {

    // 3. 直接查询出数据库中所有的用户列表 (用于博客列表页)
    public List<Cake> selectAll() {
        List<Cake> cakes = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "select * from cake";
            statement = connection.prepareStatement(sql);
            //3.执行sql
            resultSet = statement.executeQuery();
            //4.遍历集合结果
            while (resultSet.next()){
                Cake cake = new Cake();
                cake.setId(resultSet.getString("id"));
                cake.setName(resultSet.getString("name"));
                cake.setPrice(resultSet.getInt("price"));
                cake.setCount(resultSet.getInt("count"));


                cakes.add(cake);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }  finally {
            DBUtil.close(connection,statement,null);
        }
        return cakes;
    }


    public static void main(String[] args) {
        CakeDao cakeDao = new CakeDao();
        List<Cake> cakes = cakeDao.selectAll();
        for (Cake c : cakes) {
            System.out.println(c.getName());
        }
    }
}
