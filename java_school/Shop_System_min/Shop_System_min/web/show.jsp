<%@ page import="java.util.List" %>
<%@ page import="Dao.CakeDao" %>
<%@ page import="entity.Cake" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>用户注册</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
  <link type="text/css" rel="stylesheet" href="css/style.css">

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
  <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<%
  CakeDao cakeDao = new CakeDao();
  List<Cake> cakes = (List<Cake>) cakeDao.selectAll();
%>

<!-- 页面主体部分 -->
<div class="container">
  <table class="table table-dark table-hover" >
    <thead>
    <tr>
      <td>id</td>
      <td>名称</td>
      <td>价格</td>
      <td>数量</td>
    </tr>
    </thead>

    <tbody class="table-info">

    <%
      for (int i = 0;i < cakes.size();i++){
        Cake cake = cakes.get(i);
    %>

    <tr>
      <td ><p class="text-muted"><%=cake.getId()%></p></td>
      <td><p class="text-muted"><%=cake.getName()%></p></td>
      <td><p class="text-muted"><%=cake.getPrice() %></p></td>
      <td><p class="text-muted"><%=cake.getCount() %></p></td>

    </tr>
    <% } %>
    </tbody>

  </table>

</div>

</body>
</html>