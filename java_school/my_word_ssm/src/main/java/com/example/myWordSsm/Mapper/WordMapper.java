package com.example.myWordSsm.Mapper;

import com.example.myWordSsm.entity.Word;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface  WordMapper {
    @Select("select * from wordlist")
    List<Word>  getlist();
    @Insert(("insert into wordlist values (null,#{word},#{pronounce},#{description})"))
    void add(Word wordF);
    @Delete(("delete  from wordlist where id=#{id}"))
    void delete(Integer id);
    @Update("update wordlist set word=#{word},pronounce=#{pronounce},description=#{description} where id=#{id}")
    void update(Word wordF);

}
