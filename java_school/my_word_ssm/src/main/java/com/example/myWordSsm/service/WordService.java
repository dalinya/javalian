package com.example.myWordSsm.service;

import com.example.myWordSsm.Mapper.WordMapper;
import com.example.myWordSsm.entity.Word;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WordService {
    @Autowired
    WordMapper wordMapper;

   public List<Word> getList(){

       return wordMapper.getlist();
   }

   public void add(Word wordF){
       wordMapper.add(wordF);
   }

   public void delete(Integer id){
       wordMapper.delete(id);
   }
   public void update(Word wordF){
       wordMapper.update(wordF);
   }
}
