package com.example.myWordSsm.controller;

import com.example.myWordSsm.entity.Word;
import com.example.myWordSsm.pojo.Result;
import com.example.myWordSsm.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/word")
public class WordController {
    @Autowired
    WordService wordService;

    @RequestMapping("/list")
    public Result<List<Word>> getList(){
        //获取全部信息
        List<Word> list= wordService.getList();
        return Result.success(list);
    }
    @RequestMapping("/add")
    public Result<String> add(String word,String pronounce,String description){
        Word wordF = new Word(0,word,pronounce,description);

        //System.out.println(wordF);
        wordService.add(wordF);
        return Result.success("成功插入");
    }
    @RequestMapping("/delete")
    public Result<String> delete(Integer id){
        //System.out.println(id);
        wordService.delete(id);
        return Result.success("删除成功");
    }
    @RequestMapping("/update")
    public Result<String> update(Integer id,String word,String pronounce,String description){
        Word wordF = new Word(id,word,pronounce,description);
//        System.out.println("::::" + wordF);
        wordService.update(wordF);
        return Result.success("修改成功");
    }
}
