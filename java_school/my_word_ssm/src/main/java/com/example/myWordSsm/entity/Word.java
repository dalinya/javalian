package com.example.myWordSsm.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Word {
    int id;
    String  word;
    String pronounce;
    String description;

}
