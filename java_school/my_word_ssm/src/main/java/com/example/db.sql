create database word_database charset utf8;
use  word_database;
CREATE TABLE wordlist (
    id INT primary key auto_increment,
    word VARCHAR(256),
    pronounce VARCHAR(256),
    description VARCHAR(256),
    uid int
);
insert into wordlist values(1,'apple','apple','苹果',1);
insert into wordlist values(2,'banana','banana','香蕉',1);
insert into wordlist values(null,'pen','pen','钢笔',1);
create table user
(
    id       int,
    name     VARCHAR(256),
    password VARCHAR(256)
);
