package servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Type;
import service.TypeService;
import utils.Result;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/getalltype")
public class GetAllTypeServlet extends HttpServlet {
    TypeService tsService=new TypeService();
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Type> typeList = tsService.GetAllType();
        String respJson = objectMapper.writeValueAsString(Result.success(typeList));
        resp.setContentType("application/json;charset=utf8");
        resp.getWriter().write(respJson);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }
}
