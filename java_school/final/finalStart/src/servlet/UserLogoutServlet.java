package servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import utils.Result;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "user_logout",urlPatterns = "/user_logout")
public class UserLogoutServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().removeAttribute("user");
        String respJson = objectMapper.writeValueAsString(Result.success());
        response.setContentType("application/json;charset=utf8");
        response.getWriter().write(respJson);//注销成功
        //response.sendRedirect("/index");
    }
}
