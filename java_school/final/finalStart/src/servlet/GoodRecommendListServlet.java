package servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Page;
import service.GoodsService;
import utils.Result;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "goodrecommendList",urlPatterns = "/goodsrecommend_list")
public class GoodRecommendListServlet extends HttpServlet {
    private GoodsService gService = new GoodsService();
    private ObjectMapper objectMapper = new ObjectMapper();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int type = Integer.parseInt(request.getParameter("type") ) ;
        int pageNumber = 1;

        if(request.getParameter("pageNumber") != null) {
            try {
                pageNumber=Integer.parseInt(request.getParameter("pageNumber") ) ;
            }
            catch (Exception e)
            {

            }
        }
        if(pageNumber<=0)
            pageNumber=1;
        Page page = gService.getGoodsRecommendPage(type, pageNumber);

        if(page.getTotalPage()==0)
        {
            page.setTotalPage(1);
            page.setPageNumber(1);
        }
        else {
            if(pageNumber>=page.getTotalPage()+1)
            {
                page = gService.getGoodsRecommendPage(type, page.getTotalPage());
            }
        }
        Map<String,Object> map = new HashMap<>();
        map.put("page", page);
        map.put("type", type);
        String respJson = objectMapper.writeValueAsString(Result.success(map));
        response.setContentType("application/json;charset=utf8");
        response.getWriter().write(respJson);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req,resp);
    }
}
