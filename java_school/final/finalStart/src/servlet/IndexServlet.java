package servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import service.GoodsService;
import utils.Result;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(name = "IndexServlet",urlPatterns = "/index")
public class IndexServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
    private GoodsService gService=new GoodsService();
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Map<String,Object>> ScrollGood=gService.getScrollGood();
        request.setAttribute("scroll",ScrollGood);
        List<Map<String,Object>>newList=gService.getGoodsList(3);
        request.setAttribute("newList",newList);
        System.out.println("sssss");
        List<Map<String,Object>>hotList=gService.getGoodsList(2);
        request.setAttribute("hotList",hotList);
        Map<String,Object> map = new HashMap<>();
        map.put("scroll",ScrollGood);
        map.put("newList",newList);
        map.put("hotList",hotList);
        String respJson = objectMapper.writeValueAsString(Result.success(map));
        response.setContentType("application/json;charset=utf8");
        response.getWriter().write(respJson);
        //response.sendRedirect("index.jsp");
        //request.getRequestDispatcher("index.jsp").forward(request,response);


    }
}
