package servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Goods;
import model.Type;
import service.GoodsService;
import service.TypeService;
import utils.Result;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(name = "goods_detail",urlPatterns = "/goods_detail")
public class GoodsDetailServlet extends HttpServlet {
    TypeService tsService=new TypeService();
    private ObjectMapper objectMapper = new ObjectMapper();

    private GoodsService gService = new GoodsService();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request,response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Goods g = gService.getGoodsById(id);
        List<Type> typeList = tsService.GetAllType();
        Map<String,Object> map = new HashMap<>();
        map.put("goods",g);
        map.put("typeList",typeList);
        String respJson = objectMapper.writeValueAsString(Result.success(map));
        response.setContentType("application/json;charset=utf8");
        response.getWriter().write(respJson);
    }
}
