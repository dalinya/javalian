<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<!-- 网页使用的语言 -->
<html lang="zh-CN">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>用户信息管理系统</title>

  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- 2. jQuery导入，建议使用1.9以上的版本 -->
  <script src="js/jquery-2.1.0.min.js"></script>
  <!-- 3. 导入bootstrap的js文件 -->
  <script src="js/bootstrap.min.js"></script>
  <style type="text/css">
    html, body {
      height: 100%;
      /*background-image: url(../img/cat.jpg);*/
      background-image: linear-gradient(120deg, #84fab0 0%, #8fd3f4 100%);		}
    td, th {
      text-align: center;
    }
  </style>

  <script>
    function deleteUser(id){
      //用户安全提示
      if(confirm("您确定要删除吗？")){
        //访问路径
        location.href="${pageContext.request.contextPath}/delUserServlet?id="+id;
      }
    }

    window.onload = function(){
      //给删除选中按钮添加单击事件
      document.getElementById("delSelected").onclick = function(){
        if(confirm("您确定要删除选中条目吗？")){

          var flag = false;
          //判断是否有选中条目
          var cbs = document.getElementsByName("uid");
          for (var i = 0; i < cbs.length; i++) {
            if(cbs[i].checked){
              //有一个条目选中了
              flag = true;
              break;
            }
          }

          if(flag){//有条目被选中
            //表单提交
            document.getElementById("form").submit();
          }
        }
      }
      //1.获取第一个cb
      document.getElementById("firstCb").onclick = function(){
        //2.获取下边列表中所有的cb
        var cbs = document.getElementsByName("uid");
        //3.遍历
        for (var i = 0; i < cbs.length; i++) {
          //4.设置这些cbs[i]的checked状态 = firstCb.checked
          cbs[i].checked = this.checked;
        }
      }
    }
  </script>
</head>
<body>
<%

  if(session == null || session.getAttribute("user") == null){
    response.sendRedirect("login.jsp");
    return;
  }
%>
  <h3 style="text-align: center">学生信息列表</h3>
  <%--查询功能--%>
  <div style="float: left;">

    <form class="form-inline" action="${pageContext.request.contextPath}/findUserByPageServlet" method="post">
      <div class="form-group">
        <label for="exampleInputName2">学生姓名</label>
        <input type="text" name="name" value="${map.name}" class="form-control" id="exampleInputName2" >
      </div>
      <div class="form-group">
        <label for="exampleInputName3">籍贯</label>
        <input type="text" name="address" value="${map.address}" class="form-control" id="exampleInputName3" >
      </div>

      <div class="form-group">
        <label for="exampleInputEmail2">邮箱</label>
        <input type="text" name="email" value="${map.email}" class="form-control" id="exampleInputEmail2"  >
      </div>
      <button type="submit" class="btn btn btn-info">查询</button>
    </form>

  </div>
  <%--  添加删除功能 --%>
  <div style="float: right;margin: 5px;">
    <a class="btn btn-primary" href="login.jsp">退出</a>
    <a class="btn btn-success" href="${pageContext.request.contextPath}/add.jsp">添加学生</a>
    <a class="btn btn-danger" href="javascript:void(0);" id="delSelected">删除选中</a>
  </div>

  <form id="form" action="${pageContext.request.contextPath}/delSelectedServlet" method="post">
    .
    <table border="1" class="table table-condensed table-hover">
      <tr class="success">
        <th><input type="checkbox" id="firstCb"></th>
        <th>编号</th>
        <th>姓名</th>
        <th>性别</th>
        <th>年龄</th>
        <th>籍贯</th>
        <th>QQ</th>
        <th>邮箱</th>
        <th>操作</th>
      </tr>

      <c:forEach items="${pb.list}" var="student" varStatus="s">
        <tr>
          <td><input type="checkbox" name="uid" value="${student.id}"></td>
          <td>${student.id}</td>
          <td>${student.name}</td>
          <td>${student.gender}</td>
          <td>${student.age}</td>
          <td>${student.address}</td>
          <td>${student.qq}</td>
          <td>${student.email}</td>
          <td><a class="btn btn-warning  btn-sm" href="${pageContext.request.contextPath}/findUserServlet?id=${student.id}">修改</a>&nbsp;
            <a class="btn btn-danger btn-sm" href="javascript:deleteUser(${student.id});">删除</a></td>
        </tr>
      </c:forEach>


    </table>
  </form>
<%--  分页部分 --%>
  <div>
    <nav aria-label="Page navigation">
      <ul class="pager">
        <%--  如果是首页不能点上一页 --%>
        <c:if test="${pb.currentPage == 1}">
          <li class="disabled previous">
        </c:if>

        <c:if test="${pb.currentPage != 1}">
          <li class="previous">
        </c:if>
          <%--  上一页--%>
          <a href="${pageContext.request.contextPath}/findUserByPageServlet?currentPage=${pb.currentPage - 1}&rows=5&name=${map.name}&address=${map.address}&email=${map.email}" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
          <ul class="pagination">
            <%-- 页码列表 --%>
            <c:forEach begin="1" end="${pb.totalPage}" var="i" >
              <c:if test="${pb.currentPage == i}">
                <li class="active"><a href="${pageContext.request.contextPath}/findUserByPageServlet?currentPage=${i}&rows=5&name=${map.name}&address=${map.address}&email=${map.email}">${i}</a></li>
              </c:if>
              <c:if test="${pb.currentPage != i}">
                <li> <a href="${pageContext.request.contextPath}/findUserByPageServlet?currentPage=${i}&rows=5&name=${map.name}&address=${map.address}&email=${map.email}">${i}</a></li>
              </c:if>
            </c:forEach>
              <%--  目录信息--%>
              <span style="font-size: 25px;margin-left: 5px;">
                    共${pb.totalCount}条记录
                </span>
          </ul>


        <%--下一页--%>
          <c:if test="${pb.currentPage == pb.totalPage}">
          <li class="disabled next">
            </c:if>

            <c:if test="${pb.currentPage != pb.totalPage}">
          <li class="next">
            </c:if>
          <a href="${pageContext.request.contextPath}/findUserByPageServlet?currentPage=${pb.currentPage + 1}&rows=5&name=${map.name}&address=${map.address}&email=${map.email}" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </a>
        </li>
      </ul>
    </nav>
</div>
<div>

</div>


</body>
</html>
