package servlet;



import entity.PageBean;
import entity.Student;
import service.StudentService;
import service.impl.StudentServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/findUserByPageServlet")
public class FindUserByPageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        HttpSession session = request.getSession(false);
        if(session.getAttribute("user") == null){
            response.sendRedirect("login.jsp");
            return;
        }
        //1.获取参数
        String currentPage = request.getParameter("currentPage");//当前页码
        String rows = request.getParameter("rows");//每页显示条数
        //页码数的初始化
        if(currentPage == null || "".equals(currentPage)){
            currentPage = "1";
        }
        Map<String,String> map = new HashMap<>();
        map.put("name",request.getParameter("name"));
        map.put("address",request.getParameter("address"));
        map.put("email",request.getParameter("email"));
        if(rows == null || "".equals(rows)){
            rows = "5";
        }
        
        //获取条件查询参数 当前页码和每页条数
        request.getParameter("user");//获取登录用户信息

        //2.调用service查询
        StudentService service = new StudentServiceImpl();
        PageBean<Student> pb = service.findUserByPage(currentPage,rows,map);

        System.out.println(pb);

        //3.将PageBean存入request
        request.setAttribute("pb",pb);
        //request.setAttribute("condition",condition);//将查询条件存入request
        request.setAttribute("map",map);
        //4.转发到list.jsp
        request.getRequestDispatcher("/list.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}