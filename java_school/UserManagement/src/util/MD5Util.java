package util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {
/**
 * 生成32位md5码
 * @param password
 * @return
 */
	 //加盐方式
	 public static String md5Password(String password){
		 //获取信息摘要器对象
		 try {
			MessageDigest digest=MessageDigest.getInstance("md5");
			byte[] result = digest.digest(password.getBytes());
			StringBuffer buffer = new StringBuffer();
			 //把每一个byte做一个与运算0xff;
			 /*为什么要与0xFF进行与运算，
			  * 任意数与OxFF进行与运算都是取16进制的后两位
			  */
			 for(byte b:result){
				 //与运算
				 int number=b&0xff; //加盐
				 String str=Integer.toHexString(number);
				 if(str.length()==1){
					 buffer.append("0");
				 }
				 buffer.append(str);
			 }
			 //返回标准的md5加密后的结果
			 return buffer.toString();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	 }
	//二、普通方式
	 public static String MD5(String key){
		 char hexDigists[] ={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};	       
			try {
				 byte[] btInput=key.getBytes();
			       //获取MD5摘要算法的MessageDigest对象
				 MessageDigest mdInst = MessageDigest.getInstance("md5");
				  //使用指定的字节更新摘要
			      mdInst.update(btInput);
			       //获得密文
			      byte[] md=mdInst.digest();
			      //把密文转换成 十六进制的字符串形式
			       int j=md.length;
			       char str[]=new char[j*2];
			       int k=0;
			       for(int i=0;i<j;i++){
			    	   byte byte0=md[i];
			    	   str[k++]=hexDigists[byte0>>>4&0xf];
			    	   str[k++]=hexDigists[byte0&0xf];
			       }
			       return new String(str);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}	     
		 
	 }
	public static void main(String[] args) {
		String pwd= MD5Util.MD5("123456");
//		String pwd=MD5Util.md5Password("123456");
		System.out.println(pwd);

	}
	/*
	 *spring自带工具包DigestUtils
	 *System.out.println(DigestUtils.md5DigestAsHex("1234".getBytes()));
	 * 81dc9bdb52d04dc20036dbd8313ed055
	 * */
}