package dao;

import entity.User;

public interface UserDao {
    public User selectByUsername(String username);
    //插入用户
    void insert(User user);
}
