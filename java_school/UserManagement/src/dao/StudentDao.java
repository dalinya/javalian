package dao;

import entity.Student;

import java.util.List;
import java.util.Map;

public interface StudentDao {
    /**
     * 查询总记录数
     * @return
     * @param condition
     */
//    public int findTotalCount(int currentPage, int rows);
//    int findTotalCount(Map<String, String[]> condition);
    int findTotalCount(Map<String, String> map);
    /**
     * 分页查询每页记录
     * @param start
     * @param rows
     * @param condition
     * @return
     */
    List<Student> findByPage(int start, int rows, Map<String, String> map);

    /**
     * 增加用户
     * @param student
     */

    void add(Student student);

    Student findById(int id);

    void update(Student student);

    void delete(int id);
}
