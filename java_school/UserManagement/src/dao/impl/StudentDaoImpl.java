package dao.impl;

import dao.StudentDao;
import entity.Student;
import entity.User;
import util.DBUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class StudentDaoImpl implements StudentDao {
    @Override
//    public int findTotalCount(Map<String, String[]> condition) {
    public int findTotalCount(Map<String, String> map) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "select count(*) from student where 1 = 1";
            //定义参数的集合
            List<String> params = new ArrayList<>();
            for (Map.Entry<String, String> item:map.entrySet()) {
                if(item.getValue() != null){
                    sql += " and "+ item.getKey() +" like ? ";
                    params.add("%"+item.getValue()+"%");//？条件的值,模糊查询
                }
            }
            statement = connection.prepareStatement(sql);
            for (int i = 0; i < params.size(); i++) {
                statement.setString(i+1,params.get(i));
            }
            //3.执行sql
            resultSet = statement.executeQuery();
            //4.遍历集合结果
            while (resultSet.next()){
                return resultSet.getInt("count(*)");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }  finally {
            DBUtil.close(connection,statement,null);
        }
        return 0;
    }


    //分页查询每页记录
    @Override
    public List<Student> findByPage(int start, int rows, Map<String, String> map) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "select * from student where 1 = 1 ";
            //定义参数的集合
            List<String> params = new ArrayList<>();
            for (Map.Entry<String, String> item:map.entrySet()) {
                if(item.getValue() != null ){
                    sql += " and "+ item.getKey() +" like ? ";
                    params.add("%"+item.getValue()+"%");//？条件的值,模糊查询
                }
            }
            sql += "limit ?,?";
            statement = connection.prepareStatement(sql);
            for (int i = 0; i < params.size(); i++) {
                statement.setString(i+1,params.get(i));
            }
            statement.setInt(params.size() + 1,start);
            statement.setInt(params.size() + 2,rows);
            //3.执行sql
            resultSet = statement.executeQuery();
            List<Student> result = new ArrayList<>();
            //4.遍历集合结果
            while (resultSet.next()){
                Student student = new Student();
                student.setId(resultSet.getInt("id"));
                student.setName(resultSet.getString("name"));
                student.setGender(resultSet.getString("gender"));
                student.setAge(resultSet.getInt("age"));
                student.setAddress(resultSet.getString("address"));
                student.setQq(resultSet.getString("qq"));
                student.setEmail(resultSet.getString("email"));
                result.add(student);
            }
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }  finally {
            DBUtil.close(connection,statement,null);
        }
        return null;
    }
    public static void main(String[] args) {
        StudentDao studentDao = new StudentDaoImpl();
        Map<String,String> map = new HashMap<>();
        System.out.println(studentDao.findByPage(1, 3, map));
//        System.out.println(studentDao.findTotalCount());
    }

    @Override
    public void add(Student student) {

        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "insert into student values(null,?,?,?,?,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1,student.getName());
            statement.setString(2,student.getGender());
            statement.setInt(3,student.getAge());
            statement.setString(4,student.getAddress());
            statement.setString(5,student.getQq());
            statement.setString(6,student.getEmail());

            int n = statement.executeUpdate();
            System.out.println("update : " + n);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
    @Override
    public Student findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            // 1. 和数据库建立连接.
            connection = DBUtil.getConnection();
            // 2. 构造 SQL
            String sql = "select * from student where id = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            // 3. 执行 SQL
            resultSet = statement.executeQuery();
            // 4. 遍历结果集合
            if (resultSet.next()) {
                Student student = new Student();
                student.setId(resultSet.getInt("id"));
                student.setName(resultSet.getString("name"));
                student.setGender(resultSet.getString("gender"));
                student.setAge(resultSet.getInt("age"));
                student.setAddress(resultSet.getString("address"));
                student.setQq(resultSet.getString("qq"));
                student.setEmail(resultSet.getString("email"));
                return student;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return null;
    }

    @Override
    public void update(Student student) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "update student set name = ?,gender = ? ,age = ? , address = ? , qq = ?, email = ? where id = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1,student.getName());
            statement.setString(2,student.getGender());
            statement.setInt(3,student.getAge());
            statement.setString(4,student.getAddress());
            statement.setString(5,student.getQq());
            statement.setString(6,student.getEmail());
            statement.setInt(7,student.getId());

            int n = statement.executeUpdate();
            System.out.println("update : " + n);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }

    @Override
    public void delete(int id) {
        //1.定义sql
        //2.执行sql
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.和数据库建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "delete from student where id = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,id);

            int n = statement.executeUpdate();
            System.out.println("update : " + n);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
}
