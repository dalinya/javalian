package service;

import entity.User;

public interface UserService {
    /**
     * 登录方法
     */
    User login(User user);
    /**
     * 注册方法
     */
    void addUser(User user);
}
