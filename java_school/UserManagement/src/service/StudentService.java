package service;

import entity.PageBean;
import entity.Student;

import java.util.Map;

public interface StudentService {

    /**
     * 分页条件查询
     * @param currentPage
     * @param rows
     * @param condition
     * @return
     */
    public PageBean<Student> findUserByPage(String _currentPage, String _rows, Map<String, String> map);

    /**
     * 保存User
     * @param student
     */
    void addUser(Student student);

    Student findUserById(String id);

    void updateUser(Student student);

    void deleteUser(String id);

    void delSelectedUser(String[] ids);
}
