package service.impl;

import dao.UserDao;
import dao.impl.UserDaoimpl;
import entity.User;
import service.UserService;

public class UserServiceImpl implements UserService {
    private UserDao dao=new UserDaoimpl();
    @Override
    public User login(User user) {
        return dao.selectByUsername(user.getUsername());
    }

    @Override
    public void addUser(User user) {
        dao.insert(user);
    }
}
