drop database if exists school_midterm;
create database school_midterm charset utf8;
use school_midterm;

DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
   id int primary key auto_increment,
   `name` varchar(20) NOT NULL,
   `gender` varchar(5) DEFAULT NULL,
   `age` int(11) DEFAULT NULL,
   `address` varchar(32) DEFAULT NULL,
   `qq` varchar(20) DEFAULT NULL,
   `email` varchar(50) DEFAULT NULL
) ;

INSERT INTO `student` VALUES ('1', '张三', '男', '15', '陕西', '12345', 'zhangsan@itcast.cn');
INSERT INTO `student` VALUES ('2', '李四', '女', '15', '北京', '88888', 'ls@itcast.cn');
INSERT INTO `student` VALUES ('4', '1', '男', '1', '陕西', '1212131', '1212131@123.com');
INSERT INTO `student` VALUES ('5', '2', '男', '2', '陕西', '2222', '2222@132.com');
INSERT INTO `student` VALUES ('6', 'ee', '男', '23', '陕西', '12312', '12312@123.com');


INSERT INTO `student` VALUES ('11', '张三', '男', '15', '陕西', '12345', 'zhangsan@itcast.cn');
INSERT INTO `student` VALUES ('12', '李四', '女', '15', '北京', '88888', 'ls@itcast.cn');
INSERT INTO `student` VALUES ('14', '1', '男', '1', '陕西', '1212131', '1212131@123.com');
INSERT INTO `student` VALUES ('15', '2', '男', '2', '陕西', '2222', '2222@132.com');
INSERT INTO `student` VALUES ('16', 'ee', '男', '23', '陕西', '12312', '12312@123.com');
-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
    id int primary key auto_increment,
    `username` varchar(50) DEFAULT NULL,
    `password` varchar(50) DEFAULT NULL
) ;

INSERT INTO `user` VALUES ('1', 'zhangsan', '123456');
INSERT INTO `user` VALUES ('2', 'lisi', 'aaaaaa');
INSERT INTO `user` VALUES ('7', 'zhaoliu', '123456');
INSERT INTO `user` VALUES ('8', 'wangwu', '123456');
INSERT INTO `user` VALUES ('9', '小明', '123');
INSERT INTO `user` VALUES ('10', 'admin', 'admin');
INSERT INTO `user` VALUES ('11', 'test', '123456');


